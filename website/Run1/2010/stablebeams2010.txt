FillNr 1005, Tue 30.03.2010 13:22 => Tue 30.03.2010 16:29  ~3h
3.5 TeV, Single_2b_1_1_1 , LHCb +, ALICE -/-, N=~ 1e10 p/bch, 11 10 11 10 m
beam1: e_H = 0.48 nm, e_V = 0.62 nm 
beam2: e_H = 0.48 nm, e_V = 1.15 nm 
* 30.03.2010 13:24 started mini scan for IP2
IP8 from beam-gas
13:22 Stable beams declared, first physics collisions 
at 7 TeV in zero-momentum frame.

FillNr 1013, Wed 31.03.2010 21:03 => Thu 01.04.2010 05:05  ~8h
3.5 TeV, Single_2b_1_1_1 , LHCb +, ALICE -/-, N=~ 1e10 p/bch, 11 10 11 10 m
beam1: e_H = 0.38 nm, e_V = 0.68 nm   
beam2: e_H = 0.48 nm, e_V = 1.05 nm   
* 31.03.2010 21:22 started mini scan for IP2
* 31.03.2010 21:50 started mini scan for IP8
* 31.03.2010 22:30 started mini scan for IP1
* 31.03.2010 23:30 started mini scan for IP5

FillNr 1019, Sat 03.04.2010 04:23 => Sat 03.04.2010 07:23  ~3h
3.5 TeV, Single_2b_1_1_1 , LHCb -, ALICE -/-, N=~ 1e10 p/bch, 11 10 11 10 m
beam1: e_H = 0.40 nm, e_V = 0.46 nm
beam2: e_H = 0.44 nm, e_V = 0.80 nm
* 03.04.2010 06:30 started mini scan for IP5   Using a different reference orbit ? 

FillNr 1022, Sun 04.04.2010 17:26 => Mon 05.04.2010 13:29  ~20h
3.5 TeV, Single_2b_1_1_1 , LHCb -, ALICE -/-, N=~ 1.1e10 p/bch, 11 10 11 10 m
emittances at 00:30:
beam1: e_H = 0.48 nm, e_V = 0.55 nm  (1785 2040  um rad)
beam2: e_H = 0.59 nm, e_V = 1.24 nm  (2200 4620  um rad)
emittances at 09:57:
beam1: e_H = 0.55 nm, e_V = 0.69 nm  (2044 2562  um rad)
beam2: e_H = 0.66 nm, e_V = 1.52 nm  (2480 5679  um rad)
* 04.04.2010 18:19 started mini scan for IP5
* 04.04.2010 21:06 started mini scan for IP1
* 04.04.2010 23:09 started mini scan for IP8   hysteresis problems...?
* 05.04.2010 02:18 started mini scan for IP2
* 05.04.2010 06:13 started mini scan for IP5

FillNr 1023, Tue 06.04.2010 02:44 => Tue 06.04.2010 14:59  ~12.25h
3.5 TeV, Single_2b_1_1_1 , LHCb +, ALICE -/-, N=~ 1.1e10 p/bch, 11 10 11 10 m
emittances at 07:30:
beam1: e_H = 0.52 nm, e_V = 0.47 nm  (1953 1736  um rad)
beam2: e_H = 0.60 nm, e_V = 0.61 nm  (2231 2279  um rad)
emittances at 14:29:
beam1: e_H = 0.72 nm, e_V = 0.73 nm  (2700 2741  um rad)
beam2: e_H = 0.73 nm, e_V = 0.94 nm  (2730 3498  um rad)
* 06.04.2010 03:27 started mini scan for IP8
* 06.04.2010 11:06 started mini scan for IP5
* 06.04.2010 12:10 started mini scan for IP1
* 06.04.2010 12:49 started mini scan for IP2    cannot do vertical, TCT limits

FillNr 1026, Wed 07.04.2010 10:28 => Wed 07.04.2010 12:52  ~2.5h
3.5 TeV, Single_2b_1_1_1 , LHCb +, ALICE -/-, N=~ 1.1e10 p/bch, 11 10 11 10 m
emittances at 12:36:
beam1: e_H = 0.67 nm, e_V = 0.93 nm  (2491 3481  um rad)
beam2: e_H = 0.53 nm, e_V = 0.91 nm  (1980 3392  um rad)
* 07.04.2010 11:30 started mini scan for IP1
* 07.04.2010 12:13 started mini scan for IP8

FillNr 1031, Sat 10.04.2010 06:13 => Sat 10.04.2010 15:47  ~9.5h
3.5 TeV, Single_2b_1_1_1 , LHCb +, ALICE +/+, N=~ 1e10 p/bch, 11 10 11 10 m
emittances at 07:24:
beam1: e_H = 0.48 nm, e_V = 0.47 nm  (1809 1740  um rad)
beam2: e_H = 0.47 nm, e_V = 0.57 nm  (1748 2135  um rad)
emittances at 14:12:
beam1: e_H = 0.50 nm, e_V = 0.55 nm  (1862 2066  um rad)
beam2: e_H = 0.55 nm, e_V = 0.88 nm  (2065 3288  um rad)
* 10.04.2010 08:29 started mini scan for IP8
* 10.04.2010 10:36 started mini scan for IP5
* 10.04.2010 11:20 started mini scan for IP1
* 10.04.2010 12:44 started mini scan for IP8 (beams have moved down ?)
* 10.04.2010 14:36 started mini scan for IP1
* 10.04.2010 14:57 finished mini scan for IP5

FillNr 1033, Mon 12.04.2010 01:24 => Mon 12.04.2010 03:23  ~2h
3.5 TeV, Single_2b_1_1_1 , LHCb 0, ALICE +/+, N=~ 1.1e10 p/bch, 11 10 11 10 m
emittances at 02:06:
beam1: e_H = 0.32 nm, e_V = 0.30 nm  (1195 1130  um rad)
beam2: e_H = 0.42 nm, e_V = 0.62 nm  (1556 2312  um rad)
* 12.04.2010 02:12 started mini scan for IP5

FillNr 1034, Mon 12.04.2010 08:54 => Mon 12.04.2010 17:25  ~8.5h
3.5 TeV, Single_2b_1_1_1 , LHCb 0, ALICE +/+, N=~ 1.1e10 p/bch, 11 10 11 10 m
emittances at 15:20:
beam1: e_H = 0.66 nm, e_V = 0.39 nm  (2460 1440  um rad)
beam2: e_H = 1.96 nm, e_V = 1.40 nm  (7310 5220  um rad)
* 12.04.2010 09:40 started mini scan for IP5
09:20 RCBV12.L4B1 lost which distorted the orbit and caused some
intensity loss (likely collimator scraping). Corrected the orbit
taking out this corrector.

FillNr 1035, Tue 13.04.2010 05:01 => Tue 13.04.2010 09:31  ~4.5h
3.5 TeV, Single_2b_1_1_1 , LHCb +, ALICE +/+, N=~ 1.1e10 p/bch, 11 10 11 10 m
emittances at 07:26:
beam1: e_H = 0.41 nm, e_V = 0.53 nm  (1518 1975  um rad)
beam2: e_H = 0.51 nm, e_V = 0.63 nm  (1916 2363  um rad)
* 13.04.2010 05:36 started mini scan for IP8
* 13.04.2010 06:18 started mini scan for IP5
* 13.04.2010 07:23 started mini scan for IP1

FillNr 1038, Wed 14.04.2010 05:50 => Wed 14.04.2010 10:53  ~5h
3.5 TeV, Single_2b_1_1_1 , LHCb +, ALICE +/+, N=~ 1e10 p/bch, 11 10 11 10 m
emittances at 06:52:
beam1: e_H = 0.57 nm, e_V = 0.55 nm  (2109 2045  um rad)
beam2: e_H = 0.55 nm, e_V = 0.55 nm  (2062 2037  um rad)
* 14.04.2010 05:56 started mini scan for IP2
* 14.04.2010 08:20 started mini scan for IP5
* 14.04.2010 09:32 started mini scan for IP8

FillNr 1042, Thu 15.04.2010 06:22 => Thu 15.04.2010 08:54  ~2.5h
3.5 TeV, Single_2b_1_1_1 , LHCb +, ALICE +/+, CMS 0, N=~ 1e10 p/bch, 11 10 11 10 m
emittances at 06:29:
beam1: e_H = 0.60 nm, e_V = 0.91 nm  (2223 3402  um rad)
beam2: e_H = 0.44 nm, e_V = 1.28 nm  (1630 4760  um rad)
* 15.04.2010 06:31 started mini scan for IP5
* 15.04.2010 07:29 started mini scan for IP2

FillNr 1044, Fri 16.04.2010 05:50 => Fri 16.04.2010 09:12  ~3.5h
3.5 TeV, Single_2b_1_1_1 , LHCb +, ALICE +/+, N=~ 1.1e10 p/bch, 11 10 11 10 m
emittances at 06:47:
beam1: e_H = 0.42 nm, e_V = 0.41 nm  (1585 1512  um rad)
beam2: e_H = 0.52 nm, e_V = 0.46 nm  (1940 1727  um rad)
* 16.04.2010 06:01 started mini scan for IP5
* 16.04.2010 06:49 started mini scan for IP2
* 16.04.2010 08:00 started mini scan for IP1
* 16.04.2010 08:12 started mini scan for IP8

FillNr 1045, Sat 17.04.2010 05:55 => Sat 17.04.2010 14:58  ~9h
3.5 TeV, Single_2b_1_1_1 , LHCb +, ALICE +/+, N=~ 1.1e10 p/bch, 11 10 11 10 m
emittances at 08:59:
beam1: e_H = 0.38 nm, e_V = 0.87 nm  (1435 3230  um rad)
beam2: e_H = 0.57 nm, e_V = 1.40 nm  (2126 5214  um rad)
* 17.04.2010 06:34 Started mini scan in IP2
* 17.04.2010 06:57 Started mini scan in IP8
* 17.04.2010 07:23 Started mini scan in IP1
* 17.04.2010 07:37 Started mini scan in IP5
* 17.04.2010 10:07 Started mini scan in IP1
* 17.04.2010 10:34 Started mini scan in IP2
* 17.04.2010 11:28 Started mini scan in IP8

FillNr 1046, Sun 18.04.2010 06:06 => Sun 18.04.2010 06:55  ~0.8h
3.5 TeV, Single_2b_1_1_1 , LHCb +, ALICE +/+, N=~ 1.1e10 p/bch, 11 10 11 10 m
* 18.04.2010 06:06 Started mini scan in IP2
* 18.04.2010 06:36 Started manual adjustement in IP1

FillNr 1047, Sun 18.04.2010 11:28 => Sun 18.04.2010 14:39  ~3.1h
3.5 TeV, Single_2b_1_1_1 , LHCb +, ALICE +/+, N=~ 1.1e10 p/bch, 11 10 11 10 m
* 18.04.2010 11:44 Started mini scan in IP2
* 18.04.2010 12:20 Started mini scan in IP1
* 18.04.2010 12:51 Started mini scan in IP5
* 18.04.2010 13:26 Started mini scan in IP8

FillNr 1049, Mon 19.04.2010 03:55 => Mon 19.04.2010 05:14  ~1.3h
3.5 TeV, Single_2b_1_1_1 , LHCb +, ALICE +/+, N=~ 1.1e10 p/bch, 11 10 11 10 m
* 19.04.2010 04:20 Started mini scan in IP1
* 19.04.2010 04:59 Started mini scan in IP8

FillNr 1058, Sat 24.04.2010 03:13 => Sun 25.04.2010 09:30  ~30.3h
3.5 TeV, Single_3b_2_2_2 , LHCb +, ALICE +/+, N=~ 1.1e10 p/bch, 2 2 2 2 m
* 24.04.2010 03:49 Started mini scan in IP2  - inconclusive in V
* 24.04.2010 03:56 Started mini scan in IP1
* 24.04.2010 04:22 Started mini scan in IP5
* 24.04.2010 04:49 Started mini scan in IP8
* 24.04.2010 05:00 Started mini scan in IP2
* 24.04.2010 05:33 Started mini scan in IP1
* 24.04.2010 09:58 Started mini scan in IP5
@ 24.04.2010 11:36 Did vdM-V scan in IP5
@ 24.04.2010 12:00 Did vdM-H scan in IP5
* 24.04.2010 16:49 Started mini scan in IP1
o from timber
@ 24.04.2010 11:05 - 12:05 two planes vdM scan in IP5

FillNr 1059, Mon 26.04.2010 01:34 => Mon 26.04.2010 06:32  ~5h
3.5 TeV, Single_2b_1_1_1 , LHCb +, ALICE +/+, N=~ 1.1e10 p/bch, 2 2 2 2 m
* 26.04.2010 01:46 Started mini scan in IP1
* 26.04.2010 01:56 Started mini scan in IP5
* 26.04.2010 01:59 Started mini scan in IP2
* 26.04.2010 02:20 Started mini scan in IP8
@ 26.04.2010 03:21 Did vdM-H scan in IP8
@ 26.04.2010 03:33 Did vdM-V scan in IP8
@ 26.04.2010 05:01 Did vdM-H scan in IP1
@ 26.04.2010 05:26 Did vdM-V scan in IP1
o From timber:
@ 26.04.2010 03:00 - 04:18 four planes vdM scan in IP8
@ 26.04.2010 04:18 - 05:27 two  planes vdM scan in IP1

FillNr 1068, Sun 02.05.2010 14:33 => Sun 02.05.2010 21:44  ~7.2h
0.45 TeV, Single_2b_1_1_1 , LHCb +, ALICE +/+, N=~ 9e10 p/bch, 11 10 11 10 m

FillNr 1069, Mon 03.05.2010 02:03 => Mon 03.05.2010 09:18  ~7.2h
0.45 TeV, Single_2b_1_1_1 , LHCb -, ALICE +/+, N=~ 9e10 p/bch, 11 10 11 10 m

FillNr 1089, Sat 08.05.2010 22:33 => Sun 09.05.2010 18:55  ~20.4h
3.5 TeV, Single_2b_1_1_1 , LHCb -, ALICE 0/0, N=~ 2e10 p/bch, 2 2 2 2 m
@ 08.05.2010 23:15 Did vdM-HV scan in IP5
@ 09.05.2010 01:10 Did vdM-HV scan in IP1
o From timber:
@ 08.05.2010 23:15 - 09.05.2010 01:10 four planes vdM scan in IP5
@ 09.05.2010 01:10 - 09.05.2010 03:10 four planes vdM scan in IP1

FillNr 1090, Mon 10.05.2010 04:31 => Mon 10.05.2010 10:57  ~6.5h
3.5 TeV, Single_2b_1_1_1 , LHCb -, ALICE +/+, N=~ 2e10 p/bch, 2 2 2 2 m
@ 10.05.2010 05:15 Did vdM-HV scan in IP2
o From timber:
@ 10.05.2010 05:20 - 06:50 two planes vdM scan in IP2

FillNr 1101, Fri 14.05.2010 12:57 => Fri 14.05.2010 23:39 ~10.7h
3.5 TeV, Single_4b_2_2_2 , LHCb +, ALICE +/+, N=~ 2e10 p/bch, 2 2 2 2 m

FillNr 1104, Sat 15.05.2010 16:54 => Sun 16.05.2010 14:14 ~21.3h
3.5 TeV, Single_6b_3_3_3 , LHCb -, ALICE +/+, N=~ 2e10 p/bch, 2 2 2 2 m

FillNr 1107, Mon 17.05.2010 06:27 => Mon 17.05.2010 15:25 ~9.0h
3.5 TeV, Single_6b_3_3_3 , LHCb -, ALICE +/+, N=~ 2e10 p/bch, 2 2 2 2 m

FillNr 1109, Tue 18.05.2010 04:54 => Tue 18.05.2010 05:35 ~0.7h
3.5 TeV, Single_6b_3_3_3 , LHCb -, ALICE +/+, N=~ 2e10 p/bch, 2 2 2 2 m

FillNr 1112, Wed 19.05.2010 06:10 => Wed 19.05.2010 07:33 ~1.4h
3.5 TeV, Single_6b_3_3_3 , LHCb +, ALICE +/+, N=~ 2e10 p/bch, 2 2 2 2 m

FillNr 1117, Sat 22.05.2010 03:39 => Sat 22.05.2010 11:42 ~8.0h
3.5 TeV, Single_6b_3_3_3 , LHCb +, ALICE +/+, N=~ 2e10 p/bch, 2 2 2 2 m

FillNr 1118, Sun 23.05.2010 06:05 => Sun 23.05.2010 12:34 ~4.5h
3.5 TeV, Single_6b_3_3_3 , LHCb +, ALICE +/+, N=~ 2e10 p/bch, 2 2 2 2 m

FillNr 1119, Sun 23.05.2010 20:45 => Mon 24.05.2010 00:18 ~3.5h
3.5 TeV, Single_6b_3_3_3 , LHCb +, ALICE +/+, N=~ 2e10 p/bch, 2 2 2 2 m

FillNr 1121, Mon 24.05.2010 15:01 => Mon 24.05.2010 17:27 ~2.5h
3.5 TeV, Single_13b_8_8_8 , LHCb +, ALICE +/+, N=~ 2e10 p/bch, 2 2 2 2 m

FillNr 1122, Tue 25.05.2010 03:15 => Tue 25.05.2010 12:27 ~9.2h
3.5 TeV, Single_13b_8_8_8 , LHCb +, ALICE +/+, N=~ 2e10 p/bch, 2 2 2 2 m

FillNr 1128, Thu 27.05.2010 15:07 => Thu 27.05.2010 16:03  ~1.0h
0.45 TeV, Single_7b_4_4_4 , LHCb +, ALICE +/+, N=~ 9e10 p/bch, 11 10 11 10 m

FillNr 1134, Sat 05.06.2010 13:42 => Sat 05.06.2010 17:28  ~3.8h
3.5 TeV, Single_13b_8_8_8 , LHCb +, ALICE -/-, N=~ 2e10 p/bch, 2 2 2 2 m

FillNr 1179, Fri 25.06.2010 01:35 => Fri 25.06.2010 03:57  ~2.3h
3.5 TeV, Single_3b_2_2_2 , LHCb +, ALICE -/-, N=~ 8e10 p/bch, 3.5 3.5 3.5 3.5 m
o with crossing half angle -100 urad at IP1, +100 urad at IP5

FillNr 1182, Sat 26.06.2010 19:28 => Sun 27.06.2010 10:15  ~14.8h
3.5 TeV, Single_3b_2_2_2 , LHCb +, ALICE -/-, N=~ 8e10 p/bch, 3.5 3.5 3.5 3.5 m

FillNr 1185, Tue 29.06.2010 11:57 => Tue 29.06.2010 16:11  ~4.25h
3.5 TeV, Single_3b_2_2_2 , LHCb +, ALICE -/-, N=~ 10e10 p/bch, 3.5 3.5 3.5 3.5 m

FillNr 1186, Wed 30.06.2010 08:15 => Wed 30.06.2010 10:36  ~2.3h
3.5 TeV, Single_3b_2_2_2 , LHCb +, ALICE -/-, N=~ 10e10 p/bch, 3.5 3.5 3.5 3.5 m

FillNr 1188, Thu 01.07.2010 02:56 => Thu 01.07.2010 10:47  ~9.8h
3.5 TeV, Single_3b_2_2_2 , LHCb +, ALICE -/-, N=~ 8e10 p/bch, 3.5 3.5 3.5 3.5 m
!! out of stable beams from Thu 01.07.2010 07:30 to Thu 01.07.2010 07:41

FillNr 1190, Fri 02.07.2010 05:40 => Fri 02.07.2010 06:27  ~0.8h
3.5 TeV, Single_7b_4_4_4 , LHCb +, ALICE -/-, N=~ 8e10 p/bch, 3.5 3.5 3.5 3.5 m
!! with separated beams in IP2

FillNr 1192, Fri 02.07.2010 17:30 => Fri 02.07.2010 18:04  ~0.5h
3.5 TeV, Single_7b_4_4_4 , LHCb +, ALICE -/-, N=~ 9e10 p/bch, 3.5 3.5 3.5 3.5 m

FillNr 1196, Sun 04.07.2010 00:46 => Sun 04.07.2010 01:35  ~0.8h
3.5 TeV, Single_7b_4_4_4 , LHCb +, ALICE -/-, N=~ 9e10 p/bch, 3.5 3.5 3.5 3.5 m

FillNr 1197, Sun 04.07.2010 06:22 => Sun 04.07.2010 18:16  ~11.9h
3.5 TeV, Single_7b_4_4_4 , LHCb +, ALICE -/-, N=~ 9e10 p/bch, 3.5 3.5 3.5 3.5 m
!! out of stable beams from Sun 04.07.2010 15:23 to Sun 04.07.2010 16:36

FillNr 1198, Mon 05.07.2010 02:28 => Mon 05.07.2010 13:43  ~11.25h
3.5 TeV, Single_7b_4_4_4 , LHCb +, ALICE -/-, N=~ 9e10 p/bch, 3.5 3.5 3.5 3.5 m

FillNr 1199, Mon 05.07.2010 23:11 => Tue 06.07.2010 02:58  ~3.7h
3.5 TeV, Single_10b_4_2_4 , LHCb +, ALICE -/-, N=~ 8e10 p/bch, 3.5 3.5 3.5 3.5 m
!! changed lumi normalisation on Wed 07.07.2010

FillNr 1207, Fri 09.07.2010 04:16 => Fri 09.07.2010 10:17  ~6.0h
3.5 TeV, Single_10b_4_2_4 , LHCb +, ALICE -/-, N=~ 9e10 p/bch, 3.5 3.5 3.5 3.5 m

FillNr 1222, Mon 12.07.2010 03:02 => Mon 12.07.2010 11:56  ~7.9h
3.5 TeV, Single_9b_6_6_6 , LHCb +, ALICE -/-, N=~ 9e10 p/bch, 3.5 3.5 3.5 3.5 m
!! out of stable beams from Mon 12.07.2010 09:42 to Mon 12.07.2010 10:25

FillNr 1224, Tue 13.07.2010 05:08 => Tue 13.07.2010 14:59  ~9.8h
3.5 TeV, Single_12b_8_8_8 , LHCb -, ALICE -/-, N=~ 9e10 p/bch, 3.5 3.5 3.5 3.5 m

FillNr 1225, Wed 14.07.2010 02:13 => Wed 14.07.2010 17:02  ~14.8h
3.5 TeV, Single_12b_8_8_8 , LHCb -, ALICE -/-, N=~ 9e10 p/bch, 3.5 3.5 3.5 3.5 m

FillNr 1226, Thu 15.07.2010 04:19 => Thu 15.07.2010 13:15  ~8.9h
3.5 TeV, Single_13b_8_8_8 , LHCb -, ALICE -/-, N=~ 9e10 p/bch, 3.5 3.5 3.5 3.5 m
!! First with transverse damper on from collisions

FillNr 1229, Sat 17.07.2010 00:44 => Sat 17.07.2010 04:36  ~3.8h
3.5 TeV, Single_13b_8_8_8 , LHCb -, ALICE -/-, N=~ 9e10 p/bch, 3.5 3.5 3.5 3.5 m

FillNr 1232, Sat 17.07.2010 19:19 => Sun 18.07.2010 01:11  ~5.9h
3.5 TeV, Single_13b_8_8_8 , LHCb -, ALICE -/-, N=~ 9e10 p/bch, 3.5 3.5 3.5 3.5 m

FillNr 1233, Sun 18.07.2010 10:56 => Mon 19.07.2010 05:58  ~19.0h
3.5 TeV, Single_13b_8_8_8 , LHCb -, ALICE -/-, N=~ 9e10 p/bch, 3.5 3.5 3.5 3.5 m

FillNr 1250, Wed 28.07.2010 22:28 => Thu 29.07.2010 10:35  ~12.1h
3.5 TeV, Single_13b_8_8_8 , LHCb +, ALICE -/-, N=~ 9e10 p/bch, 3.5 3.5 3.5 3.5 m

FillNr 1251, Thu 29.07.2010 23:28 => Fri 30.07.2010 07:25  ~8.0h
3.5 TeV, Multi_25b_16_16_16_hyb , LHCb +, ALICE -/-, N=~ 9e10 p/bch, 3.5 3.5 3.5 3.5 m

FillNr 1253, Fri 30.07.2010 23:11 => Sat 31.07.2010 12:20  ~13.1h
3.5 TeV, Multi_25b_16_16_16 , LHCb +, ALICE -/-, N=~ 9e10 p/bch, 3.5 3.5 3.5 3.5 m

FillNr 1256, Sun 01.08.2010 03:50 => Sun 01.08.2010 04:49  ~1.0h
3.5 TeV, Multi_25b_16_16_16 , LHCb +, ALICE -/-, N=~ 9e10 p/bch, 3.5 3.5 3.5 3.5 m

FillNr 1257, Sun 01.08.2010 22:00 => Mon 02.08.2010 12:35  ~14.5h
3.5 TeV, Multi_25b_16_16_16 , LHCb +, ALICE -/-, N=~ 9e10 p/bch, 3.5 3.5 3.5 3.5 m

FillNr 1258, Tue 03.08.2010 00:22 => Tue 03.08.2010 07:39  ~7.3h
3.5 TeV, Multi_25b_16_16_16 , LHCb +, ALICE -/-, N=~ 9e10 p/bch, 3.5 3.5 3.5 3.5 m

FillNr 1260, Wed 04.08.2010 04:31 => Wed 04.08.2010 06:38  ~2.1h
3.5 TeV, Multi_25b_16_16_16 , LHCb +, ALICE -/-, N=~ 9e10 p/bch, 3.5 3.5 3.5 3.5 m

FillNr 1262, Wed 04.08.2010 17:40 => Thu 05.08.2010 11:19  ~17.65h
3.5 TeV, Multi_25b_16_16_16 , LHCb +, ALICE -/-, N=~ 9e10 p/bch, 3.5 3.5 3.5 3.5 m

FillNr 1263, Fri 06.08.2010 03:52 => Fri 06.08.2010 19:08  ~15.25h
3.5 TeV, Multi_25b_16_16_16 , LHCb +, ALICE -/-, N=~ 9e10 p/bch, 3.5 3.5 3.5 3.5 m

FillNr 1264, Sat 07.08.2010 01:42 => Sat 07.08.2010 02:14  ~0.5h
3.5 TeV, Multi_25b_16_16_16 , LHCb +, ALICE -/-, N=~ 9e10 p/bch, 3.5 3.5 3.5 3.5 m

FillNr 1266, Sat 07.08.2010 23:12 => Sun 08.08.2010 01:10  ~2.0h
3.5 TeV, Multi_25b_16_16_16 , LHCb +, ALICE -/-, N=~ 9e10 p/bch, 3.5 3.5 3.5 3.5 m

FillNr 1267, Sun 08.08.2010 05:18 => Sun 08.08.2010 18:52  ~13.5h
3.5 TeV, Multi_25b_16_16_16 , LHCb +, ALICE -/-, N=~ 9e10 p/bch, 3.5 3.5 3.5 3.5 m

FillNr 1268, Mon 09.08.2010 01:29 => Mon 09.08.2010 04:02  ~2.5h
3.5 TeV, Multi_25b_16_16_16 , LHCb +, ALICE -/-, N=~ 9e10 p/bch, 3.5 3.5 3.5 3.5 m

FillNr 1271, Tue 10.08.2010 07:24 => Tue 10.08.2010 12:22  ~5.0h
3.5 TeV, Multi_25b_16_16_16 , LHCb +, ALICE -/-, N=~ 9e10 p/bch, 3.5 3.5 3.5 3.5 m

FillNr 1283, Fri 13.08.2010 23:06 => Sat 14.08.2010 12:04  ~13.0h
3.5 TeV, Multi_25b_16_16_16 , LHCb +, ALICE -/-, N=~ 9e10 p/bch, 3.5 3.5 3.5 3.5 m

FillNr 1284, Sat 14.08.2010 15:44 => Sat 14.08.2010 19:13  ~3.5h
3.5 TeV, Multi_25b_16_16_16 , LHCb +, ALICE -/-, N=~ 1e11 p/bch, 3.5 3.5 3.5 3.5 m

FillNr 1285, Sun 15.08.2010 00:39 => Sun 15.08.2010 13:02  ~12.4h
3.5 TeV, Multi_25b_16_16_16 , LHCb +, ALICE -/-, N=~ 1e11 p/bch, 3.5 3.5 3.5 3.5 m

FillNr 1287, Sun 15.08.2010 23:01 => Mon 16.08.2010 09:24  ~10.4h
3.5 TeV, Multi_25b_16_16_16 , LHCb +, ALICE -/-, N=~ 1e11 p/bch, 3.5 3.5 3.5 3.5 m

FillNr 1293, Tue 18.08.2010 09:12 => Tue 18.08.2010 21:13  ~12.0h
3.5 TeV, Multi_25b_16_16_16 , LHCb -, ALICE -/-, N=~ 1e11 p/bch, 3.5 3.5 3.5 3.5 m

FillNr 1295, Thu 19.08.2010 23:36 => Fri 20.08.2010 14:19  ~14.7h
3.5 TeV, 1250ns_48b_36_16_36 , LHCb -, ALICE -/-, N=~ 1e11 p/bch, 3.5 3.5 3.5 3.5 m

FillNr 1298, Mon 23.08.2010 00:52 => Mon 23.08.2010 13:50  ~13.0h
3.5 TeV, 1250ns_48b_36_16_36 , LHCb -, ALICE -/-, N=~ 1e11 p/bch, 3.5 3.5 3.5 3.5 m

FillNr 1299, Tue 24.08.2010 00:11 => Tue 24.08.2010 03:26  ~3.25h
3.5 TeV, 1250ns_48b_36_16_36 , LHCb -, ALICE -/-, N=~ 1e11 p/bch, 3.5 3.5 3.5 3.5 m

FillNr 1301, Tue 24.08.2010 17:35 => Wed 25.08.2010 07:53  ~14.3h
3.5 TeV, 1000ns_50b_35_14_35 , LHCb -, ALICE -/-, N=~ 1e11 p/bch, 3.5 3.5 3.5 3.5 m

FillNr 1303, Thu 26.08.2010 04:21 => Thu 26.08.2010 17:26  ~13.1h
3.5 TeV, 1000ns_47b_32_14_32 , LHCb -, ALICE -/-, N=~ 1e11 p/bch, 3.5 3.5 3.5 3.5 m

FillNr 1305, Fri 27.08.2010 06:11 => Fri 27.08.2010 09:41  ~3.5h
3.5 TeV, 1000ns_50b_35_14_35 , LHCb -, ALICE -/-, N=~ 1e11 p/bch, 3.5 3.5 3.5 3.5 m

FillNr 1308, Sat 28.08.2010 22:43 => Sun 29.08.2010 12:22  ~13.7h
3.5 TeV, 1000ns_50b_35_14_35 , LHCb -, ALICE -/-, N=~ 1e11 p/bch, 3.5 3.5 3.5 3.5 m

FillNr 1309, Sun 29.08.2010 18:17 => Mon 30.08.2010 05:35  ~11.3h
3.5 TeV, 1000ns_50b_35_14_35 , LHCb +, ALICE -/-, N=~ 1e11 p/bch, 3.5 3.5 3.5 3.5 m

FillNr 1364, Wed 22.09.2010 16:54 => Thu 23.09.2010 06:37  ~13.7h
3.5 TeV, 150ns_24b_16_16_16_8bpi , LHCb -, ALICE +/+, N=~ 1e11 p/bch, 3.5 3.5 3.5 3.5 m

FillNr 1366, Thu 23.09.2010 19:10 => Fri 24.09.2010 09:12  ~14.0h
3.5 TeV, 150ns_56b_47_16_47_8bpi , LHCb -, ALICE +/+, N=~ 1e11 p/bch, 3.5 3.5 3.5 3.5 m

FillNr 1369, Sat 25.09.2010 09:38 => Sat 25.09.2010 11:05  ~1.5h
3.5 TeV, 150ns_56b_47_16_47_8bpi , LHCb -, ALICE -/-, N=~ 1e11 p/bch, 3.5 3.5 3.5 3.5 m

FillNr 1372, Sat 25.09.2010 19:39 => Sun 26.09.2010 11:18  ~15.7h
3.5 TeV, 150ns_104b_93_8_93_8bpi , LHCb -, ALICE -/-, N=~ 1e11 p/bch, 3.5 3.5 3.5 3.5 m
One hour 09:26-10:28 for TOTEM RP tests in ADJUST

FillNr 1373, Sun 26.09.2010 21:27 => Mon 27.09.2010 09:58  ~12.5h
3.5 TeV, 150ns_104b_93_8_93_8bpi , LHCb -, ALICE -/-, N=~ 1e11 p/bch, 3.5 3.5 3.5 3.5 m

FillNr 1375, Tue 28.09.2010 02:23 => Tue 28.09.2010 11:23  ~9.0h
3.5 TeV, 150ns_104b_93_8_93_8bpi , LHCb -, ALICE -/-, N=~ 1e11 p/bch, 3.5 3.5 3.5 3.5 m
Started moving TOTEM pots at ~9:15 one by one. Last one at 10:53.

FillNr 1381, Thu 30.09.2010 02:25 => Thu 30.09.2010 05:28  ~3.0h
3.5 TeV, 150ns_152b_140_16_140_8+8bpi11inj , LHCb -, ALICE -/-, N=~ 1e11 p/bch, 3.5 3.5 3.5 3.5 m

FillNr 1386, Fri 01.10.2010 13:30 => Fri 01.10.2010 16:24  ~2.9h
3.5 TeV, Single_19b_6_1_12_allVdm , LHCb -, ALICE -/-, N=~ 8e10 p/bch, 3.5 3.5 3.5 3.5 m
@ 01.10.2010 13:50-14:02 Did vdM-H scan in IP1
@ 01.10.2010 14:07-14:19 Did vdM-V scan in IP1
@ 01.10.2010 14:22-14:37 Did vdM-H scan in IP1
@ 01.10.2010 14:39-14:53 Did vdM-V scan in IP1
@ 01.10.2010 14:57-15:05 Did vdM-H scan in IP1 + V-offset
@ 01.10.2010 15:07-15:16 Did vdM-V scan in IP1 + H-offset
@ 01.10.2010 15:30-15:45 Did vdM-H scan in IP5
@ 01.10.2010 15:48-16:01 Did vdM-V scan in IP5
@ 01.10.2010 16:03-16:15 Did vdM-H scan in IP5 beam1 only
@ 01.10.2010 16:18-16:24 Did vdM-V scan in IP5 beam1 only, not completed

FillNr 1387, Sat 02.10.2010 05:08 => Sat 02.10.2010 07:06  ~2.0h
3.5 TeV, 150ns_152b_140_16_140_8+8bpi11inj , LHCb -, ALICE -/-, N=~ 1e11 p/bch, 3.5 3.5 3.5 3.5 m

FillNr 1388, Sat 02.10.2010 10:57 => Sat 02.10.2010 13:08  ~3.2h
3.5 TeV, 150ns_152b_140_16_140_8+8bpi11inj , LHCb -, ALICE -/-, N=~ 1e11 p/bch, 3.5 3.5 3.5 3.5 m

FillNr 1389, Sun 03.10.2010 13:16 => Sun 03.10.2010 20:27  ~7.2h
3.5 TeV, 150ns_152b_140_16_140_8+8bpi11inj , LHCb -, ALICE -/-, N=~ 1e11 p/bch, 3.5 3.5 3.5 3.5 m

FillNr 1393, Mon 04.10.2010 20:00 => Tue 05.10.2010 09:43  ~13.7h
3.5 TeV, 150ns_200b_186_8_186_8+8bpi17inj , LHCb -, ALICE -/-, N=~ 1e11 p/bch, 3.5 3.5 3.5 3.5 m
Started moving TOTEM pots in at ~6:15 one by one. Last V one at 7:15.
H pots moved in at 8:45, took data 10 min, then did the shake test (3 times
moved in-out with all pots).
o 04.10.2010 22:15 - 05.10.2010 01:26 Did H/V length scale calibration in IP1

FillNr 1394, Tue 05.10.2010 23:58 => Wed 06.10.2010 01:41  ~1.7h
3.5 TeV, 150ns_200b_186_8_186_8+8bpi17inj , LHCb -, ALICE -/-, N=~ 1e11 p/bch, 3.5 3.5 3.5 3.5 m

FillNr 1397, Thu 07.10.2010 04:23 => Thu 07.10.2010 10:54  ~6.5h
3.5 TeV, 150ns_200b_186_8_186_8+8bpi17inj , LHCb -, ALICE -/-, N=~ 1e11 p/bch, 3.5 3.5 3.5 3.5 m
Started moving TOTEM pots in at ~7:49 one by one. Last V one at 8:10.

FillNr 1400, Fri 08.10.2010 02:36 => Fri 08.10.2010 09:10  ~6.6h
3.5 TeV, 150ns_248b_233_16_233_3x8bpi15inj , LHCb -, ALICE -/-, N=~ 1e11 p/bch, 3.5 3.5 3.5 3.5 m
Started moving TOTEM pots in at ~7:29 one by one. Last V at 7:45.

FillNr 1408, Mon 11.10.2010 21:20 => Tue 12.10.2010 07:17  ~10.0h
3.5 TeV, 150ns_248b_233_16_233_3x8bpi15inj , LHCb -, ALICE -/-, N=~ 1e11 p/bch, 3.5 3.5 3.5 3.5 m

FillNr 1418, Thu 14.10.2010 03:38 => Thu 14.10.2010 12:06  ~8.5h
3.5 TeV, 150ns_248b_233_16_233_3x8bpi15inj , LHCb -, ALICE -/-, N=~ 1e11 p/bch, 3.5 3.5 3.5 3.5 m
TOTEM RPs in from ~8:30.

FillNr 1422, Fri 15.10.2010 13:14 => Fri 15.10.2010 18:27  ~5.2h
3.5 TeV, Single_16b_3_1_12_allVdmB , LHCb -, ALICE -/-, N=~ 0.8e11 p/bch, 3.5 3.5 3.5 3.5 m
@ 15.10.2010 13:43-14:02 Did vdM-H scan in IP8
@ 15.10.2010 14:02-14:21 Did vdM-V scan in IP8
@ 15.10.2010 14:25-14:40 Did vdM-H scan in IP8 single beam
@ 15.10.2010 14:44-14:58 Did vdM-V scan in IP8 single beam
@ 15.10.2010 15:18-15:33 Did vdM-H scan in IP5 beam1 only
@ 15.10.2010 15:34-15:46 Did vdM-V scan in IP5 beam1 only
@ 15.10.2010 15:50-16:02 Did vdM-H scan in IP5 beam2 only
@ 15.10.2010 16:03-16:17 Did vdM-V scan in IP5 beam2 only
@ 15.10.2010 16:31-16:51 Did vdM-H scan in IP2
@ 15.10.2010 16:53-17:11 Did vdM-V scan in IP2
@ 15.10.2010 17:15-17:32 Did vdM-H scan in IP2 reversed
@ 15.10.2010 17:33-17:50 Did vdM-V scan in IP2 reversed
o 15.10.2010 17:55-18:00 Did H-length scale calibration in IP8
o 15.10.2010 18:00-18:05 Did V-length scale calibration in IP8
o 15.10.2010 18:10-18:19 Did V-length scale calibration in IP5
o 15.10.2010 18:20-18:26 Started H-length scale calibration in IP5 (not completed)

FillNr 1424, Sat 16.10.2010 02:30 => Sat 16.10.2010 03:23  ~0.8h
3.5 TeV, 150ns_312b_295_16_295_3x8bpi19inj , LHCb -, ALICE -/-, N=~ 1e11 p/bch, 3.5 3.5 3.5 3.5 m

FillNr 1427, Sat 16.10.2010 22:56 => Sun 17.10.2010 09:31  ~10.6h
3.5 TeV, 150ns_312b_295_16_295_3x8bpi19inj , LHCb -, ALICE -/-, N=~ 1e11 p/bch, 3.5 3.5 3.5 3.5 m

FillNr 1430, Mon 18.10.2010 04:25 => Mon 18.10.2010 05:03  ~0.5h
3.5 TeV, 150ns_312b_295_16_295_3x8bpi19inj , LHCb -, ALICE -/-, N=~ 1e11 p/bch, 3.5 3.5 3.5 3.5 m

FillNr 1439, Sun 24.10.2010 09:59 => Sun 24.10.2010 20:41  ~10.6h
3.5 TeV, 150ns_312b_295_16_295_3x8bpi19inj , LHCb +, ALICE -/-, N=~ 1e11 p/bch, 3.5 3.5 3.5 3.5 m
o RP in from  beginning of stable beams
o Did H- and V-length scale calibration in IP5

FillNr 1440, Mon 25.10.2010 02:35 => Mon 25.10.2010 13:54  ~11.3h
3.5 TeV, 150ns_368b_348_15_344_4x8bpi19inj , LHCb +, ALICE -/-, N=~ 1e11 p/bch, 3.5 3.5 3.5 3.5 m

FillNr 1443, Tue 26.10.2010 05:35 => Tue 26.10.2010 07:49  ~2.1h
3.5 TeV, 150ns_368b_348_15_344_4x8bpi19inj , LHCb +, ALICE -/-, N=~ 1e11 p/bch, 3.5 3.5 3.5 3.5 m

FillNr 1444, Tue 26.10.2010 13:35 => Tue 26.10.2010 20:47  ~7.2h
3.5 TeV, 150ns_368b_348_15_344_4x8bpi19inj , LHCb +, ALICE -/-, N=~ 1e11 p/bch, 3.5 3.5 3.5 3.5 m

FillNr 1450, Thu 28.10.2010 00:45 => Thu 28.10.2010 15:17  ~14.5h
3.5 TeV, 150ns_368b_348_15_344_4x8bpi19inj , LHCb +, ALICE -/-, N=~ 1e11 p/bch, 3.5 3.5 3.5 3.5 m

FillNr 1453, Fri 29.10.2010 04:16 => Fri 29.10.2010 10:36  ~6.3h
3.5 TeV, 150ns_368b_348_15_344_4x8bpi19inj , LHCb +, ALICE -/-, N=~ 1e11 p/bch, 3.5 3.5 3.5 3.5 m
o 30.10.2010 08:43-10:30 Did H-length scale calibration in IP2
o Did not do V-length scale calibration in IP2 (problem with beam position
  monitoring at TCTs)

FillNr 1455, Sat 30.10.2010 05:33 => Sat 30.10.2010 06:32  ~1.0h
3.5 TeV, Single_5b_5_1_1 , LHCb +, ALICE -/-, N=~ 7e10 p/bch, 3.5 3.5 3.5 3.5 m
Special period of beam in ADJUST  mode for TOTEM: 
   Sat 30.10.2010 01:15 => Sat 30.10.2010 05:15  ~4.0h
with primary collimtors at 4.5 sigma and
Totem, roman pots very close at about 7 sigmas from beams.
Then, stable beams.
o 30.10.2010 06:00-06:35 Did V-length scale calibration in IP2
o 30.10.2010 06:35-06:45 Did H-length scale calibration in IP2

FillNr 1459, Sun 31.10.2010 01:24 => Sun 31.10.2010 07:25  ~6.0h
3.5 TeV, 50ns_109b_91_12_90_12bpi10inj , LHCb +, ALICE -/-, N=~ 1e11 p/bch, 3.5 3.5 3.5 3.5 m
o NB: real time of start was 02:24 summer time, but I put 01:24 winter time for simplicity

FillNr 1482, Mon 08.11.2010 11:19 => Mon 08.11.2010 20:02  ~8.7h
3.5 TeV, Single_2b_1_1_0_1bpi2inj_IONS , LHCb 0, ALICE -/-, N=~ 1e10 p/bch, 3.5 3.5 3.5 3.5 m
o 12:57 TCTVB in IR2 removed manually

FillNr 1483, Tue 09.11.2010 01:01 => Tue 09.11.2010 09:58  ~8.9h
3.5 TeV, Single_5b_4_4_0_1bpi5inj_IONS , LHCb 0, ALICE -/-, N=~ 1e10 p/bch, 3.5 3.5 3.5 3.5 m

FillNr 1485, Tue 09.11.2010 22:49 => Wed 10.11.2010 12:43  ~13.9h
3.5 TeV, 500ns_17b_16_16_0_4bpi5inj_IONS , LHCb 0, ALICE -/-, N=~ 1e10 p/bch, 3.5 3.5 3.5 3.5 m

FillNr 1488, Fri 12.11.2010 00:53 => Fri 12.11.2010 06:39  ~5.8h
3.5 TeV, 500ns_69b_65_66_0_4bpi18inj_IONS , LHCb 0, ALICE -/-, N=~ 1e10 p/bch, 3.5 3.5 3.5 3.5 m

FillNr 1489, Sat 13.11.2010 01:04 => Sat 13.11.2010 10:41  ~9.6h
3.5 TeV, 500ns_69b_65_66_0_4bpi18inj_IONS , LHCb 0, ALICE -/-, N=~ 1e10 p/bch, 3.5 3.5 3.5 3.5 m

FillNr 1490, Sun 14.11.2010 00:32 => Sun 14.11.2010 08:21  ~7.8h
3.5 TeV, 500ns_121b_113_114_0_4bpi31inj_IONS , LHCb 0, ALICE -/-, N=~ 1e10 p/bch, 3.5 3.5 3.5 3.5 m

FillNr 1491, Sun 14.11.2010 18:04 => Mon 15.11.2010 00:38  ~6.6h
3.5 TeV, 500ns_121b_113_114_0_4bpi31inj_IONS , LHCb 0, ALICE -/-, N=~ 1e10 p/bch, 3.5 3.5 3.5 3.5 m

FillNr 1492, Mon 15.11.2010 07:42 => Mon 15.11.2010 08:44  ~1.0h
3.5 TeV, 500ns_121b_113_114_0_4bpi31inj_IONS , LHCb 0, ALICE -/-, N=~ 1e10 p/bch, 3.5 3.5 3.5 3.5 m

FillNr 1493, Mon 15.11.2010 12:48 => Mon 15.11.2010 22:04  ~9.3h
3.5 TeV, 500ns_121b_113_114_0_4bpi31inj_IONS , LHCb 0, ALICE -/-, N=~ 1e10 p/bch, 3.5 3.5 3.5 3.5 m

FillNr 1494, Tue 16.11.2010 02:28 => Tue 16.11.2010 09:00  ~6.5h
3.5 TeV, 500ns_121b_113_114_0_4bpi31inj_IONS , LHCb 0, ALICE -/-, N=~ 1e10 p/bch, 3.5 3.5 3.5 3.5 m

FillNr 1496, Wed 17.11.2010 00:33 => Wed 17.11.2010 06:14  ~5.7h
3.5 TeV, 500ns_121b_113_114_0_4bpi31inj_IONS , LHCb 0, ALICE -/-, N=~ 1e10 p/bch, 3.5 3.5 3.5 3.5 m
o stopped for 75ns+50ns proton studies (Wed 06:00 to Sat 08:00)

FillNr 1504, Sat 20.11.2010 23:00 => Sun 21.11.2010 06:16  ~7.3h
3.5 TeV, 500ns_121b_113_114_0_4bpi31inj_IONS , LHCb 0, ALICE -/-, N=~ 1e10 p/bch, 3.5 3.5 3.5 3.5 m
o beam2 RF=13461 bunch is missing?
o did validation of VdM scan range in the test ramp before this fill 

FillNr 1505, Sun 21.11.2010 11:00 => Sun 21.11.2010 13:05  ~2.1h
3.5 TeV, 500ns_121b_113_114_0_4bpi31inj_IONS , LHCb 0, ALICE -/-, N=~ 1e10 p/bch, 3.5 3.5 3.5 3.5 m

FillNr 1508, Mon 22.11.2010 01:36 => Mon 22.11.2010 09:49  ~8.2h
3.5 TeV, 500ns_121b_113_114_0_4bpi31inj_IONS , LHCb 0, ALICE -/-, N=~ 1e10 p/bch, 3.5 3.5 3.5 3.5 m

FillNr 1509, Mon 22.11.2010 14:06 => Mon 22.11.2010 15:16  ~1.1h
3.5 TeV, 500ns_121b_113_114_0_4bpi31inj_IONS , LHCb 0, ALICE -/-, N=~ 1e10 p/bch, 3.5 3.5 3.5 3.5 m

FillNr 1511, Mon 22.11.2010 21:59 => Tue 23.11.2010 08:00  ~10.0h
3.5 TeV, 500ns_121b_113_114_0_4bpi31inj_IONS , LHCb 0, ALICE -/-, N=~ 1e10 p/bch, 3.5 3.5 3.5 3.5 m
o changed ALICE polarity after this fill on Tue 23.11.2010 afternoon 

FillNr 1514, Wed 24.11.2010 02:04 => Wed 24.11.2010 08:31  ~6.5h
3.5 TeV, 500ns_121b_113_114_0_4bpi31inj_IONS , LHCb 0, ALICE +/+, N=~ 1e10 p/bch, 3.5 3.5 3.5 3.5 m

FillNr 1515, Wed 24.11.2010 14:01 => Wed 24.11.2010 17:00  ~3.0h
3.5 TeV, 500ns_121b_113_114_0_4bpi31inj_IONS , LHCb 0, ALICE +/+, N=~ 1e10 p/bch, 3.5 3.5 3.5 3.5 m

FillNr 1517, Wed 24.11.2010 22:02 => Thu 25.11.2010 03:34  ~5.5h
3.5 TeV, 500ns_121b_113_114_0_4bpi31inj_IONS , LHCb 0, ALICE +/+, N=~ 1e10 p/bch, 3.5 3.5 3.5 3.5 m

FillNr 1518, Thu 25.11.2010 06:58 => Thu 25.11.2010 08:06  ~1.1h
3.5 TeV, 500ns_121b_113_114_0_4bpi31inj_IONS , LHCb 0, ALICE +/+, N=~ 1e10 p/bch, 3.5 3.5 3.5 3.5 m

FillNr 1520, Thu 25.11.2010 18:11 => Thu 25.11.2010 23:58  ~5.8h
3.5 TeV, 500ns_121b_113_114_0_4bpi31inj_IONS , LHCb 0, ALICE +/+, N=~ 1e10 p/bch, 3.5 3.5 3.5 3.5 m

FillNr 1521, Fri 26.11.2010 05:43 => Fri 26.11.2010 09:51  ~4.1h
3.5 TeV, 500ns_121b_113_114_0_4bpi31inj_IONS , LHCb 0, ALICE +/+, N=~ 1e10 p/bch, 3.5 3.5 3.5 3.5 m

FillNr 1522, Fri 26.11.2010 13:32 => Fri 26.11.2010 21:35  ~8.0h
3.5 TeV, 500ns_121b_113_114_0_4bpi31inj_IONS , LHCb 0, ALICE +/+, N=~ 1e10 p/bch, 3.5 3.5 3.5 3.5 m
o ATLAS solenoid off for calibration

FillNr 1523, Sat 27.11.2010 03:59 => Sat 27.11.2010 12:23  ~8.4h
3.5 TeV, 500ns_121b_113_114_0_4bpi31inj_IONS , LHCb 0, ALICE +/+, N=~ 1e10 p/bch, 3.5 3.5 3.5 3.5 m
o ATLAS solenoid off for calibration

FillNr 1525, Sat 27.11.2010 23:54 => Sun 28.11.2010 09:51 ~10.0h
3.5 TeV, 500ns_121b_113_114_0_4bpi31inj_IONS , LHCb 0, ALICE +/+, N=~ 1e10 p/bch, 3.5 3.5 3.5 3.5 m

FillNr 1526, Sun 28.11.2010 13:22 => Sun 28.11.2010 18:59  ~5.5h
3.5 TeV, 500ns_121b_113_114_0_4bpi31inj_IONS , LHCb 0, ALICE +/+, N=~ 1e10 p/bch, 3.5 3.5 3.5 3.5 m

FillNr 1528, Mon 29.11.2010 02:05 => Mon 29.11.2010 03:41  ~1.5h
3.5 TeV, 500ns_121b_113_114_0_4bpi31inj_IONS , LHCb 0, ALICE +/+, N=~ 1e10 p/bch, 3.5 3.5 3.5 3.5 m

FillNr 1530, Mon 29.11.2010 14:54 => Mon 29.11.2010 17:06  ~2.2h
3.5 TeV, 500ns_121b_113_114_0_4bpi31inj_IONS , LHCb 0, ALICE +/+, N=~ 1e10 p/bch, 3.5 3.5 3.5 3.5 m

FillNr 1532, Mon 29.11.2010 23:56 => Tue 30.11.2010 08:05  ~8.2h
3.5 TeV, 500ns_121b_113_114_0_4bpi31inj_IONS , LHCb 0, ALICE +/+, N=~ 1e10 p/bch, 3.5 3.5 3.5 3.5 m

FillNr 1533, Tue 30.11.2010 13:31 => Tue 30.11.2010 22:04  ~8.5h
3.5 TeV, 500ns_121b_113_114_0_4bpi31inj_IONS , LHCb 0, ALICE +/+, N=~ 1e10 p/bch, 3.5 3.5 3.5 3.5 m
@ 30.11.2010 13:46-14:06 Did vdM-H scan in IP2
@ 30.11.2010 14:06-14:28 Did vdM-V scan in IP2
@ 30.11.2010 14:33-14:54 Did vdM-H scan in IP1
@ 30.11.2010 14:54-15:16 Did vdM-V scan in IP1
@ 30.11.2010 15:29-15:50 Did vdM-H scan in IP5
@ 30.11.2010 15:50-16:12 Did vdM-V scan in IP5
@ 30.11.2010 16:15-16:37 Did vdM-H scan in IP2 (DAQ problem)
@ 30.11.2010 16:37-16:59 Did vdM-V scan in IP2
@ 30.11.2010 16:59-17:22 Did vdM-H scan in IP2
@ 30.11.2010 17:21-17:43 Did vdM-H scan in IP1
@ 30.11.2010 17:43-18:03 Did vdM-V scan in IP1

FillNr 1534, Wed 01.12.2010 08:38 => Wed 01.12.2010 15:18  ~6.7h
3.5 TeV, 500ns_121b_113_114_0_4bpi31inj_IONS , LHCb 0, ALICE +/+, N=~ 1e10 p/bch, 3.5 3.5 3.5 3.5 m

FillNr 1535, Wed 01.12.2010 22:49 => Thu 02.12.2010 01:38  ~2.8h
3.5 TeV, 500ns_121b_113_114_0_4bpi31inj_IONS , LHCb 0, ALICE +/+, N=~ 1e10 p/bch, 3.5 3.5 3.5 3.5 m

FillNr 1536, Sat 04.12.2010 13:54 => Sat 04.12.2010 20:38  ~6.7h
3.5 TeV, 500ns_137b_129_130_0_8bpi18inj_IONS , LHCb 0, ALICE +/+, N=~ 1e10 p/bch, 3.5 3.5 3.5 3.5 m

FillNr 1538, Sun 05.12.2010 11:07 => Sun 05.12.2010 11:22  ~0.25h
3.5 TeV, 500ns_137b_129_130_0_8bpi18inj_IONS , LHCb 0, ALICE +/+, N=~ 1e10 p/bch, 3.5 3.5 3.5 3.5 m

FillNr 1539, Sun 05.12.2010 17:59 => Sun 05.12.2010 23:41  ~5.7h
3.5 TeV, 500ns_137b_129_130_0_8bpi18inj_IONS , LHCb 0, ALICE +/+, N=~ 1e10 p/bch, 3.5 3.5 3.5 3.5 m

FillNr 1540, Mon 06.12.2010 04:01 => Mon 06.12.2010 09:56  ~5.9h
3.5 TeV, 500ns_137b_129_130_0_8bpi18inj_IONS , LHCb 0, ALICE +/+, N=~ 1e10 p/bch, 3.5 3.5 3.5 3.5 m

FillNr 1541, Mon 06.12.2010 14:10 => Mon 06.12.2010 18:00  ~3.8h
3.5 TeV, 500ns_137b_129_130_0_8bpi18inj_IONS , LHCb 0, ALICE +/+, N=~ 1e10 p/bch, 3.5 3.5 3.5 3.5 m
