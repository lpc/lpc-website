experiment_bunches = "";
currentSchemePath = "";
currentSchemeName = "";
currentRelPath    = "";
currentData       = undefined;
bxOffset          = 0;
var timer         = undefined;
playerSpeed       = 50;

debug = function( txt ) {
    $('div#debug').append( txt + '<br>' );
}

error = function( txt ) {
    $('div#error').append( txt );
}

clear_error = function () {
    $('div#error').html("")
}

postRequest = function( data, baseurl ) {
    var url = window.location.protocol + '//' + window.location.host;
    var path = window.location.pathname;
    path = path.substring(0,path.lastIndexOf('/'));
    url += path + '/' + baseurl;
    //debug( JSON.stringify(data));
    //debug( url );

    $.ajax( {type: "GET",
	     url: url,
	     data: data,
             timeout: 60000,
	     dataType: "json",
	     success: function(reply,status) {
		 if ( status != "success" ) {
		     error( "Error communicating with server: " + status);
		     return;
		 } else {
		     //debug( "reply came");
		     if ( reply.debug ) debug( reply.debug );
		     if ( reply.info  ) debug( reply.info );
		     if ( 'error' in reply ) {
			 $('#numberofmatches').html(reply.error);
			 $('#numberofmatches').show("blind");
			 replyPending = 0;
		     } else {
			 clear_error();
		     }
		     if ( reply.data ) {
			 handleReply( reply.data );
		     }
		 }
	     },
             error: function( jqXHR, text, errorThrown ) {
                 error( text + "<br>");
                 error( errorThrown );
	     },
	    } );
};

handleReply = function( data ) {
    $('ul#schemeMenu').menu("collapseAll", null, true );
    $('table#schemeTable tbody').html("");
    var relpath = data['relpath'];
    var schemes = data['schemes'];
    for (scheme in schemes) {
	$('table#schemeTable tbody').append( '<tr><td><a href="' + relpath + '/' + scheme + '">' + scheme + '</td><td>' +
					     schemes[scheme]['bunches_1'] + "</td><td>" +
					     (schemes[scheme]['collisions']['non_colliding_1'] + schemes[scheme]['collisions']['non_colliding_2']) + "</td><td>" +
					     schemes[scheme]['description'] +
					     "</td></tr>"); 
    }
    $('table#schemeTable').trigger("update");
    var sorting = [[1,0]];
    setTimeout( function() {
	$('table#schemeTable').trigger("sorton", [sorting]);	
    }, 100);
};

