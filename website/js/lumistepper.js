var lumicalls = 0;
var schemedata = undefined;
var plotData = {};
var gui = {};
var ip1matrix = {};
var ip2matrix = {};
var ip8matrix = {};
var tanorm = 0;
var gres = {};
var tafunc;
var colorip1 = 0;
var colorip2 = 3;
var colorip8 = 4;
var colortot = 5;
var colorfair = 6;
var inputdataconfig =
    [ { 'name' : 'expdata',
        'title' : 'Experiment Data',
        'divid' : 'inputdata',
        'columns' : 3,
        'data' : [ { 'name' : 'ip1',
                     'title' : 'IP1 / IP5',
                     'items' : [ { 'name' : "betastar",
                                   'title' : "beta *",
                                   'default' : 0.5,
                                   'unit' : "m"},
                                 { 'name' : "crossing",
                                   'title' : "Crossing Angle",
                                   'default' : 0.000280,
                                   'unit' : "rad" },
                                 { 'name' : "levelflag",
                                   'title' : "Leveling",
                                   'default' : true,
                                   'unit' : "" },
                                 { 'name' : "levelval",
                                   'title' : "Leveling Value",
                                   'default' : 6.4e27,
                                   'unit' : "cm<sup>-2</sup>s<sup>-1</sup>" } ]
                   },
                   { 'name' : 'ip2',
                     'title' : 'IP2',
                     'items' : [ { 'name' : "betastar",
                                   'title' : "beta *",
                                   'default' : 0.5,
                                   'unit' : "m"},
                                 { 'name' : "crossing",
                                   'title' : "Crossing Angle",
                                   'default' : 0.000136,
                                   'unit' : "rad" },
                                 { 'name' : "levelflag",
                                   'title' : "Leveling",
                                   'default' : true,
                                   'unit' : "" },
                                 { 'name' : "levelval",
                                   'title' : "Leveling Value",
                                   'default' : 6.4e27,
                                   'unit' : "cm<sup>-2</sup>s<sup>-1</sup>" } ]
                   },
                   { 'name' : 'ip8',
                     'title' : 'IP8',
                     'items' : [ { 'name' : "betastar",
                                   'title' : "beta *",
                                   'default' : 1.5,
                                   'unit' : "m"},
                                 { 'name' : "crossing",
                                   'title' : "Crossing Angle",
                                   'default' : -0.000192,
                                   'unit' : "rad" },
                                 { 'name' : "levelflag",
                                   'title' : "Leveling",
                                   'default' : true,
                                   'unit' : "" },
                                 { 'name' : "levelval",
                                   'title' : "Leveling Value",
                                   'default' : 1.5e27,
                                   'unit' : "cm<sup>-2</sup>s<sup>-1</sup>" }
                               ]
                   },                     
                 ],
      },
      { 'name' : "simparameters",
        'title' : "Simulation Parameters",
        'divid' : "simparameters",
        'items' : [ { 'name' : 'hours',
                      'title' : 'Simulation length',
                      'default' : 12,
                      'unit' : "hours"
                    },
                    { 'name' : 'steplength',
                      'title' : 'Simulation step length',
                      'default' : 20,
                      'unit' : "sec" }
                  ]
      },
      { 'name' : "beamdata",
        'title' : "Beam Data",
        'divid' : "beamdata",
        'items' : [ { 'name' : 'ppb',
                      'title' : 'Particles per Bunch',
//                      'default' : 2.16e8 },
                      'default' : 1.65e8 },
                    { 'name' : "epsn",
                      'title' : "Normalised Emittance",
                      'default' : 2.0e-6,
                      'unit' : 'm' },
                    { 'name' : 'emittancegrowth',
                      'title' : 'Emittance growth (constant)',
                      'default' : 0.1e-6,
                      'unit' : "m/hour" },
                    { 'name' : "mass",
                      'title' : "Mass",
                      'default' : 195,
                      'unit' : 'GeV' },
                    { 'name' : "energy",
                      'title' : "Energy",
                      'default' : 557600,
                      'unit' : 'GeV' },
                    { 'name' : "sigz",
                      'title' : "Bunch Length",
//                      'default' : 0.085,
                      'default' : 0.090,
                      'unit' : 'm' },
                    { 'name' : "sigmahad",
                      'title' : "Hadronic Cross Section",
                      'default' : 8,
                      'unit' : 'barn' },
                    { 'name' : "sigmatot",
                      'title' : "Total Cross Section",
//                      'default' : 490,
                      'default' : 515,
                      'unit' : 'barn' },
                    { 'name' : 'frev',
                      'title' : "Revolution frequency",
                      'default' : 11245,
                      'unit' : "Hz" },
                    { 'name' : 'turnaround',
                      'title' : "Turn around time",
                      'default' : 4,
                      'unit' : "hours" }
                  ]
      }
    ];

function downloadIP1Matrix() {
    var a = $("<a style='display:none;'/>");
    var url = window.URL.createObjectURL( new Blob([ JSON.stringify( ip1matrix ) ], {type : 'octet/stream'}) );
    a.attr("href", url);
    a.attr("download", "ip1Matrix.json");
    $("body").append(a);
    a[0].click();
    window.URL.revokeObjectURL( url );
    a.remove();
}

downloadData = function( event ) {

    if ( $.isEmptyObject(plotData) ) {
        error("You first need to plot something, then you can download the data of the plots.");
        return;
    }
    clerror();

    var datastr = "data:text/json;charset-utf-8," + encodeURIComponent( JSON.stringify( plotData ) );

    //console.log(datastr);

    event.currentTarget.href = datastr;
}

makegui = function() {
    gui = new GuiInput( inputdataconfig );
    $('textarea#tafunction').val('var lam = -0.35;\nvar tmin = 2.;\nif ( t < tmin ) {\n  result = 0;\n} else {\n  var tr = t-tmin;\n  result = (tr **0.2 * Math.exp( lam*tr ));\n}');
    gui.render();
    mtadistr();
    return gui;
}

loadScheme = function( data ) {

    schemedata = JSON.parse( data );
    //console.log( 'name', schemedata.schemeName);
    $('div#fillingscheme').html( schemedata.schemeName );
    $('div#fillingschemedescription').html( schemedata.description );
    makeplot( );
}

makeplot = function( ) {
    mtadistr();
    lumicalls = 0;
    if ( schemedata === undefined ) {
        error("Choose a filling scheme first");
        return;
    }
    clerror();
    //console.log( 'gui', gui );
    var config = gui.readData();    
    //console.log('config',config);
    ls = new lumistepper( config.expdata.ip1, config.expdata.ip2, config.expdata.ip8, schemedata, config.beamdata, config.simparameters );
    $('div#lumiplot').html("");
    ls.stepThrough();
    //tafolder();
    //console.log( 'lumicalls',  lumicalls );
}


class lumistepper {

    // exparr is an array with experiment hashes:
    //          levelflag  : bool,
    //          levelval      : leveling level,
    //          betastar   : beta*,
    //          crossing   : full crossing angle in rad
    // fillingscheme : as saved from the editor
    // intensity: initial ppb
    // beamParameters:
    //    ppb : initial particles per bunch
    //    epsn : normalised emittance in m
    //    mass : particle mass in GeV
    //    energy : particle energy in GeV
    //    frev : revolution frequency in Hz
    //    sigz : in m
    //    sigmahad : hardronic cross section (barn) for lumi counting
    //    sigmatot : total cross section (barn) for particle loss
    //  params : 
    //    hours : length of simulation
    //    steplength : number of seconds per step
    constructor(atlas,alice,lhcb, fillingscheme, beamParameters, params) {
        this.params = params;
        this.params.length = params.hours * 3600;
        this.atlas = atlas;
        this.alice = alice;
        this.lhcb = lhcb;
        //this.experiments = exparr;
        this.fillingscheme = fillingscheme;
        this.beamParameters = beamParameters;
        this.setEpsn( beamParameters.epsn ); // to calculate eps
        this.initlumicalc(this.atlas);
        this.initlumicalc(this.alice);
        this.initlumicalc(this.lhcb);
        this.analyzeScheme();

	// helper to easily use them in the console.log
	//console.log( beamParameters);
	//console.log( alice );
	this.bunchIntensity = beamParameters.ppb;
	this.ip2beta = alice.betastar;
    }

    stepThrough( ) {
        var cache = {};
        var res = { 'Lumi IP 1/5' : {'data' : [], 'axis' : 0, 'color' : colorip1, 'width' : 4 },
                    'Integrated Lumi IP 1/5' : {'data' : [], 'axis' : 2, 'color' : colorip1, 'width' : 4 },
                    'IP1/5 Lumi per hour' : { 'data' : [], 'axis' : 3, 'color' : colorip1, 'width' : 1 },
                    'Lumi IP 2' : {'data' : [], 'axis' : 0, 'color' : colorip2, 'width' : 4 },
                    'Integrated Lumi IP 2' : {'data' : [], 'axis' : 2, 'color' : colorip2, 'width' : 4 },
                    'IP2 Lumi per hour' : { 'data' : [], 'axis' : 3, 'color' : colorip2, 'width' : 1 },
                    'Lumi IP 8' : {'data' : [], 'axis' : 0, 'color' : colorip8, 'width' : 4 },
                    'Integrated Lumi IP 8' : {'data' : [], 'axis' : 2, 'color' : colorip8, 'width' : 4 },
                    'IP8 Lumi per hour' : { 'data' : [], 'axis' : 3, 'color' : colorip8, 'width' : 1 },
                    'Total Burned' : { 'data' : [], 'axis' : 2, 'color' : colortot, 'width' : 4 },
                    'Total Lumi per hour' : { 'data' : [], 'axis' : 3, 'color' : colortot, 'width' : 1 },

                  };
        var lumi;
        var ilumi1 = 0;
        var lumi1ptime = 0;
        var ip1tamatrix = [];
        var ip2tamatrix = [];
        var ip8tamatrix = [];
        var ilumi2 = 0;
        var lumi2ptime = 0;
        var ilumi8 = 0;
        var lumi8ptime = 0;
        var totinteg = 0;
        var totlumiptime = 0;
        var epsnstep = this.beamParameters.emittancegrowth / 3600 * this.params.steplength;

        //console.log( "emittance growth ", this.beamParameters.emittancegrowth, epsnstep );
        
        var col_ta_times = [];
        var row_fill_times = [];
        for ( var ta = 1; ta < 16; ta += 5/60.0) {
            col_ta_times.push( ta );
        }
        var irow = 0;
        for ( var t=0; t<this.params.length; t+=this.params.steplength ) {
            var th = t/3600;
            row_fill_times.push( th );
            lumi = this.stepIP( this.atlas, this.collisions_ip15, 2, cache );
            ilumi1 += (lumi * this.params.steplength *1e-30);
            lumi1ptime = ilumi1 / ( th + this.beamParameters.turnaround );
            var row = [];
            for ( var i in col_ta_times ) {
                var lpt = ilumi1 / ( th + col_ta_times[i] );
                row.push( lpt );
            }
            ip1tamatrix[irow] = row;            
            totinteg += (2 * lumi * this.params.steplength *1e-30); // fac 2 for IP1 and 5
            res['Lumi IP 1/5'].data.push( [th, lumi] );
            res['Integrated Lumi IP 1/5'].data.push([th,ilumi1]);
            res['IP1/5 Lumi per hour'].data.push([th,lumi1ptime]);

            lumi = this.stepIP( this.alice, this.collisions_ip2, 1, cache );
            ilumi2 += (lumi * this.params.steplength *1e-30);
            lumi2ptime = ilumi2 / ( th + this.beamParameters.turnaround );
            row = [];
            for ( var i in col_ta_times ) {
                var lpt = ilumi2 / ( th + col_ta_times[i] );
                row.push( lpt );
            }
            ip2tamatrix[irow] = row;            
            totinteg += (lumi * this.params.steplength *1e-30);
            res['Lumi IP 2'].data.push( [th, lumi] );
            res['Integrated Lumi IP 2'].data.push([th,ilumi2]);
            res['IP2 Lumi per hour'].data.push([th,lumi2ptime]);

            lumi = this.stepIP( this.lhcb, this.collisions_ip8, 1, cache );
            ilumi8 += (lumi * this.params.steplength *1e-30);
            lumi8ptime = ilumi8 / ( th + this.beamParameters.turnaround );
            row = [];
            for ( var i in col_ta_times ) {
                var lpt = ilumi8 / ( th + col_ta_times[i] );
                row.push( lpt );
            }
            ip8tamatrix[irow] = row;            
            totinteg += (lumi * this.params.steplength *1e-30);
            totlumiptime = totinteg / (th + this.beamParameters.turnaround);
            res['Lumi IP 8'].data.push( [th, lumi] );
            res['Integrated Lumi IP 8'].data.push([th,ilumi8]);
            res['IP8 Lumi per hour'].data.push([th,lumi8ptime]);

            res['Total Burned'].data.push([th,totinteg]);
            res['Total Lumi per hour'].data.push( [th, totlumiptime] );
            //console.log("next");

            this.setEpsn( this.beamParameters.epsn + epsnstep );
            irow++;
        }
        gres = res;
        this.plotlumi( res );

        plotData = res;
        ip1matrix = { 'col_ta_times' : col_ta_times,
                      'row_fill_times' : row_fill_times,
                      'iptamatrix' : ip1tamatrix };
        ip2matrix = { 'col_ta_times' : col_ta_times,
                      'row_fill_times' : row_fill_times,
                      'iptamatrix' : ip2tamatrix };
        ip8matrix = { 'col_ta_times' : col_ta_times,
                      'row_fill_times' : row_fill_times,
                      'iptamatrix' : ip8tamatrix };
    }

    plotlumi( data ) {
        // Do the actual highcharts plot

        // get the plotlines
        var myplotlines = [];
        var keys = { 'IP8 Lumi per hour' : colorip8,
                     'IP2 Lumi per hour' : colorip2,
                     'IP1/5 Lumi per hour' : colorip1,
                     'Total Lumi per hour' : colortot }
        var maxis = {};
	var maxisix = {};

        for ( var key in keys) {
            var dat = data[key].data;
            var max = -1;
            var pline = { color : Highcharts.getOptions().colors[keys[key]],
                          width : 2,
                          zIndex : 5};
            for (var ix=0; ix<dat.length; ix++) {
                if ( dat[ix][1] < max ) {
                    pline.value = dat[ix][0];
                    pline.label = { text : dat[ix][0].toPrecision(4) + " hours" };
                    maxis[key] = dat[ix][1];
		    maxisix[key] = ix;
                    break;
                } else {
                    max = dat[ix][1];
                }
            }
            myplotlines.push( pline );
        }

        //console.log( 'fairness line' );
        //console.log( data );
        // now calculate the fairness-line: the fill length with equal loss for IP2 and IP1/5
        var diffs = [];
        var mindiff = [-1, 9e99];
        if ( maxis['IP1/5 Lumi per hour'] == undefined ||
             maxis['IP2 Lumi per hour'] == undefined ) {
            error( "Could not find performance maximum for IP1/2/5. Try a longer simulation time" ); 
            return;
        }
        var dat1 = data['IP1/5 Lumi per hour'].data;
        var max1 = maxis['IP1/5 Lumi per hour'];
        var dat2 = data['IP2 Lumi per hour'].data;
        var max2 = maxis['IP2 Lumi per hour'];
        var datt = data['Total Lumi per hour'].data;
        var maxt = maxis['Total Lumi per hour'];
        var loss1line = [];
        var loss2line = [];
        var losstline = [];
        //console.log( 'maxis', max1, max2);
        //console.log( 'dat1', dat1 );
        for ( var ix=0; ix<dat1.length; ix++ ) {
            var ip1l = (max1 - dat1[ix][1])/max1;
            var ip2l = (max2 - dat2[ix][1])/max2;
            var totl = (maxt - datt[ix][1])/maxt;
            loss1line.push( [dat1[ix][0], 100*ip1l] );
            loss2line.push( [dat1[ix][0], 100*ip2l] );
            losstline.push( [dat1[ix][0], 100*totl] );
            var diff = Math.abs( ip1l - ip2l );
            diffs.push( [dat1[ix][0], diff, ip1l, ip2l] );
            //console.log( diff, mindiff[1], ip1l, ip2l);
            if (diff < mindiff[1]) {
                mindiff = [dat1[ix][0], diff, ip2l, ip1l, ix];
            }
        }

        data["IP1/5 ineff from optimal"] = {
            'data' : loss1line,
            'axis' : 1,
            'color' : colorip1,
            'width' : 1
        }

        data["IP2 ineff from optimal"] = {
            'data' : loss2line,
            'axis' : 1,
            'color' : colorip2,
            'width' : 1
        }

        data["Total ineff from optimal"] = {
            'data' : losstline,
            'axis' : 1,
            'color' : colortot,
            'width' : 1
        }

        //console.log('mindiff',  mindiff );

        var fline = {
            color : Highcharts.getOptions().colors[colorfair],
            width : 4,
            label : { text : "f-line: " + mindiff[0].toPrecision(3) + " hours;  " + (100*mindiff[2]).toPrecision(3) + "% inefficient from optimal length"},
            value : mindiff[0],
            zIndex : 5
        }
        
        myplotlines.push( fline );

        // now create a line for the point were we stop levelling ALICE
        var aldat = data['Lumi IP 2'].data;
        var levelval = this.alice.levelval;
        var levelflag = this.alice.levelflag;
        var eol = 0;
        var eolix = 0;
        if ( levelflag ) {
            for ( var ix=0; ix<aldat.length; ix++ ) {
                if ( aldat[ix][1] < levelval ) {
                    eol = aldat[ix][0];
                    eolix = ix;
                    break;
                }
            }
        }

        var eolline = {
            color : Highcharts.getOptions().colors[colorip2],
            width : 2,
            label : { text : "end of ALICE levelling: " + eol.toPrecision(3) + " hours" },
            value : eol,
            zIndex: 5
        };
        //console.log( eolline);
        myplotlines.push( eolline );



        // print out the spreadsheet string
        // schemename,max burned,max1,max2,t fair, ineff tot, ineff1, ineff2, lph8, eol
        // console.log( mindiff[4], data );
        //console.log( mindiff );
        //console.log( losstline, loss1line, loss2line );
        //console.log( "Spreadsheet string", data);
        //console.log( "Scheme name\tLevel IP1/5\tmax total\tmax IP1\tmax IP2\tequal ineff time\tineff tot\tineff ip1\tineff ip2\tip8 perf\tend ip2 level\tperf IP2\t[erf IP15\tperf IP8\ttot perf");
        //console.log( this.fillingscheme.schemeName + "\t" + this.atlas.levelval + "\t" + maxt + "\t" + max1 + "\t" + max2 + "\t" +  mindiff[0] + "\t" + losstline[mindiff[4]][1] + "\t" + loss1line[mindiff[4]][1] + "\t" + loss2line[mindiff[4]][1] + "\t" + data['IP8 Lumi per hour'].data[mindiff[4]][1] + "\t" + eol + "\t" + data['IP2 Lumi per hour'].data[eolix][1] + "\t" + data['IP1/5 Lumi per hour'].data[eolix][1] + "\t" + data['IP8 Lumi per hour'].data[eolix][1] + "\t" + data['Total Lumi per hour'].data[eolix][1]);
	//console.log( "waist");
	var max2ix = maxisix['IP2 Lumi per hour']
	var ip2opttime = data['IP2 Lumi per hour'].data[max2ix][0];
	//console.log( 'ip2opttime' , op2opttime);
        console.log( this.fillingscheme.schemeName + "\t" + this.atlas.levelval + "\t" + this.ip2beta + '\t' + this.bunchIntensity + '\t' + ip2opttime + "\t" + eol + "\t" + data['IP2 Lumi per hour'].data[eolix][1] + "\t" + data['IP1/5 Lumi per hour'].data[eolix][1] + "\t" + data['Total Lumi per hour'].data[eolix][1] + '\t' + data['IP2 Lumi per hour'].data[max2ix][1] + "\t" + data['IP1/5 Lumi per hour'].data[max2ix][1] + "\t" + data['Total Lumi per hour'].data[max2ix][1]);


        
        //console.log( 'plotlines', myplotlines);
        var mychart = $(function() {
	    $('div#lumiplot').highcharts({
                credits: {
                    enabled: false
                },
	        title : {
		    text: "Fill Lumi in Pb Pb"
	        },
	        chart : {
		    zoomType: 'xy',
                    height: 700,
                    events: {
                        load: function() {
                            //console.log( "in load");
                            for ( var k in data) {
                                
                                this.addSeries( {
                                    name : k,
                                    data : data[k].data,
                                    yAxis : data[k].axis,
                                    lineWidth : data[k].width,
                                    color : Highcharts.getOptions().colors[ data[k].color ]
                                });
                            }
                        }
                    }
	        },
	        exporting : {
		    sourceWidth : 1200,
                    chartOptions: {
                        legend : {
                            enabled : true
                        }
                    }
	        },
	        xAxis: {
		    title : { 
		        text: "Time in hours"
		    },
		    labels : {
		        formatter : function() {
			    return this.value.toPrecision(4);
		        }
		    },
		    gridLineWidth: 1,
                    plotLines: myplotlines
	        },
	        yAxis: [ 
		    {
		        title : {
			    text: "Inst. Lumi",
			    style: {
			        color : Highcharts.getOptions().colors[1]
			    }
		        },
		        min : 0,
		        labels : {
			    formatter : function() {
			        return this.value;
			    },
			    style: {
			        color : Highcharts.getOptions().colors[1]
			    }
		        }
		    },
		    {
		        title : {
			    text: "inefficiency [%]",
			    style: {
			        color : Highcharts.getOptions().colors[1]
			    }
		        },
		        min : 0,
                        max : 40,
		        labels : {
			    formatter : function() {
			        return this.value;
			    },
			    style: {
			        color : Highcharts.getOptions().colors[1]
			    }
		        }
		    },
		    {
		        title : {
			    text: "Integ Lumi [ub-1]",
			    style: {
			        color : Highcharts.getOptions().colors[0]
			    }
		        },
		        labels : {
			    formatter : function() {
			        return this.value;
			    },
			    style: {
			        color : Highcharts.getOptions().colors[0]
			    }
		        },
		        opposite: true
		    },
		    {
		        title : {
			    text: "Integ Lumi per hour [pb-1/h]",
			    style: {
			        color : Highcharts.getOptions().colors[3],
                                fontSize: '12px'
			    }
		        },
		        labels : {
			    formatter : function() {
			        return this.value;
			    },
			    style: {
			        color : Highcharts.getOptions().colors[3]
			    }
		        },
		        opposite: true
		    },
	        ],
	        plotOptions : {
		    line : {
		        marker : {
			    enabled : false
		        }
		    },
                    series : {
                        events : {
                            legendItemClick: function(e) {
                                var legname = e.target.name;
                                var visible = e.target.visible;
                                //var chart = Highcharts.charts[0];
                                var chart = $('div#lumiplot').highcharts();
                                //console.log( chart );
                                //var cpl = $.extend( true, {}, plotlines_c);
                                //if ( legname == "levelled lumi per hour" ) {
                                //    if (visible) {
                                //        chart.xAxis[0].removePlotLine('optlengthlevel');
                                //    } else {
                                //        chart.xAxis[0].addPlotLine( cpl[1] );
                                //    }
                                //} else if ( legname == "lumi per hour" ) {
                                //    if (visible) {
                                //        chart.xAxis[0].removePlotLine('optlength');
                                //    } else {
                                //        chart.xAxis[0].addPlotLine( cpl[0] );
                                //    }
                                //}
                            }
                        }
                    }
	        },
	        legend : {
		    enabled : true
	        },
	    });
        });

        //console.log( "highcharts done");        

    };
    
    stepIP( exp, coll, fac, cache ) {
        var lumisum = 0;

        //console.log(coll);
        //console.log('coll bunches', coll.length);
        var dnarr = [];
        for ( var i=0; i<coll.length; i++ ){
            var b = coll[i];
            var lumi = this.calclumi( exp, this.beam1[b[0]],this.beam2[b[1]] );
            //if ( exp.levelflag )
            //console.log( exp.levelval, lumi);
            //console.log( "lumi", lumi );
            var steplumi_c = this.params.steplength * lumi;
            var dn = steplumi_c * this.beamParameters.sigmatot * 1e-24;
            //this.beam1[b[0]] -= (fac * dn);
            //this.beam2[b[1]] -= (fac * dn);
            dnarr.push(dn);
            //console.log('dn', dn );
            lumisum += lumi;
        }

        var scale = 1;
        //console.log( "lumisum", lumisum);
        if ( exp.levelflag && (exp.levelval <= lumisum) ) {
            scale = exp.levelval/lumisum;
            //    console.log( 'scale', scale );
            lumisum = exp.levelval;
        }

        for ( var i=0; i<coll.length; i++ ){
            var b = coll[i];
            this.beam1[b[0]] -= (fac * scale * dnarr[i]);
            this.beam2[b[1]] -= (fac * scale * dnarr[i]);
        }
        
        return lumisum;
    }

    setEpsn( epsn ) {
        this.beamParameters.epsn = epsn;
        this.beamParameters.eps = this.beamParameters.epsn * this.beamParameters.mass / this.beamParameters.energy;
    } 

    calclumi( exp, N1, N2 ) {
        //console.log( 'eps',this.beamParameters.eps, N1, N2);
        var seps = Math.sqrt( this.beamParameters.eps );
        var lumi = exp.A * N1 * N2 / Math.sqrt( this.beamParameters.eps ) / Math.sqrt( exp.B * this.beamParameters.eps + exp.C) / 10000; 
        //var lumi = exp.A * N1 * N2 / Math.sqrt( exp.B + exp.C / (this.beamParameters.eps ));
        // console.log( exp, lumi, this.beamParameters.eps );
        lumicalls += 1;
        return  lumi;
    }

    // prepare the lumi calculations for the various IPs. 
    // assume that only emittance and bunch intensities can vary.
    initlumicalc(exp) {

        var cos = Math.cos(exp.crossing/2) * Math.cos(exp.crossing/2);        
        var sin = Math.sin(exp.crossing/2) * Math.sin(exp.crossing/2);
        ///console.log( 'cos', cos, sin);

        
        exp.A = this.beamParameters.frev * cos / 4.0 / Math.PI / Math.sqrt( exp.betastar );
        exp.B = exp.betastar * cos;
        exp.C = this.beamParameters.sigz ** 2 * sin;  

    }

    analyzeScheme() {
        var b1 = this.fillingscheme.beam1;
        var b2 = this.fillingscheme.beam2;

        this.beam1 = this.initialiseBeam( this.fillingscheme.beam1, this.beamParameters.ppb );
        this.beam2 = this.initialiseBeam( this.fillingscheme.beam2, this.beamParameters.ppb );

        this.collisions_ip15 = this.getCollisions( this.beam1, this.beam2, 0 );
        this.collisions_ip2  = this.getCollisions( this.beam1, this.beam2, 891 );
        this.collisions_ip8  = this.getCollisions( this.beam1, this.beam2, 2670 );

    }
    
    // helper function to make a collision array ( does not use the object this)
    // beams are those created by initialise Beam
    // offset is 0 for IP1/5, 891 for ALICE and 2670 for LHCb
    getCollisions( b1, b2, offset ) {
        var collarr = [];
        for (var i=0; i<b1.length; i++) {
            var j = (i+offset)%3564;
            if ( (b1[i] != 0) && (b2[j] != 0) ) {                
                collarr.push([i,j]);
                // console.log( collarr );         
            }
        }
        return collarr;
        
    };

    // helper function (not using object)
    // creates array with initial bunch intensities
    initialiseBeam( schemeBeam, intensity ) {

        var beam = [];
        for ( var i=0; i<3564; i++ )
            beam.push(0);
        for ( var injix = 0; injix < schemeBeam.length; injix++ ) {
            var inj = schemeBeam[injix];
            var inj_start = inj.lhcbunch;
            var batchArray = inj.batches;
            for (var i=0; i<batchArray.length; i++ ) {
                var ba = batchArray[i].bunchArray;
                var batch_start = batchArray[i].injBunch
                for (var j=0; j<ba.length; j++) {                
                    if ( ba[j] != 0 ) 
                        beam[inj_start + batch_start + j] = intensity;
                }
            }
        }

        return beam;

    }

};




//function tafunc( t ) {
//    var lam = -0.35;
//    var tmin = 2.;
////    var lam = -0.6;
////    var tmin = 2.;
//    if ( t < tmin ) {
//        return 0;
//    } else {
//        var tr = t-tmin;
//        return (tr **0.2 * Math.exp( lam*tr ));
//    }
//}

function mtadistr() {
    var min = 0;
    var max = 30;
    var dat = [];
    var funcstr = $('textarea#tafunction').val();
    
    eval ( "function ta(t) { var result = 0; " + funcstr + " return result; } " );
    tafunc = ta;

    var norm = integ_adapt( tafunc, min, max, 0.000001, 0.00000 );
    //console.log( "turn around norm: ", norm );
    tanorm = norm[0];

    var mean = 0;
    var ix = 0;
    var ts = 0;
    var dt = 1./60.;
    for ( var t=min; t<max; t+=dt ) {
        var val = tafunc(t)/norm[0];
        dat.push( [t, val] );
        mean += t*val*dt;
        ts += t;
        ix++;
    }
    mean = mean ;
    //console.log( "turn around mean: ", mean, "ts", ts);
    $('div#tadmean').html( "mean &nbsp;&nbsp;&nbsp;: " +  mean.toPrecision(3) );

    // now the median
    var median = (max-min)/2;
    var p = [0,0];
    var ix = 0;
    var amax = max;
    var amin = min;
    var help = function(x) { return tafunc(x) / norm[0] };
    while (Math.abs(0.5 - p[0]) > 0.0001) {
        //p = integ_adapt( tafunc, min, median, 0.00001, 0.00001 );
        p = integ_adapt( help, min, median, 0.001, 0.00 );
        //console.log( "m", median, 'p', p[0], p[1], 'ix',ix);
        if (p[0] < 0.5 ){
            amin = median;
            //console.log("go up", amax, median);
            median = (amax+median)/2;
        }else{
            amax = median;
            //console.log("go down", amin, median);
            median = (median+amin)/2;
            
        }
        ix ++;
        if (ix == 100)
            break;
    }
    //console.log( "turn around median: ", median, 'ix', ix, 'p', p[0] );
    $('div#tadmedian').html( "median : " +  median.toPrecision(3) );
    
    simplePlot( 'tadistr', "Assumed Turn-Around-Distribution (normalised)", 'time [h]', '', [{ name : 'turn-around distribution', data : dat, axis : 0, lineWidth : 3, color : 0} ]);
}


function tafolder() {
    var foldedyield1 = new Array(ip1matrix['row_fill_times'].length);
    var foldedyield2 = new Array(ip1matrix['row_fill_times'].length);
    var foldedyield8 = new Array(ip1matrix['row_fill_times'].length);
    //console.log( "folding");
    for ( var itf=0; itf< ip1matrix['row_fill_times'].length; itf++ ) {
        var filltime = ip1matrix.row_fill_times[ itf ];
        //console.log( itf, " / ", ip1matrix['row_fill_times'].length);
        var tyield1 = 0;
        var tyield2 = 0;
        var tyield8 = 0;
        var dtime = ip1matrix.col_ta_times[1] - ip1matrix.col_ta_times[0];
        for (var ita=0; ita<ip1matrix.col_ta_times.length; ita++) {
            var tatime = ip1matrix.col_ta_times[ita];
            var fac = tafunc( tatime )/tanorm * dtime;
            tyield1 += ip1matrix.iptamatrix[itf][ita] * fac;
            tyield2 += ip2matrix.iptamatrix[itf][ita] * fac;
            tyield8 += ip8matrix.iptamatrix[itf][ita] * fac;
        }
        foldedyield1[itf] = [filltime, tyield1];
        foldedyield2[itf] = [filltime, tyield2];
        foldedyield8[itf] = [filltime, tyield8];
    }

    //ip1matrix = {};
    //ip2matrix = {};
    //ip8matrix = {};

    //console.log( "plotting");

    simplePlot( 'optifold', 'Optimised yield folded with Turn Around Distribution', 
                'filltime [h]', 'Integ lumi per hour [pb-1/h]', 
                [ 
                    { data : foldedyield1, name: 'folded yield IP1/5', 
                      lineWidth : 4, 
                      color : Highcharts.getOptions().colors[colorip1],
                      axis : 0},
                    { data : gres['IP1/5 Lumi per hour']['data'], 
                      name: 'IP1/5 yield with fixed turn-around (from plot above)', 
                      lineWidth: 2, 
                      color:Highcharts.getOptions().colors[colorip1],
                      dashStyle: 'ShortDot',
                      axis:0},

                    { data : foldedyield2, name: 'folded yield IP2', 
                      lineWidth : 4, 
                      color : Highcharts.getOptions().colors[colorip2],
                      axis : 0},
                    { data : gres['IP2 Lumi per hour']['data'], 
                      name: 'IP2 yield with fixed turn-around (from plot above)', 
                      lineWidth: 2, 
                      color: Highcharts.getOptions().colors[colorip2],
                      dashStyle: 'ShortDot',
                      axis:0},

                    { data : foldedyield8, name: 'folded yield IP8', 
                      lineWidth : 4, 
                      color : Highcharts.getOptions().colors[colorip8],
                      axis : 0},
                    { data : gres['IP8 Lumi per hour']['data'], 
                      name: 'IP8 yield with fixed turn-around (from plot above)', 
                      lineWidth: 2, 
                      color: Highcharts.getOptions().colors[colorip8],
                      dashStyle: 'ShortDot',
                      axis:0} 
                ] )

}

function simplePlot( id, ptitle,xtit,ytit,sdata ) {
    //console.log( sdata );
    $(function() {
	    $('div#'+id).highcharts({
                credits: {
                    enabled: false
                },
	        title : {
		    text: ptitle
	        },
	        chart : {
		    zoomType: 'xy',
                    height: 500,
	        },
	        exporting : {
		    sourceWidth : 960,
                    chartOptions: {
                        legend : {
                            enabled : false
                        }
                    }
	        },
	        xAxis: {
		    title : { 
		        text: xtit
		    },
		    labels : {
		        formatter : function() {
			    return this.value.toPrecision(4);
		        }
		    },
		    gridLineWidth: 1,
	        },
	        yAxis: [ 
		    {
		        title : {
			    text: ytit,
			    style: {
			        color : Highcharts.getOptions().colors[1]
			    }
		        },
		        min : 0,
		        labels : {
			    formatter : function() {
			        return this.value;
			    },
			    style: {
			        color : Highcharts.getOptions().colors[1]
			    }
		        }
		    },
	        ],
	        plotOptions : {
		    line : {
		        marker : {
			    enabled : false
		        }
		    }
                },
                series : sdata,	        
	        legend : {
		    enabled : true
	        },
	    });
    });

};

evalAllPbPbSchemes = function() {
    root = $('div#schemeTree').tree('getTree').children[0];
    
    for (var i=0; i < root.children.length; i++) {
        var child = root.children[i];
        if (child.name == "2018")
            break;
    }
    var y = child;
    for (var i=0; i < y.children.length; i++) {
        var ions = y.children[i];
        if (ions.name == "Pb Pb")
            break;
    }

    for (var i=0; i < ions.children.length; i++) {
        var scheme = ions.children[i];
        //console.log( scheme.path );
        getScheme(scheme.path )
    }


}
