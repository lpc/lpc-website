var pbeam1 = [];
var pbeam2 = [];
experiment_bunches = "";
currentSchemePath = "";
currentSchemeName = "";
currentRelPath    = "";
currentData       = undefined;
bxOffset          = 0;
var timer         = undefined;
playerSpeed       = 50;
swapB2            = true;

atlasColor = "#008000";
aliceColor = "#b070a0";
lhcbColor  = "#b04000";

b1col = new Array( 3564 ).fill(0);
b2col = new Array( 3564 ).fill(0);
colarr = [b1col, b2col];

toggleSwap = function(){ 
    swapB2 = !swapB2; 
    drawScheme( currentData );
}
bxShift = function() {
    bxOffset += playerSpeed;
    if ( bxOffset > 35640 ) {
        bxOffset -= 35640;
    }
    drawScheme( currentData );
}

player = function( cmd ) {
    if ( cmd == "playpause" ) {
	if ( (currentData == undefined) )
	    return;
        if ( timer ) {
            $('span.ui-icon-pause').map( function() {
                $(this).removeClass('ui-icon-pause' );
                $(this).addClass('ui-icon-play' );
            });
	    clearInterval( timer );
	    timer = undefined;
        } else {
            $('span.ui-icon-play').map( function() {
                $(this).removeClass('ui-icon-play' );
                $(this).addClass('ui-icon-pause' );
            });
	    timer = setInterval( bxShift, 1 );
        }
    } else if ( cmd == "stop" ) {
	if (timer) {
	    clearInterval( timer );
	    timer = undefined;
	}
	bxOffset = 0;
	drawScheme( currentData );	
    } else if ( cmd == "pause" ) {
	if ( timer ) {
	    clearInterval( timer );
	    timer = undefined;
	}
    } else if ( (cmd == "faster") && timer ) {
	playerSpeed += 10;
    } else if ( (cmd == "slower") && timer ) {
	if (playerSpeed > 10)
	    playerSpeed -= 10;
    }
}


debug = function( txt ) {
    $('div#debug').append( txt + '<br>' );
}

error = function( txt ) {
    $('div#error').html( txt );
}

clear_error = function () {
    $('div#error').html("")
}

$(window).resize( function() {
    drawScheme( currentData );
    drawInfo( currentData );
});

drawBunchRuler = function( ctx ) {
    ctx.save()
    recH = 50;
    rec2y = 70;
    ctx.translate( 5, cheight/2 + radiuso + 50 );
    var width = cwidth - 10 ;
    var recolor = "#a0a0a0";
    var excolor = "#007000";
    ctx.beginPath();
    ctx.lineWidth="1";
    ctx.rect( 0, 0, width, recH);
    ctx.rect( 0,rec2y, width, recH);
    ctx.strokeStyle = recolor;
    ctx.stroke();
    f = width/3564
    var x_8 = 891*f;
    ctx.lineWidth="1";
    // The drawline function substitutes the bunch number for B2 with
    // 3564 - bu 
    drawLine( 0, 1, excolor );
    drawLine( 0, 2, excolor );
    drawLine( 445.5, 1, excolor );
    drawLine( 445.5, 2, excolor );
    drawLine( 3120, 1, excolor ); // LHCb is offset by 1.5 bx
    drawLine( 3120, 2, excolor ); // LHCb is offset by 1.5 bx
    drawLine( 3564, 1, excolor );
    drawLine( 3564, 2, excolor );
    drawLine( 891, 1, recolor );
    drawLine( 891, 2, recolor );
    drawLine( 1782, 1, recolor );
    drawLine( 1782, 2, recolor );
    drawLine( 2673, 1, recolor );
    drawLine( 2673, 2, recolor );

    ctx.font = "bold 15px Arial";
    ctx.textAlign = "center";
    ctx.fillStyle = atlasColor;
    ctx.fillText("AT-",60*f,-5);
    ctx.fillText("-LAS",3500*f,-5);
    ctx.fillText("CMS", 1782*f, -5);
    ctx.fillStyle = aliceColor;
    ctx.fillText("ALICE", 3120*f, -5);
    ctx.fillStyle = lhcbColor;
    ctx.fillText("LHCb", 445*f, -5);

    if ( swapB2 ) {
        ctx.fillStyle = atlasColor;
        ctx.fillText("AT-",(60)*f, 2*recH + 35);
        ctx.fillText("-LAS",(3500)*f, 2*recH + 35);
        ctx.fillText("CMS", (1782)*f,  2*recH + 35);
        ctx.fillStyle = aliceColor;
        ctx.fillText("ALICE", (3120)*f,  2*recH + 35);
        ctx.fillStyle = lhcbColor;
        ctx.fillText("LHCb", (445)*f,  2*recH + 35);

    } else {
        ctx.fillStyle = atlasColor;
        ctx.fillText("AT-",(3564-10)*f, 2*recH + 35);
        ctx.fillText("-LAS",(3564-3500)*f, 2*recH + 35); 
        ctx.fillText("CMS", (3564-1782)*f,  2*recH + 35);
        ctx.fillStyle = lhcbColor;
        ctx.fillText("LHCb", (3564-445)*f,  2*recH + 35);
        ctx.fillStyle = aliceColor;
        ctx.fillText("ALICE", (3564-3120)*f,  2*recH + 35);
    }
    ctx.restore();



}

drawLine = function( bu, beam, color, collisionmask ) {
    if ( swapB2 && beam == 2 ) {
	bu = 3564 - bu;
    }
    var x_8 = bu * f;
    var ybase = (beam-1) * rec2y;
    var h = recH / 3;
    ctx.beginPath();

    if ( (collisionmask != null) && (collisionmask & 1) ){
        ctx.strokeStyle = atlasColor;
    } else {
        ctx.strokeStyle = color;
    }
    ctx.moveTo( x_8, ybase );
    ctx.lineTo( x_8, ybase + h );
    ctx.stroke();

    ctx.beginPath();
    ybase += h;
    if ( (collisionmask != null) && (collisionmask & 2) ){
        ctx.strokeStyle = aliceColor;
    } else {
        ctx.strokeStyle = color;
    }
    ctx.moveTo( x_8, ybase );
    ctx.lineTo( x_8, ybase + h );
    ctx.stroke();

    ctx.beginPath();
    ybase += h;
    if ( (collisionmask != null) && (collisionmask & 4) ){
        ctx.strokeStyle = lhcbColor;
    } else {
        ctx.strokeStyle = color;
    }
    ctx.moveTo( x_8, ybase );
    ctx.lineTo( x_8, ybase + h );
    ctx.stroke();


}

drawLHC = function( ctx ) {
    canvas = ctx.canvas;
    if ( ! canvas ) return;

    // calculate the center;
    xmid = cwidth/2;
    ymid = cheight/2;

    radiuso = 0.95 * Math.min( xmid, ymid );
    radiusi = 0.96 * radiuso;

    ctx.save()
    ctx.translate( xmid, ymid );
    lhcpath(ctx);
    ctx.restore();

}

unselectExp = function() {
    experiment_bunches = undefined;
    drawScheme( currentData );
}

selectExp = function( exp ) {
    experiment_bunches = exp;
    drawScheme( currentData );
}

drawBunches = function ( ctx, btab, buckets, beam, ipcollisions ) {
    pix_n = bucketpix( 0 );
    pix_l = bucketpix( 1 );
    var butab = btab.beam1;
    if ( beam == 2 ) {
	butab = btab.beam2;
    }

    
    for ( var i=0; i<buckets.length; i++ ) {
	bucket = buckets[i];
	pix = pix_n;
	if ( experiment_bunches ) {
	    if ( $.inArray( bucket, ipcollisions[experiment_bunches][beam-1]) > -1 ) {
		pix = pix_l;
	    }
	}
	bfin = (bucket - bxOffset);
        if (bfin < 0) {
            bfin += 35640;
        }
	//debug(bfin);
	ctx.putImageData( pix, butab[bfin].x, butab[bfin].y );


    }

    var color = "#ff0000";
    if ( beam == 1 ) {
	color = "#0000ff";
    }

    ctx.save()
    ctx.translate( 5, cheight/2 + radiuso + 50 );

    for ( var i=0; i<buckets.length; i++ ) {
	bucket = buckets[i] / 10;
	//bfin = (bucket + bxOffset/10) % 3564 ;
	bfin = (bucket - bxOffset/10);
        if (bfin < 0) {
            bfin = bfin + 3564;
        }
        collisionmask = colarr[beam - 1][parseInt(bucket)];
        //debug( beam + " " + bucket + " " + collisionmask );
	drawLine( bfin, beam, color, collisionmask );
    }

    ctx.restore();
}

lhcpath = function( ctx ) {

    ctx.lineWidth = radiuso*0.03;

    var red = "#d08080";
    var blue = "#8080d0";
    
    lhcSection( ctx, radiusi, radiuso, Math.PI/-2, 5*Math.PI/-4, 1 );
    lhcSection( ctx, radiuso, radiusi, 5*Math.PI/-4, 3*Math.PI/-2, 1 );
    lhcSection( ctx, radiusi, radiuso, 3*Math.PI/-2, 7*Math.PI/-4, 1 );
    lhcSection( ctx, radiuso, radiusi, 7*Math.PI/-4, Math.PI/-2, 1 );

    lhcSection( ctx, radiuso, radiusi, Math.PI/-2, 5*Math.PI/-4, 2 );
    lhcSection( ctx, radiusi, radiuso, 5*Math.PI/-4, 3*Math.PI/-2, 2 );
    lhcSection( ctx, radiuso, radiusi, 3*Math.PI/-2, 7*Math.PI/-4, 2 );
    lhcSection( ctx, radiusi, radiuso, 7*Math.PI/-4, Math.PI/-2, 2 );

    //ctx.translate( xmid, ymid );
    var btab = bucketTable( ctx, radiusi, radiuso, Math.PI/128);

}

// draw a section with radius 1 from angle 1 to angle 2and a transition at the end
lhcSection = function( ctx, radius1, radius2, angle1, angle2, beamNo ) {
    var deltaAng = Math.PI/128;
    var startsec = angle1 - deltaAng;
    var stopsec = angle2 + deltaAng;

    var grad = ctx.createRadialGradient( 0,0,radius1*0.98, 0,0,radius1*1.02);
    if ( beamNo == 2 ){
	color = "#ffa0a0";
	grad.addColorStop( 0, '#803030' );
	grad.addColorStop( 0.5, '#ffc0c0' );
	grad.addColorStop( 1, '#803030' );
    } else {
	color = "#a0a0ff";
	grad.addColorStop( 0, '#303080' );
	grad.addColorStop( 0.5, '#c0c0ff' );
	grad.addColorStop( 1, '#303080' );
    }

    ctx.beginPath();
    ctx.arc( 0, 0, radius1, startsec, stopsec, true );
    ctx.strokeStyle = grad;
    ctx.stroke();
    
    ctx.beginPath();
    var sx = radius1 * Math.cos( stopsec );
    var sy = radius1 * Math.sin( stopsec );
    ctx.moveTo(sx,sy);
    var rm = (radius1 + radius2 ) / 2;
    var anglee = angle2 - deltaAng;
    var crtx1 = radius1 * Math.cos( angle2 );
    var crty1 = radius1 * Math.sin( angle2 );
    var crtx2 = radius2 * Math.cos( angle2 );
    var crty2 = radius2 * Math.sin( angle2 );
    var ex = radius2 * Math.cos( anglee );
    var ey = radius2 * Math.sin( anglee );
    ctx.bezierCurveTo( crtx1, crty1, crtx2, crty2, ex, ey );
    ctx.strokeStyle = color;
    ctx.stroke();
}

bucketTable = function( ctx, radiusi, radiuso, crossang ) {
    beam1 = [];
    beam2 = [];

    var NOBUCKETS = 35640;
    
    deltaAng = 10 * Math.PI * 2 / NOBUCKETS;

    ang_2 = -3 * Math.PI / 2;
    ang_1 = Math.PI / 2;

    rad1 = radiuso;

    dradang = (radiuso - radiusi)/ (2 * crossang);
    
    for ( var bucket = 1; bucket <= NOBUCKETS; bucket+= 10 ) {

	if ( ang_1 > Math.PI / 2 - crossang ) {
	    var drad = dradang * (Math.PI / 2 - ang_1);
	    rad1 = (radiuso+radiusi)/2 - drad;

	} else if( (ang_1 < Math.PI / 4 + crossang) &&
		   (ang_1 > Math.PI / 4 - crossang ) ) {
	    var drad = dradang * (Math.PI / 4 - ang_1);
	    rad1 = (radiuso+radiusi)/2 + drad;

	} else if( (ang_1 < -Math.PI / 2 + crossang ) &&
		   (ang_1 > -Math.PI / 2 - crossang ) ) {
	    var drad = dradang * (-Math.PI / 2 - ang_1);
	    rad1 = (radiuso+radiusi)/2 - drad;

	} else if( (ang_1 < -5*Math.PI / 4 + crossang ) &&
		   (ang_1 > -5*Math.PI / 4 - crossang ) ) {
	    var drad = dradang * (-5*Math.PI / 4 - ang_1);
	    rad1 = (radiuso+radiusi)/2 + drad;

	} else if (ang_1 < -3*Math.PI / 2 + crossang) {
	    var drad = dradang * (-3*Math.PI / 2 - ang_1);
	    rad1 = (radiuso+radiusi)/2 - drad;

	}
	var x = rad1 * Math.cos( ang_1 ) + xmid;
	var y = rad1 * Math.sin( ang_1 ) + ymid;
	beam1[bucket] = { x:x, y:y };
	ang_1 -= deltaAng;

	x = rad1 * Math.cos( ang_2 ) + xmid;
	y = rad1 * Math.sin( ang_2 ) + ymid;
	beam2[bucket] = { x:x, y:y };
	ang_2 += deltaAng;	
    }
    return { beam1 : beam1, beam2 : beam2 };
}

bucketpix = function( light ) {
    var pix = ctx.createImageData(2,2);
//    var pix = ctx.createImageData(1,1);
    var d=pix.data;
    for ( var i = 0; i < 16; i += 4 ) {
//    for ( var i = 0; i < 1; i += 4 ) {
	if (light == 1){
	    d[i+0] = 255;
	    d[i+1] = 255;
	    d[i+2] = 0;
	} else {
	    d[i+0] = 10;
	    d[i+1] = 80;
 	    d[i+2] = 10;
	}
	d[i+3] = 255;
    }
    return pix;
}


postRequest = function( data, baseurl ) {
    var url = window.location.protocol + '//' + window.location.host;
    var path = window.location.pathname;
    //debug( JSON.stringify(data) );
    //debug( data );
    path = path.substring(0,path.lastIndexOf('/'));
    url += path + '/' + baseurl;
    //debug( JSON.stringify(data));
    //debug( url );
    //console.log( "posting", data, url );
    $.ajax( {type: "GET",
	     url: url,
	     data: data,
             timeout: 10000,
	     dataType: "json",
	     success: function(reply,status) {
		 if ( status != "success" ) {
		     error( "Error communicating with server: " + status);
		     return;
		 } else {
		     //debug( "reply came");
                     //console.log( "reply", reply );
		     if ( reply.debug ) debug( reply.debug );
		     if ( reply.info  ) debug( reply.info );
		     if ( 'error' in reply ) {
			 $('#numberofmatches').html(reply.error);
			 $('#numberofmatches').show("blind");
			 replyPending = 0;
		     } else {
			 clear_error();
		     }
		     if ( reply.data ) {
			 handleReply( reply.data );
		     }
		 }
	     },
	    }
	  );
};

handleReply = function( data ) {
    //console.log("data",  data );
    drawScheme( data );
    drawInfo( data );
    $('ul#uschemeMenu').menu("collapseAll", null, true );

    // make the full detuning phase plots
    // get the beam arrays
    pbeam1 = [];
    pbeam2 = [];
    for ( var ib = 0; ib<3564; ib++ ) {
        pbeam1.push(0);
        pbeam2.push(0);
    }
    for (ix=0; ix<data.ring_1[0].length; ix++) 
        pbeam1[(data.ring_1[0][ix]-1)/10] = 1;
    for (ix=0; ix<data.ring_2[0].length; ix++) 
        pbeam2[(data.ring_2[0][ix]-1)/10] = 1;

    fullDetuning();          

}

fullDetuning = function() {

    //  establish the parameters
    var params = { 'V0' : $('input#cavity_HV').val() * 1000000,
                   'ppb' : $('input#ppb').val(),
                   'f_RF' : 400800000.0,
                   'f_rev' : 11245.5,
                   'RoverQ' : 45,
                   'b_len' : $('input#blen').val() * 1e-9
                 }

    //console.log( "params ", params);
    //console.log( "beam1", beam1);
    //console.log( "beam2", beam2);
    //console.log(currentData)
    makePhasePlots( params, pbeam1, pbeam2, currentData.ipcollisions);  
}

drawInfo = function( data ) {
    if ( data == undefined )
        return;
    $('div#info').show();
    $('p#schemename').html(data.schemeName);
    //debug( data.relPath);
    $('p#schemename').html('<a href="'+ data.relPath + '/' + data.schemeName + '" download>'+data.schemeName+'</a>');
    currentSchemeName = data.schemeName;
    currentSchemePath = data.schemePath;
    currentRelPath = data.relPath;
    currentData = data;
    //debug(currentSchemePath);
    $('table#expinfo td#ATLAS').html(data.collisions.ATLAS);
    $('table#expinfo td#CMS').html(data.collisions.CMS);
    $('table#expinfo td#ALICE').html(data.collisions.ALICE);
    $('table#expinfo td#LHCb').html(data.collisions.LHCb);

    $('table#beaminfo td#bunches-1').html(data.bunches.bunches_1);
    $('table#beaminfo td#bunches-2').html(data.bunches.bunches_2);
    $('table#beaminfo td#nc-1').html(data.collisions.non_colliding_1);
    $('table#beaminfo td#nc-2').html(data.collisions.non_colliding_2);
    $('table#beaminfo td#injections-1').html(data.injections_1);
    $('table#beaminfo td#injections-2').html(data.injections_2);

     
    //debug("r "+radiusi);
    maxwidth = 2*radiusi*0.6;
    $('p#comment').css( 'max-width', maxwidth );
    $('p#comment').html( data.description );
    
    
    //$(window).trigger( 'resize' );
    
    var iw = $('div#info').outerWidth();
    var ih = $('div#info').outerHeight();
    var cp = $('canvas#canvas1').offset();
    var cw = $(window).width();
    var ch = $('canvas#canvas1').height();

    //debug( iw +" ih "+ ih + " cw " + cw + " ch " + ch + " cpy " + cp.top);
    
    var left = cw/2 - iw/2;
//    var top = cp.top + ch/2 - ih/2;
    var top = cp.top + ymid - ih/2;

    $('div#info').offset( { top  : top,
			    left : left });
    
    //debug ( currentScheme );

}


// This is called on selection of a scheme
drawScheme = function( inj ) {
    var can = document.getElementById( "canvas1" );
    var head = document.getElementById( "table_header" );
    var conttop = $('div#contents').offset().top;
    ctx = can.getContext("2d");
    if ( ! can ) return;

    can.width  = window.innerWidth;
//    can.height = window.innerHeight;
    can.height = window.innerHeight + 200;
    diff = Math.max( window.innerHeight - window.innerWidth, 0 );
    can.height = window.innerHeight + 200 - diff;
    cwidth = can.width;
    cheight = window.innerHeight - diff;

    ctx.clearRect( 0,0,can.width, can.height );

    drawLHC( ctx );
    drawBunchRuler( ctx );

    if ( inj == undefined ) {
	return;
    }
    
    var btab = bucketTable( ctx, radiusi, radiuso, Math.PI/128);
    
    // make a table for which bucket is colliding in which experiment
    atlas = inj['ipcollisions']['ATLASCMS'];
    alice = inj['ipcollisions']['ALICE'];
    lhcb  = inj['ipcollisions']['LHCb'];
    nonco = inj['ipcollisions']['NC'];
    
    b1col.fill(0);
    b2col.fill(0);

    for (var bucket in atlas[0]) { b1col[(atlas[0][bucket]-1)/10] += 1 };
    for (var bucket in atlas[1]) { b2col[(atlas[1][bucket]-1)/10] += 1 };
    for (var bucket in alice[0]) { b1col[(alice[0][bucket]-1)/10] += 2 };
    for (var bucket in alice[1]) { b2col[(alice[1][bucket]-1)/10] += 2 };
    for (var bucket in lhcb[0])  { b1col[(lhcb[0][bucket]-1)/10] += 4 };
    for (var bucket in lhcb[1])  { b2col[(lhcb[1][bucket]-1)/10] += 4 };
    //for (var bucket in nonco[0]) { debug(nonco[bucket]); b1col[(nonco[bucket]-1)/10] += 8 };
    //for (var bucket in nonco[1]) { b2col[(nonco[bucket]-1)/10] += 8 };

    //for (var i in b1col ){
    //    debug(i + ":" +  b1col[i] );
    //}

    for ( var i = 0; i< inj['ring_1'].length; i++ ) {
	drawBunches( ctx, btab, inj['ring_1'][i], 1, inj['ipcollisions'] );
    }
    for ( var i = 0; i< inj['ring_2'].length; i++ ) {
	drawBunches( ctx, btab, inj['ring_2'][i], 2, inj['ipcollisions'] );
    }

    
}
