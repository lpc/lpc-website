var SCHEDULES = {};
var startregion = 0;
var endregion = 0;
var category = "";

var pickeroffset = 0;

var holidays;

Date.prototype.isLeapYear = function() {
    var year = this.getFullYear();
    if((year & 3) != 0) return false;
    return ((year % 100) != 0 || (year % 400) == 0);
};

// Get Day of Year
Date.prototype.getDOY = function() {
    var dayCount = [0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334];
    var mn = this.getMonth();
    var dn = this.getDate();
    var dayOfYear = dayCount[mn] + dn;
    if(mn > 1 && this.isLeapYear()) dayOfYear++;
    return dayOfYear;
};

cernholidays = function( year ) {

    // New year
    var CernHolidays = { 0 : "NY Eve" };
    CernHolidays[ 1 ] = "New Year";

    // for special days like compensation days for holidays falling on a Sunday.
    var ExtraTab = { 2016 : [[5, 6, "1st May comp"]] };    

    if (year in ExtraTab) {
        for (i = 0; i < ExtraTab[year].length; i ++ ) {
            var nd = new Date( year, ExtraTab[year][i][0]-1, ExtraTab[year][i][1] );
            CernHolidays[ nd.getDOY() ] = ExtraTab[year][i][2];
        }
    }

    var EasterTab = { 2015 : [4, 5], 2016 : [3, 27], 2017 : [4, 16],
                      2018 : [4, 1], 2019 : [4, 21], 2020 : [4, 12], 
                      2021 : [4, 4], 2022 : [4, 17], 2023 : [4, 9],
                      2024 : [3, 31], 2025 : [4, 20], 2026 : [4, 5], 
                      2027 : [3, 28], 2028 : [4, 16], 2029 : [4, 1],
                      2030 : [4, 21], 2031 : [4, 13], 2032 : [3, 28],
                      2033 : [4, 17], 2034 : [4, 9], 2035 : [3, 25] };

    var easterSun = new Date( year, EasterTab[year][0]-1, EasterTab[year][1] );

    // Friday and Monday
    var easterMon = new Date(easterSun);
    var easterFri = new Date(easterSun);
    easterFri.setDate( easterFri.getDate() - 2 );
    easterMon.setDate( easterMon.getDate() + 1 );
    CernHolidays[ easterFri.getDOY() ] = "Good Friday";
    CernHolidays[ easterSun.getDOY() ] = "Easter";
    CernHolidays[ easterMon.getDOY() ] = "Easter";

    CernHolidays[ (new Date( year, 4, 1 )).getDOY() ] = "Labour";

    var ascension = new Date( easterSun );
    ascension.setDate( ascension.getDate() + 39 );
    CernHolidays[ ascension.getDOY() ] = "Ascension";

    var whit = new Date( easterSun );
    whit.setDate( whit.getDate() + 50 );
    CernHolidays[ whit.getDOY() ] = "Whit Mon";

    // Jeune Genevois: thrursday following the first Sunday in September
    var fs = new Date( year, 8, 1);
    var fsday = fs.getDay(); // 0 is Sunday
    if (fsday == 0)
	var jg = new Date( year, 8, 5 );
    else
	var jg = new Date( year, 8, (12-fsday) );
    CernHolidays[ jg.getDOY() ] = "J.Genevois";

    xmas = new Date( year, 11, 24 );
    CernHolidays[ xmas.getDOY() ] = "Xmas";
    xmas = new Date( year, 11, 25 );
    CernHolidays[ xmas.getDOY() ] = "Xmas";

    nye = new Date( year, 11, 31 );
    CernHolidays[ nye.getDOY() ] = "NY Eve";
    CernHolidays[ nye.getDOY() + 1 ] = "New Year";
    
    return CernHolidays;
}


initialize = function() {
    pickeroffset = $('div#pickerspace').offset().top;
    $('div#picker').css( "top", pickeroffset );
    $('div#pickerspace').height( $('div#picker').height() );    
}

$(window).scroll( function() {
    var offset = $('div#picker').offset();
    var scrolltop = $(window).scrollTop();

    var top = Math.max(0, pickeroffset - scrolltop);
    $('div#picker').css( { "top"      : top } );
    $('div#picker').css( { "top"      : top } );
    
});

selectCat = function( event ) {
    category = $(this).attr("id");
}

startselect = function( event ){
    id = $(this).attr("id");
    startregion = id.substr(3);
    endregion = 0;
    return false;
};

endselect = function( event ){
    id = $(this).attr("id");
    endregion = id.substr(3);
    //debugn( startregion + ' - ' + endregion );
    for ( var i = parseInt(startregion); i<=parseInt(endregion); i++ ) {
	id = "day" + i;
	$('td#'+id).removeClass('physics special hi ionsetup ireco reco scrubbing ts md');
	//$('td#'+id).removeClass();
	$('td#'+id).addClass( category );
    }
    $('div#configstring').html("");
    for ( var i=1; i<365; i++ ) {
	id = "day" + i;
        var addcl = $('td#'+id).attr("class");
        if ( addcl ) {
            addcl = addcl.replace( "holiday", "" ).trim();
	    $('div#configstring').append( addcl + ' ' );
        }
    }
    countcats();
    return false;
};

countcats = function() {
    catcounter = {};
    $('table.sched td').each( function() {
	var cl = $( this ).attr( 'class' );
        cl = cl.replace( / ?holiday ?/, "" );
	if ( cl in catcounter ) {
	        catcounter[cl] += 1;
	    } else {
	        catcounter[cl] = 1;
	    }
            if ( cl == "physics" ) {
                //debug( $( this ).attr( 'id' ) );
            }
    });

    $('table.picker tr#sums td').each( function() {	
	$(this).html( 0 );
    });
    for (cat in catcounter) {
	$('table.picker td#no'+cat).html( catcounter[cat] );
    }
}

loadconfig = function( key ) { 
    configstr = SCHEDULES[key].config;
    if ( $('input#calname') ) {
        $('input#calyear').val( SCHEDULES[key].year);
        $('input#callabel').val( key );
        $('input#calname').val( SCHEDULES[key].name);
    }
    if ( configstr == undefined ) {
	configstr = $('textarea#loadstring').val();
    } else {
	$('div#configstring').html( configstr );
    }
    var physday = 0;
    var classarr = configstr.split(" ");
    for (var ix=1; ix<=classarr.length; ix++) {
	$('td#day'+ix).removeClass('physics special hi ionsetup reco scrubbing ts md');	
	if (classarr[ix-1] != "undefined") {
	    $('td#day'+ix).addClass( classarr[ix-1] );
            if(classarr[ix-1] == "physics") {
                physday ++;
                $('td#day'+ix).attr('title', "pp physics day " + physday );
            }
	}
    }
    countcats();
    
}

debug = function( txt ) {
    $('div#debug').append( txt + "<br>" );
}
debugn = function( txt ) {
    $('div#debug').html( txt );
}
debugnn = function( txt ) {
    $('div#debug').append( txt );
}
var MONTHS = [ "January",
	       "February",
	       "March",
	       "April",
	       "May",
	       "June",
	       "July",
	       "August",
	       "September",
	       "October",
	       "November",
	       "December"
	     ];
var MONTHS_s = [ "Jan",
	         "Feb",
	         "Mar",
	         "Apr",
	         "May",
	         "June",
	         "July",
	         "Aug",
	         "Sep",
	         "Oct",
	         "Nov",
	         "Dec",
                 "Jan >>"
	     ];
var DAYS_s = [ "Mo", "Tu", "We", "Th", "Fr", "Sa", "Su" ];

initialiseCalendars = function() {
    getSchedules();
}

sortCalendars = function(schedules) {
    var keys = Object.keys( schedules );
    keys.sort( function( a, b ) {
        var at = a.split('-').map( Number ) ;
        var bt = b.split('-').map( Number );
        for ( var i=0; i<at.length; i++ ) {
            if ( bt[i] - at[i] != 0 ) {
                return (bt[i]-at[i]);
            }
            if ( i == (at.length - 1) )
                return 0;
        }
    });
    return keys;
}

getSchedules = function() {
    var url = "/cgi-bin/handleCalendar.py";
    data = { 'action' : 'getCalendars' };
    //console.log( url, data );
    $.post( url, data, function( data ) {
        if (data.error) {
            error( data.error );
            return;
        }
        if ( data.debug ) {
            debug( data.debug );
        }
        clerror();
        SCHEDULES = data.data;
        var today = new Date();
        var year = today.getYear() + 1900;
        // draw an empty calendar
        calendar( "calendar", year );
        var calordered = sortCalendars( SCHEDULES );
        populateDropDown( calordered, SCHEDULES );
        cur = getCurrentSchedule( calordered, year );
        $('select#lhcschedules option[value='+cur+']').prop( "selected", true );
        loadconfig( cur );
        initialize();
    });
}

getCurrentSchedule = function( keys, cyear ){
    var lyear = 0;
    var lkey = "";
    for ( var ix = 0; ix < keys.length; ix++ ) {
        var k = keys[ix];
        var year = Number( k.substring(0,4) )
        if (year > cyear) {
            lyear = cyear;
            lkey = k;
        } else if( year == cyear ) {
            // The latest of the current year            
           return k;
        }
        if ( lyear == 0 ) {
            return keys[0];
        }
    }
    return lkey;    
}

populateDropDown = function( cals, schedules ) {
    $('select#lhcschedules').html("");
    cals.forEach( function(a) {
        $('select#lhcschedules').append( '<option value="' + a + '">' + schedules[a].name + '</option>' )
    });
  
}

scheduleSelected = function() {
    ps = $('select#lhcschedules option:selected').val();
    calendar( "calendar", SCHEDULES[ps].year );
    loadconfig( ps );
}

calendar = function( el, year ) {
    holidays = cernholidays( year );
    var first = new Date( year, 0, 1, 0, 0, 0, 0);
    var day = first.getDay();
    //debugnn( day + "<br>" );
    start = new Date( (year-1), 11, (28+day) );( day );
    // Calculcate the date of the first day (Monday) of week 1.
    // Week 1 is defined as containig the first Thursday of the year. 
    var offset = 0;
    // calculate the start of our calendar (the Monday of week 1 of the year)
    // Note that this might be in Decmeber of the previous year.
    if ( day == 0 ) {
	start = new Date( year, 0, 2 ); // the first Jan is a Sunday
	offset = 1;
	//debug(1);
    } else if ( day == 1 ) {            // the first Jan is a Monday
	start = first;
	//debug(2);
    } else if ( day > 1 && day <= 4 ) { // the first Jan is Tu or We or Th
	start = new Date( year, 0, (2 - day) );
	offset = -1*day+1;
	//debug(3 + " " + offset);
    } else {                            // the first Jan is Fri or Sa 
	start = new Date( year, 0, (9-day) );
	offset = (8-day);
	//debug(4);
    }

    // 4 tables with 13 weeks
    var weekdate = new Date(start);
    var html = "";
    var iweek = 1;
    var imon = 1;
    for ( var itab = 0; itab < 4; itab ++ ) {
	html += '<table class="sched" id="tab' + itab + '" >\n';

        html += '<tr><th class="noborder"></th>';
        if ( imon == 1 ) {
            html += '<th class="month">'+MONTHS_s[0]+"</th>";
            var iw = 1;
        } else {
            var iw = 0;
        }
	for ( ; iw<13; iw++ ) {
            // calculate the last day (Su) of column
            var deltad = itab*13*7+iw*7+6;
//            var dat = new Date();
            var dat = new Date(start);
            dat.setDate( start.getDate() + deltad );
            var dday = dat.getDate();
            //debugnn(dday+"-"+deltad+"-"+dat+"  <br>");
            if ( dday <= 7 ){
                html += '<th class="month">' + MONTHS_s[imon] + "</th>";
                imon += 1;
            } else {
                html += '<th class="noborder"></th>';
            }
	}
	html += "</tr>\n";

	html += '<tr><th>Wk</th>';
	for ( var iw=0; iw<13; iw++ ) {
	    html += '<th>'+iweek+'</th>';
	    iweek += 1;
	}
	html += "</tr>\n";

	for ( var id=0; id<7; id ++ ) {
	    html += '<tr><th>'+DAYS_s[id]+'</th>';
	    for ( var iw=0; iw<13; iw++ ) {
		date = "";
		if ( id == 0 ) {
		    date = weekdate.getDate();
		    weekdate.setDate(weekdate.getDate()+7);
		}
		var iday = itab * 13 * 7 + iw * 7 + id + 1;
		var dclass = "";
		var text = "";
		var holiix = iday + offset;

		if (holiix in holidays ) {
		    //debug("holiday " + holiix);
		    dclass = 'class="holiday" '; 
		    text = holidays[holiix];
		}
		html += '<td id="day' + iday + '" ' + dclass +'>'+date+' ' + text+'</td>';
		iday += 1;
	    }
	    html += "</tr>\n";
	}

	html += "</table>\n";
    }

    $('div#'+el).html( html );
    $('table.sched').on( { 'mousedown' : startselect,
			   'mouseup' : endselect }, "td" );
    $('div.cat').on( 'click', selectCat );
    return html;
}

saveCalendar = function() {
    clerror();
    var configstr = $('div#configstring').html();
    var name = $('input#calname').val();
    var year = $('input#calyear').val();
    var label = $('input#callabel').val();
    if (name == "" || year == "" || label == "" ) {
        error("You must fill in the entries above before saving.");
        return;
    }
    var calendar = { 'name' : name,
                     'year' : parseInt(year),
                     'config' : configstr };
    var url = "adminCalendar.py"; 
    var data = { 'action' : 'saveCalendar',
                 'key' : label,
                 'calendar' : JSON.stringify(calendar) };

    $.post( url, data, function( data ) {
        if (data.error) {
            error( data.error );
            return;
        }
        clerror();
        SCHEDULES = data.data;
        var calordered = sortCalendars( SCHEDULES );
        populateDropDown( calordered, SCHEDULES );
    });
};

deleteCalendar = function() {
    clerror();
    var label = $('input#callabel').val();
    if ( label == "" ) {
        error("You must fill in the entries above before saving.");
        return;
    }
    var url = "adminCalendar.py"; 
    var data = { 'action' : 'deleteCalendar',
                 'key' : label };


    $.post( url, data, function( data ) {
        
        if (data.error) {
            error( data.error );
            return;
        }
        clerror();
        
        SCHEDULES = data.data;
        var calordered = sortCalendars( SCHEDULES );
        populateDropDown( calordered, SCHEDULES );
    });
};
