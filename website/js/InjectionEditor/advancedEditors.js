"use strict";

//var be = undefined;
//var ie = undefined;

class Patterneditor {
    constructor( id, infoid, controlid, harmonic, type ) {
	this.id = id;
	this.infoid = infoid;
	this.controlid = controlid;
	this.harmonic = harmonic;
	this.type = type; // used also in the text of the controls

	// graphics defaults
	this.bucketHeight = 40;
	this.canvasMargin = 10;
	this.canvasHeight = 50;
	
	this.canvas = document.getElementById(id);
	this.context = this.canvas.getContext('2d');
	this.canvas.height = this.canvasHeight;
	
        // basic mouse positions for graphical operations
        // these are filled in the base class
	this.mouseDownBucket = undefined;      // the start
	this.mouseUpBucket   = undefined;      // the start
	this.currentBucket   = undefined;      // where the mouse is in this call
	this.lastCurrentBucket   = undefined;  // where the mouse was in the last call
	
	this.patternHash = {};
	this.filledBuckets = [];
	for ( var i=0; i < harmonic; i++ )
	    this.filledBuckets.push(0);
	
	$('canvas#'+id).css( "position", "relative" );
	$('canvas#'+id).css( "left", this.canvasMargin );
	
	var self = this;
	
	$(window).resize( function() {
	    self.resize();
	});
	
	$('canvas#'+id).mousedown( function( event ) { return self.doMouseDown( event ); } );
	$('canvas#'+id).mouseup(   function( event ) { return self.doMouseUp( event );   } );
	$('canvas#'+id).mousemove( function( event ) { return self.doMouseMove( event ); } );
//	$('canvas#'+id).click( function( event ) { return self.doMouseClick( event ); } );
	

	// build up the control div: some standard controls go here:

	$('div#'+infoid).append( '<div id="curpos"></div>' );
	$('div#'+infoid).append( '<div id="dbucket"></div>' );
	$('div#'+controlid).append( '<label>'+this.type+' length: </label><input id="plength" type="number" value="'+this.harmonic+'"/><button id="changeplength" >change</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;');
	$('div#'+controlid + ' button#changeplength').click( this, this.changePLength );
	
	$('div#'+controlid).append( '<label>'+this.type+' name: </label><input id="pname" type="text" value=""/>' );
	$('div#'+controlid).append( '<button id="savePattern" >save</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' );
	$('div#'+controlid + ' button#savePattern').click( this, this.savePattern );
	
	$('div#'+controlid).append( '<label>Choose '+this.type+' to load: </label><select id="patternchooser"></select>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' );
	$('div#'+controlid + ' select#patternchooser').change( this, this.selectBucketPattern );
        $('div#'+controlid).append('<button id="removepattern">Remove</button>')
	$('div#'+controlid + ' button#removepattern').click(this, this.removeData );

	$('div#'+controlid).append( '<br><label>'+this.type+' string: </label><input id="patternstring" style="width:700px"  /><button id="loadpattern">load</button>' );
	$('div#'+controlid + ' button#loadpattern').click( this, this.loadPatternString );
	
    }


    ///////////////////// event handlers ////////////////////////////

    // when the pattern selector has been changed to load a new pattern
    selectBucketPattern(e) {
	var self = e.data;
	var key = $('div#'+self.controlid+' select#patternchooser option:selected').val();
	if ( ! key )
	    return;
	var patt = self.patternHash[key];
	self.filledBuckets = patt;
	self.harmonic = self.filledBuckets.length;
	$('div#'+self.controlid+' input#plength').val( self.harmonic );
	$('div#'+self.controlid+' input#pname').val( key );
	self.resize();
	self.updateFillPatternString();
    }

    // called when the user clicks on the button to change the pattern length
    changePLength( e ) {
	self = e.data;
	var plength = parseInt( $('div#'+ self.controlid + ' input#plength').val() );
	var clen = self.filledBuckets.length;

	if ( plength > clen ) {
            for ( var i=clen; i<plength; i++ ) {
		self.filledBuckets.push( 0 );
            }
	} else if ( plength < clen ) {
            self.filledBuckets = self.filledBuckets.slice( 0, plength );
	}

	self.harmonic = plength;
	self.resize();
	self.updateFillPatternString();
    }

    // remove all the leading and trailing empty buckets. 
    trimPattern() {
        while (this.filledBuckets[ this.filledBuckets.length-1 ] == 0)
            this.filledBuckets.pop()
        var first = 0;
        //console.log( "trim", this.filledBuckets );
        for( ; first < this.filledBuckets.length; first++ ) {
            //console.log( first );
            if ( this.filledBuckets[first] != 0 ) 
                break;
        }
        //console.log( "slice with ", first );
        this.filledBuckets = this.filledBuckets.slice( first );
        //console.log( "trim", this.filledBuckets );
    }
    

    // called when the user wants to load a pattern from the string input field by clicking the button
    loadPatternString( e ) {
	self = e.data;

	var pattern = $('div#'+ self.controlid + ' input#patternstring').val();
	self.filledBuckets = [];
	pattern.replace( ",", "" );
	for ( var i=0; i<pattern.length; i++ ) {
	    if ( pattern[i] == 1 ) 
		self.filledBuckets.push( 1 );
	    else
		self.filledBuckets.push( 0 );
	}
	self.harmonic = self.filledBuckets.length;
	self.resize();
    }

    doMouseMove( e ) {
        if ( this.currentBucket != undefined ) {
	    var x = e.clientX;
            this.lastCurrentBucket = this.currentBucket;
	    this.currentBucket = Math.ceil((x - this.canvasMargin) / this.bucketWidth) -1;
	    $('div#'+this.infoid+ ' div#curpos').html( "Bucket No: " +  this.currentBucket );
	}
        return;
    }

    doMouseDown( e ) {
	//console.log( e.clientX, this.canvasMargin, this.bucketWidth );
	this.mouseDownBucket = Math.ceil((e.clientX - this.canvasMargin) / this.bucketWidth) -1;
        this.currentBucket = this.mouseDownBucket;
	this.mouseDownx = e.clientX;
	this.mouseDowny = e.clientY;
	//console.log( "mouse down bucket ", this.mouseDownBucket);
    }

    doMouseUp( e ) {
        this.mouseUpBucket = Math.ceil((e.clientX - this.canvasMargin) / this.bucketWidth) -1;
	//this.mouseDownBucket = undefined;
	this.currentBucket = undefined;
    }

    removeData( e ) {
        var self = e.data;
	var userName = $('input#userName').val();
	var pname = $('div#'+self.controlid+' input#pname').val();
	if (userName == "" ) {
	    error( "You must enter a valid user name");
	    return;
	} else if( pname == "" ) {
            error( "You must enter a name for the data to save");
	    return;
	} else {
	    error("");
	}
	
	var data = { 'pname' : pname,
		     'user' : userName,
                     'type': self.type,
		     'action' : "removeBatch" };

	$.post( "cgi-bin/patternEditor.py", data, function( data ) {
	    $('div#status').html( data.status );
            if ( data.error ) {
		error( data.error );
		return;
            }
            self.patternHash = data.data;
            self.fillPatternSelector( data.data );
	    //console.log( data.data );
	});
    }

    // when the user clicks to save the pattern in his account
    // the pattern is transmitted and then saved together with all other patterns of the user in a json file.
    // the name of the json file is derived from the "type" of the pattern editor.    
    saveData( cb ) {
	var userName = $('input#userName').val();
	var pname = $('div#'+this.controlid+' input#pname').val();
	if (userName == "" ) {
	    error( "You must enter a valid user name");
	    return;
	} else if( pname == "" ) {
            error( "You must enter a name for the data to save");
	    return;
	} else {
	    error("");
	}
	
	var data = { 'pattern' : JSON.stringify(this.dataToSave),
		     'pname' : pname,
		     'user' : userName,
                     'type': this.type,
		     'action' : "saveBatch" };

        var self = this;

	$.post( "cgi-bin/patternEditor.py", data, function( data ) {
	    //console.log( data );
	    $('div#status').html( data.status );
            if ( data.error ) {
		error( data.error );
		return;
            }
            self.patternHash = data.data;
            self.fillPatternSelector( data.data );
	    //console.log( data.data );
            if (cb )
                cb();
	});
    }


/////////////////////////////////////////////////////////////////////////////////
    
    updateFillPatternString() {
	var res = "";
	for( var i=0; i<this.filledBuckets.length; i++) {
	    if ( this.filledBuckets[i] == 1 )
		res += '1';
	    else
		res += '0';	
	}
	$('div#' + this.controlid + ' input#patternstring').val(res);
    }


    fillPatternSelector( patterns, selid ) {
        if (selid == undefined)
            selid = "patternchooser"
	var sel = $('div#'+this.controlid+' select#' + selid );
	sel.html("");
	sel.append( '<option value=""></option>' );
	this.patterns = patterns;
	for (var key in patterns) {
            sel.append( '<option value="' + key + '">' + key + '</option>' );
	}
    }

    // All batches of a user are saved in one file. 
    // This will be loaded all at once.
    load( type, selid, dataname, cb ) {
	if ( type === undefined )
	    type = this.type;
        if ( dataname === undefined )
            dataname = "patternHash";
	var userName = $('input#userName').val();
	if (userName == "" ) {
	    error( "You must enter a valid user name");
	    return;
	} else {
            error('');
	}
	var self = this;
	var data = { 'action' : "loadBatches",
                     'type' : type,
		     'user' : userName }; 
	$.post( "cgi-bin/patternEditor.py", data, function( data ) {
	    self[dataname] = data.data;
	    $('div#status').html( data.status );
            if ( data.error ) {
		error( data.error );
		return;
            }
            self.fillPatternSelector( data.data, selid );
	    //console.log( "load", data.data );
            if ( cb ) 
                cb();
	});
    }


    resize() {	
	//    var winWidth = window.innerWidth;
	var winWidth = $('body').innerWidth();
	this.canvasWidth = winWidth - 2 * this.canvasMargin ; // 10px left and right
        // the -4 is due to the translate(2,2) of the drawing region into the middle of the canvas so that
        // the borders of rectangles are completely drawn
	//console.log( "canvas width ", this.canvasWidth, " harmonic ", this.harmonic );
	this.bucketWidth = Math.floor( (this.canvasWidth-4) / (this.harmonic) );
	//console.log( "bucket width : ", this.bucketWidth);
	this.canvas.width = this.canvasWidth;
	this.draw();
    }

    draw() {
	
	var c = this.context;
        c.save();
        c.translate( 2,2 );
	c.clearRect(0,0,this.canvasWidth, this.canvasHeight);

	c.lineWidth = 1;
	c.strokeStyle = 'black';
	c.fillStyle = "rgba(0,127,0,0.5)";
	c.beginPath();
	for ( var i=0; i<this.harmonic; i++ ) {
	    var x = i * this.bucketWidth;
	    c.rect( i * this.bucketWidth, 0, this.bucketWidth, this.bucketHeight);
	    //console.log( "draw ", (i*this.bucketWidth ));
	    if ( this.filledBuckets[i] != 0 ) {
		c.fillRect( i * this.bucketWidth, 0, this.bucketWidth, this.bucketHeight);
	    }
	}
	c.stroke();
	c.restore();
    }
}

/////////////////////////////////////////////////////////////////////////////////
class Batcheditor extends Patterneditor {
    constructor(  id, infoid, controlid ) {
	super( id, infoid, controlid, 84, "batch" );
    }  

    savePattern( e ) {
        var self = e.data;
        self.trimPattern();
        self.dataToSave = self.filledBuckets;
        self.saveData();
    }

    doMouseDown( e ) {
        super.doMouseDown( e );
        this.bucketclone = this.filledBuckets.slice(0);
        this.filledBuckets[this.currentBucket] ^= 1;
	this.updateFillPatternString();
	this.draw();
    }

    doMouseMove(e) {
        super.doMouseMove( e );

        // This means that somebody pressed the button. (is undefined after mouseup)
	if (this.currentBucket != undefined) {
            // if nothing changed, nothing to do
	    if ( this.currentBucket == this.lastCurrentBucket ) {
		return
            }

            this.filledBuckets = this.bucketclone.slice(0);

            if ( this.currentBucket < this.mouseDownBucket )
                for( var ix = this.currentBucket; ix <= this.mouseDownBucket; ix += 1 )
                    this.filledBuckets[ix] ^= 1;
            else
                for( var ix = this.currentBucket; ix >= this.mouseDownBucket; ix -= 1 )
                    this.filledBuckets[ix] ^= 1;

	    this.draw();
	    this.updateFillPatternString();
            var delta = Math.abs(this.currentBucket - this.mouseDownBucket) + 1;
	    $('div#'+this.infoid+ ' div#dbucket').html( "current bucket-string-length: " + delta );
	}
    }
  
}

class Injectioneditor extends Patterneditor {

    constructor( id, infoid, controlid, batchEditor  ) {
	super( id, infoid, controlid, 320, "injection" );
        this.batchSpacing = 8;
	$('div#'+controlid).append( '<br><label>Minimal batch distance: </label><input type="number" id="batchDistance" value="200"/>[ns]&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;');
        var self = this;
        $('input#batchDistance').change( function() {
            debug( "running");
            var dist = $('input#batchDistance').val();
            debug( dist );
            if ( dist % 25 != 0 ) {
                error( "Batch spacing must be a multiple of 25!" );
            } else {
                error("");
                self.batchSpacing = dist/25;
                //console.log( "batchSpacing is now" , self.batchSpacing);
            }
        });
	$('div#'+controlid).append( '<label>Choose batch to add: </label><select id="batchpatternchooser"></select>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' );
        this.batcheditor = batchEditor;
        this.selectedBatch = undefined;
        this.batches = []; // an array of arrays (array of batches)
        this.buckets = []; // an array of buckets indicating the batch index (-1 for free)
        for ( var i=0; i<this.harmonic; i++ )
            this.buckets.push( -1 );
	$('div#'+controlid + ' select#batchpatternchooser').change( this, this.selectBatch );
    }

    // called when the user clicks on the button to change the pattern length
    changePLength( e ) {
	self = e.data;
	self = e.data;
//        super.changePLength( e );
	var plength = parseInt( $('div#'+ self.controlid + ' input#plength').val() );
	var clen = self.filledBuckets.length;
	if ( plength > clen ) {
            for ( var i=clen; i<plength; i++ ) {
		self.filledBuckets.push( 0 );
                self.buckets.push( -1 );
            }
	} else if ( plength < clen ) {
            self.filledBuckets = self.filledBuckets.slice( 0, plength );
            self.buckets = self.buckets.slice( 0, plength );
	}

	self.harmonic = plength;
	self.resize();
	self.updateFillPatternString();
    }


    loadPatternString( e ) {
	self = e.data;
        super.loadPatternString( e );
        self.buckets = [];
        self.batches = [];
        //console.log( "Now we can insert a batch");
        self.batches.push( { 'pattern' : self.filledBuckets.slice(0),
                             'first' : 0,
                             'last' : self.filledBuckets.length -1 } );
        for ( var i=0; i<self.filledBuckets.length -1; i++ ) {
            self.buckets.push( 0 );
        }
        self.resize();
    }

    draw() {
        super.draw();
        var c = this.context;
        c.save();
        c.translate( 2,2 );
        c.strokeStyle = 'red';
        c.lineWidth = 2;
        c.beginPath();
        for ( var ix = 0; ix < this.batches.length; ix++ ) {
            c.rect( this.batches[ix].first * this.bucketWidth, 0, this.batches[ix].pattern.length * this.bucketWidth, this.bucketHeight);
        }
        c.stroke();
        c.restore();
    }

    savePattern( e ) {
        var self = e.data;
        self.dataToSave = { 'filledBuckets' : self.filledBuckets,
                            'batches'       : self.batches,
                            'buckets'       : self.buckets,
                            'batchSpacing'  : self.batchSpacing }
        // update the main editor advanced injection selector
        self.saveData( function(){self.loadInjectionSelector()} );
    }

    doMouseMove( e ) {        
        if ( this.currentBucket === undefined )
            return;
        super.doMouseMove( e );
        //console.log( "down, last, cur", this.mouseDownBucket, this.lastCurrentBucket, this.currentBucket );
        var delta = this.currentBucket - this.lastCurrentBucket;
        if (delta == 0 ) 
            return;

        if ( delta > 0 ) {

            if (this.moveInjection.last + delta >= this.harmonic) {
                //console.log( "out of range to the right");
                return;
            }
            for (var ix = this.moveInjection.last + 1; ix <= this.moveInjection.last + delta + this.batchSpacing - 1; ix++) {
                if ( (this.buckets[ix] != -1) && (this.buckets[ix] != this.moveInjection.batchid) ) {
                    //console.log( "forward limit hit" );
                    return;
                }
            }
            //console.log( "Now move forward" );
            // delta is positive
            var ic = 0;
            var bid = this.moveInjection.batchid;
            for( var ix=this.moveInjection.first+delta; ix<=this.moveInjection.last+delta; ix++) {
                this.filledBuckets[ix] = this.batches[bid].pattern[ic];
                ic++;
                this.buckets[ix] = bid;
            }
            for ( var i=1; i<=Math.abs(delta); i++ ) {
                var ix = this.moveInjection.first+i-1;
                this.filledBuckets[ix] = 0;
                this.buckets[ix] = -1;
            }

            this.batches[bid].first += delta;
            this.batches[bid].last += delta;
            this.moveInjection.first += delta;
            this.moveInjection.last += delta;

        
        } else {

            if (this.moveInjection.first + delta < 0 ) {
                //console.log( "out of range to the left");
                return;
            }
            for (var ix = this.moveInjection.first - 1; ix >= Math.max(0,this.moveInjection.first + delta - this.batchSpacing + 1); ix--) {
                if ( (this.buckets[ix] != -1) && (this.buckets[ix] != this.moveInjection.batchid) ) {
                    //console.log( "backward limit hit" );
                    return;
                }
            }
            //console.log( "Now move backwards" );
            // delta is negative
            var ic = 0;
            var bid = this.moveInjection.batchid;
            for( var ix=this.moveInjection.first+delta; ix<=this.moveInjection.last+delta; ix++) {
                this.filledBuckets[ix] = this.batches[bid].pattern[ic];
                ic++;
                this.buckets[ix] = bid;
            }
            for ( var i=1; i<=Math.abs(delta); i++ ) {
                var ix = this.moveInjection.last+delta+i;
                this.filledBuckets[ix] = 0;
                this.buckets[ix] = -1;
            }

            this.batches[bid].first += delta;
            this.batches[bid].last += delta;
            this.moveInjection.first += delta;
            this.moveInjection.last += delta;

        }
        this.updateFillPatternString();
        this.draw();
   }
 
    doMouseDown( e ) {
        //console.log( "mouse down", e);
        super.doMouseDown( e );


        // if over a batch: prepare to move that one
        // if over an empty space: see if batch can be injected
        if (this.buckets[this.mouseDownBucket] == -1 ) {
            if ( this.selectedBatch == undefined )
                return;

            for ( var i = this.mouseDownBucket-1; i>=Math.max(0, this.mouseDownBucket-this.batchSpacing+1); i-- ) {
                if ( this.buckets[i] != -1 ){
                    console.log( "Not enough space before" );
                    return;
                }
            }
            var batchlength = this.selectedBatch.length
            for( var i = this.mouseDownBucket; i<(this.mouseDownBucket + batchlength + this.batchSpacing -1); i++) {
                if ( (i >= this.harmonic) || (this.buckets[i] != -1) ) {
                    console.log( "Not enough space behind", this.harmonic, i, this.buckets[i] );
                    return;
                }
            }
            
            //console.log( "Now we can insert a batch");
            this.batches.push( { 'pattern' : this.selectedBatch.slice(0),
                                 'first' : this.mouseDownBucket,
                                 'last' : this.mouseDownBucket+batchlength-1 } );
            var nix = this.batches.length - 1;
            var ibx = 0;
            
            for (var ix = this.mouseDownBucket; ix < this.mouseDownBucket+batchlength; ix++ ) {
                //console.log( "in buckets: setting ", ix, " to batch id ", nix);
                this.buckets[ix] = nix;
                this.filledBuckets[ix] = this.selectedBatch[ibx];
                ibx++;
            }

            

        } else {
            //console.log( "buckets", this.buckets );
            var batchid = this.buckets[this.mouseDownBucket];
            // find borders of batch
            // find first bucket of batch
            
            // If the Shift key is pressed remove the batch:
            if ( e.shiftKey ) {
                var first = this.batches[batchid].first;
                var last =  this.batches[batchid].last;
                // remove from batches
                this.batches.splice(batchid, 1);
                // remove from filledBuckets and buckets
                for ( var ix = first; ix <= last; ix++ ) {
                    this.filledBuckets[ix] = 0;
                    this.buckets[ix] = -1;
                }
                
            } else {
                this.moveInjection = { 'first' : this.batches[batchid].first,
                                       'last' : this.batches[batchid].last,
                                       'batchid': batchid };
                var ifirst = this.batches[batchid].first
                var ilast = this.batches[batchid].last;
                //console.log( "first", ifirst, "last", ilast);
            }
        }

        this.updateFillPatternString();
        this.draw();
    }

    // This one is called when a new batch is selected in the INjectionEditor for addition.
    selectBatch(e) {
        var self = e.data;
        var key = $('div#'+self.controlid+' select#batchpatternchooser option:selected').val();
        if( ! key ) {
            self.selectedBatch = undefined;
        } else {
            self.selectedBatch = self.batcheditor.patternHash[key].slice(0);
            // truncate the trailing 0 of the batch
            while( self.selectedBatch[ self.selectedBatch.length - 1 ] == 0 )
                self.selectedBatch.pop();
            // strip the leading 0 of the batch
            while( self.selectedBatch[0] == 0 )
                self.selectedBatch.shift();
        }
    }

    // when the pattern selector has been changed to load a new Injection pattern
    selectBucketPattern(e) {
	var self = e.data;
	var key = $('div#'+self.controlid+' select#patternchooser option:selected').val();
	if ( ! key )
	    return;
        //console.log( self );
       
	var patt = self.patternHash[key];
	self.filledBuckets = patt.filledBuckets;
	self.harmonic = self.filledBuckets.length;
	$('div#'+self.controlid+' input#plength').val( self.harmonic );
	$('div#'+self.controlid+' input#pname').val( key );
        self.batches = patt.batches;
        self.buckets = patt.buckets;

	self.resize();
	self.updateFillPatternString();
    }

    loadInjectionSelector() {
	var sel = $('select#advancedInjections');
	sel.html("");
	sel.append( '<option value=""></option>' );
        //console.log( "in load injectionselector", this);
        var key;
	for (key in this.patternHash) {
            sel.append( '<option value="' + key + '">' + key + '</option>' );
            //console.log( "appending", key);
	}
    }

}


var filleditors = function( batchid, infoid, controlid, injid, injinfoid, injcontrolid ) {
    var batchEditor = new Batcheditor( batchid, infoid, controlid );
    batchEditor.resize();
    batchEditor.updateFillPatternString();

    var injectionEditor = new Injectioneditor( injid, injinfoid, injcontrolid, batchEditor  );
    injectionEditor.resize();
    injectionEditor.updateFillPatternString();
    
    return { 'batchEditor' : batchEditor,
             'injectionEditor' : injectionEditor };
}
