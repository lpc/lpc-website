//var pendingListSchemes = false;
var needpassword = false;

debug = function( txt ) {
    $('div#debug').append( txt + '<br>' );
}


//error = function( txt ) {
//    $('div#error').html( txt );
//}


clear_error = function () {
    $('div#error').html("")
}

goAdvanced = function() {
    $('tbody.standard').css('display', 'none' );
    $('tbody.advanced').css('display', 'initial' );   
    batchEditor.load();
    injectionEditor.load(undefined,undefined,undefined,function(){injectionEditor.loadInjectionSelector()});
    injectionEditor.load( "batch", "batchpatternchooser", "batchHash" );
}
goBasic = function() {
    $('tbody.standard').css('display', 'initial' );
    $('tbody.advanced').css('display', 'none' );   
}

advancedEditors = function() {
    $('div#advancedEditors').show();
}

removeDirectory = function() {
    var sel = $('div#saveSchemeTree').tree( 'getSelectedNode' );
    //debug ( "sel ", sel, sel.dpath )
    if ( sel && sel.dpath) {
        var dir = sel.dpath;
    } else {
        error("You must select a directory to delete");
        return;
    }
    var userName = $('input#userName').val();
    if ( userName == "" ) {
        error( "You must give a valid user name!" );
        return;
    }
    var data = { 'user' : userName,
                 'password' : $('input#password').val(),
                 'dir' : dir}
    data['action'] = "removeDirectory";
    postIt( data, "cgi-bin/handleScheme.py" );
}

createDirectory = function() {
    var dname = $('input#createDirName').val();
    if ( dname == "" ) {
        error( "You must enter a valid name for a new directory!");
        return;
    }
    var sel = $('div#saveSchemeTree').tree( 'getSelectedNode' );
    //debug ( "sel ", sel, sel.dpath )
    if ( sel && sel.dpath) {
        dir = sel.dpath;
    } else {
        dir = "";
    }
    //debug( dir );
    var userName = $('input#userName').val();
    if ( userName == "" ) {
        error( "You must give a valid user name!" );
        return;
    }
    var data = { 'user' : userName,
                 'password' : $('input#password').val(),
                 'parent' : dir,
                 'newdir' : dname,}
    data['action'] = "createDirectory";
    postIt( data, "cgi-bin/handleScheme.py" );

}


saveDialog = function() {
    
    var userName = $('input#userName').val();

    var data = { 'user' : userName,
                 'action' : "listSchemeDirs" }

    postIt( data, "cgi-bin/handleScheme.py" );
    $('div#savebox').show( 'slideDown' );
}

// catch crtl-S
$(document).keydown(function(e) {
    if ((e.which == '115' || e.which == '76' ) && (e.ctrlKey || e.metaKey) && !(e.altKey)) {
        e.preventDefault();
        getScheme();
        return false;
    }
    return true;
});

$(document).keydown(function(e) {
    if ((e.which == '83' ) && (e.ctrlKey || e.metaKey) && !(e.altKey)) {
        e.preventDefault();
        saveDialog();
        return false;
    }
    return true;
});



getInput = function() {

    res= {}
    
    res.bunchSpacing = parseInt( $('input#bunchspacing').val() );
    res.injSpacing   = parseInt( $('input#injspacing').val() );
    res.noBatches    = parseInt( $('input#nobatches').val() );
    res.batchLength  = parseInt( $('input#batchlength').val() );
    res.batchSpacing = parseInt( $('input#batchspacing').val() );
    res.AGK          = parseInt( $('input#agk').val() );
    //console.log( "AGK from UI is ", res.AGK );
    res.fillAlgo     = $('select#fillAlgo').val();
    res.lhcbripple   = $('input#lhcbripple').prop('checked');
    res.qripple      = $('input#qripple').prop('checked');
    res.fillB1       = $('input#fillB1').prop('checked');
    res.fillB2       = $('input#fillB2').prop('checked');
    res.shiftspeed   = $('input#shiftspeed').val();
    if ( typeof lhc == 'undefined' ) 
        return res;
    lhc.setInjectionSpacing( res.injSpacing );
    lhc.setAGK( res.AGK );    
    res.b1ptype = $('input#b1ptype').val()
    res.b2ptype = $('input#b2ptype').val()
    return res;

}

newLhc = function() {
    //debug("new lhc");
    lhc = new LHC();
    redraw();
}


fill = function( para ) {

    if ( window['lhc'] === undefined )
        newLhc();

    var input = getInput();

    if ( ! input.AGK ) {
        error("Select or enter an Abort Gap Keeper first!");
        return;
    } 
    clerror();
    
    if ( para == 1 ) {
    
        switch (input.fillAlgo) {
            
        case "ion2013":

	    hiinjector = new HIInjector();
            break;

        case "proton":

            var ain = new AutoInjector();
 
            var next = ain.inject_12_12();
            
            inj = new Injection("dummy"); // the particle type is dummy here and will be
	    // overwritten y the autoinjector
            inj.addBatches( new Batch( input.batchLength, input.bunchSpacing ), input.noBatches, input.batchSpacing );
            
            ain.injectOptimally( inj );

            $('div#res').html('');
            break;

        case "protonLead":
            pPb = new ProtonLeadInjector();
            break;
        }
    }

    disp.change();
    
}

getScheme = function( scheme ) {
    var userName = $('input#userName').val();
    if ( userName == "" ) {
        error( "You must give a valid user name!" );
        return;
    }
    var data = { 'user' : userName }
    if ( scheme ) {
        data['scheme'] = scheme;
        data['action'] = "loadScheme"
	window.history.pushState({}, document.title, "/schemeEditor.html?user="+userName+"&scheme="+scheme);
   } else {
        $('div#schemeTreeContainer').toggle( "slideDown" );
        data['action'] = "listSchemes"
    }
    postIt( data, "cgi-bin/handleScheme.py" );
}

deleteScheme = function( scheme ) {
    var userName = $('input#userName').val();

    var data = { 'user' : userName,
                 'password' : $('input#password').val(),
                 'filename' : scheme,
                 'action' : "delete" }

    postIt( data, "cgi-bin/handleScheme.py" );
}

suggestName = function() {
    if ( typeof lhc == 'undefined' ) {
        error( "There is no scheme is in the editor." );
        return;
    }
    lhc.suggestName();
    $('input#schemeName').val( lhc.schemeName );
}

saveScheme = function( action ) {

    if ( lhc.checkEmptyWitnessZone() ) {
        var save = confirm( "The Witness-Zone is empty (slots 0..44) in at least one of the beams. Do you really want to save this scheme?" );
        if ( ! save ) {
            return;
        }
    }

    var schemeGroup = $('select#schemegroup').val();
    var schemeName = $('input#schemeName').val();
    var b1type = $('select#b1ptype').val();
    var b2type = $('select#b2ptype').val();
    if ( b1type == undefined || b2type == undefined ) {
	error( "You must choose a particle type for both beams!" );
	return;
    }
    lhc.beam1.setPType( b1type );
    lhc.beam2.setPType( b2type );
    var schemeData = {'AGK'               : lhc.AGK,
		      'schemeGroup'       : schemeGroup,
                      'injSpacing'        : lhc.injSpacing,
		      'beam1ParticleType' : b1type,
		      'beam2ParticleType' : b2type,
                      'beam1'             : lhc.beam1.dump(),
                      'beam2'             : lhc.beam2.dump(),
                      'schemeName'        : schemeName,
                      'description'       : $('div#schemedescription').html(),
		      'schemebeam1'       : lhc.collisionArrays.SCHEMEB1[0],
                      'schemebeam2'       : lhc.collisionArrays.SCHEMEB2[0],
	              'collsIP1/5'        : lhc.collisionArrays.ATLASCMS[0],
	              'collsIP2'          : lhc.collisionArrays.ALICE[0],
	              'collsIP8'          : lhc.collisionArrays.LHCb[1],
		      'collsPatternB1'    : lhc.collisionArrays.HEADONB1,
	 	      'collsPatternB2'    : lhc.collisionArrays.HEADONB2
                     };


    if (schemeName == "" || schemeName == undefined) {
	error("You must give a name to the scheme before saving.");
        return;
    }
    if (schemeGroup == undefined ) {
	error("You must choose a scheme-group  before saving.");
        return;
    }

    if (action == "saveLocal") {
	var jsonscheme = "data:text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(schemeData));
	$('a#downloadLocal').attr( "href", jsonscheme );
	$('a#downloadLocal').attr( "download", schemeName + ".json" );
	document.getElementById( 'downloadLocal' ).click();

    } else {

	var userName = $('input#userName').val();
	if ( userName == "" ) {
            error( "You must give a valid user name!" );
            return;
	}
    
	// get the directory name
	var sel = $('div#saveSchemeTree').tree( 'getSelectedNode' );
	if ( sel && sel.dpath) {
            var dir = sel.dpath;
	} else {
            error("You must select a directory to save the scheme in");
            return;
	}
    
	var data = { 'user'       : userName,
                     'schemeName' : schemeName,
                     'password'   : $('input#password').val(),
                     'directory'  : dir,
                     'schemeData' : JSON.stringify(schemeData),
                     'action'     : action
		   };
	//console.log( data );
	postIt( data, "cgi-bin/handleScheme.py" );
    }
    
    $('label#currentSchemeName').html( schemeName );

}

downloadBunchArrays = function( event ) {

    if ( typeof lhc == 'undefined' ) {
        error( "There is no scheme in the editor." );
        return;
    }
    clerror();

    var schemeGroup = $('select#schemegroup').val();
    var schemeName = $('input#schemeName').val();
    var b1type = $('select#b1ptype').val();
    var b2type = $('select#b2ptype').val();
    if ( b1type == undefined || b2type == undefined ) {
	b1type = 'empty';
	b2type = 'empty';
    }
    
    var data = { 'AGK'               : lhc.AGK,
		 'schemeGroup'       : schemeGroup,
                 'injSpacing'        : lhc.injSpacing,
		 'beam1ParticleType' : b1type,
		 'beam2ParticleType' : b2type,
                 'beam1'             : lhc.beam1.dump(),
                 'beam2'             : lhc.beam2.dump(),
                 'schemeName'        : schemeName,
                 'description'       : $('div#schemedescription').html(),
		 'schemebeam1'       : lhc.collisionArrays.SCHEMEB1[0],
                 'schemebeam2'       : lhc.collisionArrays.SCHEMEB2[0],
	         'collsIP1/5'        : lhc.collisionArrays.ATLASCMS[0],
	         'collsIP2'          : lhc.collisionArrays.ALICE[0],
	         'collsIP8'          : lhc.collisionArrays.LHCb[1],
		 'collsPatternB1'    : lhc.collisionArrays.HEADONB1,
		 'collsPatternB2'    : lhc.collisionArrays.HEADONB2
	       };
    
    //var data = { 'schemebeam1' : lhc.collisionArrays.SCHEMEB1[0],
    //             'schemebeam2' : lhc.collisionArrays.SCHEMEB2[0],
    //	         'collsIP1/5' : lhc.collisionArrays.ATLASCMS[0],
    //	         'collsIP2' : lhc.collisionArrays.ALICE[0], 
    //	         'collsIP8' : lhc.collisionArrays.LHCb[1],
    //		 'collsPatternB1' : lhc.collisionArrays.HEADONB1,
    //	 	 'collsPatternB2' : lhc.collisionArrays.HEADONB2 };
    
    var datastr = "data:text/json;charset-utf-8," + encodeURIComponent( JSON.stringify( data ) );
    event.currentTarget.href = datastr;
}

downloadFourier = function( event ) {
    var buffersize = 3564;
    var samplerate = 40.0e6; 
    var signal = new Float32Array(buffersize)

    for ( var i=0; i<buffersize; i++ ) {
        if ( i >= lhc.beam1.bunches.length ) {
            signal[i] = 0.0;
            continue;
        } else if ((lhc.beam1.bunches[i] == 0) || (lhc.beam2.bunches[i] == 0) )
            signal[i] = 0.0;
        else
            signal[i] = 1.0;
    }
    var fft = new DFT(buffersize, samplerate);
    fft.forward( signal );
    var spectrum = fft.spectrum;
    var plotspec = [];
    for ( var i=0; i<spectrum.length; i++ ){
        plotspec.push( [40.0e6*i/buffersize,spectrum[i]] );
    }
  
    var datastr = "data:text/json;charset-utf-8," + encodeURIComponent( JSON.stringify( plotspec ) );
    event.currentTarget.href = datastr;
    
}


/////////////
calcFourier = function( event ) {
    var buffersize = 3564;
    var samplerate = 40.0e6; 
    var signal = new Float32Array(buffersize)

    for ( var i=0; i<buffersize; i++ ) {
        if ( i >= lhc.beam1.bunches.length ) {
            signal[i] = 0.0;
            continue;
        } else if ((lhc.beam1.bunches[i] == 0) || (lhc.beam2.bunches[i] == 0) )
            signal[i] = 0.0;
        else
            signal[i] = 1.0;
    }
    var fft = new DFT(buffersize, samplerate);
    
    fft.forward( signal );
    var spectrum = fft.spectrum;
        
    var plotspec = [];
    for ( var i=0; i<spectrum.length; i++ ){
        plotspec.push( [40.0e6*i/buffersize,spectrum[i]] );
    }

    $(function() {
        theChart = $('#fourierplot').highcharts({
            credits: {
                enabled: false
            },
            chart: {
                zoomType: 'x'
            },
            title: {
                text : "Fourier Spectrum IP 1&5",
                style: {
                    color: '#000080',
                    'font-size': "150%"
                }
            },
            xAxis: {
		title : {
		    text : 'Frequency'
		}
            },
            yAxis: [
                {
                    title : {
                        text : "spectrum",
                        style: {
                            'font-size':"130%",
                            color : 'black'
                        }
                    }
                }
            ],
            series: [
                {
                    name: 'Fourier Spectrum IP 1 & 5',
                    data: plotspec,
                    color: '#0000FF'
                },
            ]
        });
    });

};

////////////
init = function() {
    if ( window['disp'] === undefined ) {
        disp = new InjectionDisplay( "beams" );
    }
    var res = filleditors( 'batchgui', 'batchinfo', 'batchcontrols', 'injectiongui', 'injectioninfo', 'injectioncontrols');
    batchEditor = res.batchEditor;
    injectionEditor = res.injectionEditor;
    var location = window.location.href;
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    if ( urlParams.has('user') ) {
	$('input#userName').val(urlParams.get('user'));
	getScheme(urlParams.get('scheme'));
    }
}

redraw = function() {
    if ( typeof lhc == 'undefined' ) 
        return;
    disp.drawBeamStructure();
    disp.drawB1();
    disp.dumpCollisions( $('div#res'), $('div#longrange') );
    
}

addInjection=function() {
    var agk = parseInt( $('input#agk').val() );
    if ( typeof lhc === 'undefined' )
        error("Click on 'Start new scheme' first!");
    else if ( ! agk ) {
        error("Select or enter an Abort Gap Keeper first!");
    } else {
        clerror();
        disp.addInjection();
    }
}

addAdvancedInjection=function() {
    var agk = parseInt( $('input#agk').val() );
    if ( typeof lhc === 'undefined' )
        error("Click on 'Start new scheme' first!");
    else if ( ! agk ) {
        error("Select or enter an Abort Gap Keeper first!");
    } else {
        clerror();
        disp.addAdvancedInjection();
    }
}


postIt = function( data, baseurl ) {
    var url = window.location.protocol + '//' + window.location.host;
    var path = window.location.pathname;
    path = path.substring(0,path.lastIndexOf('/'));
    url += path + '/' + baseurl;
    //debug( url );
    $.post( { 
        url: url,
        data: data
    }).done( function(reply,status) {
        //debug( "reply");
	if ( status != "success" ) {
	    error( "Error communicating with server: " + status);
	    return;
	} else {
            //debug( "reply came");
            if ( typeof reply === "string" )
                debug( reply );
	    //debug( "reply came, " + reply.action);
	    if ( reply.debug ) debug( reply.debug );
	    if ( reply.info  ) debug( reply.info );
	    if ( reply.error ) error(reply.error);
	    else clear_error();
            if ( ! 'action' in reply ) {
                debug( "no action found");
                debug( reply );
            }
            //debug( reply.action );
            if ( reply.action == "listSchemes" )
                listSchemes( reply.data );
            else if ( reply.action == "listSchemeDirs" )
                listSchemes( reply.data, "saveSchemeTree" )
            else if ( reply.action == "loadScheme" )
                loadScheme( reply.data );
//            else if ( reply.action == "confirmNewUser" )
//                confirmNewUser( reply.data );
            else if ( reply.action == "delete" ) 
                listSchemes( reply.data );
            else if ( reply.action == "save" ) {
                //$('div#status').html( reply.status );
                $('div#savebox').hide( 'slideDown' );
            }
            else if ( reply.action == "createUser" )
                listSchemes( reply.data, 'saveSchemeTree' )
            else if ( reply.action == "createDirectory" )
                listSchemes( reply.data, 'saveSchemeTree' )
            else if ( reply.action == "removeDirectory" )
                listSchemes( reply.data, 'saveSchemeTree' )
            else if ( reply.action == "move" )
                listSchemes( reply.data )
            else if ( reply.action == 'downloadBackup' ) {
                if ( reply.data.backupurl ) {
                    var backupurl = reply.data.backupurl;
                    $('#backupurl').html( "<a href='"+backupurl+"'> Download your backup from here if it has not been downloaded automatically.</a>");
                    window.location.href = backupurl;
                }
            }
            else {
                error( "No valid action in reply: " + reply.action );
            }
	}
    }).fail( function(jqXHR, text, error) {
        alert("error: ajax fail handler called " + error + " " + text);
    });  
};

downloadBackup = function() {
    var userName = $('input#userName').val();
    if ( userName == "" ) {
        error( "You must give a valid user name!" );
        return;
    }
    data = { 'user' : userName,
             'action' : "downloadBackup"
           };
    postIt( data, "cgi-bin/handleScheme.py" );
}

createUser = function() {
    userName = $('input#userName').val();
    if (userName.match( /^\s*$/ )) {
        return;
    }
    var data = { 'newdir' : userName, 
                 'action' : 'createDirectory' };
    //console.log( "create user: ", data );
    postIt( data, "cgi-bin/handleScheme.py" );
}


loadScheme = function( data ) {

    // we reconstruct the filling scheme with the contents of the string
    
    var schemeData = JSON.parse( data );
    
    //lhc = new LHC();
    newLhc();
    lhc.setAGK( schemeData.AGK );
    lhc.injSpacing = schemeData.injSpacing;
    lhc.schemeName = schemeData.schemeName;
    
    $('input#injspacing').val( parseInt( 25*schemeData.injSpacing ) );
    $('input#agk').val(schemeData.AGK);
    $('select#b1ptype').val( schemeData.beam1ParticleType );
    $('select#b2ptype').val( schemeData.beam2ParticleType );
    $('select#schemegroup').val( schemeData.schemeGroup );
    
    if ( 'description' in schemeData ) {
        $('div#schemedescription').html( schemeData.description );
    } else {
        $('div#schemedescription').html("");
    }
    
    
    var b1 = lhc.beam1;
    var b2 = lhc.beam2;
    
    b1.fill( schemeData.beam1 );
    b2.fill( schemeData.beam2 );

    $('input#schemeName').val(lhc.schemeName);
    $('label#currentSchemeName').html(lhc.schemeName);

    redraw();
    fullDetuning();
}

//listSchemes = function( data, id="schemeTree" ) {
listSchemes = function( data, id ) {
    if ( id == undefined ) {
        id = "schemeTree";
    }
    

    if ( data.length > 0 ) {
        if ( 'password' in data[0] && data[0]['password'] == true ) {
            needpassword = true;
            $('span#pwdentry').css( 'display', 'inline' );
        } else {
            $('span#pwdentry').css( 'display', 'none' );
            needpassword = false;
        }
    } else {
        $('span#pwdentry').css( 'display', 'none' );
    }            

    //console.log( "listing ",id, data );
    $('div#' + id ).tree( 'loadData', data  );
    root = $('div#schemeTree').tree('getTree').children[0];
    $('div#schemeTree').tree('openNode', root );
}



fullDetuning = function() {
    //  establish the parameters
    var params = { 'V0' : $('input#cavity_HV').val() * 1000000,
                   'ppb' : $('input#ppb').val(),
                   'f_RF' : 400800000.0,
                   'f_rev' : 11245.5,
                   'RoverQ' : 45,
                   'b_len' : $('input#blen').val() * 1e-9
                 }
    
    if ( (disp.injhash_b1 == undefined) || (disp.injhash_b2 == undefined) ) {
        error( "Noting to investigate here...");
        return;
    }
    var dh1 = disp.injhash_b1;
    var dh2 = disp.injhash_b2;

    // go through the injections of beam 1
    var b1pop = getpop( dh1 );
    var b2pop = getpop( dh2 );

    // makePhase plots
    var beam1 = create_beam( b1pop );
    var beam2 = create_beam( b2pop );
    //console.log( b1pop );
    makePhasePlots( params, beam1, beam2, lhc.collisionArrays );
    
}

getpop = function(dh) {
    bpop = [];
    for ( var key in dh ) {
        inj = dh[key];
        //console.log( "getpop inj to analyse ", inj);
        var inja = create_injection( inj.nbunch, inj.nbatch, inj.batch_sp )
        bpop.push( { 'injection' : inja,
                     'positions' : inj.positions } );
    }
    return bpop;
}
