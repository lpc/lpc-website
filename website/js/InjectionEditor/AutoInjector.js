AutoInjector = function( lhc ) {

}



AutoInjector.prototype.inject_12_12 = function() {

    var b12 = new Batch( 12, 25 );

    var nextB1 = lhc.beam1.inject( new Injection().addBatches( b12 ), 12 );
    var nextB2 = lhc.beam2.inject( new Injection().addBatches( b12 ), 0 );

    //nextB1 = lhc.beam1.inject( new Injection().addBatches( b12 ), nextB1 );
    //nextB2 = lhc.beam2.inject( new Injection().addBatches( b12 ), nextB2 ); 

    return [nextB1,nextB2];
}



AutoInjector.prototype.inject_12_12nc = function() {
    var b12 = new Batch( 12, 25 );

    var nextB1 = lhc.beam1.inject( new Injection().addBatches( b12 ), 0 );
    var nextB2 = lhc.beam2.inject( new Injection().addBatches( b12 ), 0 );

    nextB2 += 6;

    nextB1 = lhc.beam1.inject( new Injection().addBatches( b12 ), nextB1 );
    nextB1 = lhc.beam2.inject( new Injection().addBatches( b12 ), nextB2 ); 

    return [nextB1,nextB2];
}


AutoInjector.prototype.injectOptimally = function( injection ) {

    var b1 = lhc.beam1;
    var b2 = lhc.beam2;
    
    start = lhc.agk;
    ilen  = injection.getLength();
    if ( start + ilen - 1 > 3442 ) {
        start = 3442 - ilen + 1;
    }
    
    cbx = start;
    
    stop = false;
    
    while ( ! stop ) {
        
        // Q4
        b1.inject( injection.clone(), cbx );
        b2.inject( injection.clone(), cbx );
        // Q3
        cbx = cbx - 894;
        b1.inject( injection.clone(), cbx );
        b2.inject( injection.clone(), cbx );
        // Q2
        cbx = cbx - 894;
        b1.inject( injection.clone(), cbx );
        b2.inject( injection.clone(), cbx );
        // Q1
        cbx = cbx - 894;

        var noq4 = false;
        if ( cbx >= 0 ) {
            for (var bx = cbx - lhc.injSpacing + 1; bx < cbx + injection.getLength() + lhc.injSpacing - 1; bx ++)

                if ( ( b1.bunches[bx] != 0 ) || ( b2.bunches[bx] != 0 ) ) {
                    noq4 = true;
                    break;
                };

        } else {
            noq4 = true;
        }

        if ( ! noq4 ) {
        
            b1.inject( injection.clone(), cbx );
            b2.inject( injection.clone(), cbx );

        }

        checke = start - 1;
        start  = start - injection.getLength() - lhc.injSpacing + 1;
        cbx = start;
        check  = start - lhc.injSpacing + 1;

        for ( var bx = check; bx <= checke; bx ++) {
            if ( ( b1.bunches[bx] != 0 ) || ( b2.bunches[bx] != 0 ) ) {
                stop = true;
                break;
            };
            
        }

    }
    
}

// fill the first quarter as much as possible and fill the
// corresponding quarters behind so that LHCb collides.
AutoInjector.prototype.injectRadically = function( injection, next ) {

    var b1 = lhc.beam1;
    var b2 = lhc.beam2;

    next = typeof next === 'undefined' ? Math.max( b1.nextFree, b2.nextFree ) : next;

    while (next < 891) {

        // Check if there is space
        var veto      = false;
        var freeUpper = next + injection.getLength() + lhc.injSpacing - 1;
        var bu1       = lhc.beam1.bunches;

        for (var i = next; i <= freeUpper; i++ ) {
            if ( bu1[i] != 0 ) {
                veto = true;
                break;
            }
        }

        if (veto) break;

        // Q1
        b1.inject( injection.clone(), next );
        var nnext = b2.inject( injection.clone(), next );

        // Q2
        next = next + 894;
        b1.inject( injection.clone(), next );
        b2.inject( injection.clone(), next );

        // Q3
        next = next + 894;
        b1.inject( injection.clone(), next );
        b2.inject( injection.clone(), next );

        // Q4
        next = next + 894;
        if ( (next <= lhc.agk) && ( next + injection.getLength() - 1 <= 3442 )  ) {
            b1.inject( injection.clone(), next );
            b2.inject( injection.clone(), next );
        }

        next = nnext;
        
    }

}

// arguments: injection to clone, bunch of the injection to ripple, beamNo of the injction to ripple (1 or 2)
AutoInjector.prototype.rippleInject = function(inj, lhcBunch, offset ) {

    while ((lhcBunch > 0) && (lhcBunch <= lhc.agk) ) {

        if ( this.checkInjectionPossible( inj, lhcBunch, lhc.beam1 ) ) {
            lhc.beam1.inject( inj.clone(), lhcBunch );
        } else {
            return;
        }

        if ( this.checkInjectionPossible( inj, lhcBunch, lhc.beam2 ) ) {
            lhc.beam2.inject( inj.clone(), lhcBunch );
        } else {
            return;
        }
        
        lhcBunch = lhcBunch + offset;
    }
}

AutoInjector.prototype.checkInjectionPossible = function( inj, lhcBunch, beam )
{

    // this value is the number of bx to be checked + 1
    var bxforward = inj.getLength + lhc.injSpacing - 1;
    var bxbackward = lhc.injSpacing - 1;

    var ok = 0;
    for ( var ix = lhcBunch; ix < bxforward + lhcBunch; ix++ ) {
        if ( beam.bunches[ix] != 0 )
            return false;
    }

    // We avoid negative values...
    if ( (lhcBunch - bxbackward) < 0 )
        bxbackward = lhcBunch;
    
    for ( var ix = lhcBunch - bxbackward; ix < lhcBunch; ix++ ) {
        if ( beam.bunches[ix] != 0 )
            return false;
    }

    // check that we do not go in the abortgap
    if ( lhcBunch + inj.getLength() - 1 > 3442 )
        return false;
    
    return true;
    
}
