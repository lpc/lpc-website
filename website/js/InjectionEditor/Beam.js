Beam = function( beamNo ) {

    this.beamNo     = beamNo;
    this.nextFree   = 0;
    this.injections = new Array();
    this.bunches    = new Array(3564);
    this.pType      = "proton";

    // initialise the bunch array
    for( var i = 0; i < this.bunches.length; i++ ) { this.bunches[i] = 0; }

}

Beam.prototype.setPType = function(ptype) {
    this.pType = ptype;
}

Beam.prototype.checkEmptyWitnessZone = function() {    
    for (var ix=0; ix<=44; ix++ ) {
        if ( this.bunches[ix] != 0 )
            return false;
    }
    return true;
}

Beam.prototype.fill = function( dumpData ) {

    for ( var i = 0; i < dumpData.length; i++ ) {
    
        var batches = dumpData[i].batches;
        
        var inj = new Injection();
        inj.batchSpacing = dumpData[i].batchSpacing;
	// Try to be tolerant to the old version where the injectionType was not yet present:
	if ( dumpData[i].injectionType ) {
	    inj.injectionType = dumpData[i].injectionType;
	}
        //var batch = undefined;
        for ( var j = 0; j < batches.length; j ++ ) {

            if ( 'bunchArray' in batches[j] ) {
                //console.log( "Loading scheme with new batches (bunchArray saved to disk)" );
                var batch = new Batch( batches[j].bunchArray );
            } else {
                var batch = new Batch( batches[j].bunches, batches[j].bunch_spacing );
                //console.log( "Loading scheme with old batches (only standard batches)" );
            }

            inj.addBatch( batch, batches[j].injBunch );
            
        }

        if ( inj.batchSpacing == undefined ) {
            //console.log( "batch spacing is not defined in saved scheme" );
            // calculate it
            if (batches.length > 1) {
                inj.batchSpacing = batches[1].injBunch - ( batches[0].injBunch + batch.getlength() );
            } else {
                inj.batchSpacing = 0;
            }
        }
        
        this.inject( inj, dumpData[i].lhcbunch );
        
    }

}

Beam.prototype.dump = function() {
    
    injections = [];
    
    for (var i = 0; i < this.injections.length; i++ ) {

        var inj = this.injections[i];
        var idump = { batches       : [],
                      lhcbunch      : inj.lhcbunch,
                      batchSpacing  : inj.batchSpacing,
		      injectionType : inj.injectionType };

        for ( j = 0; j < inj.batches.length; j ++ ) {

            var batch = inj.batches[j];

            var bdump = { injBunch      : batch.injBunch,
                          bunches       : batch.bunches,
                          bunch_spacing : batch.bunch_spacing };
            if ( 'bunchArray' in batch ) {
                //console.log( 'Dumping new type of Batch (bunchArray)' );
                bdump.bunchArray = batch.bunchArray
            } else {
                //console.log( "dumping old type of batch" );
            }
            idump.batches.push( bdump );

        }

        injections.push( idump );
        
    }

    return injections;
    
}

Beam.prototype.inject = function( injection, lhcbunch ) { 
    
    var agk = lhc.agk;
    var injlength = injection.getLength();
    var lastbx = lhc.agk + injlength - 1;
    if (lastbx > 3442) {
        error( "This injection is incompatible with the AGK and can leak into the abort gap by " + (lastbx - 3442) + " bx slots!");
        return;
    }
    
    injection.beamNo = this.beamNo;

    this.injections.push( injection );

    injection.lhcbunch = lhcbunch;
    
    for ( var i = 0; i < injection.getLength(); i++ ) {
	this.bunches[i + lhcbunch ] = injection.bunchArray[i];
    }

    // return the next possible injection
    this.nextFree = lhcbunch + injection.getLength() + lhc.injSpacing - 1;

    // sort the injections since later it is assumed they are ordered
    this.injections.sort( function( a,b ) { return a.lhcbunch - b.lhcbunch } );
    
    return this.nextFree;
}



Beam.prototype.maxSelectionMove = function() {

    var maxFwd = 999999;
    var maxBwd = 999999;

    for ( var i=0; i<this.injections.length; i++ ) {

        var inj = this.injections[i];

        if ( inj.selected == false ) continue;

        // check forward:
        var j;

        for ( j=i+1; j<this.injections.length; j++ ) {
            
            if (this.injections[j].selected == false) break;

        }

        // the last selected injection of a selected "train"
        inj = this.injections[j-1];

        if ( j < this.injections.length ) {

            // this is not the last injection before the AGK
            var space = this.injections[j].lhcbunch - (inj.lhcbunch + inj.getLength() + lhc.injSpacing - 1);

        } else {

            // this is the last injection before the AGK
            // check the AGK
            var space = lhc.agk - inj.lhcbunch;
            // and also check that we do not leak in the abortgap
            var space2 = 3442 - (inj.lhcbunch + inj.getLength() - 1);
            space = Math.min(space, space2);
        }

        maxFwd = Math.min( space, maxFwd );

        // check backward:
        var k;
        
        for ( k=i-1; k>=0; k-- ) {
            
            if (this.injections[k].selected == false) break;

        }

        // the first selected injection of a "train"
        inj    = this.injections[k+1];
        var ll = 0;

        if ( k >= 0 ) {

            ll = this.injections[k].lhcbunch + this.injections[k].getLength() + lhc.injSpacing - 1;

        }

        var space = inj.lhcbunch - ll;
        maxBwd    = Math.min( space, maxBwd );            

        i=j;
    }
    
    return { "forwards"  : maxFwd,
             "backwards" : maxBwd };
}


Beam.prototype.toggleInjectionTypeSelected = function() {

    for ( var i=0; i < this.injections.length; i++ ) {

        if ( this.injections[i].selected ) {

            this.injections[i].toggleType();

	}

    }

}

Beam.prototype.deleteSelected = function() {

    newInjArr = [];

    for ( var i=0; i < this.injections.length; i++ ) {

        if ( this.injections[i].selected ) {

            var inj = this.injections[i];

            for ( var j = inj.lhcbunch; j < inj.lhcbunch+inj.getLength(); j++ ) {

                this.bunches[j] = 0;

            }

        } else {

            newInjArr.push( this.injections[i] );

        }

    }

    this.injections = newInjArr;

}



Beam.prototype.moveSelection = function( delta ) {

    for ( var i = 0; i<this.injections.length; i++ ) {

        var inj = this.injections[i];

        if (inj.selected ) {

            // delete the bunches of the injection in the bunch array
            for ( var j = 0; j < inj.getLength(); j++ ) {

                this.bunches[j+inj.lhcbunch] = 0;

            }

            // update the first lhcbunch
            inj.lhcbunch += delta;

            // re-insert the bunches into the buncharray with the move-offset
            for ( var j = 0; j < inj.getLength(); j++ ) {

                this.bunches[j+inj.lhcbunch] = inj.bunchArray[j];

            }
            
        }

    }

}



Beam.prototype.nextFree = function() {

    return this.nextFree;

}
