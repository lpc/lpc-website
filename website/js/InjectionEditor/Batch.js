Batch = function( bunches, bunch_spacing ) {

    if ( arguments.length == 2 ) {

        //console.log( "old batch constructor" );

        this.injBunch = 0; // the bunch number relative to the injection start
        // make the bunch array
        this.bunchArray = [];
        this.bunches = bunches;
        this.bunch_spacing = bunch_spacing;
        
        var bsp_slot = bunch_spacing/25;
        for ( var b=0; b<bunches; b++ ) {
            this.bunchArray.push(1);
            // Do not append 0 for the last batch. An injection / batch should not end with 0!
            if ( b < bunches - 1 ) {
                for ( var c=0; c < bsp_slot - 1; c++ )
                    this.bunchArray.push(0);
            }
        }

    } else if( arguments.length == 1 ) {

        //console.log( "bunchArray batch constructor" );
        //console.log( bunches );
        this.bunchArray = bunches.slice(0);
        this.bunches = 0;
        // cut away trailing empty buckets
        while ( this.bunchArray[ this.bunchArray.length-1 ] == 0 )
            this.bunchArray.pop()
        
        // calculate number of bunches and try to deduce bunch spacing (heuristic!)
        // assume that there is a bunch in the first array slot
        this.bunch_spacing = 0;
        for ( var i=0; i<this.bunchArray.length; i++ ) {
            if ( this.bunchArray[i] == 1 ) {
                this.bunches++;
                if ( (this.bunch_spacing == 0) && (i > 0)  ) {
                this.bunch_spacing = i*25
                } 
            }
        }
        
        this.injBunch = 0;
    }
    //this.bunches = bunches;
    //this.bunch_spacing = typeof bunch_spacing === 'undefined' ? 25 : bunch_spacing;
    //debug( "new batch with buch spacing " + this.bunch_spacing);
};


// The length is the number of "slots" up to the last slot with a bunch
// I.e. a 50ns with 1 bunch has legnth 1. With 2 bunches it has length 3.
Batch.prototype.getlength = function() {
    return this.bunchArray.length
//    return this.bunches * this.bunch_spacing/25 - ( this.bunch_spacing/25 - 1 );
}

Batch.prototype.clone = function() {
    ba = new Batch( this.bunchArray );
//    ba = new Batch( this.bunches, this.bunch_spacing );
    ba.injBunch = this.injBunch;
    ba.bunches = this.bunches;
    ba.bunch_spacing = this.bunch_spacing;
    return ba;
}


//AdvancedBatch = function( bunchArray ) {
//    this.bunchArray = bunchArray;
//    // cut away trailing empty buckets
//    while ( this.bunchArray[ this.bunchArray.length-1 ] == 0 )
//        this.bunchArray.pop()
//    this.injBunch = 0;
//}
//
//AdvancedBatch.prototype.getlength = function() {
//    return this.bunchArray.length;
//}
//
//AdvancedBatch.prototype.clone = function() {
//    ba = new AdvancedBatch( this.bunchArray )
//    ba.injBunch = this.injBunch;
//    return ba;
//}

