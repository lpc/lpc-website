HIInjector = function( input ) {

    this.input = input;

    var b1 = lhc.beam1;
    var b2 = lhc.beam2;

    this.batch = new Batch( input.batchLength, input.bunchSpacing );

    var inj = new Injection();

    // prepare the maximal injection with all batches to start with
    inj.addBatches( this.batch, input.noBatches, input.batchSpacing );

    var start = lhc.agk;
    if ( start + inj.getLength() - 1 > 3442 )
        start = 3442 - inj.getLength() + 1;
    var cbx = start;
    // Algorithm: Start to inject at the AGK for both beams. 
    // Go backwards and keep collisions for CMS and ATLAS at max.
    // At each step fill all quarters in away that ALICE gets 
    // max number of collisions. 
    // Once the space gets tight try to reduce the number of 
    // batches in the injection. 

    stop = false;
    var lastq3 = 0;
    while ( ! stop ) {
        //debug("inject q4 in " + cbx );
	b1.inject( inj.clone(), cbx );
	b2.inject( inj.clone(), cbx );
	cbx -= 891;
        //debug("inject q3 in " + cbx );
	b1.inject( inj.clone(), cbx );
	b2.inject( inj.clone(), cbx );
	cbx -= 891;
        //debug("inject q2 in " + cbx );
	b1.inject( inj.clone(), cbx );
        var linj = inj.clone();
	b2.inject( linj, cbx );
        lastq3 = linj.lhcbunch;
	cbx -= 891;
	if ( cbx >= 0 ) {
            //debug("inject in " + cbx );
	    b1.inject( inj.clone(), cbx );
	    b2.inject( inj.clone(), cbx );
	} else {
            //debug( "cbxelse " + cbx );
            var q4inj = inj.clone()
            // we have to shorten the injection by an integer number of batches
            // so that the remaining batches will be injected on top of the
            // positions which lead to collisions in ALICE. The last bunch should
            // be here:

            //debug ("linj " + linj.lhcbunch + "len " + linj.getLength() );
            var lastBx = linj.lhcbunch + linj.getLength() - 892;
            //debug( "lastgbx " + lastBx );
            var balen = this.batch.getlength();
            var basp = this.input.batchSpacing / 25

            if (lastBx >= (balen + this.input.injSpacing/25 - 1) ) {

                // at least one batch fits
                var ib = 1;
                var insertbx = lastBx - balen + 1;
                //debug ("insertbx  " + insertbx);

                do {
                    var nbx = insertbx - balen - basp + 1;
                    //debug( nbx );
                    if (nbx < 0 ) break;
                    ib ++;
                    insertbx = nbx;
                } while( true );
                
                var rinj = new Injection();
                rinj.addBatches(this.batch, ib, this.input.batchSpacing );
                //debug( "final insert " + insertbx );
                b1.inject( rinj.clone(), insertbx );
                b2.inject( rinj.clone(), insertbx );
                
            }
        }

	//cbx = start - inj.getLength() - input.injSpacing / 25 + 1; 
	//start = cbx;

        // Now check if there is enough space to go for another round of injections:
        // Check for enough empty space: calculated where the check should start
        // the check considers the length of the injection and injection space before
        // and after the injection itself. The start of the check is put in cbx and
        // will go until one bx before the previous injection (which begins at "start")
	cbx = start - inj.getLength() - 2 * input.injSpacing / 25 + 2; 

        // max will contain the smalles bx after which only empty bx are encountered
        // up to the start of the previous injections.
	var max = cbx;
	for( var i = cbx; i<start; i++ ) {
	    if (( b1.bunches[i] != 0 ) || ( b2.bunches[i] != 0 ) )
		max = i+1
	}


        // if there have been encountered filled bx, we need to reduce the number
        // of batches and see if such an injection fits between max and start:
	if ( max != cbx ) {
            inj = this.reduceBatches( inj, start -  max );
//	    var available = start - max;
//	    //debug( "av " + available );
//	    do {
//		var nb = inj.batches.length - 1;
//		inj = new Injection();
//		inj.addBatches( this.batch, nb, input.batchSpacing)
//		//debug( "inj " + inj.getLength() + " nb " + nb + " injbat " + inj.batches.length );
//	    } while ( (inj.getLength() + 2*(input.injSpacing/25-1) > available) && 
//		      (inj.batches.length > 0));

//	    if ( inj.getLength() + 2*(input.injSpacing / 25 - 1) > available ) {
	    if ( inj.batches.length == 0  ) {
		//debug( "need to stop " );
		stop = true;
	    } 
	}

	cbx = start - inj.getLength() - input.injSpacing / 25 + 1; 
	start = cbx;

    }

}


HIInjector.prototype.reduceBatches = function( inj, available ) {
    //debug( "av " + available );
    do {
	var nb = inj.batches.length - 1;
	inj = new Injection();
	inj.addBatches( this.batch, nb, this.input.batchSpacing)
	//debug( "inj " + inj.getLength() + " nb " + nb + " injbat " + inj.batches.length );
    } while ( (inj.getLength() + 2*(this.input.injSpacing/25-1) > available) && 
	      (inj.batches.length > 0));
    
    return inj;

}
