var changed = false;

doScroll =  function() {
    today = new Date();
    var row;
    $('tr[data-date]').each( function( ix ) {
        var dstr = $(this).attr('data-date').split('-');
        trdat = new Date(dstr[0], dstr[1]-1, dstr[2]);
	if ( trdat < today ) {
	    row = this;
	} else {
	    row = this;
	    return false;
	}
	return true;
    });
    // largest scroll value:
    var maxscr = $(document).height() - $(window).height();
    var scrollit = Math.min( maxscr, $(row).offset().top );    
    $('html, body').animate( {
	scrollTop: scrollit
    }, 4000);
};


addempty = function() {
    $('table#commissioningtab tbody').append('<tr><td data-name="date"><input type="date" value="" onchange="dateChange( event )" /></td> <td data-name="datestr" contenteditable="true"></td><td data-name="title" contenteditable="true"></td><td data-name="description" contenteditable="true"></td></tr>');
}

dateChange = function( e ) {
    var darr = $(e.target).val().split('-');
    var dstr = darr[2] + '/' + darr[1] + '/' + darr[0]
    var domstr = $(e.target).parent().next().html();
    if ( domstr == "" || domstr.match(/^\d\d\/\d\d\/\d\d\d\d$/)) {
        $(e.target).parent().next().html( dstr );
    }
}

compareTableEntries = function( a,b ) {
    if( a.date < b.date )
        return -1;
    return 1;
}

savetab = function( dest ) {
    var table=[];
    $('table#commissioningtab tbody tr').each( function() {
        var item = {}
        $(this).find( 'td' ).each( function() {
            var attr = $(this).attr( 'data-name' )
            var val;
            if ( attr == "date" ) {
                val = $(this).find( 'input' ).val();
            } else {
                val = $(this).html();
            }
            item[attr] = val;
        });
        if (item.date)
            table.push( item );
    });

    // sort the table
    table.sort( compareTableEntries );

    var url = window.location.protocol + '//' + window.location.host;
    var path = window.location.pathname;
    path = path.substring(0,path.lastIndexOf('/'));
    url += path + '/commissioningPageHandler.py';
    var action = "save";
    if ( dest == "release" )
        action = "release";
    data = { 'year' : year, 
             'action' : action,
             'table' : JSON.stringify(table) };
    $.post( url, data, function( data ) {
        $('div#status').html(data.status);
        if (data.error) {
            error( data.error );
            return;
        } else {
            clerror();
        }
    });

}

