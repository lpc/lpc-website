debug = function( txt ) {
    $('div#debug').append( txt + "<br>" );
}
warning = function( txt ) {
    $('div#warning').html( txt );
}
error = function( txt ) {
    if ( txt == "" ) {
        $('div#error').hide();
    } else {   
        $('div#error').html( txt );
        $('div#error').show();
    }
}

clerror = function() {
        $('div#error').hide();
    $('div#error').html( '' );
}
                            

class GuiInput {
    constructor( config ) {
        this.config = config;
        this.data = {};
        this.ithash = {};
    }

    render() {
        this.data = {};
        for ( var it = 0; it < this.config.length; it++ ) {
            var cstr = "";
            var config = this.config[it];
            var divid = config.divid;
            var mydiv = "div#"+divid;
            cstr +=  "<h2>" + config.title + "</h2>";
            this.data[ config.name ] = {};
            if ('items' in config ) {
                // A single table needs to be rendered
                cstr += this.rendertable( config, this.data[ config.name ]  );
            } else {
                // A set of tables need to be rendered
                var data = config.data;
                cstr +=  "<table><tr>";
                for ( var idat = 0; idat < data.length; idat++ ) {
                    cstr +=  '<td style="padding:20px;">';
                    cstr += "<h3>" + data[idat].title + "</h3>"
                    this.data[ config.name ][data[idat].name] = {}
                    cstr += this.rendertable( data[idat], this.data[ config.name ][data[idat].name], data[idat].name );
                    cstr +=  "</td>";
                    if ( (idat+1) % config.columns == 0 ) {
                        cstr +=  "</tr><tr>";
                    }
                }
                cstr +=  "</tr></table>" ;
            }
            $(mydiv).append(cstr);
        }
        //console.log( "data", this.data );
        return this.data;
    }

    rendertable( data, datlookup, prefix ) {
        var cstr =  "<table>" ;
        if ( prefix == undefined ) {
            prefix = "";
        } else {
            prefix += "_";
        }

        for (var ix=0; ix<data.items.length; ix++ ) {
            var it = data.items[ix];
            cstr += "<tr><td>" + it.title + "</td><td> : </td><td>" ;
            datlookup[ it.name ] = it.default;
            this.ithash[prefix + it.name] = datlookup;
            if ( typeof( it.default ) === "boolean" ) {
                var checked = ""
                if ( it.default )
                    checked = "checked";
                cstr +=  '<input id="' + prefix + it.name + '" type="checkbox" ' + checked + '/>' ;
            } else {
                cstr +=  '<input id="' + prefix + it.name + '" type="number" value="' + it.default + '"/>';
            }
            cstr +=  "</td><td>" ;
            //console.log( it );
            if ( 'unit' in it )
                cstr +=  it.unit ;
            cstr +=  "</td>" ;
            cstr += "</tr>";
        }
        cstr +=  "</table>" ;
        return cstr;
    }

    readData() {
        this.itemhash = {};
        for ( var key in this.ithash ) {
            var tag = "#"+key;
            if ($(tag).is(':checkbox')) {
                var val = $(tag).is(':checked');
            } else {
                var val = parseFloat($(tag).val());
            }
            var k = key.replace( /.*_/, "" );
            //console.log( "form", key, k );
            this.ithash[key][k] = val;
            this.itemhash[ tag ] = val;
            //console.log( tag, val );
        }

        return this.data;
    }
    
    getConfigString( divid ) {
        this.readData();
        $(divid).html( JSON.stringify( this.itemhash ) );
        $(divid).append("<br><br><button onclick=\"$('div#inputdatastring').hide()\">Cancel</button>");

        $(divid).show();
    }

    loadConfigString( id ) {
        var lstring = $('textarea#'+id).val();
        var data = JSON.parse( lstring );
        for ( var key in data ) {
            var tstr = typeof( data[key] );
            if (typeof(data[key]) === "boolean") {
                //console.log( "BOOOL");
                $(key).prop( 'checked', data[key] );
            } else {                
                //console.log( key, data[key] );
                $(key).val( data[key] );
            }
        }
    }
}


calcLumi = function( i ) {
    var o = {};
    
    o.gamma      = i.energy / i.mass ;
    o.eps        = i.epsn / o.gamma ;
    o.sigmaxy    = Math.sqrt( o.eps * i.beta ) * 1.0e6 ;
    o.sigmax     = Math.sqrt( 2.0 * o.sigmaxy * o.sigmaxy * Math.cos( i.alpha / 2.0 ) * Math.cos( i.alpha / 2.0 ) + 2.0e12 * i.sigz * i.sigz * Math.sin( i.alpha / 2.0 ) * Math.sin( i.alpha / 2.0 ) ) ;
    o.sfactor    = Math.sqrt(2.0) * o.sigmaxy / o.sigmax ;
    o.sigmay     = Math.sqrt( 2.0 * o.sigmaxy * o.sigmaxy ) ;
    o.sepfac     = Math.exp( -0.5 * i.dsep * i.dsep / ( o.sigmay * o.sigmay )) ;
    o.lumipbp    = i.nbch * i.nbch * i.frev * Math.cos( i.alpha / 2.0 ) * Math.cos( i.alpha / 2.0 ) * o.sepfac / ( 6.28 * ( o.sigmax * o.sigmay / 1.0e12 ) * 10000 ) ;
    o.mu         = i.xsec * o.lumipbp / i.frev ;
    o.lumi       = i.nbb * o.lumipbp ;
    o.lumint     = o.lumi * i.hubner * i.runtime * 3600.0 * 24.0 / 1.0e36 ;
    o.rate       = o.lumi * i.xsec ;
    o.tau        = 1.15e-4 * i.nbch / ( i.xsec * o.lumipbp )
    o.estored    = i.energy * i.nbch * i.kb * 1.0e9 * 1.6e-19 / 1.0e6 ;
    o.beamspotxy = o.sigmaxy / Math.sqrt(2) ;
    o.beamspotz  = i.sigz / Math.sqrt(2) * o.sfactor * 100; 
    return o;
};

// Numerical integration (Gadaptive integrator based on open 2/4 quadratures)
// acc is absolute accuracy, eps is relative accuracy requested
integ_adapt = function( f,a,b,acc,eps, oldfs ) {
    var x = [1/6,2/6,4/6,5/6];
    var w = [2/6,1/6,1/6,2/6];
    var v = [1/4,1/4,1/4,1/4];
    var p = [1,0,0,1];
    var n = x.length, h = b-a;

    if ( typeof(oldfs) == "undefined" ) {
        var fs = [];
        for (i in x) {
            fs.push( f(a+x[i]*h) );
        }
    } else {
        fs = new Array(n);
        for( var  k=0,i=0; i<n; i++ ) {
            if( p[i] ) {
                fs[i] = f(a+x[i]*h );
            } else {
                //console.log( fs, oldfs );
                fs[i] = oldfs[k++];
            }
        }
    }

    for( var q4=q2=i=0; i<n; i++ ) {
        q4 += w[i] * fs[i]*h;
        q2 += v[i] * fs[i]*h;
    }

    var tol = acc+eps*Math.abs(q4);
    var err = Math.abs(q4-q2)/3;
    //console.log( err, tol );
    if( err < tol)
        return [q4,err];
    else {
        acc /= Math.sqrt(2.);
        var mid = (a+b)/2;
        var left = [];
        for ( i in fs ) 
            if( i<n/2 )
                left.push( fs[i] );
        var rght = [];
        for ( i in fs )
            if ( i >= n/2 )
                rght.push(fs[i])

        var [ql,el] = integ_adapt(f,a,mid,eps,acc,left);
        var [qr,er] = integ_adapt(f,mid,b,eps,acc,rght);

        return [ql+qr, Math.sqrt(el*el+er*er)];
    }
}


/////////////////////////////////////////////////////
    
Glidein = function( divid ) {
    this.div = divid;
    var tmp = $('div#' + divid );
    $('div#' + divid ).addClass( 'glidein' );
    $('div#' + divid ).mouseleave( function(){
        tmp.slideToggle( 500 );
    });
}

Glidein.prototype.toggle = function() {
    $('div#' + this.div).slideToggle( 500 );
}

Glidein.prototype.hide = function() {
    $('div#' + this.div).hide( "slide" );
}

//////////////////////////////////////////////////////

checkIE = function(){
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf( "MSIE " );
    if (msie > 0) {
	alert( "This page only work with Web standards compliant browsers.\n\nInternet Explorer does not belong to this group.\n\nTry to use a recent version of Firefox, Chrome or Safari." );
    }
}
 
//////////////////////////////////////////////////////////

class Luminometer {
    constructor( config ) {
        // must contain:
        // min max title tooltiptext
        // bands [red,orange,yellow,darkgreen] (the upper values for the colors ranges)
        this.config = config;
        this.gauge =  Highcharts.chart( this.config.id, {
            credits: {
                enabled: false
            },
            chart: {
                type: 'gauge',
                height: '130px',
                plotBackgroundColor: null,
                plotBackgroundImage: null,
                plotBorderWidth: 0,
                plotShadow: false
            },
            
            title: {
                text: null,
                useHTML : true
            },
            
            pane: {
                size: '100%',
                startAngle: -130,
                endAngle: 130,
                background: [{
                    backgroundColor: {
                        linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                        stops: [
                            [0, '#FFF'],
                            [1, '#333']
                        ]
                    },
                    borderWidth: 0,
                    outerRadius: '109%'
                }, {
                    backgroundColor: {
                        linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                        stops: [
                            [0, '#333'],
                            [1, '#FFF']
                        ]
                    },
                    borderWidth: 1,
                    outerRadius: '107%'
                }, {
                    // default background
                }, {
                    backgroundColor: '#DDD',
                    borderWidth: 0,
                    outerRadius: '105%',
                    innerRadius: '103%'
                }]
            },
            
            // the value axis
            yAxis: {
                min: this.config.min,
                max: this.config.max,
                
                minorTickInterval: 'auto',
                minorTickWidth: 1,
                minorTickLength: 10,
                minorTickPosition: 'inside',
                minorTickColor: '#666',
                
                tickPixelInterval: 30,
                tickWidth: 2,
                tickPosition: 'inside',
                tickLength: 10,
                tickColor: '#666',
                labels: {
                    step: 2,
                    rotation: 'auto'
                },
                title: {
                    text: this.config.title,
                    y: 19
                },
                plotBands: [{
                    from: 0,
                    to: this.config.bands[0],
                    color: '#DF5353' // red
                }, {
                    from: this.config.bands[0],
                    to: this.config.bands[1],
                    color: '#F8C05D' // orange
                }, {
                    from: this.config.bands[1],
                    to: this.config.bands[2],
                    color: '#eDeF0D' // yellow
                }, {
                    from: this.config.bands[2],
                    to: this.config.bands[3],
                    color: '#55BF3B' // darkgreen
                }, {
                    from: this.config.bands[3],
                    to: this.config.max,
                    color: '#55FF3B' // lightgreen
                }]
            },
            
            series: [{
                name: this.config.tooltiptext,
                data: [0.0],
                tooltip: {
                    valueSuffix: ' ' + this.config['unit']
                }
            }]
        });
    };

    // data contains the float value in fb-1 for the luminosity
    update( data ) {
        var point = this.gauge.series[0].points[0];
        if ( data ) 
            point.update( parseFloat(data.toPrecision(3)) );
    };
};
