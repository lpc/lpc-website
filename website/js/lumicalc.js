debug = function( txt ) {
    $('div#debug').append( txt + "<br>");
}

error = function( txt ) {
    $('div#error').html( txt + "<br>");
}

clearerror = function() {
    $('div#error').html( "" );
}

// Philosophy: If a new preset is chosen by the user or if the page is
// constructed first the default values are loaded and then the presets
// overwrite the defaults. (This helps to keep the presets readable and
// short)
// The defaults object is also used to populate the table with the input
// values in the html file.

var defaults = { 'nbch'    : { 'value' : "1.4e11",
			       'unit'  : "nuclei",
			       'abbrev': "N",
			       'descr' : "Bunch population" },
		 'nbb'     : { 'value' : 2736,
			       'unit'  : "",
			       'abbrev': "n<sub>bb</sub>",
			       'descr' : "Colliding bunch pairs" },
		 'kb'      : { 'value' : 2748,
			       'unit'  : "",
			       'abbrev': "k<sub>b</sub>",
			       'descr' : "Number of bunches per beam" },
		 'frev'    : { 'value' : 11245,
			       'unit'  : "Hz",
			       'abbrev': "f<sub>rev</sub>",
			       'descr' : "Revolution frequency" },
		 'energy'  : { 'value' : 6800,
			       'unit'  : "GeV",
			       'abbrev': "E",
			       'descr' : "Beam energy" },
		 'mass'    : { 'value' : 0.938,
			       'unit'  : "GeV",
			       'abbrev': "m",
			       'descr' : "Particle mass" },
		 'epsn'    : { 'value' : "2.3e-6",
			       'unit'  : "m",
			       'abbrev': "&epsilon;<sub>n</sub>",
			       'descr' : "Normailised transverse emittance" },
		 'beta'    : { 'value' : 0.3,
			       'unit'  : "m",
			       'abbrev': "&beta;*",
			       'descr' : "Beta*" },
		 'alpha'   : { 'value' : 320,
			       'unit'  : "&mu;rad",
			       'abbrev': "&alpha;",
			       'descr' : "<strong>Full</strong> crossing angle (xz)" },
		 'dsep'    : { 'value' : 0.0,
			       'unit'  : "&mu;m",
			       'abbrev': "d",
			       'descr' : "Separation in separation plane" },
		 'sigz'    : { 'value' : 0.08994,
                               'defaultix' : 0,
                               'selectFunc' : "bunchLengthUnit",
			       'unit'  : ["m","ns"],
			       'abbrev': "&sigma;<sub>z</sub>",
			       'descr' : "Bunch length",
                               'remark': "[m] : rms, &nbsp;&nbsp;&nbsp;  [ns] : 4&sigma;" },
		 'xsec'    : { 'value' : 80,
			       'unit'  : "mb",
			       'abbrev': "&sigma;<sub>proc</prob>",
			       'descr' : "Process cross section" },
		 'runtime' : { 'value' : 160,
			       'unit'  : "days",
			       'abbrev': "T",
			       'descr' : "Planned run time" },
		 'hubner'  : { 'value' : 0.2,
			       'unit'  : "",
			       'abbrev': "H",
			       'descr' : "H&uuml;bner factor" }
	       };

var presets = { 'default' : { },
		'pp - Atlas/CMS 60cm' : { 'beta'    : { 'value' : 0.6 } },
		'pp - Atlas/CMS 30cm' : { 'beta'    : { 'value' : 0.3 },
					  'nbch'    : { 'value' : "1.2e11" }
					},
		'pp - LHCb'      : { 'alpha'   : { 'value' : -678 },
                                     'nbb'     : { 'value' : 2376 },
				     'dsep'    : { 'value' : 43 },
                                     'beta'    : { 'value' : 2 } },
		'pp - ALICE'     : { 'alpha'   : { 'value' : 400 },
                                     'nbb'     : { 'value' : 2250 },
				     'dsep'    : { 'value' : 258 },
                                     'beta'    : { 'value' : 10 } },
                'PbPb 2015'      : { 'energy'  : { 'value' : 522340 },
                                     'mass'    : { 'value' : 193.7 },
                                     'epsn'    : { 'value' : 1.5 } },
				'PbPb 2023' : { 'nbch'    : {'value': "1.8e8"},
					            'energy'  : {'value': 536000 },
							    'mass'    : {'value': 193.7 },
								'nbb'     : {'value': 1088},
								'kb'      : {'value': 1240},
                                'epsn'    : { 'value' : "1.65e-6"},
								'xsec'    : {'value' : 7670},
								'alpha'   : {'value' : 196},
								'beta'    : { 'value' : 0.5}}
	      };


////////////////////////// INITIALIZATION //////////////////////////////////


// This function populates the table with the input values for the
// calculator, then the select element for the presets.
// Then it loads the default settings into the input elements. 
// Finally it populates the select elements of the graphical plotter
// which allow to choose the x-axis and the two y-axis variables. 
initialize = function() {
    fillParaTable();
    loadSelect();
    loadsettings( defaults );
    loadxyselect();
}

bunchLengthUnit = function( element ) {
    var unit = $('select#sigzunit option:selected').val();
    var value = $('input#sigz').val();

    if (unit == "ns") {
        value = (value * 40 / 2.99792).toPrecision(4);
    } else if ( unit == "m" ) {
        value = (value / 40 * 2.99792).toPrecision(4);
    }

    $('input#sigz').val( value );
    
    var xname = $('select#in_1_select option:selected').val();
    if (xname == 'sigz') {
	$('span#unit_xmin').html( unit );
	$('span#unit_xmax').html( unit );
	var minval = $('input#min_1').val();
	var maxval = $('input#max_1').val();
	if( unit == "ns" ) {
	    minval = (minval * 40 / 2.99792).toPrecision(4);
	    maxval = (maxval * 40 / 2.99792).toPrecision(4);
	} else if( unit == "m" ) {
	    minval = (minval / 40 * 2.99792).toPrecision(4);
	    maxval = (maxval / 40 * 2.99792).toPrecision(4);
	}
	$('input#min_1').val( minval );
	$('input#max_1').val( maxval );
    }
    
} 


// This function is called during initialization and is populating the table
// with the input values for the calculator. Input and output values are contained
// in one single table (to have a nice alignment) but they are kept in distinct
// tbody sections. This function appends to the section for the inputs.
fillParaTable = function() {
    var item;
    for ( key in defaults ) {
        var item  = defaults[key];
        var remark = "";
        if ( "remark" in item ) {
            remark = item.remark;
        }
        if ( "defaultix" in item ) {
            var ix = item.defaultix;
            var html = "<tr><td>" + item.descr + "</td><td>" + item.abbrev + "</td><td> = </td><td><input type='text' id='" + key + "' value='" + item.value + "'/> <select id='"+key+"unit' onChange='" +item.selectFunc+ "()'>";
            for ( var io = 0; io<item.unit.length; io++ ) {
                html +=  "<option value='"+ item.unit[io]+"'>"+item.unit[io]+"</option>";
            }
            html += "</select></td><td>"+remark+"</td></tr>\n" ;
            $('tbody#inputvalues').append( html );
        } else {
	    $('tbody#inputvalues').append(
	        "<tr><td>" + item.descr + "</td><td>" + item.abbrev + "</td><td> = </td><td><input type='text' id='" + key + "' value='" + item.value + "'/> " + item.unit + "</td><td>"+remark+"</td></tr>\n" );
        }
    }
}

// This populates the "select" element with the presets
// according to the available presets data structure
// defined above.
loadSelect = function() {
    var preset;
    var ps = document.getElementById( "preset_select" );
    for (preset in presets) {
	var opt = $('<option/>');
	opt.attr( {'value' : preset} ).text( preset );
	$('select#preset_select').append( opt );
    }
}

// sets the values of the input fields in the HTML form
// according to the values of the presets object. Make
// sure that the bunch population and the normalized
// emittance are displayed in exponential notation.
loadsettings = function ( presets ) {
    var input;
    for ( input in presets ) {
	$('input#'+input).val(presets[input].value);
    }
};

// This function populates the select elements in the graphical plotter:
// Two selects for the two possible y-axis (left and right) and one select
// for the x-axis. 
loadxyselect = function() {
    var inp = getInput();
    // Just to get dynamically all possible output values we do a calculation
    var out = calcLumi( inp );
    var prop = "";
    var ys = $('select#out_1_select');
    for (prop in out) {
	var opt = $('<option/>');
	opt.attr( {'value': prop} ).text( prop );
	if (prop == 'lumi'){
	    opt.prop( 'selected', 'selected');
	}
	ys.append( opt );
    }
    var y2s = $('select#out_2_select');
    for (prop in out) {
	var opt = $('<option/>');
	opt.attr( {'value': prop} ).text( prop );
	if (prop == 'mu'){
	    opt.prop( 'selected', 'selected');
	}
	y2s.append( opt );
    }
    var xs = $('select#in_1_select');
    for (prop in inp) {
	var opt = $('<option/>');
	opt.attr( {'value' : prop } ).text(prop);
	if ( prop == 'nbch' ) {
	    opt.prop( 'selected' , 'selected' );
	}
	xs.append( opt );
	xchange();
    }	
}

///////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////// Graphical helper callbacks /////////////////////////////////

// This callback makes sure that if the select element for the
// x-axis in the plotter changes, the unit names in the min and
// max input fields are adapted accordingly.
xchange = function( element ) {
    var xname = $('select#in_1_select option:selected').val();
    var unit = defaults[xname].unit;
    if ( $.isArray( unit ) ) {
	var unit = $('select#'+xname+'unit option:selected').val();
    }
    $('span#unit_xmin').html( unit );
    $('span#unit_xmax').html( unit );

    // load default values from main calclator
    $('input#min_1').val( $('input#'+xname).val() ); 
    $('input#max_1').val( $('input#'+xname).val() ); 
}

// Callback when the user changes the presets: gets the
// name of the selected presets, sets all values to default
// first and then overwrites the values of the preset into
// the input fields of the HTML form. 
presetSelected = function( element ) {
    ps = $('select#preset_select option:selected').val();
    loadsettings( defaults );
    loadsettings( presets[ ps ] );
};

///////////////////////////////////////////////////////////////////////////////////////////

///////////////////////// Functions to do the lumi calculations ///////////////////////////

// Gets the input parameters from the HTML input fields as an object, 
// passes this object to the calcLumi function, and sets all output
// values according to the result object from the calculation.
calculate = function() {
    var inp = getInput();
    var out = calcLumi( inp );
    setOutput( out );
    return out;
}

// Get the values from the inputs in the HTML form and put
// them in a Javascript object.
getInput = function() {
    var inp = {}
    $('tbody#inputvalues input').each( function( index ) {
	inp[this.id] = this.value;
    });
    // special treatment of sigz
    var unit = $('select#sigzunit option:selected').val();
    if (unit == "ns") {        
        inp.sigz = (inp.sigz / 40 * 2.99792).toPrecision(4);
        //debug ("transunted to " + inp.sigz );
    }
    return inp;
}

// Set the output values on the page according to the values
// in the object "out"
setOutput = function( out ) {
    var prop;
    for ( prop in out ) {
	$('#'+prop).html( out[prop].toPrecision(4) );
    }
}

// Do the calculations. Input and output are Javascript Objects. 
calcLumi = function( i ) {
    var o = {};
    
    o.gamma      = i.energy / i.mass ;
    o.eps        = i.epsn / o.gamma ;
    o.sigmaxy    = Math.sqrt( o.eps * i.beta ) * 1.0e6 ;
    o.sigmax     = Math.sqrt( 2.0 * o.sigmaxy * o.sigmaxy * Math.cos( i.alpha / 2.0e6 ) * Math.cos( i.alpha / 2.0e6 ) + 2.0e12 * i.sigz * i.sigz * Math.sin( i.alpha / 2.0e6 ) * Math.sin( i.alpha / 2.0e6 ) ) ;
    o.sfactor    = Math.sqrt(2.0) * o.sigmaxy / o.sigmax ;
    o.sigmay     = Math.sqrt( 2.0 * o.sigmaxy * o.sigmaxy ) ;
    o.sepfac     = Math.exp( -0.5 * i.dsep * i.dsep / ( o.sigmay * o.sigmay )) ;
    o.lumipbp    = i.nbch * i.nbch * i.frev * Math.cos( i.alpha / 2.0e6 ) * Math.cos( i.alpha / 2.0e6 ) * o.sepfac / ( 6.28 * ( o.sigmax * o.sigmay / 1.0e12 ) * 10000 ) ;
    o.mu         = i.xsec * 1.0e-27 * o.lumipbp / i.frev ;
    o.lumi       = i.nbb * o.lumipbp ;
    o.lumint     = o.lumi * i.hubner * i.runtime * 3600.0 * 24.0 / 1.0e36 ;
    o.rate       = o.lumi * i.xsec / 1.0e27 ;
    o.tau        = 1.15e-4 * i.nbch / ( i.xsec * 1.0e-27 * o.lumipbp )
    o.estored    = i.energy * i.nbch * i.kb * 1.0e9 * 1.6e-19 / 1.0e6 ;
    o.beamspotxy = o.sigmaxy / Math.sqrt(2) ;
    o.beamspotz  = i.sigz / Math.sqrt(2) * o.sfactor * 100; 
    return o;
};
////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////// Create a lumi-plot ///////////////////////////////////////

// This function creates a plot: It first retrieves from the
// input elements of the graphical plotter the y-variables to be
// plotted and the x-variable with its min and max limits. Then
// it calculates 100 values for the varying x-values adn pushes
// them into the data structures which then are used to create
// the highcharts plot.
createplot = function() {
    clearerror();
    var xvar = $('select#in_1_select option:selected').val();
    var yvar = $('select#out_1_select option:selected').val();
    var y2var = $('select#out_2_select option:selected').val();
    var minstr = $('input#min_1').val();
    var maxstr = $('input#max_1').val();
    if ( minstr == "" || maxstr == "" ) {
	error( "No min or max given!" );
	return;
    }
    var min = parseFloat($('input#min_1').val());
    var max = parseFloat($('input#max_1').val());
    if ( max <= min ) {
	error("max has to be larger than min");
	return;
    }
    
    var step = (max-min)/100.0;
    var x = parseFloat(min);
    var i;
    plotdata = [];
    plot2data = [];
    var inp = getInput();
    var unit = $('select#sigzunit option:selected').val();
    for ( i = 0; i < 101; i++ ) {
	inp[ xvar ] = x;
	if (xvar == 'sigz' && unit == 'ns') {
	    inp[ xvar ] = x / 40 * 2.99792;
	}
	var out = calcLumi(inp);
	plotdata.push( [x,out[yvar]] );
	plot2data.push( [x,out[y2var]] );
	x = x + step;
    }

    // Do the actual highcharts plot
    $(function() {
	$('div#lumiplot').highcharts({
            credits: {
                enabled: false
            },
	    title : {
		text: "Simple Lumi plot"
	    },
	    xAxis: {
		title : { 
		    text: xvar
		},
		labels : {
		    formatter : function() {
			return this.value.toPrecision(4);
		    }
		},
		gridLineWidth: 1
	    },
	    yAxis: [ 
		{
		    title : {
			text: yvar,
			style: {
			    color : Highcharts.getOptions().colors[1]
			}
		    },
		    labels : {
			formatter : function() {
			    return this.value;
			},
			style: {
			    color : Highcharts.getOptions().colors[1]
			}
		    }
		},
		{
		    title : {
			text: y2var,
			style: {
			    color : Highcharts.getOptions().colors[0]
			}
		    },
		    labels : {
			formatter : function() {
			    return this.value;
			},
			style: {
			    color : Highcharts.getOptions().colors[0]
			}
		    },
		    opposite: true
		}
	    ],
	    plotOptions : {
		line : {
		    lineWidth : 4,
		    marker : {
			enabled : false
		    }
		}
	    },
	    legend : {
		enabled : false
	    },
	    series: [ 
		{
		    name: 'plot1',
		    data: plotdata,
		    color: Highcharts.getOptions().colors[1],
		    yAxis: 0
		},
		{
		    name: 'plot2',
		    data: plot2data,
		    color: Highcharts.getOptions().colors[0],
		    yAxis: 1
		}
	    ],
	});
    });
}

////////////////////////////////////////////////////////////////////////////////////////
/////////////////////// functions to retrieve / load all settings //////////////////////

getSettings = function() {
    var inputs = [];
    $('input, select').each( function() {
	var id = $(this).attr('id');
	var val = $(this).val();
	inputs.push( id + " : " + val );
    });

    $('div#currentSettings').html('');
    for (var ix=0; ix<inputs.length; ix++ ) {
	$('div#currentSettings').append( inputs[ix]+';' );
    }
    $('textarea#usersettings').val('')
    $('div#settings').show( 'blind' );
}

loadSettings = function() {
    var setstr = $('textarea#usersettings').val();
    //debug (setstr);
    items = setstr.split(';');
    for (var ix=0; ix<items.length; ix++) {
	it = items[ix].split(':');
	$('#'+$.trim(it[0])).val($.trim(it[1]));
    }
    $('div#settings').hide( 'blind' );
   
}
