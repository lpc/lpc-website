var pmarkers; // global which holds the processed markers
var markers = undefined; // variable which either has empty markers or points to processed markers.
var fillData = {};
var fillTable = {};
var fillRatData = {};
var fillBSData = {};
var fillDataInteg = {};
var fillDataIntegOpt = {};
var processingcnt = 0;
var fillbsplot = "";
var fillbwplot = "";
var fillanaplot = "";
var fillanaplot2 = "";
var activeTab = "tab-lumi";
var pdata = undefined;
var infodata;
var filltabwindow = undefined;
var rattabwindow = undefined;
var bswindow = undefined;
var unit = "";
var dataProcessor;
var optiFillLengthHi;

// html for the fill tables:
var fillTab;
var ratFillTab;
var bsFillTab;

debug = function( txt ) {
    $('div#debug').append( txt + "<br>");
}
error = function( txt ) {
    $('div#error').html( txt );
}
clear_error = function() {
    $('div#error').html("");
}

dataExtraction = function( node ) {
    return $(node).text().replace( /[^0-9\.]/g,"" ); 
}

dateSliderChanged = function( e, data ) {
    if ( processingcnt > 0 ){
	pdata = dataProcessor.processData( data.values.min.getTime(), data.values.max.getTime() );
        var year = $('select#yearselect').val();
	makeplots( pdata, year );
    }
    processingcnt += 1;
}


tabactivate = function( event, ui ) {
    panel = ui.newPanel.attr('id');
    if ( panel == "tab-fillanalysis") {
        $('div#dateslider').hide();
	if ( filltabwindow == undefined ){
	    filltabwindow = window.open('', '', 'location=no,toolbar=0,status=0,width=1000,height=600,left=20,top=20');
	    filltabwindow.document.open().write( fillTab );	
	} else if ( filltabwindow.closed ) {
            alert( "Sorry, you closed the Fill table. You have to reload the page to re-open it. Do not close the Fill table window as long as you might need it.");
        }
    } else if ( panel == "tab-cmsatlas") {
        $('div#dateslider').hide();
	if( rattabwindow == undefined ) {
	    rattabwindow = window.open('', 'rattabwindow', 'location=no,toolbar=0,status=0,width=1000,height=600,left=20,top=20');
	    rattabwindow.document.open().write( ratFillTab );	
	} else if( rattabwindow.closed ) {
            alert( "Sorry you closed the Fill table. You have to reload the page to re-open it. Do not close the Fill table window as long as you might need it.");
        }
    } else if ( panel == "tab-beamspot") {
        $('div#dateslider').hide();
	if( bswindow == undefined ) {
	    bswindow = window.open('', 'bswindow', 'location=no,toolbar=0,status=0,width=1000,height=600,left=20,top=20');
	    bswindow.document.open().write( bsFillTab );
	} else if( bswindow.closed ) {
            alert( "Sorry you closed the Fill table. You have to reload the page to re-open it. Do not close the Fill table window as long as you might need it.");
        }
    } else {
        $('div#dateslider').show();
    }
    
    activeTab = panel;
    var year = $('select#yearselect').val();
    makeplots( undefined, year );
}

handleBSReply = function( fill, cmd ) {
    var data = fillBSData[ fill ];
    var yaxis = 0;
    var coordinate = 'x';
    if ( cmd == 'bsz' ){
        yaxis = 1;
        coordinate = 'z';
    } else if ( cmd == 'bsy' ) 
        coordinate = 'y';
    else if ( cmd == 'bszu' ){
	yaxis = 1;
        coordinate = 'zsu';
    }
    else if ( cmd == 'bsyu' ) 
        coordinate = 'ysu';
    else if ( cmd == 'bsxu' ) 
        coordinate = 'xsu';
    
    var bsdat = {};
    if ('ATLAS' in data) {
	bsdat['ATLAS']=data.ATLAS.fill_lumireg;
    }
    if ('CMS' in data) {
        bsdat['CMS']=data.CMS.fill_lumireg;
    }
    if ('LHCb' in data) {
	bsdat['LHCb']=data.LHCb.fill_lumireg;
    }
    if ('ALICE' in data) {
	bsdat['ALICE']=data.ALICE.fill_lumireg;
    }


    for (var exp in bsdat) {
        //console.log( exp );
        if ( bsdat[exp] ) {
            var time = bsdat[exp].time.map( function(x) { return x-bsdat[exp].time[0]; } ) ;
            var plotdat = time.map( function( a, ix ) {
                return [ a/3600, bsdat[exp][coordinate][ix]];
            });
            //console.log( 'plotdat', plotdat );
	    if(cmd == 'bszu' || cmd == 'bsyu' || cmd == 'bsxu')
	    {
		window.opener.fillbwplot.addData( { 'name' : fill + "_" + cmd + "_" + exp,
                                                    'data' : plotdat,
			                            'yAxis' : yaxis,
                                                    'lineWidth' : 1,
                                                    'legend' : {'floating' : true, 'layout':'horizontal' },
                                                    'marker': { 'radius' : 2 }
						  });
	    }
	    else
	    {
		window.opener.fillbsplot.addData( { 'name' : fill + "_" + cmd + "_" + exp,
                                                    'data' : plotdat,
			                            'yAxis' : yaxis,
                                                    'lineWidth' : 1,
                                                    'legend' : {'floating' : true, 'layout':'horizontal' },
                                                    'marker': { 'radius' : 2 }
						  });
	    }
        }                                  
    }
}

handleRatReply = function( fill, cmd ) {
    var data = fillRatData[fill];
    var yaxis = 0;
    //console.log( data );
    //console.log( "handle cmd ", cmd);
    try {
	if (cmd == 'zrawatlas') {
            var dat = data.ATLAS.fill_lumireg;
	    var time = dat.time.map( function(x){ return x-dat.time[0] } );
	    var plotdat = time.map( function( a, ix ) {
		return [a/3600, dat.zsu[ix]];
	    });
	} else if( cmd == 'zrawcms' ) {
            var dat = data.CMS.fill_lumireg	
	    var time = dat.time.map( function(x) { return x-dat.time[0];});
	    var plotdat = time.map( function( a, ix ) {
		return [a/3600, dat.zsu[ix]] ;
	    });
	} else if ( cmd == 'zatlas' ) {
            var dat = data.lureg_z_analysis
	    var time = dat.time.map( function(x) {return x-dat.time[0];});
	    var plotdat = time.map( function( a, ix ) {
		return [a/3600, dat.dat2[ix]];
	    });
	} else if ( cmd == 'zcms' ) {
            var dat = data.lureg_z_analysis
	    var time = dat.time.map( function(x){ return x-dat.time[0];});
	    var ix = 0;
	    var plotdat = time.map( function( a, ix ) {
		ix++;
		return [a/3600, dat.dat1[ix-1]];
	    });
	} else if ( cmd == 'zrat' ) {
	    var dat = data.lureg_z_analysis;
	    var time = dat.time.map( function(x) {return x-dat.time[0];} );
	    var ix = 0;
	    var plotdat = time.map( function( a, ix) {
		ix++;
		return [a/3600, dat.ratio[ix-1]];
	    });
	    yaxis = 1;
	} else {
            error( "No such command: " + cmd );
            return;
	}
	
	window.opener.fillratplot.addData( { 'name' : fill + "_" + cmd,
					     'data' : plotdat,
					     'yAxis' : yaxis,
					     'lineWidth' : 1,
					     'legend' : {'floating' : true, 'layout':'horizontal' },
					     'marker': { 'radius' : 2 }
					   });
    }
    catch(typeerror) {
	console.log("No data found");
	error( "No data for "+cmd);
    }
}

reduceData = function( arr ) {

    var minchange = 0.01; // %
    var newarr = [];
    var newarri = [];
    newarr.push( arr[0] );
    refix = 0;
    for ( var ix=1; ix<arr.length; ix++ ) {
        // calculate change to ref
        if ( arr[refix][2] == 0 ) {
            refix = ix;
            continue;
        }
        var change = Math.abs((arr[ix][2] - arr[refix][2]) / arr[refix][2]);
        if (change >= 0.005) {
            if ( ix - refix > 1 ) {
                newarr.push( arr[ix-1] );
            }
            newarr.push( arr[ix] );
            refix = ix;
            newarri.push( arr[ix] );
        }
    }
    //console.log( "reduction: from ", arr.length, " to ", newarr.length );
    return [newarr, newarri];
}


// Fill the histograms with lumi for each fill
handleReply = function( data, name, newdata ) {
    //console.log( 'handle reply');
    if ( newdata ) {
        if ( data.fillData.action == "fillData" ) {
            // need to extract columns 0 and 2
            var arr = data.fillData.data;
            
            var res = reduceData( arr );           
            arr = res[0];
            arri = res[1];
            //arri = arr;
            var col0 = arr.map( function(x) { return x[0]/3600.; } );
            var col1 = arr.map( function(x) { return x[1]; } );
            var col2 = arr.map( function(x) { return x[2]; } );
            var plotdat = col0.map( function( a, ix ) {
                return [ a-col0[0], col2[ix]*col1[ix] ];
            });

            
            var col0 = arri.map( function(x) { return x[0]/3600.; } );
            var col1 = arri.map( function(x) { return x[1]; } );
            var col2 = arri.map( function(x) { return x[2]; } );
            var integ = 0;
            var plotdat2 = col0.map( function( a, ix ) {
                var dt = 0;
                if ( ix < (col0.length-1) ) {
                    dt = col0[ix+1]-col0[ix];
                }
                integ += (col2[ix] * dt * 3600 * col1[ix]);
                return [ a-col0[0], integ/1000000.0 ];
            });

            integ = 0;
            var turnaround = parseFloat($('input#turnaround', window.opener.document).val());
            var plotdat3 = col0.map( function( a, ix ) {
                var dt = 0;
                if ( ix < (col0.length-1) ) {
                    dt = col0[ix+1]-col0[ix];
                }
                integ += (col2[ix] * dt * 3600 * col1[ix]);
                var time = a-col0[0] + turnaround;
                //console.log('time ', time);
                return [ a-col0[0], integ/1000000.0/time ];
            });
            //console.log( 'name', name);
            fillData[name] = { 'type': 'scatter',
			       'name': name,
			       'data': plotdat,
                               'legend' : {'floating' : true, 'layout':'horizontal' },
                               'marker': { 'radius' : 2 }
                             };
            fillDataInteg[name] = { 'type': 'scatter',
			       'name': name,
			       'data': plotdat2,
                               'legend' : {'floating' : true, 'layout':'horizontal' },
                               'marker': { 'radius' : 2 }
                             };
            fillDataIntegOpt[name] = { 'type': 'scatter',
			       'name': name,
			       'data': plotdat3,                                       
                               'legend' : {'floating' : true, 'layout':'horizontal' },
                               'marker': { 'radius' : 2 },
                               'yAxis' : 1
                             };
        }
    }
    window.opener.fillanaplot.addData( fillData[name] );
    window.opener.fillanaplot2.addData( fillDataInteg[name] );
    window.opener.fillanaplot2.addData( fillDataIntegOpt[name] );
}

newOptimalFillLength = function() {
    
}

makeplots = function( procdata, year ) {

    //console.log( "makeplots zrathisto ",procdata.zrathisto );

    // new data needs to be processed
    if ( procdata) {
        // decide on the units:
        totrange = (pdata.filllumis.ATLAS.totlumi + pdata.filllumis.CMS.totlumi)/2
        unit = "ub<sup>-1</sup>";
        var scale = 1;
        if ( totrange > 1000000000 ) {
            unit = "fb<sup>-1</sup>";
            scale = 1000000000;
        } else if ( totrange > 1000000 ) {
            unit = "pb<sup>-1</sup>";
            scale = 1000000;
        } else if ( totrange > 1000 ) {
            unit = "nb<sup>-1</sup>";
            scale = 1000;
        }


        pdata.filllumis.ATLAS.totlumi /= scale;
        pdata.filllumis.CMS.totlumi /= scale;
        pdata.filllumis.LHCb.totlumi /= scale;
        pdata.filllumis.ALICE.totlumi /= scale;

        pscale = 10000;
        for ( var i=0; i<pdata.filllumis.ATLAS.lumivec.data.length; i++ ) {
            pdata.filllumis.ATLAS.lumivec.data[i][1] /= scale;
            pdata.filllumis.CMS.lumivec.data[i][1] /= scale;
            pdata.filllumis.LHCb.lumivec.data[i][1] /= scale;
            pdata.filllumis.ALICE.lumivec.data[i][1] /= scale;
            pdata.filllumis.ATLAS.lumivect.data[i][1] /= scale;
            pdata.filllumis.CMS.lumivect.data[i][1] /= scale;
            pdata.filllumis.LHCb.lumivect.data[i][1] /= scale;
            pdata.filllumis.ALICE.lumivect.data[i][1] /= scale;
            
            //pdata.peaklumis.ATLAS.lumivec.data[i][1] /= pscale;
            //pdata.peaklumis.ATLAS.lumivec.data[i][1] = Math.max(pdata.peaklumis.ATLAS.lumivec.data[i][1], 0.0001);
            //pdata.peaklumis.CMS.lumivec.data[i][1] /= pscale;
            //pdata.peaklumis.CMS.lumivec.data[i][1] = Math.max(pdata.peaklumis.CMS.lumivec.data[i][1], 0.0001);
            //pdata.peaklumis.LHCb.lumivec.data[i][1] /= pscale;
            //pdata.peaklumis.LHCb.lumivec.data[i][1] = Math.max(pdata.peaklumis.LHCb.lumivec.data[i][1], 0.0001);
            //pdata.peaklumis.ALICE.lumivec.data[i][1] /= pscale;
            //pdata.peaklumis.ALICE.lumivec.data[i][1] = Math.max(pdata.peaklumis.ALICE.lumivec.data[i][1], 0.0001);
            //
            //pdata.peaklumis.ATLAS.lumivect.data[i][1] /= pscale;
            //pdata.peaklumis.ATLAS.lumivect.data[i][1] = Math.max(pdata.peaklumis.ATLAS.lumivec.data[i][1], 0.0001);
            //pdata.peaklumis.CMS.lumivect.data[i][1] /= pscale;
            //pdata.peaklumis.CMS.lumivect.data[i][1] = Math.max(pdata.peaklumis.CMS.lumivec.data[i][1], 0.0001);
            //pdata.peaklumis.LHCb.lumivect.data[i][1] /= pscale;
            //pdata.peaklumis.LHCb.lumivect.data[i][1] = Math.max(pdata.peaklumis.LHCb.lumivec.data[i][1], 0.0001);
            //pdata.peaklumis.ALICE.lumivect.data[i][1] /= pscale;
            //pdata.peaklumis.ALICE.lumivect.data[i][1] = Math.max(pdata.peaklumis.ALICE.lumivec.data[i][1], 0.0001);

            pdata.peaklumis.ATLAS.lumivec.data[i][1] /= pscale;
            pdata.peaklumis.ATLAS.lumivec.data[i][1] = pdata.peaklumis.ATLAS.lumivec.data[i][1];
            pdata.peaklumis.CMS.lumivec.data[i][1] /= pscale;
            pdata.peaklumis.CMS.lumivec.data[i][1] = pdata.peaklumis.CMS.lumivec.data[i][1];
            pdata.peaklumis.LHCb.lumivec.data[i][1] /= pscale;
            pdata.peaklumis.LHCb.lumivec.data[i][1] = pdata.peaklumis.LHCb.lumivec.data[i][1];
            pdata.peaklumis.ALICE.lumivec.data[i][1] /= pscale;
            pdata.peaklumis.ALICE.lumivec.data[i][1] = pdata.peaklumis.ALICE.lumivec.data[i][1];

            pdata.peaklumis.ATLAS.lumivect.data[i][1] /= pscale;
            pdata.peaklumis.ATLAS.lumivect.data[i][1] = pdata.peaklumis.ATLAS.lumivec.data[i][1];
            pdata.peaklumis.CMS.lumivect.data[i][1] /= pscale;
            pdata.peaklumis.CMS.lumivect.data[i][1] = pdata.peaklumis.CMS.lumivec.data[i][1];
            pdata.peaklumis.LHCb.lumivect.data[i][1] /= pscale;
            pdata.peaklumis.LHCb.lumivect.data[i][1] = pdata.peaklumis.LHCb.lumivec.data[i][1];
            pdata.peaklumis.ALICE.lumivect.data[i][1] /= pscale;
            pdata.peaklumis.ALICE.lumivect.data[i][1] = pdata.peaklumis.ALICE.lumivec.data[i][1];
        }

        pdata.filllumis.ATLAS.lumivec.name = "ATLAS : " + pdata.filllumis.ATLAS.totlumi.toPrecision(4) + " " + unit
        pdata.filllumis.CMS.lumivec.name = "CMS : " + pdata.filllumis.CMS.totlumi.toPrecision(4) + " " + unit
        pdata.filllumis.LHCb.lumivec.name = "LHCb : " + pdata.filllumis.LHCb.totlumi.toPrecision(3) + " " + unit
        pdata.filllumis.ALICE.lumivec.name = "ALICE : " + pdata.filllumis.ALICE.totlumi.toPrecision(3) + " " + unit
        pdata.filllumis.ATLAS.lumivect.name = "ATLAS : " + pdata.filllumis.ATLAS.totlumi.toPrecision(4) + " " + unit
        pdata.filllumis.CMS.lumivect.name = "CMS : " + pdata.filllumis.CMS.totlumi.toPrecision(4) + " " + unit
        pdata.filllumis.LHCb.lumivect.name = "LHCb : " + pdata.filllumis.LHCb.totlumi.toPrecision(3) + " " + unit
        pdata.filllumis.ALICE.lumivect.name = "ALICE : " + pdata.filllumis.ALICE.totlumi.toPrecision(3) + " " + unit
        
    }
    
    // Now do the plots

    if ( $('input#noannotations').is(':checked') ) {
        markers = { 'time' : { lines : [],
                               bands : [] },
                    'fill' : { lines : [],
                               bands : [] }
                  };
    } else {
        markers = pmarkers;
    }
    
    
    if ( $('input#lplotxunits').is(':checked') ) {
        var xaxistype = 'linear';
        var ms = markers.fill;
        var xtitle = "Fill Number";
        var dataselstr = 'lumivec';
        var ratdat = pdata.ratio_fill_lumi.fill;
        var pratdat = pdata.ratio_peak_lumi.fill;
        var acclumiratio = pdata.atlas_cms_acc_ratio.fill;
        var turnaround = pdata.turnaround.fill;
        var sbDuration = pdata.sbDuration.fill;
        var zbosonratio = pdata.zrathisto.fill;
        var zbosonratioerr = pdata.zrathisto.fillerr;
        var acczbosonratio = pdata.zrathisto.fillacc;
   } else {
        var xaxistype = 'datetime';
        var xtitle = "Date";
        var dataselstr = 'lumivect';
        var ms = markers.time;
        var dataselstr = 'lumivect';
        var ratdat = pdata.ratio_fill_lumi.time;
        var pratdat = pdata.ratio_peak_lumi.time;
        var acclumiratio = pdata.atlas_cms_acc_ratio.time;
        var turnaround = pdata.turnaround.time;
        var sbDuration = pdata.sbDuration.time;
        var zbosonratio = pdata.zrathisto.time;
        var zbosonratioerr = pdata.zrathisto.timeerr;
        var acczbosonratio = pdata.zrathisto.timeacc;
    }


    if ( activeTab == "tab-lumi" ) {
        var opleg = { 'layout' : "vertical",
                      'floating' : true,
                      'bwidth' : 1,
                      'align' : 'left',
                      'verticalAlign' : 'top',
                      'title' : {'text' : "Preliminary",
                                 'style' : {"fontSize" : "140%" }},
                      'x' : 107,
                      'y' : 51}

        var oplegbr = { 'layout' : "vertical",
                        'floating' : true,
                        'bwidth' : 1,
                        'align' : 'right',
                        'verticalAlign' : 'bottom',
                        'title' : {'text' : "Preliminary",
                                   'style' : {"fontSize" : "140%" }},
                        'x' : -6,
                        'y' : -51 }



        //console.log("makers", xmarkers);

        flumiplot = new Scatterplot( {
            xmarkers: ms.lines,
            xbands: ms.bands,
            lineWidth: 2,
            exportWidth: 700,
            exportHeight: 600,
            exportScale: 2,
            xtype: xaxistype,
            title : "Delivered Luminosity " + year,
            xtit : xtitle,
            ytit : "Delivered integ. luminosity [" + unit + "]",
            data : [pdata.filllumis.ATLAS[dataselstr], 
                    pdata.filllumis.CMS[dataselstr], 
                    pdata.filllumis.LHCb[dataselstr], 
                    pdata.filllumis.ALICE[dataselstr]],
	    legend : true,
            'opleg' : opleg,
	    avg : 0 });
        flumiplot.plot( 'lumiplot1' );
        
        flumiplotlog = new Scatterplot( {
            exportHeight: 600,
            exportWidth: 700,
            exportScale: 2,
            xmarkers: ms.lines,
            xbands: ms.bands,
            lineWidth: 2,
            xtype: xaxistype,
            title : "Delivered Luminosity " + year,
            xtit : xtitle,
            ytit : "Delivered Integ. luminosity [" + unit + "]",
            ytype : "logarithmic",
            min : 0.00001, 
            data : [pdata.filllumis.ATLAS[dataselstr], 
                    pdata.filllumis.CMS[dataselstr], 
                    pdata.filllumis.LHCb[dataselstr], 
                    pdata.filllumis.ALICE[dataselstr]],
	    legend : true,
            'opleg' : opleg,
	    avg : 0 });        
        flumiplotlog.plot( 'lumiplot2' );
        

        plumiplot = new Scatterplot( {
            exportScale: 2,
            exportHeight: 600,
            exportWidth: 700,
            xmarkers: ms.lines,
            xbands: ms.bands,
            xtype: xaxistype,
            title : "Peak Luminosity in 'Stable Beams'",
            xtit : xtitle,
            ytit : "Luminosity [10<sup>34</sup> cm<sup>-2</sup> s<sup>-1</sup>]",
            data : [pdata.peaklumis.ATLAS[dataselstr], 
                    pdata.peaklumis.CMS[dataselstr], 
                    pdata.peaklumis.LHCb[dataselstr], 
                    pdata.peaklumis.ALICE[dataselstr]],
	    legend : true,
            'opleg' : opleg,
	    avg : 0 });
        plumiplot.plot( 'lumiplot3' );
        
        plumiplotlog = new Scatterplot( {
            exportScale: 2,
            exportHeight: 600,
            exportWidth: 700,
            xmarkers: ms.lines,
            xbands: ms.bands,
            xtype: xaxistype,
            title : "Peak Luminosity in 'Stable Beams'",
            xtit : xtitle,
            ytit : "Luminosity [10<sup>34</sup> cm<sup>-2</sup> s<sup>-1</sup>]",
            ytype: "logarithmic",
            //min: 0.001,
            data : [pdata.peaklumis.ATLAS[dataselstr], 
                    pdata.peaklumis.CMS[dataselstr], 
                    pdata.peaklumis.LHCb[dataselstr], 
                    pdata.peaklumis.ALICE[dataselstr]],
	    legend : true,
            'opleg' : opleg,
	    avg : 0 });
        plumiplotlog.plot( 'lumiplot4' );

        
        lpflumiplot1 = new Scatterplot( {
            xmarkers: ms.lines,
            xbands: ms.bands,
//            lineWidth: 2,
            exportWidth: 700,
            exportHeight: 600,
            exportScale: 2,
            xtype: xaxistype,
            title : "Luminosity per Fill " + year,
            xtit : xtitle,
            ytit : "Fill luminosity [pb]",
            data : [pdata.lpflumis.ATLAS[dataselstr], 
                    pdata.lpflumis.CMS[dataselstr]],
	    legend : true,
            'opleg' : opleg,
	    avg : 0 });
        lpflumiplot1.plot( 'lumiplot5' );
        lpflumiplot2 = new Scatterplot( {
            xmarkers: ms.lines,
            xbands: ms.bands,
//            lineWidth: 2,
            exportWidth: 700,
            exportHeight: 600,
            exportScale: 2,
            xtype: xaxistype,
            title : "Luminosity per Fill " + year,
            xtit : xtitle,
            ytit : "Fill luminosity [pb]",
            data : [pdata.lpflumis.LHCb[dataselstr], 
                    pdata.lpflumis.ALICE[dataselstr]],
	    legend : true,
            'opleg' : opleg,
	    avg : 0 });
        lpflumiplot2.plot( 'lumiplot6' );

        
        lpfhisto = new Histogram( 50, 0, 100 );
        makelpfhisto( lpfhisto, pdata, dataselstr );
        
    }        


    if( activeTab == "tab-lumicmp") {

        flumiratplot = new Scatterplot( { 
            min: 0.8, 
            max: 1.2, 
            xmarkers: ms.lines,
            xbands : ms.bands,
            xtype : xaxistype,
            xtit : xtitle,
            title : "Integrated Fill Lumi Ratio: ATLAS / CMS",
            ytit : "Ratio",
            legend: true,
            data : [ { type: 'scatter',
                       data : ratdat,
                       color: 'blue',
                       name: "fillratio" },
                     { type: 'line',
                       data: acclumiratio,
                       color: "#00a000",
                       name: "accumulated ratio ATLAS/CMS" } ],
        } );
        flumiratplot.plot( 'plot1' );
        
        plumiratplot = new Scatterplot({ 
            min: 0.8, 
            max: 1.2, 
            xmarkers: ms.lines,
            xbands: ms.bands,
            xtype : xaxistype,
            xtit : xtitle,
            title : "Peak Fill Lumi Ratio: ATLAS / CMS",
            xtit : "Fill Number",
            ytit : "Ratio",
            data : pratdat,
            avg : pdata.avg_peak_lumi });
        plumiratplot.plot( 'plot2' );
    }


    if( activeTab == "tab-turnaround" ) {

        turnaroundplot = new Scatterplot ( {
            title : "Turn Around Times (for all fills < 48h)",
            xtit : xtitle,
            xtype : xaxistype,
            ytit : "Turn Around Time[h]",
            xmarkers : ms.lines,
            xbands : ms.bands,
            data : turnaround,
            avg : pdata.avg_turnaround,
            median : pdata.turnaround_median } );
        turnaroundplot.plot( 'turnaround_1' );
        
        
        var turnAroundHi = new Histogram( 48, 0, 48 );
        for(var i=0; i<pdata.turnaround.fill.length; i++ ) {
	    turnAroundHi.insert( pdata.turnaround.fill[i][1] );
        }
        turnAroundHi.plotHisto('turnaround_2', 
                               { title : "Turn Around Time (for all fills < 48h)",
                                 xtit : "Turn Around Time [h] ",
                                 ytit : "Entries",
		                 avg  : turnAroundHi.getMean(),
                                 median: pdata.turnaround_median } );

        optiFillLengthHi = new Histogram(40, 0, 20);
        for( var i=0; i<pdata.optfilllength.fill.length; i++ ) { 
            optiFillLengthHi.insert( pdata.optfilllength.fill[i][1] / 3600 );
        }
        optiFillLengthHi.plotHisto( 'turnaround_3',
                                    { title : "Optimal fill length for selected turn around time",
                                      xtit: "Fill Length [h]",
                                      ytit: "Entries" }
                                  );
        
    }


    if ( activeTab == "tab-stablebeams" ) {
        var tms = markers.time;
        var sbTimeplot = new Timeplot( { title : "Stable Beams  [" + (pdata.sbfrac*100).toFixed(1) + "%]", 
				         xtit : "Date",
				         ytit : "Fraction [%]",
				         min : 0,
				         max : 1,
				         begin : pdata.startUnixTime,
				         end : pdata.endUnixTime,
				         xranges : pdata.sbranges,
                                         xbands : tms.bands,
                                         xmarkers : tms.lines,
 				         data : pdata.stableBeamfrac_days
				       }); 
        
        sbTimeplot.plot("stablebeams_1", { glavg : 7 });
        sbDurationplot = new Scatterplot ( {
            title : "Stable Beams Duration",
            xtit : xtitle,
            xtype : xaxistype,
            ytit : "Stable Beams Duration[h]",
            xmarkers : ms.lines,
            xbands : ms.bands,
            data : sbDuration,
            //avg : pdata.avg_sbDuration }
        });
        sbDurationplot.plot( 'stablebeams_2' );
        
        var sbDurationDistribution = new Histogram( 48, 0, 48, [ {name: 'unforeseen dump'}, {name : 'operator dump'} ] );
        
        sbDurationDistribution.insert( 0, 0 );
        sbDurationDistribution.insert( 0, 1 );
        for( var i=0; i<pdata.sbDuration.fill.length; i++ ) {
            var ix = 0;
            if ( fillTable[ pdata.sbDuration.fill[i][0] ].end == 'operator' ){
                ix = 1;
            }
            // If this rounding is not done, Highcharts does not correctly stack the histograms. Bug in highcharts? 
            var rounded = Math.round( 100 * pdata.sbDuration.fill[i][1] ) / 100.0; 
            //var rounded = pdata.sbDuration.fill[i][1]; 
            sbDurationDistribution.insert( rounded, ix );
            //console.log( "insert ", rounded, ix );
        }
        $('div#opstat').html( 'Operator : ' +  sbDurationDistribution.nentry[1] + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Others : ' + sbDurationDistribution.nentry[0]);
        sbDurationDistribution.plotHisto( 'stablebeams_3', 
                                          { title : "Stable Beams Duration Distribution",
                                            xtit : "Stable Beams Duration [h]",
                                            ytit : "Entries",
                                            avg : sbDurationDistribution.getMean(),
                                            median: sbDurationDistribution.getMedian(),
                                            showLegend: true });
    }

    if  ( activeTab == "tab-zcounter" ) {
        //console.log("zcounter");
        zratioplot = new Scatterplot( {
            title : "Z ratio ATLAS CMS",
            xtit: xtitle,
            xtype: xaxistype,
            ytit: "Z-yield ATLAS/CMS per fill",
            min: 0.8, 
            max: 1.4, 
            xmarkers: ms.lines,
            xbands : ms.bands,
	    legend : true,
            data: [ { type: 'errorbar',
		      data: zbosonratioerr,
		      color: 'blue',
		      name: "Errors"},
	            { type: 'scatter',
		      data: zbosonratio,
		      color: 'blue',
		      name: "Z count ratio"},
		     { type: 'scatter',
                       data : ratdat,
                       color: 'red',
                       name: "Lumi ratio" },
		    { type: 'line',
                      data: acczbosonratio,
                      color: "#00a000",
                      name: "accumulated lumi-weighted ratio ATLAS/CMS" }],
        });
        zratioplot.plot( 'zcounting_rat' );
                                    
    }

}

processFillRatSelection = function( e, year ) {
    var fillexp = $(e.target).val();
    var cmp = fillexp.split('_');
    var fillnr = cmp[0];
    var cmd = cmp[1];
    //console.log("zrat cmd", fillexp, cmd);

    if ($(e.target).is( ':checked' ) ) {
        if ( fillnr in fillRatData ) {
            handleRatReply( fillnr, cmd );            
        } else {
            url = (window.location.href.substr(0,window.location.href.lastIndexOf('/')));
            //path = "https://lpc.web.cern.ch/lpc/cgi-bin"
            url +=  '/cgi-bin/fillZRatAnalysis.py?year=' + year + 
                '&action=fillCache&fillnr=' + fillnr +
                '&cmd=' + cmd;
            $.getJSON( url, function( reply ) {
                //console.log( "rat reply", reply );
                fillRatData[fillnr] = reply.data.data                
                handleRatReply( fillnr, cmd );
            });
        }
    } else {
        //debug( "deleted " + fillnr );
        window.opener.fillratplot.deleteSeries( fillexp );
    }

}

processFillBSSelection = function( e, year ) {
    //console.log( 'processfillbsselection' );
    var fillexp = $(e.target).val();
    var cmp = fillexp.split('_');
    var fillnr = cmp[0];
    var cmd = cmp[1];
    //console.log("cmd", cmd);
    
    if ($(e.target).is( ':checked' ) ) {
        if ( fillnr in fillBSData ) {
            handleBSReply( fillnr, cmd );            
        } else {
            url = (window.location.href.substr(0,window.location.href.lastIndexOf('/')));
            url +=  '/cgi-bin/fillZRatAnalysis.py?year=' + year + 
                '&action=fillCache&fillnr=' + fillnr +
                '&cmd=' + cmd;
            //console.log( 'url', url );
            $.getJSON( url, function( reply ) {
                //console.log( reply );
                fillBSData[fillnr] = reply.data.data
                handleBSReply( fillnr, cmd );
            });
        }
    } else {
        //debug( "deleted " + fillnr );
	if(cmd == 'bszu' || cmd == 'bsyu' || cmd == 'bsxu')
	{
	    window.opener.fillbwplot.deleteSeries( fillexp + "_ATLAS" );
            window.opener.fillbwplot.deleteSeries( fillexp + "_CMS" );
            window.opener.fillbwplot.deleteSeries( fillexp + "_LHCb" );
            window.opener.fillbwplot.deleteSeries( fillexp + "_ALICE" );
	}
	else
	{
	    window.opener.fillbsplot.deleteSeries( fillexp + "_ATLAS" );
            window.opener.fillbsplot.deleteSeries( fillexp + "_CMS" );
            window.opener.fillbsplot.deleteSeries( fillexp + "_LHCb" );
            window.opener.fillbsplot.deleteSeries( fillexp + "_ALICE" );

	}
        //window.opener.fillratplot.deleteSeries( fillexp );
    }

}

getFillTable = function( year ) {
    //var url = (window.location.href.substr(0,window.location.href.lastIndexOf('/')));
    url = 'cgi-bin/fillTableReader.py?action=load&year='+year;
    //console.log( url );
    $.getJSON( url, function( reply ) {
        //console.log( reply );
        fillTable = reply.data;
    });
}

processFillSelection = function(e, year) {
    //#debug( e );
    //debug( e.target );
    //debug( $(e.target).val() ) ;

    var fillexp = $(e.target).val();
    var cmp = fillexp.split('_');
    var fillnr = cmp[0];
    var exp = cmp[1];

    if ($(e.target).is( ':checked' ) ) {
        if ( fillexp in fillData ) {
            handleReply( fillData.fillexp, fillexp );
        } else {
            url = (window.location.href.substr(0,window.location.href.lastIndexOf('/')));
            //path = "https://lpc.web.cern.ch/lpc/cgi-bin"
	    //url = "https://lpc-eos.web.cern.ch/lpc-eos/cgi-bin" # - doesnt work because of CORS limitation, arghh
            url +=  '/cgi-bin/fillAnalysis.py?year=' + year + 
                '&action=fillData&fillnr=' + fillnr +
                '&exp=' + exp;
            $.getJSON( url, function( reply ) {                
                handleReply( reply.data, fillexp, 1 );
            });
        }
    } else {
        //debug( "deleted " + fillnr );
        window.opener.fillanaplot.deleteSeries( fillexp );
        window.opener.fillanaplot2.deleteSeries( fillexp );
    }
}

changeDateSlider = function( period ) {
    var searchParams = new URLSearchParams(window.location.search);
    searchParams.set('runtype',period)
    history.pushState({},"","/plots.html?"+searchParams.toString());

    if ( period == "protons" ) {
        //console.log(dataProcessor);
        $('#dateslider').dateRangeSlider( "values",
					  new Date( dataProcessor.start_pp ).setHours(0),
					  new Date( dataProcessor.end_pp ).setHours(23)
					);
    } else if ( period == "ions" ) {
 	$('#dateslider').dateRangeSlider( "values",
					  new Date( dataProcessor.start_ions ).setHours(0),
					  new Date( dataProcessor.end_ions ).setHours(23)
					);
    }
}


loaddata = function(year) {
    markers = undefined;
    var searchParams = new URLSearchParams(window.location.search);
    searchParams.set('year',year)
    history.pushState({},"","/plots.html?"+searchParams.toString());
    getFillTable( year );

    //console.log( 'fill table' , fillTable );
    url = "cgi-bin/getLumiData.py?year=" + year 
    
    $.getJSON( url, function( data ) {

        //console.log('getlumidata',  data );
        pmarkers = processMarkers( data.markers );

        infodata = data.infodata;
        dataProcessor = new DataProcessor( data );
	fillTab = dataProcessor.fillTableHTML("filltable");
	ratFillTab = dataProcessor.fillTableHTML("ratfilltab");
	bsFillTab = dataProcessor.fillTableHTML("bsfilltab");
	//bwFillTab = dataProcessor.fillTableHTML("bwfilltab");
        pdata = dataProcessor.processData();
        dataProcessor.analysePeriods();
	// set the date slider limits
	$('#dateslider').dateRangeSlider( "option","bounds",
					  { min: new Date( pdata.startUnixTime ),
					    max: new Date( pdata.endUnixTime + 3600000*72 ) }
					);

        changeDateSlider( $('select#periodselect').val() );

        $('#dateslider').bind("valuesChanged", dateSliderChanged );

        makeplots(pdata, year);
	// generate the fill rat plot for analysis
        fillratplot = new Scatterplot( {
            exportScale: 1.5,
            exportHeight: 900,
            title : "Luminous region in Z",
	    ytit2: "LumReg Z ratio CMS/ATLAS",
            xtit : "Time [h]",
            ytit : "Luminous region [mm]", 
	    ytit2: "LumReg Z ratio CMS/ATLAS",
	    min2: 0.95,
	    max2: 1.15,
	    legend : true });
 	fillratHS = fillratplot.plot( 'cmsatlas_rat1' );
            

	// generate the fill beamspot plot for analysis
        fillbsplot = new Scatterplot( {
            exportScale: 1.5,
            exportHeight: 900,
            title : "Beamspot x y z",
            xtit : "Time [h]",
            ytit : "x,y-position [mm]", 
	    ytit2: "z-position [mm]",
	    legend : true });
 	fillbsHS = fillbsplot.plot( 'bsposition' );

	fillbwplot = new Scatterplot( {
            exportScale: 1.5,
            exportHeight: 900,
            title : "Beamspot width x y z",
            xtit : "Time [h]",
            ytit : "x,y-width [mm]", 
	    ytit2: "z-width [mm]",
	    legend : true });
	fillbwHS = fillbwplot.plot( 'bswidth' );
        
	// generate the fill lumi plot for the analysis
	fillanaplot = new Scatterplot( {
            exportScale: 2.0,
            exportHeight: 600,
            exportWidth: 900,
            title : "Instantaneous Fill Lumi",
            xtit : "Time [h]",
            ytit : "Instant lumi [ub Hz]",
            lineWidth : 3,
	    legend : true });
 	fillanaHS = fillanaplot.plot( 'fillana_1' );

	fillanaplot2 = new Scatterplot( {
            exportScale: 1.5,
            exportHeight: 900,
            title : "Integrated Fill Lumi",
            xtit : "Time [h]",
            ytit : "Integrated lumi [pb<sup>-1</sup>]", 
            ytit2 : "Lumi per hour", 
            lineWidth : 3,
	    legend : true });
 	fillanaHS2 = fillanaplot2.plot( 'fillana_2' );

    })
        .fail( function(jqXHR, text, error) {
            $('div#contents').append("error<br>");
            $('div#contents').append(text + "<br>");
            $('div#contents').append(error + "<br>");
        });
}

rgbacolor = function( color, opacity ) {
    var r = parseInt(color.substring( 1,3 ),16);
    var g = parseInt(color.substring( 3,5 ),16);
    var b = parseInt(color.substring( 5,7 ),16);
    ret = 'rgba(' + r + ',' + g + ',' + b + ',' + opacity + ')';
    //console.log( color,ret )
    return ret;
}


processMarkers = function( raw_markers ) {

    //markers is a global variable
    var markers = { 'time' : { lines : [],
                           bands : [] },
                'fill' : { lines : [],
                           bands : [] }
              };
    
    //console.log('makrers', markers);
    for ( var ix=0; ix<raw_markers.length; ix++ ) {
        var m = raw_markers[ix];
        if ( m.end_date ) {
            //plotband;
            var nb_t = { 'color' : rgbacolor(m.color, m.opacity),
                         'label' : {
                             'text' : m.label,
			     'rotation' : 90,
			     'align' : 'left',
			     'textAlign' : 'left'
                         },
                         'from' : Date.parse(m.start_date + " " + m.start_time),
                         'to' : Date.parse(m.end_date + " " + m.end_time)
                       };
            var nb_f = { 'color' : rgbacolor(m.color, m.opacity),
                         'label' : {
                             'text' : m.label,
			     'rotation' : 90,
			     'align' : 'left',
			     'textAlign' : 'left'
                         },
                         'from' : m.start_fillno,
                         'to' : m.end_fillno
                       };
            markers.time.bands.push( nb_t );
            markers.fill.bands.push( nb_f );
        } else {
            // plotlines
            var nm_t = { 'width' : 3,
                         'dashStyle' : "shortdash",
                         'zIndex' : 5,
                         'color' : rgbacolor(m.color, m.opacity),
                         'label' : {
                             'text' : m.label
                         },
                         'value' : Date.parse(m.start_date + " " + m.start_time)
                       };
            var nm_f = { 'width' : 3,
                         'dashStyle' : "shortdash",
                         'zIndex' : 5,
                         'color' : rgbacolor(m.color, m.opacity),
                         'label' : {
                             'text' : m.label
                         },
                         'value' : m.start_fillno
                     };
            markers.time.lines.push( nm_t );
            markers.fill.lines.push( nm_f );
        }        
    }
    return markers;
}
    


updateOptimalTa = function() {
    var optfl = dataProcessor.updateOptimalFl();

    optiFillLengthHi.clear();

    for( var i=0; i<optfl.fill.length; i++ ) { 
        optiFillLengthHi.insert( optfl.fill[i][1] / 3600 );
    }
    optiFillLengthHi.plotHisto( 'turnaround_3',
                                { title : "Optimal fill length for selected turn around time",
                                  xtit: "Fill Length [h]",
                                  ytit: "Entries",
                                  }
                              );
}


makelpfhisto = function(lpfhisto, pdata, dataselstr) {
    var exp = $("select#lpfhistoselector").val();
    //console.log( exp, dataselstr );
    //console.log( pdata.lpflumis );
    //console.log( pdata.lpflumis[exp][dataselstr].data );
    var dat = pdata.lpflumis[exp][dataselstr].data;
    if ( exp == 'ALICE' ) {
        // need to divide by 100
        dat = [];
        for ( var i=0; i<pdata.lpflumis[exp][dataselstr].data.length; i++ ){
            dat.push( [pdata.lpflumis[exp][dataselstr].data[i][0], pdata.lpflumis[exp][dataselstr].data[i][1]/100.] );
            //dat[i][1] = dat[i][1]/100.;
        }
    }
    //console.log( dat );
    max = 0;
    for ( var i=0; i<dat.length; i++ ){
        if (dat[i][1]>max) {
            max = dat[i][1];
            //console.log( 'max',i,max );
        }
    }

    var hmax = max;
    var n=1;
    while ( hmax/10. > 1 ) {
        hmax = hmax / 10.;
        n = n*10;
    }

    
    var hmax =  hmax  * (n+1);
    //console.log('hmax' , max,hmax);

    lpfhisto.clear( 25, 0, hmax );

    for ( var i=0; i<dat.length; i++ ){
        //console.log( i, dat[i][1] );
        lpfhisto.insert( dat[i][1] );
    }
    lpfhisto.plotHisto( "lumiplot7", { 'title' : exp + ' lumi distribution per fill',
                                       'xtit' : "Luminosity [pb]",
                                       'ytit' : "Entries",
                                       'showLegend' : false,
                                       'avg' : lpfhisto.getMean()
                                     }
                      );
    var avg = lpfhisto.getMean();
    var median = lpfhisto.getMedian();

    $('div#lpfstat').html( "<p>" + lpfhisto.getEntries() + " Fills </p> <p> Mean : " + avg.toPrecision(3) + " pb<sup>-1</sup><br>Median : " + median.toPrecision(3) +" pb<sup>-1</sup></p>");
}
