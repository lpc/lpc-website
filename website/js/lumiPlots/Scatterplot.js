Highcharts.setOptions({
    plotOptions: {
        series: {
            animation: false
        }
    }
});

Scatterplot = function( params ) {
    this.title = params.title;
    this.xtit = params.xtit;
    this.ytit = params.ytit;
    this.ytype = 'linear';
    this.ytit2 = ''
    this.ytype2 = 'linear';
    this.data = [];
    this.chart = undefined;
    this.avg = undefined;
    this.min = undefined;
    this.max = undefined;
    this.min2 = undefined;
    this.max2 = undefined;
    this.data = undefined;
    this.legend = false;
    this.opleg = {};
    this.exportWidth = 1200;
    this.exportHeight = 500;
    this.exportScale = 1.0;
    this.lineWidth = 0;
    this.xtype = 'linear';
    if ( 'xtype' in params ) this.xtype = params.xtype;
    if ( 'exportWidth' in params ) this.exportWidth = params.exportWidth;
    if ( 'exportHeight' in params ) this.exportHeight = params.exportHeight;
    if ( 'exportScale' in params ) this.exportScale = params.exportScale;
    if ( 'min' in params ) this.min = params.min;
    if ( 'max' in params ) this.max = params.max;
    if ( 'min2' in params ) this.min2 = params.min2;
    if ( 'max2' in params ) this.max2 = params.max2;
    if ( 'avg' in params ) this.avg = params.avg;
    if ( 'lineWidth' in params ) this.lineWidth = params.lineWidth;
    if ( 'legend' in params ) this.legend = params.legend;
    if ( 'opleg' in params ) this.opleg = params.opleg;
    if ( 'data' in params ){
	this.data = params.data;
    }
    if ( 'ytype' in params ) this.ytype = params.ytype;
    if( 'ytit2' in params ) this.ytit2 = params.ytit2;
    if ( 'ytype2' in params ) this.ytype2 = params.ytype2;

    this.xmarkers = [];
    if ( 'xmarkers' in params ) {
        this.xmarkers = params.xmarkers;
    }
    this.xbands = [];
    if ( 'xbands' in params ) {
        this.xbands = params.xbands;
    }
}

Scatterplot.prototype.deleteSeries = function( name ) {
    var chart = $('div#' + this.divid ).highcharts();
    for (var ix = 0; ix <= chart.series.length; ix++ ) {
        if ( chart.series[ix].name == name ) {
            chart.series[ix].remove();
            break;
        }
    }
}

Scatterplot.prototype.addData = function( data ) {
    this.data = data ;
    var chart = $('div#' + this.divid ).highcharts();
    chart.addSeries(data, redraw=false);
    //console.log( "set axis");
    //chart.xAxis[0].setExtremes( data.data[0][0], data.data[ data.data.length-1 ][0] );
    //console.log( "now redraw");
    chart.redraw( animation = false );
    //chart.series[0].setData([[0,4],[3,4],[7,4]]); //works
}

Scatterplot.prototype.plot = function( divid ) {
    var tp = this;
    this.divid = divid;
    // Do the actual highcharts plot
    if ( tp.data === undefined ) {
	var series = [];
    }
    else if ( 'name' in tp.data[0] ) {
	var series = tp.data;
    } else {
	var series = [
	    {
		type: 'scatter',
                name: divid,
		data: tp.data,
		color: "blue"
	    }];
    }

    var yaxarr = [
    	{
	    title : {
		text: tp.ytit,
                useHTML: true,
                align : "high",
                offset : 70,
                style: {
                    "font-size":"130%",
                    "color" : "black"
                }
	    },
            type : tp.ytype,
            plotLines: plotlines,
            max : tp.max,
            min : tp.min,
            labels: {
                style: {
                    "font-size":"120%"
                }
            }
	}
    ]

    if ( tp.ytit2 != "" ) {
	yaxarr.push( {
	    title : {
		text: tp.ytit2,
                useHTML: true,
                align : "high",
                offset : 70,
                style: {
                    "font-size":"130%",
                    "color" : "black"
                }
	    },
	    opposite : true,
            type : tp.ytype2,
            max : tp.max2,
            min : tp.min2,
            labels: {
                style: {
                    "font-size":"120%"
                }
            }
	});
    }
    var opleg = {
        'align' : 'center',
        'bwidth' : 0,
        'floating' : false,
        'layout' : 'horizontal',
        'align' : 'center',
        'verticalAlign' : 'bottom',
        'x' : 0,
        'y' : 0,
        'title' : { text : "" },
    }
    $.each(tp.opleg, function(key,value){
        opleg[key] = value;
    });


    var plotlines = [];
    if (this.avg) {
	plotlines.push( {
            color : "#606060",
            value : tp.avg,
            dashStyle: "shortdash",
            width: 3,
            zIndex: 4,
            label : {
                text: "AVG : " + tp.avg.toPrecision(4),
                style: {
                    "font-size": "150%",
                    "font-weight": "bold",
                    "color" : "#606060",
                },
                y : -10
            }
        });
    }
    var theChart = undefined;
    $(function() {
	theChart = $('div#' + divid ).highcharts({
            credits: {
                enabled: false
            },
            exporting: {
                sourceWidth: tp.exportWidth,
                sourceHeight: tp.exportHeight,
                scale: tp.exportScale,
                chartOptions: {
                    yAxis: {
                        labels : {
                            style : {
                                'font-size': '200%'
                            },
                            y : 10
                        },
                        title : {
                            style : {
                                'font-size': '200%'
                            },
                            'margin': 200
                        }
                    },                    
                    xAxis : {
                        labels : {
                            style : {
                                'font-size': '200%'
                            },
                            y : 30},
                        title : {
                            style : {
                                'font-size': '200%'
                            }
                        },
                        tickPixelInterval: 200
                    },
                    legend: {
                        useHTML : true,
                        itemStyle : {
                            'font-size': '180%'
                        },
                        title : {
                            style : {
                                'font-size': '180%'
                            }
                        },
                        padding : 20
                    },
                    title: {
                        style : {
                            'font-size': '230%',
                            'margin-top' : '14px'
                        },
                    }
                },
            },

            chart: {
                type: 'scatter',
                zoomType: 'xy',
                plotBorderColor: '#808080',
                plotBorderWidth: 2
            },
	    title : {
		text: tp.title,
                style: {
                    "color": "blue",
                    "font-size": "200%"
                }
	    },
            tooltip: {
                formatter: function() {
                    if ( this.x > 999999 ) {
                        var dat = new Date(this.x);
                        var xstr = dat.getDate() + "/" + (parseInt(dat.getMonth()) + 1) + "/" + dat.getFullYear();
                    } else {
                        var xstr = this.x;
                    }
                    return  xstr +' : '+ this.y.toPrecision(4);
                }
            },
	    xAxis: {
                type: tp.xtype,
		title : { 
		    text: tp.xtit,
                    align: 'high',
                    style: {
                        "font-size":"130%",
                        "color" : "black"
                    }
		},
		gridLineWidth: 1,
                plotLines: tp.xmarkers,
                plotBands: tp.xbands,
                labels: {
                    style: {
                        "font-size":"120%"
                    },
                },
	    },
	    yAxis: yaxarr,
	    plotOptions : {
		scatter : {
		    lineWidth : tp.lineWidth,
		    marker : {
			enabled : true,
                        radius: 4
		    }
		}
	    },
	    legend : {
                useHTML : true,
		enabled : tp.legend,
                backgroundColor : 'rgba(240,240,240,0.7)',
                borderWidth : opleg.bwidth,
                floating : opleg.floating,
                layout: opleg.layout,
                x : opleg.x,
                y : opleg.y,
                itemStyle: {
                    fontSize: '16px'
                },
                title: opleg.title,
                align: opleg.align,
                verticalAlign: opleg.verticalAlign
                
	    },
	    'series': series,
	});
    });

    this.chart = theChart;
    return this.chart;
}
