DataProcessor = function (data) {
    //console.log( 'create data processor with', data);
    this.rawData = data.filldata;
    this.infoData = data.infodata;
    this.zratioData = data.zratio;
    this.turnaroundData = data.turnaround;
    this.stopDateUnix = 0;
    this.fillselect = true;
}

DataProcessor.prototype.tableHeader = function( tabid ) {
    var header = '<!DOCTYPE html><html><head><title>Fill Table</title><link rel="stylesheet" type="text/css" href="../css/lpc.css"> <link rel="stylesheet" type="text/css" href="../css/plots.css"><link rel="stylesheet" type="text/css" href="../js/themes/blue/style.css"><script src="../js/jquery-2.2.0.min.js"></script><script src="../js/jquery.tablesorter.min.js"></script><script src="../js/lumiPlots/plotter.js"></script></head><body>';
    header +=  '<table id="' + tabid + '" class="tablesorter filltable" style="border: 1px solid grey">\
<thead>\
<tr><th>Fill No</th><th>Start</th><th>Length</th>';

    if ( tabid == 'filltable' ) {
        header += '<th>ATLAS &int;</th><th>CMS &int;</th><th>LHCb &int;</th><th>ALICE &int;</th><th>ATLAS peak</th><th>CMS peak</th><th>LHCb peak</th><th>ALICE peak</th></tr>\
</thead>\
<tbody onclick="processFillSelection( event, ' + this.infoData.year + ' )">';
    } else if ( tabid == "ratfilltab" ) {
        header += '<th>ATLAS &int;</th><th>CMS &int;</th><th>z-ratios</th><th>z Atlas<br>(processed)</th><th>z CMS<br>(processed)</th><th>z Atlas<br>(raw)</th><th>z CMS<br>(raw)</th></tr></thead>\
<tbody onclick="processFillRatSelection( event, ' + this.infoData.year + ' )">';
    } else if ( divid = 'bsfilltab' ) {
        header += '<th>x pos</th><th>y pos</th><th>z pos</th><th>x wid</th><th>y wid</th><th>z wid</th></thead>\
<tbody onclick="processFillBSSelection( event, ' + this.infoData.year + ' )">';
    }

    return header;
}

DataProcessor.prototype.tableEnd = function(tabid) {
    return  '</tbody>\n</table>\
<script>\
$("table#' + tabid + '").tablesorter( { sortList:[[0,1]], textExtraction: dataExtraction,  headers: { 0:{sortInitialOrder:"desc"}, 1:{sorter:false} } } )\
</script>\n</body>\n</html>\n';

}

DataProcessor.prototype.fillTableHTML = function( tabid ) {
    var html = this.tableHeader( tabid );

    for ( var j=0; j<this.rawData.length; j++ ) {
        var val = this.rawData[j];
        var hours = (val.stopUnix - val.startUnix)/3600000.0;

        html += '<tr><td>'+ val.fill + '</td><td>' + val.startDateTime + "</td><td>" +
            hours.toPrecision(3) + " hours </td><td>";

        if ( tabid == "filltable" ) {
            html += (parseFloat(val.ATLAS_integ_fillLumi)/1000000).toFixed(1) + ' [pb]<sup>-1</sup><input type="checkbox" value="' + val.fill + '_ATLAS"> </td><td>' +
		(parseFloat(val.CMS_integ_fillLumi)/1000000).toFixed(1) + ' [pb]<sup>-1</sup><input type="checkbox" value="' + val.fill + '_CMS"> </td><td>' +
		(parseFloat(val.LHCb_integ_fillLumi)/1000000).toFixed(2) + ' [pb]<sup>-1</sup><input type="checkbox" value="' + val.fill + '_LHCb"> </td><td>' +
		(parseFloat(val.ALICE_integ_fillLumi)/1000).toFixed(2) + ' [nb]<sup>-1</sup><input type="checkbox" value="'+ val.fill + '_ALICE"> </td><td>' +
		(parseFloat(val.ATLAS_peak_fillLumi)).toFixed(0) + "x 10<sup>30</sup></td><td>" +
		(parseFloat(val.CMS_peak_fillLumi)).toFixed(0) + " x 10<sup>30</sup></td><td>" +
		(parseFloat(val.LHCb_peak_fillLumi)).toFixed(0) + " x 10<sup>30</sup></td><td>" +
		(parseFloat(val.ALICE_peak_fillLumi)).toFixed(3) + " x 10<sup>30</sup></td></tr>"; 
        } else if( tabid == "ratfilltab" ) {
	    html += (parseFloat(val.ATLAS_integ_fillLumi)/1000000).toFixed(1) + ' [pb]<sup>-1</sup> </td><td>' +
		//<input type="checkbox" value="' + val.fill + '_ATLAS"> </td><td>' +
		(parseFloat(val.CMS_integ_fillLumi)/1000000).toFixed(1) + ' [pb]<sup>-1</sup> </td><td>' + 
		//<input type="checkbox" value="' + val.fill + '_CMS"> </td><td>' +
		'<input type="checkbox" value="' + val.fill + '_zrat"> </td><td>' +
                '<input type="checkbox" value="' + val.fill + '_zatlas"> </td><td>' +
                '<input type="checkbox" value="' + val.fill + '_zcms"> </td><td>' +
                '<input type="checkbox" value="' + val.fill + '_zrawatlas"> </td><td>' +
                '<input type="checkbox" value="' + val.fill + '_zrawcms"> </td><tr>';
        } else if ( tabid == "bsfilltab" ) {
 	    html += '<input type="checkbox" value="' + val.fill + '_bsx"> </td><td>' +
                '<input type="checkbox" value="' + val.fill + '_bsy"> </td><td>' +
                '<input type="checkbox" value="' + val.fill + '_bsz"> </td><td>' +
		'<input type="checkbox" value="' + val.fill + '_bsxu"> </td><td>' +
		'<input type="checkbox" value="' + val.fill + '_bsyu"> </td><td>' +
		'<input type="checkbox" value="' + val.fill + '_bszu"> </td><td><tr>';
        }

    }
    html+= this.tableEnd(tabid);

    return html;
}

DataProcessor.prototype.setDates = function( unixtime, name ) {

    this[name + "DateTime"] = new Date(unixtime);
    var tmp = new Date(unixtime);
    tmp.setHours(0);
    tmp.setMinutes(0);
    tmp.setSeconds(0);
    tmp.setMilliseconds(0);
    this[name + "DateUnix"] = tmp.getTime();
    this[name + "Date"] = tmp;
}

DataProcessor.prototype.analysePeriods = function() {
    var info = this.infoData;
    this.start_pp = 0;
    this.start_ions = 0;
    this.end_pp = 0;
    this.end_ions = 0;
    for ( var j=0; j<this.rawData.length; j++ ) {
        var fill = this.rawData[j];
        var filln = parseInt(fill['fill']);
        if (filln == parseInt(info.firstFill_pp) ) {
            this.start_pp = fill['startUnix'];
        } else if ( filln == parseInt(info.firstFill_ions) ) {
            this.start_ions = fill['startUnix'];
        } else if ( filln == parseInt(info.lastFill_pp) ) {
            this.end_pp = fill['stopUnix'] + 3600000*72;
        } else if ( filln == parseInt(info.lastFill_ions) ) {
            this.end_ions = fill['stopUnix'] + 3600000*72;
        }
    }
    //console.log( this );
    if (this.end_pp == 0){
        this.end_pp = this.stopDateUnix + 3600000*72;        
    }
    if (this.end_ions == 0){
        this.end_ions = this.stopDateUnix + 3600000*72;        
    }
}

DataProcessor.prototype.processData = function( startIv, stopIv ) {
    var fills_v = [];
    var ratios_fill_v = { 'time' : [],
                          'fill' : [] };
    var acclumirat_v = { 'time' : [],
                         'fill' : [] };   // accumulated ratio of integrated lumis
    var ratios_peak_v = { 'time' : [],
                          'fill' : [] };
    var ic1 = 0;
    var ic2 = 0;
    var sumfill = 0;
    var sumpeak = 0;
    var pstop = 0; // previous stop time
    var ta_scat_v = { 'fill' : [],
                      'time' : [] };
    var ta_medianv = [];
    var avgta = 0;
    var ita = 0;
    var begin = 0;
    var end = 0;
    var sbsum = 0;
    var sbranges_v = [];
    var daysb_v = [];
    var daylen = 24*3600000;
    var sbDuration_v = { 'time' : [] ,
                         'fill' : [] };
    var sbfills = 0;
    var xcheck = 0;
    var xxcheck = 0;
    var filllumis = {ATLAS :  {totlumi: 0,
			       lumivec: { data : [],
					  name : "ATLAS integrated lumi",
					  color : "black" },
			       lumivect: { data : [],
					   name : "ATLAS integrated lumi",
					   color : "black" },
			      },
		     CMS : { totlumi: 0, 
			     lumivec: { data :[],
					name: "CMS integrated lumi",
					color : "green" },
			     lumivect: { data : [],
					 name : "CMS integrated lumi",
					 color : "green" },
			   },
                     LHCb : { totlumi: 0, 
			      lumivec: { data :[],
					name: "LHCb integrated lumi",
					color : "blue" },
			      lumivect: { data :[],
					  name: "LHCb integrated lumi",
					  color : "blue" }
			    },
		     ALICE : { totlumi: 0, 
			      lumivec: { data :[],
					name: "ALICE integrated lumi",
					color : "red" }, 
			      lumivect: { data :[],
					name: "ALICE integrated lumi",
					color : "red" },
			     },
		    };
    var lpflumis = {ATLAS :  {lumivec: { data : [],
					 name : "ATLAS",
					 color : "black",
                                         type : 'scatter'},
			      lumivect: { data : [],
					  name : "ATLAS",
					  color : "black",
                                          type : 'scatter' },
			     },
		    CMS : { lumivec: { data :[],
				       name: "CMS",
				       color : "green",
                                       type : "scatter" },
			    lumivect: { data : [],
					name : "CMS",
					color : "green",
                                        type : "scatter" },
			  },
                    LHCb : { lumivec: { data :[],
					name: "LHCb",
					color : "blue",
                                        type : "scatter" },
			     lumivect: { data :[],
					 name: "LHCb",
					 color : "blue",
                                         type : "scatter" }
			   },
		    ALICE : { lumivec: { data :[],
					 name: "ALICE x 100",
					 color : "red",
                                         type : "scatter" }, 
			      lumivect: { data :[],
					  name: "ALICE x 100",
					  color : "red",
                                          type : "scatter" },
			    },
		   };
    var peaklumis = {ATLAS :  {lumivec: { data : [],
					  name : "ATLAS",
					  color : "black" },
			       lumivect: { data : [],
					  name : "ATLAS",
					  color : "black" },
			      },
		     CMS : { lumivec: { data :[],
					name: "CMS",
					color : "green" },
		             lumivect: { data :[],
					 name: "CMS",
					 color : "green" }
			   },
                     LHCb : { lumivec: { data :[],
					name: "LHCb",
					color : "blue" },
                              lumivect: { data :[],
					  name: "LHCb",
					  color : "blue" },
			    },
		     ALICE : { lumivec: { data :[],
					name: "ALICE",
					color : "red" },
                               lumivect: { data :[],
					   name: "ALICE",
					   color : "red" },
			     }
		    };

    activeFills = {}
    fillLumiATLASCMS={}
    for ( var j=0; j<this.rawData.length; j++ ) {
        var val = this.rawData[j];
//    $.each( this.rawData, function( ix, val ) {
        var filln = parseInt(val['fill']);
        if ( (val['startUnix'] < startIv ) || ( val['stopUnix'] > stopIv ) ) {
            continue;
        }
        activeFills[filln] = val['startUnix']

        this.setDates( val['startUnix'], 'start' );
        this.setDates( val['stopUnix'], 'stop' );

        
        // Initialise the array of stable beams and set the first day
        // "bday" to calculate the index into the day-array
	if ( begin == 0 ) {
	    begin = this.startDateUnix;
	    bday = Math.floor(begin/daylen);
            //console.log("c bday", bday);
	    var bc = (bday+1) * daylen;
	    for ( var i=0; i<366; i++ ) { 
		daysb_v.push( [bc,0] );
		bc += daylen;
	    }
	}

	sbranges_v.push( [val.startUnix, val.stopUnix] );
	end = this.stopDateUnix;
	
	sblen = val['stopUnix'] - val['startUnix']; // in ms
        sbDuration_v.fill.push([filln, sblen / 3600000] );
        sbDuration_v.time.push([val['startUnix'], sblen / 3600000] );
        sbfills += 1; 
	sbsum += sblen / 3600000;
	startday = Math.floor(this.startDateUnix/daylen)-bday; // first day is 0
	stopday = Math.ceil( this.stopDateUnix/daylen)-bday;   // 

        // start stop are the rounded days as time (ms)
        clow = this.startDateUnix + daylen - val['startUnix']

	for ( var i=startday; i<stopday; i++ ) {
	    sbi = Math.min( sblen, clow);
	    daysb_v[i][1]+=sbi;
            //console.log( i, startday, stopday, sbi );
	    xcheck += sbi;
	    xxcheck += sbi;
	    sblen -= sbi;
	    clow = daylen;
	}

        if ( pstop == 0 ) {
            pstop = this.stopDateUnix;
        } else {
            var taround = (this.startDateTime - pstop)/3600000;
            pstop = this.stopDateTime;
            if ( taround <= 48 ) {
                avgta += taround;
                ta_medianv.push( taround );
                ta_scat_v.fill.push( [filln,taround] );
                ta_scat_v.time.push( [val['startUnix'],taround] );
                ita += 1;
            }
        }

        fills_v.push( filln );
	filllumis.ATLAS.totlumi += val.ATLAS_integ_fillLumi;
	filllumis.CMS.totlumi += val.CMS_integ_fillLumi;
	filllumis.LHCb.totlumi += val.LHCb_integ_fillLumi;
	filllumis.ALICE.totlumi += val.ALICE_integ_fillLumi;
	
	if ( filllumis.CMS.totlumi > 0 ) {
            acclumirat_v.fill.push( [filln, filllumis.ATLAS.totlumi / filllumis.CMS.totlumi] );
            acclumirat_v.time.push( [val['startUnix'], filllumis.ATLAS.totlumi / filllumis.CMS.totlumi] );
        }
        
        //console.log( "fill " , filln, filllumis.CMS.totlumi);

	fillLumiATLASCMS[filln]=(val.ATLAS_integ_fillLumi+val.CMS_integ_fillLumi)/2.;
	
	filllumis.ATLAS.lumivec.data.push( [filln,filllumis.ATLAS.totlumi] ); 
	filllumis.CMS.lumivec.data.push( [filln, filllumis.CMS.totlumi]); 
	filllumis.LHCb.lumivec.data.push( [filln, filllumis.LHCb.totlumi]); 
	filllumis.ALICE.lumivec.data.push( [filln, filllumis.ALICE.totlumi]); 

	filllumis.ATLAS.lumivect.data.push( [val['startUnix'],filllumis.ATLAS.totlumi] );
	filllumis.CMS.lumivect.data.push( [val['startUnix'], filllumis.CMS.totlumi]); 
	filllumis.LHCb.lumivect.data.push( [val['startUnix'], filllumis.LHCb.totlumi]); 
	filllumis.ALICE.lumivect.data.push( [val['startUnix'], filllumis.ALICE.totlumi]); 

	lpflumis.ATLAS.lumivec.data.push( [filln, val.ATLAS_integ_fillLumi/1e6] ); 
	lpflumis.CMS.lumivec.data.push( [filln, val.CMS_integ_fillLumi/1e6]); 
	lpflumis.LHCb.lumivec.data.push( [filln, val.LHCb_integ_fillLumi/1e6]); 
	lpflumis.ALICE.lumivec.data.push( [filln, val.ALICE_integ_fillLumi/1e4]); 
        
	lpflumis.ATLAS.lumivect.data.push( [val['startUnix'], val.ATLAS_integ_fillLumi/1e6] );
	lpflumis.CMS.lumivect.data.push( [val['startUnix'], val.CMS_integ_fillLumi/1e6]); 
	lpflumis.LHCb.lumivect.data.push( [val['startUnix'], val.LHCb_integ_fillLumi/1e6]); 
	lpflumis.ALICE.lumivect.data.push( [val['startUnix'], val.ALICE_integ_fillLumi/1e4]); 

        // some peaklumis are 0 
        peaklumis.ATLAS.lumivec.data.push( [filln, parseFloat(val.ATLAS_peak_fillLumi) ] );
        peaklumis.CMS.lumivec.data.push( [filln, parseFloat(val.CMS_peak_fillLumi) ] );
        peaklumis.LHCb.lumivec.data.push( [filln, parseFloat(val.LHCb_peak_fillLumi) ] );
        peaklumis.ALICE.lumivec.data.push( [filln, parseFloat(val.ALICE_peak_fillLumi) ] );

        peaklumis.ATLAS.lumivect.data.push( [val['startUnix'], parseFloat(val.ATLAS_peak_fillLumi) ] );
        peaklumis.CMS.lumivect.data.push( [val['startUnix'], parseFloat(val.CMS_peak_fillLumi) ] );
        peaklumis.LHCb.lumivect.data.push( [val['startUnix'], parseFloat(val.LHCb_peak_fillLumi) ] );
        peaklumis.ALICE.lumivect.data.push( [val['startUnix'], parseFloat(val.ALICE_peak_fillLumi) ] );
        //peaklumis.ALICE.lumivec.data.push( [filln, parseInt(1) ] );


        var rat = val['ATLAS_integ_fillLumi'] / val['CMS_integ_fillLumi'];
        if (rat == Infinity ) {
            rat = 9999999;
        }
        ratios_fill_v.fill.push( [filln, rat] );
        ratios_fill_v.time.push( [val['startUnix'],rat]);
        if ( rat > 0.8 && rat < 1.2 ) {
            sumfill += rat;
            ic1++;
        }
            
        rat =  val['ATLAS_peak_fillLumi'] / val['CMS_peak_fillLumi'];
        if (rat == Infinity ) {
            rat = 9999999;
        }
        ratios_peak_v.fill.push( [filln,rat] );            
        ratios_peak_v.time.push( [val['startUnix'],rat] );            
        if ( rat > 0.8 && rat < 1.2 ) {
            sumpeak += rat;
            ic2++;
        }
    }

    ta_medianv.sort( function(a,b){return a-b} );
    var vl = ta_medianv.length
    if (vl % 2 == 0) {
       var ta_median = (ta_medianv[ vl/2 - 1 ] + ta_medianv[ vl/2 ] ) / 2.0 ;
    } else {
        var ta_median = ta_medianv[ (vl-1)/2 ];  
    }

    var dsbfrac_v = [];
    for( var i=0; i<366; i++ ) {
	dsbfrac_v.push( [daysb_v[i][0], daysb_v[i][1] / daylen] );
    }

    var avgfill = sumfill / ic1;
    var avgpeak = sumpeak / ic2;

    sbfrac =  sbsum / ((end-begin)/3600000);
    xcheck = xcheck/daylen / Math.ceil((end - begin) / daylen);
    //console.log( "c xxcheck ", xxcheck );
    //console.log( "c xcheck ", xcheck );
    //console.log("c sbsum", sbsum, sbfrac);

    if (this.fillselect) {
        this.fillTaChoices( this.turnaroundData, ta_median );
    }

    // the turn around data
    var optfl = this.updateOptimalFl();
    
    //console.log( "optfl", optfl);

    // finally we need to process the zrat data
    zrathisto = { 'time' : [],
                  'fill' : [],
		  'timeerr' : [],
                  'fillerr' : [],
		  'timeacc': [],
		  'fillacc': []};
    var zratesum=0;
    var zratelumisum=0;
    for (fillno in this.zratioData) {
        if ( (parseInt(fillno) in activeFills) && ('rat' in this.zratioData[fillno]) ) {
            zrathisto.fill.push( [ parseInt(fillno), this.zratioData[fillno].rat] );
            zrathisto.fillerr.push( [ parseInt(fillno), this.zratioData[fillno].rat-this.zratioData[fillno].err, this.zratioData[fillno].rat+this.zratioData[fillno].err] );
            zrathisto.time.push( [activeFills[parseInt(fillno)], this.zratioData[fillno].rat] );
            zrathisto.timeerr.push( [activeFills[parseInt(fillno)], this.zratioData[fillno].rat-this.zratioData[fillno].err, this.zratioData[fillno].rat+this.zratioData[fillno].err] );
	    var lumi=fillLumiATLASCMS[fillno];
	    zratesum+=lumi*this.zratioData[fillno].rat;
	    zratelumisum+=lumi;
	    zrathisto.timeacc.push( [activeFills[parseInt(fillno)],zratesum/zratelumisum]);
	    zrathisto.fillacc.push( [parseInt(fillno),zratesum/zratelumisum]);
        }
    }
        

    var result = { 
	startUnixTime : begin,
	endUnixTime : end,
	stableBeamTime : sbsum,
	sbfrac : sbfrac,
	fills : fills_v,
	ratio_fill_lumi : ratios_fill_v,
	avg_fill_lumi : avgfill,
	ratio_peak_lumi : ratios_peak_v,
        atlas_cms_acc_ratio : acclumirat_v,
	avg_peak_lumi : avgpeak,
	turnaround : ta_scat_v,
        turnaround_median : ta_median,
	avg_turnaround : avgta/ita,
	sbranges : sbranges_v,
	stableBeamfrac_days : dsbfrac_v,
        sbDuration : sbDuration_v,
        avg_sbDuration : sbsum/sbfills,
	filllumis : filllumis,
        lpflumis : lpflumis,
        peaklumis : peaklumis,
        zrathisto : zrathisto,
        optfilllength : optfl
    };
    //console.log("acclumirat", acclumirat_v);
    //console.log( "length ", dsbfrac_v.length );
    //console.log( lpflumis.ATLAS );
    return result;   
}


DataProcessor.prototype.updateOptimalFl = function() {
    var optfl = { 'time' : [],
              'fill' : [] };
    //console.log( 'raw',this.turnaroundData);
    var exp = $('select#taexp').val();
    var ix = $('select#tachoice').val();
    //console.log( 'CHOICES', exp, ix);

    //console.log( this.turnaroundData );

    for (var fillno in this.turnaroundData ) { 
        if ( (parseInt(fillno) in activeFills) ) {
            if ( this.turnaroundData[fillno][exp] == undefined )
                continue
            if ( this.turnaroundData[fillno][exp].fldat[ix]['opt length'] > 0 && 
                 this.turnaroundData[fillno][exp].fldat[ix]['opt length'] == this.turnaroundData[fillno][exp].fldat[ix]['tlpt']  ) {
                optfl.fill.push( [ parseInt(fillno), this.turnaroundData[fillno][exp].fldat[ix]['opt length'] ]);
                optfl.time.push( [activeFills[parseInt(fillno)], this.turnaroundData[fillno][exp].fldat[ix]['opt length'] ] );
            }
        }
    }
    return optfl;
}
    

DataProcessor.prototype.fillTaChoices = function( obj, median ) {

    var tarr = [];
    for ( k in obj ) {
        if( "CMS" in obj[k] ) {
            tarr = obj[k].CMS.fldat
            break;
        }
    }

    //console.log( "choices" , tarr);

    $('select#tachoice').html("");
    var selectfl = true;
    for (var ix=0; ix<tarr.length; ix++) {        
        var el = tarr[ix]['turn around'];
        var selected = "";
        if ( (el > median) && selectfl ){
            var selected = "selected";
            selectfl = false;
        }
        $('select#tachoice').append('<option value="'+ix+'" '+selected+'>'+el+' hours</option>');
    }
    this.fillselect = false;
}


