Timeplot = function( params ) {
    this.begin = params.begin;
    this.end = params.end;
    this.title = params.title;
    this.xtit = params.xtit;
    this.ytit = params.ytit;
    this.data = [];
    this.exportWidth = 1200;
    this.exportHeight = 500;
    this.exportScale = 1.0;
    if ( 'exportWidth' in params ) this.exportWidth = params.exportWidth;
    if ( 'exportHeight' in params ) this.exportHeight = params.exportHeight;
    if ( 'exportScale' in params ) this.exportScale = params.exportScale;
    if ( 'min' in params ) this.min = params.min;
    if ( 'max' in params ) this.max = params.max;
    if ( 'data' in params ){
	this.data = params.data;
    }

    this.xbands = [];
    if ( 'xranges' in params ) {
	for (var i=0; i<params.xranges.length; i++) {
	    this.xbands.push( { from: params.xranges[i][0],
				 to: params.xranges[i][1],
				 color: "#30f030"
			       } );
	    //console.log( "range ", params.xranges[i][0],params.xranges[i][0]);
	}
    }
    
    if ( 'xbands' in params ) {
        this.xbands = this.xbands.concat( params.xbands );
    }
    this.xmarkers = [];
    if ( 'xmarkers' in params ) {
	this.xmarkers = params.xmarkers;
    }
}

Timeplot.prototype.plot = function( divid, options ) {
    // Do the actual highcharts plot
    var tp = this;

    var plotData = [ { data: this.data,
                       name: "Daily SB fraction"}  ];

    if ('glavg' in options) {
        var ic = 0;
        var glsum = 0;
        var glarr = [];
        var hsum = 0;
        var iarr = [];
        for ( var i = 0; i < this.data.length; i ++ ) {
            glsum += this.data[i][1];
            hsum += 24*(this.data[i][1]);
            iarr.push( [this.data[i][0],hsum] );
            if (ic >= options.glavg) { 
                glsum -= this.data[i-options.glavg][1];
                glarr.push( [this.data[i][0], glsum/options.glavg] );                
            } else {
                glarr.push( [this.data[i][0], 0] );
            }
            ic++;
        }
        //console.log( iarr )
        plotData.push( { data: glarr,
                         name : "Gliding Avg " + options.glavg + " days" 
                       });
        plotData.push( { data: iarr,
                         name : "Hours of Stable Beams" ,
                         yAxis: 1,
                         color: '#F08000'});
    }

    //console.log( plotData );

    $(function() {
        Highcharts.setOptions({
            credits: {
                enabled: false
            },
            global: {
                useUTC: false
            }
        });
	$('div#' + divid ).highcharts({
            exporting: {  
                sourceWidth: tp.exportWidth,
                sourceHeight: tp.exportHeight,
                scale: tp.exportScale,
                chartOptions: {
                    yAxis: [
                        {
                            labels : {
                                style : {
                                    'font-size': '200%'
                                },
                                y : 10
                            },
                            title : {
                                text: "Fraction [%]",
                                style : {
                                    'font-size': '200%'
                                },
                                'margin': 20
                            }
                        },
                        {
                            labels : {
                                style : {
                                    'font-size': '200%'
                                },
                                y : 10
                            },
                            title : {
                                text: "Hours of Stable Beams",
                                style : {
                                    'font-size': '200%',
                                    'color': "#F08000",
                               },
                                'margin': 20
                            },
                            opposite: true
                        }
                    ],                    
                    xAxis : {
                        labels : {
                            style : {
                                'font-size': '200%'
                            },
                            y : 30},
                        title : {
                            style : {
                                'font-size': '200%'
                            }
                        },
                        tickPixelInterval: 200
                    },
                    legend: {
                        itemStyle : {
                            'font-size': '180%'
                        },
                        title : {
                            style : {
                                'font-size': '180%'
                            }
                        },
                        padding : 20
                    },
                    title: {
                        style : {
                            'font-size': '230%',
                            'margin-top' : '14px'
                        },
                    }
                },
            },
            chart: {
		type: "line",
		zoomType: 'xy',
                plotBorderColor: '#808080',
                plotBorderWidth: 2

	    },

	    title : {
		text: tp.title,
                style: {
                    "color": "blue",
                    "font-size": "200%"
                }
	    },

	    xAxis: {
		title : { 
		    text: tp.xtit,
                    style: {
                        "font-size":"150%",
                        "color" : "black"
                    }
		},
		type: 'datetime',
		min: tp.begin,
		max: tp.end,
		gridLineWidth: 1,
                labels: {
                    style: {
                        "font-size":"120%"
                    },
                },
                plotLines : tp.xmarkers,
		plotBands : tp.xbands,
	    },

	    yAxis: [
                {		
		    title : {
		        text: tp.ytit,
                        style: {
                            "font-size":"150%",
                            "color" : "black"
                        }
		    },
                    max : tp.max,
                    min : tp.min,
                    labels: {
                        style: {
                            "font-size":"140%"
                        }
                    }
	        },
                {
                    title : {
                        text: "Hours of Stable Beams",
                        style: {
                            "font-size":"150%",
                            "color" : "#F08000"
                        }
		    },
                    min : 0,
                    opposite: true,
                    labels: {
                        style: {
                            "font-size":"140%"
                        }
                    }
	        },                
	    ],
	    legend : {
		enabled : true
	    },

	    series : plotData

	});
    });
}
