Histogram = function(nbins, min, max, stacks) {
    this.nbins = nbins;
    this.lower = min;
    this.upper = max; // just outside the range : [min,max[
    this.binwidth = (max-min)/nbins;
    if ( stacks === undefined )
        stacks = [{ name : "no name" }];
    this.stacks = stacks;
    this.mean = [];
    this.nentry = [];
    this.sum = [];
    this.contents = [];
    this.init(nbins, min, max, stacks);
}


Histogram.prototype.init = function(nbins,min,max,stacks) {
    if ( nbins != undefined && min != undefined && max != undefined ) {
        this.nbins = nbins;
        this.lower = min;
        this.upper = max; // just outside the range : [min,max[
        this.binwidth = (max-min)/nbins;
    }
    if ( stacks === undefined )
        stacks = [{ name : "no name" }];
    this.stacks = stacks;
    
    this.contents = []
    this.mean = [];
    this.nentry = [];
    this.sum = [];
    for ( var i=0; i<this.stacks.length; i++ ) {
        this.contents.push( [] );
        this.mean.push(0.0);
        this.nentry.push(0.0);
        this.sum.push(0.0);
    }
    // One more for the total
    this.contents.push( [] );
    this.mean.push(0.0);
    this.nentry.push(0.0);
    this.sum.push(0.0);
}

Histogram.prototype.clear = function(nbins, min, max, stacks) {
    var chart = $('div#' + this.divid ).highcharts();
    if ( chart ) {
        for (var ix = 0; ix < chart.series.length; ix++ ) {
            chart.series[ix].remove();
        }
    }
    this.init(nbins, min, max, stacks);
}

Histogram.prototype.insert = function( val, stack ) {
    if ( val === undefined ) {
        console.log( "try to enter undefined value!" );
        return;
    }
    
    var stack = (stack === undefined) ? 0 : stack; 

    // If the precision of the nunmbers is not limited Highcharts plots very thin columns
    // This is probably a highcharts bug. (... or does in this programme something goes
    // wrong with mixing strings and numbers???)
    this.contents[stack].push( parseFloat(val.toPrecision(8)));
    this.contents[this.stacks.length].push( parseFloat(val.toPrecision(8)));
    this.sum[stack] += (val);
    this.nentry[stack]++;
    this.sum[this.stacks.length] += (val);
    this.nentry[this.stacks.length]++;
}

Histogram.prototype.getMean = function( stack ) {
    if ( stack === undefined )
        stack = this.stacks.length;
//    return { colval : this.sum[stack] / this.nentry[stack] / this.binwidth,
//             mean : this.sum[stack] / this.nentry[stack] };
    return this.sum[stack] / this.nentry[stack];
}

Histogram.prototype.getEntries = function( stack ) {
    if ( stack === undefined )
        stack = this.stacks.length;
    return this.nentry[stack];
}

Histogram.prototype.getMedian = function( stack ) {
    if ( stack === undefined )
        stack = this.stacks.length;

    this.contents[stack].sort(function(a,b){return a-b});
    var len = this.contents[stack].length
    var median = 0;
    if (len%2 == 0){
        median = (this.contents[stack][Math.trunc(len/2)] + this.contents[stack][Math.trunc(len/2)+1]) / 2.0;
        //console.log( 'median even num is', median, len);
    } else {
        median = this.contents[stack][(len-1)/2];
        //console.log( 'median odd num is', median, len);
    }
    return median;
}

Histogram.prototype.getColumnData = function() {
    // sort in x might be necessary for the highcharts stacking of histograms
    for (var i=0; i<this.contents.length; i++){
        this.contents[i].sort(function(a,b){return a-b});
    }
    return this.contents;
}

/////////////////////////////////
//
// Required in options:
//    title
//    xtit
//    ytit
// optional in options
//   avg
//   median
//   showLegend
//   min  (for y-axis)
//   max  (for y-axis)
//
//////////////////////////////////

Histogram.prototype.plotHisto = function( divid, options ) {
    this.divid = divid;

    // for the y axis
    var max = ('max' in options) ? options['max'] : undefined;
    var min = ('min' in options) ? options['min'] : undefined;
    
    var bw =this.binwidth;

    var legendFlag = ('showLegend' in options) ? options['showLegend'] : false; 
    //construct the series
    var myseries = [];
    var contents = this.getColumnData();
    for ( var i=0; i<contents.length-1; i++ ) {
        myseries.push( { type: 'histogram',
                         baseSeries: 2*i+1,
                         stacking: 'normal',
                         binWidth : bw,
                         name : this.stacks[i].name
                     } );
        myseries.push ( { data : contents[i],
                          visible: false,
                          showInLegend: false} );
    };
    //console.log( myseries );
    // Do the actual highcharts plot
    $(function() {
	$('div#' + divid ).highcharts({
            credits: {
                enabled: false
            },
            chart: {
                //type: 'column',
                zoomType: 'xy',
                plotBorderColor: '#808080',
                plotBorderWidth: 2

            },
	    title : {
		text: options.title,
                style: {
                    "color": "blue",
                    "font-size": "200%"
                }
	    },
	    xAxis: {
		title : { 
		    text: options.xtit,
                    style: {
                        "font-size":"150%",
                        "color" : "black"
                    }
		},
		gridLineWidth: 1,
                //categories : data.categories,
                labels: {
                    style: {
                        "font-size":"120%"
                    },
                },
	    },
	    yAxis: [ 
		{
		    title : {
			text: options.ytit,
                        style: {
                            "font-size":"150%",
                            "color" : "black"
                        }
		    },
                    max : max,
                    min : min,
                    labels: {
                        style: {
                            "font-size":"140%"
                        }
                    }
		},
	    ],
	    plotOptions : {
//		column : {
//		    pointPadding : 0,
//                    borderWidth : 0,
//                    groupPadding : 0,
//                    shadow : false,
//		    grouping:false
		//		}
		series: {
		    stacking:'normal'
		}
	    },
	    legend : {
		enabled : legendFlag,
                align: 'right',
                x: -40,
                verticalAlign: 'top',
                y: 60,
                floating: true,
                layout: 'vertical',
                backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white', 
                borderColor: '#CCC',
                borderWidth: 1,
	    },
	    series: myseries
	});
    })
 
    if ('avg' in options ) {
	$('div#' + divid).highcharts().xAxis[0].addPlotLine({            
            color : "#606060",
            value : options.avg,
            dashStyle: "shortdash",
            width: 3,
            zIndex: 4,
            label : {
                text: "Mean : " + options.avg.toPrecision(3),
                style: {
                    "font-size": "150%",
                    "font-weight": "bold",
                    "color" : "#606060",
                },
                y : 5
            }
	});
    };
    if ('median' in options ) {
	$('div#' + divid).highcharts().xAxis[0].addPlotLine({            
            color : "#606060",
            value : options.median,
            dashStyle: "shortdash",
            width: 3,
            zIndex: 4,
            label : {
                text: "Median : " + options.median.toPrecision(3),
                style: {
                    "font-size": "150%",
                    "font-weight": "bold",
                    "color" : "#606060",
                },
                y : 5
            }
	});
    }
}
