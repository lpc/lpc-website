var p = {};
var clt = 0.0;
var plotlines_c = [];

debug = function (txt) {
    $('div#debug').append(txt + "<br>");
}

error = function (txt) {
    $('div#error').html(txt + "<br>");
}

clearerror = function () {
    $('div#error').html("");
}

putParams = function (p) {
    for (k in p) {

        $('input#' + k).val(p[k])
        console.log(k, p[k]);
    }
}

getParams = function () {
    var params = {
        'levellumi': parseFloat($('input#levellumi').val()) * 1e34,
        'turnaround': parseFloat($('input#turnaround').val()) * 3600,
        'nbch': parseFloat($('input#intensity').val()) * 1e11,
        'xsec': parseFloat($('input#sigma').val() * 1e-27),
        'nbb': parseInt($('input#ncolliding').val()),
        'epsn': parseFloat($('input#emittance').val()) * 1e-6,
        'betastar': parseFloat($('input#betastar').val()) * 1e-2,
        'crossing': parseFloat($('input#crossing').val()) * 1e-6,
        'sigz': parseFloat($('input#bunchlength').val()) / 40 * 2.99792,
        'blowup': parseFloat($('input#blowup').val()) * 1e-6 / 3600,
        'energy': parseFloat($('input#energy').val()),
        'mass': parseFloat($('input#mass').val())
    };
    var ll = $('input#levellumi').val();
    if (!isNaN(ll)) {
        p.levellumi = parseFloat(ll) * 1e34;
    }

    var betas = [];
    var betacrossings = [];
    $('input[name="beta"]').map(function (ix, e) {
        betas.push(parseFloat($(e).val()) / 100.0);
    });
    $('input[name="betacrossing"]').map(function (ix, e) {
        betacrossings.push(parseFloat($(e).val()) / 1000000);
    });
    params.betacrossings = betacrossings;
    params.betas = betas;
    //console.log( 'betas', betas,betacrossings,times );

    var crossingtimes = [];
    var crossingangles = [];
    var crossingintensities = [];
    var xinglvltype = $('#xing-leveltype').val();

    $('#xing-' + xinglvltype + ' input[name="time"]').map(function (ix, e) {
        if ($(e).val) {
            var tstr = $(e).val() + ':00';
            //console.log( tstr );
            var datetime = new Date('1970-01-01T' + tstr + 'Z');
            var tsec = datetime.getTime() / 1000.0;
            crossingtimes.push(datetime.getTime() / 1000);
            //console.log('pushing ' , tsec );
        } else {
            //console.log( 'pushing nan');
            crossingtimes.push(NaN);
        }
    });
    $('#xing-' + xinglvltype + ' input[name="intensity"]').map(function (ix, e) {
        crossingintensities.push(parseFloat($(e).val()));
    });
    $('#xing-' + xinglvltype + ' input[name="crossing"]').map(function (ix, e) {
        crossingangles.push(parseFloat($(e).val()) / 1000000);
    });
    var xinglvlstep = parseFloat($('#xing-' + xinglvltype + ' input[name="xing-cont-step"]').val()) / 1000000;
    console.log("times, angles, intensities", crossingtimes, crossingangles, crossingintensities);
    params.crossingleveltype = xinglvltype;
    params.crossingangles = crossingangles;
    params.crossingtimes = crossingtimes;
    params.crossingintensities = crossingintensities;
    params.crossingcontinuousstep = xinglvlstep;
    params.initialcrossing = params.crossing;

    var betatimes = [];
    var betasteps = [];
    $('input[name="betatime"]').map(function (ix, e) {
        if ($(e).val) {
            var tstr = $(e).val() + ':00';
            //console.log( tstr );
            var datetime = new Date('1970-01-01T' + tstr + 'Z');
            var tsec = datetime.getTime() / 1000.0;
            betatimes.push(datetime.getTime() / 1000);
            //console.log('pushing ' , tsec );
        } else {
            //console.log( 'pushing nan');
            betatimes.push(NaN);
        }
    });
    $('input[name="betastep"]').map(function (ix, e) {
        betasteps.push(parseFloat($(e).val()) / 100.0);
    });
    params.betasteps = betasteps;
    params.betatimes = betatimes;
    console.log("betatimes, betasteps", betatimes, betasteps);

    params.losses = 0.0e-27;
    params.lossduration = 3600 * 2;

    // for lumi calculation:
    params.frev = 11245;
    params.hubner = 0;
    params.kb = 0;
    params.dsep = 0;
    params.runtime = 0;
    // to be filled before calculation: beta alpha

    if ($('input#betalevelling').is(':checked')) {
        params.betalevelling = true;
    } else {
        params.betalevelling = false;
    }
    if ($('input#crossinglevelling').is(':checked')) {
        params.crossingantilevelling = true;
        params.crossix = 0;
    } else {
        params.crossingantilevelling = false;
    }
    if ($('input#crossinglevellingveto').is(':checked')) {
        params.crossingvetowhenlevelling = true;
    } else {
        params.crossingvetowhenlevelling = false;
    }
    if ($('input#betaantilevelling').is(':checked')) {
        params.betaantilevelling = true;
    } else {
        params.betaantilevelling = false;
    }
    //console.log( $('input#betalevelling').val() );
    //console.log(params);

    return params;
}


///////////////////////////////////
createplot = function () {
    clearerror();

    var maxstr = $('input#max_1').val();
    if (maxstr == "") {
        error("No max given!");
        return;
    }

    p = getParams();

    var min = 0;
    var max = 3600 * parseFloat($('input#max_1').val());

    var nstep = 5000;
    var step = (max - min) / nstep;
    var x = min;
    var i;
    var lumidata = [];
    var integlumidata = [];
    var bunchintdata = [];
    var lumiperhourdata = [];
    var pileupdata = [];
    var l_lumidata = [];
    var l_integlumidata = [];
    var l_bunchintdata = [];
    var l_lumiperhourdata = [];
    var l_lumiloss = [];
    var l_pileupdata = [];
    var betastar = [];
    var xingangle = [];
    var maxlph = 0;
    var xmax = 0;
    var maxlph_l = 0;
    var xmax_l = 0;

    var N0 = p.nbch * p.nbb;
    var N_c = N0;
    var N_cl = N0;
    var integlumi = 0.0;
    var integlumi_l = 0.0;
    var levellinewidth = 2;
    var linewidth = 2;
    var level = false;

    if (p.levellumi > 0) {
        level = true;  // means levelling is on at the start of the fill
        linewidth = 1;
    }

    // VIRTUAL peaklumi
    p.beta = p.betastar;
    p.alpha = p.crossing;
    var o = calcLumi(p);
    //console.log( 'lumi', o );
    p.peaklumi = o.lumi;
    //console.log( 'peaklumi', p.peaklumi);
    var L_c = o.lumi;
    var L_cl = o.lumi;

    var blevelstep = 0;
    var crossinglevelstep = 0;
    var betaantilevelstep = 0;
    // when we stop levelling by separation we populate all pileup values before
    // with the same value.
    var firstool = true;
    var blowupstep = step * p.blowup;
    var anglestep = false;
    var inlevelling = level; // used to indicate when we go out levelling

    for (i = 0; i <= nstep; i++) {

        ilumi = L_c;
        ilumi_l = L_cl;

        // in case there is emittance blowup we need to calculate the new 
        // emittance and insert it into the data structure
        if (p.blowup != 0) {
            p.epsn += blowupstep;
        }

        var dn = 2.0 * L_c * p.xsec * step; // there are 2 experiments burning off...
        N_c -= dn;

        if (p.crossingantilevelling && !(inlevelling && p.crossingvetowhenlevelling)) {
            // check if we have to change the crossing angle due to antileveling active

            if (p.crossingleveltype == 'step' && !isNaN(p.crossingtimes[crossinglevelstep])) {
                if (p.crossingtimes[crossinglevelstep] <= x) {
                    // we need to load a new crossing angle
                    p.crossing = p.crossingangles[crossinglevelstep];
                    crossinglevelstep++;
                    anglestep = true;
                }
            }

            if (p.crossingleveltype == 'cont-time' && !isNaN(p.crossingtimes[crossinglevelstep])) {
                if (p.crossingtimes[crossinglevelstep] <= x) {
                    p.crossing = p.crossingangles[crossinglevelstep];
                    crossinglevelstep++;
                }
                var nexttime = p.crossingtimes[crossinglevelstep];
                var prevtime = crossinglevelstep > 0 ? p.crossingtimes[crossinglevelstep - 1] : 0;
                var nextangle = p.crossingangles[crossinglevelstep];
                var prevangle = crossinglevelstep > 0 ? p.crossingangles[crossinglevelstep - 1] : p.initialcrossing;
                var target = prevangle + (x - prevtime) * (nextangle - prevangle) / (nexttime - prevtime);
                if (p.crossing - p.crossingcontinuousstep > target) {
                    p.crossing -= p.crossingcontinuousstep;
                    anglestep = true;
                }
            }
            if (p.crossingleveltype == 'cont-int') {
                var nbnch = N_c / (p.nbb * 1e11);
                var prevint = p.crossingintensities[0];
                var nextint = p.crossingintensities[0];
                var nextangle = p.crossingangles[0];
                var prevangle = p.crossingangles[0];
                for (var jj = 1; jj < p.crossingintensities.length; jj++) {
                    prevint = p.crossingintensities[jj - 1];
                    nextint = p.crossingintensities[jj];
                    prevangle = p.crossingangles[jj - 1];
                    nextangle = p.crossingangles[jj];
                    if (nbnch <= prevint && nbnch >= nextint) break;
                    if (isNaN(nextangle)) break;
                }
                var target = nextangle + (nbnch - nextint) * (prevangle - nextangle) / (prevint - nextint);
                if (p.crossing - p.crossingcontinuousstep > target) {
                    p.crossing -= p.crossingcontinuousstep;
                    anglestep = true;
                }
            }
        }
        if (p.betaantilevelling && !inlevelling) {
            if (!isNaN(p.betatimes[betaantilevelstep])) {
                if (p.betatimes[betaantilevelstep] <= x) {
                    p.betastar = p.betasteps[betaantilevelstep];
                    betaantilevelstep++;
                }
            }
        }

        // recalculate lumi
        p.beta = p.betastar;
        p.alpha = p.crossing;
        p.nbch = N_c / p.nbb;
        //console.log(p);
        var lcres = calcLumi(p);
        L_c = lcres.lumi;
        //console.log( 'currnet lumi', L_c);
        // scale lumi
        //L_c = p.peaklumi * N_c /N0 * N_c / N0;	
        integlumi += (step * (ilumi + L_c) / 2);

        var lumiperhour = integlumi / (x + p.turnaround) * 3600;
        if (lumiperhour > maxlph) {
            maxlph = lumiperhour;
            xmax = x / 3600;
        }
        lumidata.push([x / 3600, ilumi]);
        integlumidata.push([x / 3600, integlumi / 1e36]);
        lumiperhourdata.push([x / 3600, lumiperhour / 1e36]);
        bunchintdata.push([x / 3600, p.nbch/1e11]);
        pileupdata.push([x / 3600, lcres.mu]);
        betastar.push([x / 3600, p.beta * 100]);
        xingangle.push([x / 3600, 1e6 * p.alpha / 2]);

        if (level) {
            if ((!p.betalevelling) && L_cl > p.levellumi && inlevelling) {
                // levelling by separation

                var dnl = 2.0 * p.levellumi * p.xsec * step; // there are 2 experiments burning off...
                N_cl -= dnl;
                // recalc lumi
                p.nbch = N_cl / p.nbb;
                lcres = calcLumi(p);
                L_cl = lcres.lumi;
                //L_cl = p.peaklumi * N_cl / N0 * N_cl /  N0;
                var plotlumi_l = p.levellumi;
                integlumi_l += (step * p.levellumi);
            } else if (p.betalevelling && (!isNaN(p.betas[blevelstep + 1]))) {
                // beta * levelling and there is one more beta star step
                // What would be the next beta start step lumi?
                p.nbch = N_cl / p.nbb;
                p.beta = p.betas[blevelstep + 1];
                p.alpha = p.betacrossings[blevelstep + 1];
                var lu = calcLumi(p)
                if (lu.lumi >= p.levellumi) {
                    // revert
                    p.beta = p.betas[blevelstep];
                    p.alpha = p.betacrossings[blevelstep];
                } else {
                    blevelstep++;
                }
                // let decay
                var dnl = 2.0 * L_cl * p.xsec * step; // there are 2 experiments burning off...
                N_cl -= dnl;
                p.nbch = N_cl / p.nbb;
                lcres = calcLumi(p);
                L_cl = lcres.lumi;
                //L_cl = p.peaklumi * N_cl /N0 * N_cl / N0;                
                integlumi_l += (step * (ilumi_l + L_cl) / 2);
                var plotlumi_l = ilumi_l;

            } else {
                if (p.betalevelling) {
                    // if we come out of beta star levelling we want to continue with the 
                    // last crossing angle there...
                    p.alpha = p.betacrossings[blevelstep];
                }
                inlevelling = false;
                // out of levelling: let lumi decay
                var dnl = 2.0 * L_cl * p.xsec * step; // there are 2 experiments burning off...
                N_cl -= dnl;
                p.nbch = N_cl / p.nbb;
                lcres = calcLumi(p);
                if (firstool && !p.betalevelling) {
                    firstool = false;
                    for (var ix = 0; ix < l_pileupdata.length; ix++)
                        l_pileupdata[ix][1] = lcres.mu;
                }
                L_cl = lcres.lumi;
                //L_cl = p.peaklumi * N_cl /N0 * N_cl / N0;                
                integlumi_l += (step * (ilumi_l + L_cl) / 2);
                var plotlumi_l = ilumi_l;
            }
            var lumiperhour_l = integlumi_l / (x + p.turnaround) * 3600;
            if (lumiperhour_l > maxlph_l) {
                maxlph_l = lumiperhour_l;
                xmax_l = x / 3600;
            }



            l_lumidata.push([x / 3600, plotlumi_l]);
            l_integlumidata.push([x / 3600, integlumi_l / 1e36]);
            l_lumiperhourdata.push([x / 3600, lumiperhour_l / 1e36]);
            l_bunchintdata.push([x / 3600, (N_cl / p.nbb)/1e11]);
            l_lumiloss.push([x / 3600, (integlumi - integlumi_l) / integlumi * 100]);
            l_pileupdata.push([x / 3600, lcres.mu]);
        }


        //lumidata.push([x / 3600, ilumi]);
        //integlumidata.push([x / 3600, integlumi / 1e36]);
        //lumiperhourdata.push([x / 3600, lumiperhour / 1e36]);
        //bunchintdata.push([x / 3600, p.nbch/1e11]);
        //pileupdata.push([x / 3600, lcres.mu]);
        //betastar.push([x / 3600, p.beta * 100]);
        //xingangle.push([x / 3600, 1e6 * p.alpha / 2]);

        x = x + step;
    }
    var my_plotlines = [];
    my_plotlines.push(
        {
            color: Highcharts.getOptions().colors[3],
            width: linewidth,
            value: xmax,
            id: "optlength",
            label: {
                text: xmax.toPrecision(4) + " hours"
            }
        }
    );

    if (level) {
        my_plotlines.push(
            {
                color: Highcharts.getOptions().colors[3],
                width: levellinewidth,
                value: xmax_l,
                id: "optlengthlevel",
                label: {
                    text: xmax_l.toPrecision(4) + " hours"
                }
            }
        );
    }

    // make a deep copy
    plotlines_c = $.extend(true, {}, my_plotlines);

    //console.log( plot5data );
    // Do the actual highcharts plot
    $(function () {
        $('div#lumiplot').highcharts({
            credits: {
                enabled: false
            },
            title: {
                text: "Integrated Lumi Optimum"
            },
            chart: {
                zoomType: 'xy',
                height: 500
            },
            exporting: {
                sourceWidth: 960,
                chartOptions: {
                    legend: {
                        enabled: false
                    }
                }
            },
            xAxis: {
                title: {
                    text: "Time in hours"
                },
                labels: {
                    formatter: function () {
                        return this.value.toPrecision(4);
                    }
                },
                gridLineWidth: 1,
                plotLines: my_plotlines,
            },
            yAxis: [
                {
                    title: {
                        text: "Inst. Lumi",
                        style: {
                            color: Highcharts.getOptions().colors[1]
                        }
                    },
                    min: 0,
                    labels: {
                        formatter: function () {
                            return this.value;
                        },
                        style: {
                            color: Highcharts.getOptions().colors[1]
                        }
                    }
                },
                {
                    title: {
                        text: "Integ Lumi [pb-1]",
                        style: {
                            color: Highcharts.getOptions().colors[0]
                        }
                    },
                    labels: {
                        formatter: function () {
                            return this.value;
                        },
                        style: {
                            color: Highcharts.getOptions().colors[0]
                        }
                    },
                    opposite: true
                },
                {
                    title: {
                        text: "Integ Lumi per hour [pb-1/h]",
                        style: {
                            color: Highcharts.getOptions().colors[3],
                            fontSize: '12px'
                        }
                    },
                    labels: {
                        formatter: function () {
                            return this.value;
                        },
                        style: {
                            color: Highcharts.getOptions().colors[3]
                        }
                    },
                    opposite: true
                },
                {
                    title: {
                        text: "avg. pile-up",
                        style: {
                            color: Highcharts.getOptions().colors[4]
                        }
                    },
                    min: 0,
                    labels: {
                        formatter: function () {
                            return this.value;
                        },
                        style: {
                            color: Highcharts.getOptions().colors[4]
                        }
                    }
                }
            ],
            plotOptions: {
                line: {
                    marker: {
                        enabled: false
                    }
                },
                series: {
                    events: {
                        legendItemClick: function (e) {
                            var legname = e.target.name;
                            var visible = e.target.visible;
                            //var chart = Highcharts.charts[0];
                            var chart = $('div#lumiplot').highcharts();
                            var cpl = $.extend(true, {}, plotlines_c);
                            if (legname == "levelled lumi per hour") {
                                if (visible) {
                                    chart.xAxis[0].removePlotLine('optlengthlevel');
                                } else {
                                    chart.xAxis[0].addPlotLine(cpl[1]);
                                }
                            } else if (legname == "lumi per hour") {
                                if (visible) {
                                    chart.xAxis[0].removePlotLine('optlength');
                                } else {
                                    chart.xAxis[0].addPlotLine(cpl[0]);
                                }
                            }
                        }
                    }
                }
            },
            legend: {
                enabled: true
            },
            series: [
                {
                    name: 'instant lumi',
                    data: lumidata,
                    color: Highcharts.getOptions().colors[1],
                    yAxis: 0,
                    lineWidth: linewidth
                },
                {
                    name: 'integrated lumi',
                    data: integlumidata,
                    color: Highcharts.getOptions().colors[0],
                    yAxis: 1,
                    lineWidth: linewidth
                },
                {
                    name: 'lumi per hour',
                    data: lumiperhourdata,
                    color: Highcharts.getOptions().colors[3],
                    yAxis: 2,
                    lineWidth: linewidth
                },
                {
                    name: 'avg pileup',
                    data: pileupdata,
                    color: Highcharts.getOptions().colors[4],
                    yAxis: 3,
                    lineWidth: linewidth
                },
            ],
        });
        $('div#beamparamplot').highcharts({
            credits: {
                enabled: false
            },
            title: {
                text: "Beam Parameter Evolution during Anti-Leveling"
            },
            chart: {
                zoomType: 'xy',
                height: 500
            },
            exporting: {
                sourceWidth: 960,
                chartOptions: {
                    legend: {
                        enabled: false
                    }
                }
            },
            xAxis: {
                title: {
                    text: "Time in hours"
                },
                labels: {
                    formatter: function () {
                        return this.value.toPrecision(4);
                    }
                },
                gridLineWidth: 1
            },
            yAxis: [
                {
                    title: {
                        text: "Bunch Current [1e11 ppb]",
                        style: {
                            color: Highcharts.getOptions().colors[4]
                        }
                    },
                    min: 0,
                    labels: {
                        formatter: function () {
                            return this.value;
                        },
                        style: {
                            color: Highcharts.getOptions().colors[4]
                        }
                    }
                },
                {
                    title: {
                        text: "half xing angle [murad]",
                        style: {
                            color: Highcharts.getOptions().colors[6]
                        }
                    },
                    labels: {
                        formatter: function () {
                            return this.value;
                        },
                        style: {
                            color: Highcharts.getOptions().colors[6]
                        }
                    },
                    opposite: true
                },
                {
                    title: {
                        text: "beta* [cm]",
                        style: {
                            color: Highcharts.getOptions().colors[7]
                        }
                    },
                    labels: {
                        formatter: function () {
                            return this.value;
                        },
                        style: {
                            color: Highcharts.getOptions().colors[7]
                        }
                    },
                    opposite: true
                }
            ],
            plotOptions: {
                line: {
                    marker: {
                        enabled: false
                    }
                },
            },
            legend: {
                enabled: true
            },
            series: [
                {
                    name: 'Beam Current',
                    data: bunchintdata,
                    color: Highcharts.getOptions().colors[4],
                    yAxis: 0,
                    lineWidth: linewidth
                },
                {
                    name: 'half xing angle',
                    data: xingangle,
                    color: Highcharts.getOptions().colors[6],
                    yAxis: 1,
                    lineWidth: levellinewidth
                },
                {
                    name: 'beta*',
                    data: betastar,
                    color: Highcharts.getOptions().colors[7],
                    yAxis: 2,
                    lineWidth: levellinewidth
                },
            ],
        });
    });

    if (level) {
        var chart = $('div#lumiplot').highcharts();
        var beamparamchart = $('div#beamparamplot').highcharts();
        //console.log( chart, l_lumiperhourdata );
        chart.addAxis({
            title: {
                text: "levelling lumi loss [%]",
                style: {
                    color: Highcharts.getOptions().colors[5]
                }
            },
            labels: {
                formatter: function () {
                    return this.value;
                },
                style: {
                    color: Highcharts.getOptions().colors[5]
                }
            },
            opposite: true,
            id: "lvlaxis"
        });
        chart.addSeries({
            name: 'levelled lumi',
            data: l_lumidata,
            color: Highcharts.getOptions().colors[1],
            lineWidth: levellinewidth,
            yAxis: 0
        });

        chart.addSeries({
            name: 'integrated levelled lumi',
            data: l_integlumidata,
            color: Highcharts.getOptions().colors[0],
            lineWidth: levellinewidth,
            yAxis: 1
        });

        chart.addSeries({
            name: 'levelled lumi per hour',
            data: l_lumiperhourdata,
            color: Highcharts.getOptions().colors[3],
            lineWidth: levellinewidth,
            yAxis: 2
        });

        beamparamchart.addSeries({
            name: 'levelled beam current',
            data: l_bunchintdata,
            color: Highcharts.getOptions().colors[4],
            lineWidth: levellinewidth,
            yAxis: 0
        });

        chart.addSeries({
            name: 'levelled avg pileup',
            data: l_pileupdata,
            color: Highcharts.getOptions().colors[4],
            lineWidth: levellinewidth,
            yAxis: 3
        });

        chart.addSeries({
            name: 'lumi loss due to levelling',
            data: l_lumiloss,
            color: Highcharts.getOptions().colors[5],
            lineWidth: levellinewidth,
            yAxis: 'lvlaxis'
        });

    }

}
