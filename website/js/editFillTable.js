debug = function( txt ) {
    $('div#debug').append( txt + "<br>" );
}
warning = function( txt ) {
    $('div#warning').html( txt );
}
error = function( txt ) {
    $('div#error').html( txt  );
}
clerror = function( txt ) {
    $('div#error').html('');
}

var config = [{ 'name' : 'fillno',
                'edt' : 0, 
                'type' : 'text' },
              { 'name' : 'start_sb',
                'edt' : 0 , 
                'type' : 'text' },
              { 'name' : 'length_sb',
                'edt' : 0 , 
                'type' : 'text' },
              { 'name' : 'type',
                'edt' : 1 , 
                'type' : 'drop',
                'choices' : [ 'physics', 'rampup', 'ions', , 'VdM', 'special', 'MD' ]},
              { 'name' : 'scheme',
                'edt' : 0 , 
                'type' : 'text' },
              { 'name' : 'end',
                'edt' : 1 , 
                'type' : 'drop',
                'choices' : ['operator', 'operational mistake', 'pwr glitch', 'pwr converter', 'cryo', 'QPS', 'quench','RF', 'collimators', 'UFO', '16L2', 'others']},
              { 'name' : 'ta',
                'edt' : 1 , 
                'type' : 'flag' },
              { 'name' : 'fl',
                'edt' : 1 , 
                'type' : 'flag' },
              { 'name' : 'remarks',
                'edt' : 1 , 
                'type' : 'text' },
             ];

changeYear = function() {
    $('table#FillTable tbody').html('');
    year = $('select#yearselect').val();
    loadTable( year );
}

downloadJson = function() {
    year = $('select#yearselect').val();
    var newwin = window.open( "../cgi-bin/fillTableReader.py?action=load&year=" + year, "_blank");
}

loadYear = function() {
    $('table#FillTable tbody').html('');
    year = $('select#yearselect').val();
    readTable( year );
}


readTable = function( year ) {
//    var url = window.location.protocol + '//' + window.location.host;
//    var path = window.location.pathname;
//    path = path.substring(0,path.lastIndexOf('/'));
//    url += path + '/fillTableReader.py';
    url = '/cgi-bin/fillTableReader.py';
    data = { 'year' : year,
             'action' : 'load' };
    //debug( "loading table for " + year );
    $.post( url, data, function( data ) {
        $('div#status').html(data.status);
        if (data.error) {
            error( data.error );
            return;
        } else {
            clerror();
        }        
        //console.log( data );
        fillTable( data.data, true );
        $('table#FillTable').trigger("update", [true]);
    });   
}

loadTable = function( year ) {
    var url = window.location.protocol + '//' + window.location.host;
    var path = window.location.pathname;
    path = path.substring(0,path.lastIndexOf('/'));
    url += path + '/fillTableHandler.py';
    data = { 'year' : year,
             'action' : 'load' };
    //debug( "loading table from " + url );
    $.post( url, data, function( data ) {
        //console.log( data );
        $('div#status').html(data.status);
        if (data.error) {
            error( data.error );
            return;
        } else {
            clerror();
        }        
        //console.log( data );
        fillTable( data.data, false );
        
    });   
}

addRow =  function() {
    var entry = { 'end' : "",
                  'fillno' : "",
                  'fl' : false,
                  'ta' : false,
                  'scheme' : "",
                  'remarks' : "",
                  'type' : '' ,
                  'start_sb' : "",
                  'length_sb' : ""
                }
    var row = makeRow(entry, config, true)
    $('table#FillTable tbody').append( row );
}

saveTable = function( action ) {

    if ( action === undefined )
        action = "save";

    var fdata = {};
    $('table#FillTable tbody tr').each( function( ix, el ) {
        fill = {};
        for (var ix in config) {
            var it = config[ix];
            var td = $(this).find( 'td[name="'+it.name+'"]' );
            if (it.type == 'text') 
                fill[it.name] = $(td).html();
            else if( it.type == 'flag' )
                fill[it.name] = $(td).find('input').is( ':checked');
            else if( it.type == 'drop' )
                fill[it.name] = $(td).find('select').val();
        }
        if (fill.fillno != "" )
            fdata[ fill.fillno ] = fill;
    });
    
    //console.log( fdata );

    var url = window.location.protocol + '//' + window.location.host;
    var path = window.location.pathname;
    path = path.substring(0,path.lastIndexOf('/'));
    url += path + '/fillTableHandler.py';
    data = { 'year' : year, 
             'action' : action,
             'table' : JSON.stringify(fdata) };
    //debug( "saving table for " + year );
    $.post( url, data, function( data ) {
        $('div#status').html(data.status);
        if (data.error) {
            error( data.error );
            return;
        } else {
            clerror();
        }
        //console.log( data );
    });
}
    
fillTable = function( data, readonly ) { 
    // sort the fills
    rkeys = Object.keys( data ).sort().reverse();
    for (var i=0; i<rkeys.length; i++) {
        entry = data[rkeys[i]];
        if ( readonly )
            row = makeReadonlyRow( entry, config);
        else
            row = makeRow( entry, config );
        $('table#FillTable tbody').append( row );
    }
}

makeRow = function ( entry, items, editall ) {
    var row = "<tr>"
    //console.log(entry);
    for (i in items) {
        var key = items[i].name;

        var name = ' name="'+key+'" ';

        var cedt = 'contenteditable="false"';
        if ( editall == true || entry.fulledit || items[i].edt )
        //if ( editall == true || items[i].edt )
            cedt = 'contenteditable="true"'

	if ( items[i].type == 'drop' )
	    cedt = '';
        var contents = "";
        if ( key in entry ) 
            contents = entry[key];
        
        if ( items[i].type == 'flag' ) {
	    cedt = '';
            var checked = "";
            if ( contents == true ) 
                checked = " checked ";
            contents ='<input ' + name + checked + ' type="checkbox" />'
        }

        if ( items[i].type == 'drop' ) {
            select ='<select' + name + '>'
            for ( var ci in items[i].choices ) {
                var ch = items[i].choices[ci];
                var selected = "";
                if ( contents == ch )
                    selected = " selected ";
                select += '<option value="'+ch+'" '+selected+'>'+ch +'</option>';
            }
            select += "</select>"
            contents = select;
        }

        row += "<td " + name + cedt + ">" + contents + "</td>";
    }
    return row;
}

makeReadonlyRow = function ( entry, items ) {
    var row = "<tr>"

    //console.log( entry );
    
    for (i in items) {
        var key = items[i].name;

        var name = ' name="'+key+'" ';

        var contents = "";
        if ( key in entry ) 
            contents = entry[key];
        

        row += "<td " + name + ">" + contents + "</td>";
    }
    return row;
}
