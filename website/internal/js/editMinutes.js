debug = function( txt ) {
    $('div#debug').append( txt + "<br>" );
}
warning = function( txt ) {
    $('div#warning').html( txt );
}
error = function( txt ) {
    console.log(txt);
    $('#error').text( txt  );
    if (txt!="") {
	$('#error').css('display', 'inline-block');
    } else {
	$('#error').css('display', 'none');
    }
}

collectData = function( destination, orgName ) {
    editor=CKEDITOR.instances.pagetext;
    if (editor) {
        var text = editor.getData(false);
    } else {
        var text = "";
    }
    var date = $('input#date').val();
    var suffix = $('input#suffix').val();
    var indicourl = $('input#indicourl').val();
    var shortdesc = $('input#shortdesc').val();
    var purpose = $('textarea#purpose').val();
    if (date == "" || shortdesc == "" || purpose == "" || text == "") {
        error( "You must have filled out all fields to save (...including some text in the editor...). The suffix or indicourl field may be empty." );
        return false;
    }
    var record = { 'text' : text,
                   'date' : date,
                   'suffix' : suffix,
                   'indicourl' : indicourl,
                   'shortdesc' : shortdesc,
                   'purpose' : purpose,
                   'destination' : destination,
		   'orgName' : orgName
		 };
    return record;
}

saveSummary = function( destination) {
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    orgName = urlParams.get('minutes');
    data = collectData( destination, orgName );
    if ( ! data ) {
        return;
    }
    var url = '/cgi-bin/internal/minutesHandler.py';
    //debug("posting to " + url );
    $.post( url, data, function( data ) {
	console.log(data);
	if (data['newName']!="") {
	    var searchParams = new URLSearchParams(window.location.search);
	    searchParams.set('minutes',data['newName']);
	    history.pushState({},"","/cgi-bin/internal/editMinutes.py?"+searchParams.toString());
	}
        //debug( data );
        //if ('debug' in data) { debug( data.debug ); }
        error(data['error']);
    } );
    return;
}



