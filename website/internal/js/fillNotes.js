var changed = false;
var txtfile = ""
var relPath = ""

uploadCSV = function( event ) {
    event.preventDefault();
    var fd = new FormData( $('form#uploadcsv')[0] );
    fd.append( "action", "upload" );
    $.ajax( {
        'url': "fillNotesHandler.py",
        'type':'POST',
        'data':fd,
        'success' : function( data ) {
            debug( data );
            $('div#status').html( data.status );
            if ( 'error' in data )
                error( data.error );
            else
                clerror();
            if ('debug' in data )
                debug( data.debug );
        },
        'cache' : false,
        'contentType':false,
        'processData':false
    });
} 

checkData = function() {
    if ( ! changed )
        return;

    var text = $('p#schemeAnnotationText').html();
    text.replace( /<\/?span>/gi,'');
    var data = {'text' : text,
                'txtfile' : txtfile };
    changed = false;
    postRequest( 'save', data );
}
setInterval( checkData, 3000 );

releaseAnnotation = function() {
    if ( txtfile == "" ) {
        alert( "You have to choose a filling scheme first!" );
        return;
    }

    postRequest( 'release', { 'txtfile' : txtfile,
                              'relPath' : relPath });
    
}

postRequest = function( action, data ) {
    var url = window.location.protocol + '//' + window.location.host;
    var path = window.location.pathname;
    path = path.substring(0,path.lastIndexOf('/'));
    url += path + '/fillNotesHandler.py';
    data = { 'action' : action,
             'data' : JSON.stringify(data) };

    $.post( url, data, function( data ) {
        //debug( data );
        $('div#status').html(data.status);
        if ( data.debug) {
            debug( data.debug);
        }
        if (data.error) {
            error( data.error );
            return;
        } else {
            clerror();
        }        
        if ( data.action == "load" ) {
            $('ul#uschemeMenu').menu("collapseAll", null, true );
            $('p#schemeAnnotationText').html( data.data.text );
            $('p#schemeName').html( data.data.schemeName );
            txtfile = data.data.txtfile;
            relPath = data.data.relPath;
        } else if ( data.action == "save" ) {

        } else if ( data.action == "release" ) {

        }
    });

}

