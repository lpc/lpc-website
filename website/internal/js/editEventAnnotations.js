debug = function( txt ) {
    $('div#debug').append( txt + "<br>" );
}
warning = function( txt ) {
    $('div#warning').html( txt );
}
error = function( txt ) {
    $('div#error').html( txt  );
}
clerror = function( txt ) {
    $('div#error').html('');
}

var config = [{ 'name' : 'start_date',
                'edt' : 0, 
                'type' : 'date' },
              { 'name' : 'start_time',
                'edt' : 0, 
                'type' : 'time' },
              { 'name' : 'end_date',
                'edt' : 0, 
                'type' : 'date' },
              { 'name' : 'end_time',
                'edt' : 0, 
                'type' : 'time' },
              { 'name' : 'start_fillno',
                'edt' : 0, 
                'type' : 'number' },
              { 'name' : 'end_fillno',
                'edt' : 0, 
                'type' : 'number' },
              { 'name' : 'color',
                'edt' : 1 , 
                'type' : 'color' },
              { 'name' : 'opacity',
                'edt' : 1 , 
                'type' : 'number' },
              { 'name' : 'label',
                'edt' : 1 , 
                'type' : 'textinput' },
             ];


changeYear = function() {
    $('table#EventTable tbody').html('');
    year = $('select#yearselect').val();
    readTable( year );
}

downloadJson = function() {
    year = $('select#yearselect').val();
    var newwin = window.open( "../cgi-bin/fillTableReader.py?action=load&year=" + year, "_blank");
}

loadYear = function() {
    $('table#EventTable tbody').html('');
    year = $('select#yearselect').val();
    readTable( year );
}


readTable = function( year ) {
    var url = '/cgi-bin/internal/eventAnnotationsHandler.py';
    data = { 'year' : year,
             'action' : 'load' };
    //debug( "loading table for " + year );
    $.post( url, data, function( data ) {
        $('div#status').html(data.status);
        if (data.error) {
            error( data.error );
            return;
        } else {
            clerror();
        }        
        //console.log( data );
        fillTable( data.data );
    });   
}


addRow =  function() {
    var entry = { 'start_date' : "",
                  'end_date' : "",
                  'start_time' : "",
                  'end_time' : "",
                  'start_fillno' : "",
                  'end_fillno' : "",
                  'color' : "",
		  'opacity' : "1",
                  'label' : "" 
                }
    var row = makeRow(entry, config, true);
    $('table#EventTable tbody').append( row );
}

saveTable = function( action ) {

    if ( action === undefined )
        action = "save";

    var adata = [];
    $('table#EventTable tbody tr').each( function( ix, el ) {
        annotation = {};
        for (var ix in config) {
            var it = config[ix];
            var td = $(this).find( 'td[name="'+it.name+'"]' );
            if (it.type == 'text') 
                annotation[it.name] = $(td).html();
            else if( it.type == 'flag' )
                annotation[it.name] = $(td).find('input').is( ':checked');
            else if( it.type == 'drop' )
                annotation[it.name] = $(td).find('select').val();
            else if( it.type == 'date' )
                annotation[it.name] = $(td).find('input').val()
            else if( it.type == 'time' )
                annotation[it.name] = $(td).find('input').val()
            else if( it.type == 'number' )
                annotation[it.name] = $(td).find('input').val()
            else if( it.type == 'color' )
                annotation[it.name] = $(td).find('input').val()
            else if( it.type == 'opacity' )
                annotation[it.name] = $(td).find('input').val()
            else if( it.type == 'textinput' )
                annotation[it.name] = $(td).find('input').val()
        }
        if ( (annotation.start_fillno != "") || (annotation.start_date != "") )
            adata.push( annotation );
    });
    
    //console.log( adata );

    var url = '/cgi-bin/internal/eventAnnotationsHandler.py';
    data = { 'year' : year, 
             'action' : action,
             'table' : JSON.stringify(adata) };
    //console.log("saving ", data );
    $.post( url, data, function( data ) {
        $('div#status').html(data.status);
        if (data.error) {
            error( data.error );
            return;
        } else {
            clerror();
        }
        //console.log( "save response data", data );
	//debug( data );
        $('table#EventTable tbody').html("");
        fillTable( data.data );
    });
}
    
fillTable = function( data ) { 
    //console.log( data );
    // sort the fills
    for ( var i=0; i<data.length; i++ ){
        var annot = data[i];
        row = makeRow( annot, config );
        //console.log(row);
        $('table#EventTable tbody').append( row );
    }
    addRow();
}

deleteTRow = function( dom ) {
    $(dom).parent().parent().remove();    
}

makeRow = function ( entry, items ) {
    var row = '<tr><td><button onclick="deleteTRow( this )"><span style="font-weight:900; color: red;">X</button></td>'
    //console.log(entry);
    for (i in items) {
        var key = items[i].name;

        var name = ' name="'+key+'" ';

        cedt = ''

        var contents = "";
        if ( key in entry ) {
            contents = entry[key];
            //cedt = 'contenteditable="true"'
        }

        if ( items[i].type == 'flag' ) {
            var checked = "";
            if ( contents == true ) 
                checked = " checked ";
            contents ='<input ' + name + checked + ' type="checkbox" />'
        }

        if ( items[i].type == 'drop' ) {
            select ='<select' + name + '>'
            for ( var ci in items[i].choices ) {
                var ch = items[i].choices[ci];
                var selected = "";
                if ( contents == ch )
                    selected = " selected ";
                select += '<option value="'+ch+'" '+selected+'>'+ch +'</option>';
            }
            select += "</select>"
            contents = select;
        }

        if ( items[i].type == 'date' ) {
            value = contents;
            contents ='<input type="date" value="' + value + '" ' + name + '/>'
        }

        if ( items[i].type == 'time' ) {
            value = contents;
            contents ='<input type="time" value="' + value + '" ' + name + '/>'
        }

        if ( items[i].type == 'number' ) {
            value = contents;
            contents ='<input style="width: 60px" type="number" value="' + value + '" ' + name + '/>'
        }

        if ( items[i].type == 'color' ) {
            if (contents == "") {
                value = "#0000ff";
            } else {
                value = contents;
            }
            contents ='<input type="color" value="' + value + '" ' + name + '/>'
        }

        if ( items[i].type == 'textinput' ) {
            value = contents;
            contents ='<input type="text" size="80" value="' + value + '" ' + name + '/>'
        }

        row += "<td " + name + cedt + ">" + contents + "</td>";
    }
    row += "</tr>";
    return row;
}
