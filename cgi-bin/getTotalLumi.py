#!/usr/bin/python3

import json
import os
import datetime

def getTotalLumiData(year):

    datapath=f"/data/lumiData/{year}/lumidata.json"
    if not os.access(datapath,os.R_OK):
        lumidata={ 'filldata': [] }
    else:
        infd = open(datapath, "r")
        lumidata = json.load(infd)


    i_cms = 0
    i_atlas = 0
    i_lhcb = 0
    i_alice = 0
    for fill in lumidata['filldata'] :
        i_cms   += fill["CMS_integ_fillLumi"]
        i_atlas += fill["ATLAS_integ_fillLumi"]
        i_lhcb  += fill["LHCb_integ_fillLumi"]
        i_alice += fill["ALICE_integ_fillLumi"]

    i_mean = (i_cms + i_atlas) / 2000000.0 
    mean_fb = i_mean / 1000.0

    i_lhcb = i_lhcb / 1000000.0  # in pb

    i_alice = i_alice / 1000000.0  # in pb

    lhcb_fb = i_lhcb / 1000

    if i_lhcb < 1000:
        unit_lhcb = "pb"
    else:
        unit_lhcb = "fb"
        i_lhcb /= 1000

    lumistr_lhcb = str(round(i_lhcb,1)) + " " + unit_lhcb
    lumistr_alice = str(round(i_alice,1)) + " pb"

    if i_mean < 1000:
        unit = "pb"
    else:
        i_mean /= 1000.0
        unit = "fb"

    lumistr = str(round(i_mean,1)) + " " + unit


    res = { "lumi_fb"  : mean_fb,
            "lumi_str" : lumistr,
            "lumi_fb_lhcb" : lhcb_fb,
            "lumi_str_lhcb" : lumistr_lhcb,
            "lumi_alice" : i_alice,
            "lumi_str_alice" : lumistr_alice }    
    return res

if __name__ == "__main__":
    year = str(datetime.date.today().year)
#    year = 2018
    res=getTotalLumiData(year)
    print("Content-type: application/json")
    print()
    print(json.dumps( res ))

