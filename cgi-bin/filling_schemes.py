#!/usr/bin/python3
import cgi
import os
import re
import json
import urllib.parse
from scanScheme import scanFile


RELPATTERN = re.compile( ".+(fillingSchemes.+)")
schemeDir = "/data/fillingSchemes"

def procDir( cdir ):
    html=""
    for dir in sorted(os.listdir( cdir )):
        if os.path.isdir( os.path.join( cdir,dir) ):
            if dir == 'csvlock_free' or dir == 'csvlock_taken':
                continue
            html+="<li>" + dir + '<ul class="noborder">\n'
            html+=procDir( os.path.join( cdir, dir ) )
            html+="</ul></li>\n"
    for scheme in sorted(os.listdir( cdir )):
        if scheme.endswith( "csv" ):
            mo = RELPATTERN.match( cdir );
            relpattern = "."
            if mo:
                relpattern = "../" + mo.group(1)
            html+='<li class="scheme" data-relpath="' + relpattern + '" data-path="' + cdir + '">' + scheme + "</li>\n"
    return html

def makePage(query):
    querydict = urllib.parse.parse_qs( query )
    
    schemeName = ""
    fillNumber = ""
    if 'schemeName' in querydict:
        schemeName = querydict["schemeName"][0]
    if 'fillNumber' in querydict:
        fillNumber = querydict["fillNumber"][0]

    html='''<!DOCTYPE HTML>
<html>
<head>
  <meta charset="UTF-8">
  <title>LHC Filling Scheme Viewer</title>
  <link rel="stylesheet" type="text/css" href="/css/lhc.css">
  <link rel="stylesheet" type="text/css" href="/css/lpc.css">
  <link rel="stylesheet" type="text/css" href="/css/jquery-ui.css">
  <script src="/js/jquery-2.2.0.min.js"></script>
  <script src="/js/highcharts.js"></script>
  <script src="/js/exporting.js"></script>
  <script src="/js/offline-exporting.js"></script>
  <script src="/js/jquery-ui.min.js"></script>
  <script src="/js/lhcDisplay.js"></script>
  <script src="/js/detuning.js"></script>
  <script src="/js/utils.js"></script>
  <script>
  doit = function( ) {
    checkIE();
    drawScheme();
    delta = $(document).height() - $(window).height();
    //debug( delta );
    $('html, body').animate( { scrollTop: delta}, 700 );
    $('html, body').animate( {
	scrollTop: $('div#contents').offset().top
    }, 1000);
  }'''
    if schemeName != "":
        html+='postRequest( { schemeName : "' + schemeName + '" }, "selectScheme.py" );\n'
    html+='''
  </script>
</head>
<body onload="doit()">
    <!-- page header -->

    <table id="table_header" style="width:100%">
      <tr>
	<td style="width:72px">
	  <a href="http://user.web.cern.ch/user/Welcome.asp">
	    <img src="/images/CERNlogo.gif" alt="CERN" width="72" height="72">
	  </a>
	</td>
	<td>
	  <p class="header-headline">
	    Filling Schemes
	  </p>
	  <p class="center">
	    <a href="fillingSchemeTab.py">Filling Schemes (overview)</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href="/">LPC home</a>
	  </p>
	</td>
	<td style="width:72px">
	  <img alt="LPC: 79977" src="/images/lpcnum.gif" width="72" height="72">
	</td>
      </tr>
    </table>

    <hr>
    
    <!-- page header end -->

    <div id="error"></div>
    <div id="debug"></div>
    <div id="contents">
    <div id="detuning">
      <h3 style="display:inline">Full RF detuning information</h3><h3 style="float: right; display:inline; margin:0px" onclick="$('div#detuning').fadeToggle(1000);">&nbsp;X&nbsp;</h3>
      <p>
       <em>Conventions used</em>: positive delays (in ps) denote a delay added to the reference LHC clock (i.e. the bunch comes later to the IP). Positive z-direction points into the direction of Beam 1 (i.e. clockwise).
      </p>
      <table class="inputs">
        <tr>
          <td><label>HV in one cavity</label></td><td> : </td><td class="number"><input id="cavity_HV" type="number" value="1.5"/>MV</td>
        </tr>
        <tr>
          <td><label>Protons per bunch</label></td><td> : </td><td class="number"><input id="ppb" type="number" value="1.15e11" /></td>
        </tr>
        <tr>
          <td><label>bunch length (4&sigma;)</label></td><td> : </td><td class="number"><input id="blen" type="number" value="1.0" />ns</td>
        </tr>
        <tr>
          <td><button onclick="fullDetuning()">estimate phase shifts and delays</button></td><td></td><td><a href="#" id="downloadJson" onclick="downloadDetuningJson( event )" download="data.json"><button type="button">download detuning data as JSON</button></a></td>
        </tr>
      </table>
      
      <div id="detuningplots">
        
        <div class="phaseplot" id="detuning1_plot"></div>
        <div class="phaseplotcomment" id="detuning1_comment"></div>

        <div class="phaseplot" id="detuning2_plot"></div>
        <div class="phaseplotcomment" id="detuning2_comment"></div>

        <div class="phaseplot" id="detuning3_plot"></div>
        <div class="phaseplotcomment" id="detuning3_comment"></div>

      </div>
    </div>

    <div id="info">
    <p id="schemename"></p>
    <p id="comment"></p>
    <table id="expinfo">
      <tr><th class="bottomline" onmouseover="selectExp('ATLASCMS');" onmouseleave="unselectExp()" >ATLAS</th>
<th class="bottomline" onmouseover="selectExp('ATLASCMS');" onmouseleave="unselectExp()">CMS</th>
<th class="bottomline" onmouseover="selectExp('ALICE');" onmouseleave="unselectExp()">ALICE</th>
<th class="bottomline" onmouseover="selectExp('LHCb');" onmouseleave="unselectExp()">LHCb</th></tr>
      <tr><td id="ATLAS"></td><td id="CMS"></td><td id="ALICE"></td><td id="LHCb"></td></tr>
    </table>
    <table id="beaminfo">
      <tr><th class="bottomline"></th><th class="bottomline">Beam 1</th><th class="bottomline">Beam 2</th></tr>
      <tr><th class="right">Bunches</th><td id="bunches-1"></td><td id="bunches-2"></td></tr>
      <tr><th class="right" onmouseover="selectExp('NC');" onmouseleave="unselectExp()">Non colliding</th><td id="nc-1"></td><td id="nc-2"></td></tr>
      <tr><th class="right">Injections</th><td id="injections-1"></td><td id="injections-2"></td></tr>
    </table>

<p onclick="$('div#detuning').fadeToggle(700);setTimeout(fullDetuning,800);" style="border: 3px solid #0000a0; border-radius: 5px; padding: 3px; background-color: #d0d0ff;">
   RF Full Detuning phase/delay info
</p>
    </div>

    <div id="schemeMenu">
      <ul id="uschemeMenu">
'''  

    html+=procDir( schemeDir )

    html+='''
      </ul>
      <script>
        $('div#schemeMenu li').on( "click", function(event) {
          event.stopPropagation();
          //console.log( event.target.textContent, event.target.getAttribute( 'data-path'), event.target.getAttribute('data-relpath'));
          postRequest( { schemeName : event.target.textContent, schemePath : event.target.getAttribute( 'data-path' ), relPath : event.target.getAttribute( 'data-relpath' ) }, "selectScheme.py" );
          });
        $(function() { $('ul#uschemeMenu').menu({position: {my: "left top", at: "right top" } }); } ); 
      </script>
    </div>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <div id="player" title="">
      <span class="ui-icon ui-icon-help"></span>
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <span class="ui-icon ui-icon-play" onclick="player( 'playpause' )"></span>
      <span class="ui-icon ui-icon-seek-prev" onclick="player( 'slower' )"></span>
      <span class="ui-icon ui-icon-seek-next" onclick="player( 'faster' )"></span>
      <span class="ui-icon ui-icon-stop" onclick="player( 'stop' )"></span>
    </div>
  <script>
    $('div#player span:nth-child(1)').tooltip( { tooltipClass: "tooltipStyle", content : "First select an injection scheme in the menu on the left.<br><br>To highlight the bunches colliding in a specific experiment, move the mouse over the experiments name in the info-box in the center.<br><br>To highlight non-colliding bunches move the mouse over the 'Non-colliding' label in th same box.<br><br>At the bottom you find a linear representations of the orbit: the upper row for Beam 1 and the lower one for Beam 2. Color coding helps you to identify for each bunch in which experiments it collides. A bunch is represented by a vertical line which is vertically divided in three zones: the top one for collisions in IP1/5, the middle one for collisions in IP2 and the lower one for collisions in IP8. The color in each of the three zones indicates that the bunch is colliding in the respective experiment if it matches the color of the experiment label. If the bunch does NOT collide in the respective experiment, the section of the line is painted with the color of the beam (blue for beam 1 and red for beam 2). "});
    $('div#player span:nth-child(2)').tooltip( { content : "- bring the beams in collision<br>- burn-in your browser<br>- heat your office<br>- drain your battery"});
  </script>
  <canvas class="large" id="canvas1" ></canvas>
    <div id="player2" title="">
      <span class="ui-icon ui-icon-play" onclick="player( 'playpause' )"></span>
      <span class="ui-icon ui-icon-seek-prev" onclick="player( 'slower' )"></span>
      <span class="ui-icon ui-icon-seek-next" onclick="player( 'faster' )"></span>
      <span class="ui-icon ui-icon-stop" onclick="player( 'stop' )"></span>
    </div>
  <div style="text-align: center;">
    <button type="button" onclick="toggleSwap()" style="background-color: white; border: none;"><img alt="missing image" style="width:20px" src="../images/arrow_swap.png"/></button>
  </div>
  </div>

</body>
</html>
'''
    return html

if __name__ == "__main__":
    html=makePage(str(os.environ["QUERY_STRING"]))
    
    print('''Content-Type: text/html

''')
    print(html)
