#!/usr/bin/python3
import cgi
import os
import re
import json
from scanScheme import scanFile

RELPATTERN = re.compile( ".+(fillingSchemes.+)")
schemeDir = "/data/fillingSchemes"

def procDir( cdir ):
    html=""
    for dir in sorted(os.listdir( cdir )):
        if os.path.isdir( os.path.join( cdir,dir) ):
            if dir == 'takenlock' or dir == 'freelock':
                continue
            mo = RELPATTERN.match( cdir );
            relpattern = "../fillingSchemes"
            if mo:
                relpattern = "../" + mo.group(1)
            html+='<li data-relpath="'+ relpattern +'" data-path="'+ cdir +'">' + dir + '<ul class="noborder">\n'
            html+=procDir( os.path.join( cdir, dir ) )
            html+="</ul></li>\n"
    return html



def makePage():
    html='''<!DOCTYPE HTML>
<html>
<head>
  <link rel="stylesheet" type="text/css" href="/css/lpc.css">
  <link rel="stylesheet" type="text/css" href="/css/lhc.css">
  <link rel="stylesheet" type="text/css" href="/js/themes/blue/style.css">
  <link rel="stylesheet" type="text/css" href="/css/jquery-ui.css">
  <script src="/js/jquery-2.2.0.min.js"></script>
  <script src="/js/jquery-ui.min.js"></script>
  <script src="/js/jquery.tablesorter.min.js"></script>
  <script src="/js/lhctab.js"></script>
  <script>
  doit = function( ) {
    $('table#schemeTable').tablesorter( { headers: { 3 : { sorter: false } } } );
  }

  $.fn.directtext = function() {
    var str = '';

    this.contents().each(function() {
        if (this.nodeType == 3) {
            str += this.textContent || this.innerText || '';
        }
    });
    return str;
  };

  </script>
</head>
<body onload="doit()">
    <!-- page header -->

    <table id="table_header" style="width:100%">
      <tr>
	<td style="width:72px">
	  <a href="http://user.web.cern.ch/user/Welcome.asp">
	    <img src="/images/CERNlogo.gif" alt="CERN" width="72" height="72">
	  </a>
	</td>
	<td>
	  <p class="header-headline">
	    Filling Schemes
	  </p>
	  <p class="center">
	   <a href="filling_schemes.py">Filling Schemes (details)</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href="/">LPC home</a>
	  </p>
	</td>
	<td style="width:72px">
	  <img alt="LPC: 79977" src="/images/lpcnum.gif" width="72" height="72">
	</td>
      </tr>
    </table>

    <hr>
    
    <!-- page header end -->

    <div id="error"></div>
    <div id="debug"></div>
    <div id="contents">


    <div id="schemeMenu">
      <ul id="schemeMenu">
'''  
    html+=procDir( schemeDir )

    html+='''
      </ul>
      <script>
        $('div#schemeMenu li').on( "click", function(event) {
          event.stopPropagation();
          postRequest( { schemeName : $(event.target).directtext(), schemePath : event.target.getAttribute( 'data-path' ), relPath : event.target.getAttribute( 'data-relpath' ) }, "makeSchemeTab.py" );
          });
        $(function() { $('ul#schemeMenu').menu({position: {my: "left top", at: "right top" } }); } ); 
      </script>
    </div>
  
    <div id='schemeTable'>
      <table id="schemeTable" class="tablesorter" style="border: 1px solid grey">
        <thead>
           <tr><th style="min-width: 50px">Name</th><th style="min-width: 50px">Bunch</th><th style="min-width: 30px">NC</th><th style="min-width: 80px">Description</th></tr>
        </thead>
        <tbody>
        </tbody>
      </table>
    <p>&nbsp;</p>
    <h3>Convention for naming standard filling schemes: </h3>
    <p>{spacing}_{bunches}_{IP1/5}_{IP2}_{IP8}_{trainlength}_{injections}_{special info}</p>
    <p>with
       <table class="smallsimple">
         <tr><td>spacing</td><td>:</td><td>bunch spacing</td></tr>
         <tr><td>bunches</td><td>:</td><td>number of bunches per beam</td></tr>
         <tr><td>IP1/5</td><td>:</td><td>number of collisions in IP1 and IP5</td></tr>
         <tr><td>IP2</td><td>:</td><td>number of collisions in IP 2</td></tr>
         <tr><td>IP8</td><td>:</td><td>number of collisions in IP 8</td></tr>
         <tr><td>trainlength</td><td>:</td><td>the maximal length of a train</td></tr>
         <tr><td>injections</td><td>:</td><td>number of injections per beam</td></tr>
         <tr><td>special info</td><td>:</td><td>any other useful information</td></tr>
      </table>
    </p>
    <p>
       NC denotes the number of non-colliding bunches.
    </p>
    </div>

  </div>
</body>
</html>
'''
    return html

if __name__ == "__main__":
    html=makePage()
    print('''Content-Type: text/html

''')
    print(html)
