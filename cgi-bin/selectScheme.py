#!/usr/bin/python3
import json
import os
import urllib.parse
from scanScheme import scanFile, analyseScheme, findScheme

schemeDir = "/data/fillingSchemes"

def selectScheme(query):
    querydict = urllib.parse.parse_qs( query )

    error = ""
    schemePath = ""
    relPath = ""

    if 'schemePath' in querydict:
        schemeName = querydict["schemeName"][0]
        schemePath = querydict["schemePath"][0]
        relPath    = querydict["relPath"][0]
    else:
        error = "oh"
        if 'schemeName' in querydict:
            schemeName = querydict["schemeName"][0]
            if schemeName[-4:] != ".csv":
                schemeName += ".csv"
            #error = "arrived " + schemeName + " " + schemeDir
            (schemePath,relPath,error) = findScheme( schemeName, schemeDir )
            #error = "found " + schemePath + " " + relPath
        elif 'fillNumber' in querydict:
            schemeName = findSchemeFromFill( fillNumber )
            if schemeName != "":
                (schemePath,relPath,error) = findScheme( schemeName )
            else:
                error = "Could not find Filling Scheme for fill " + str(fillNumber)

    if error == "":    
        injections = scanFile( os.path.join( schemePath, schemeName ) )
        analyseScheme( injections )
        injections['schemePath'] = schemePath
        injections['schemeName'] = schemeName
        injections['relPath']    = relPath
        injson = json.dumps( injections )
    else:
        injson = json.dumps( error )
    return injson

if __name__ == "__main__":
    injson=selectScheme(str(os.environ["QUERY_STRING"]))
    print("Content-type: application/json")
    print()
    print('{ "debug" : "", "data" : ' + injson + ' }')


