#!/usr/bin/python3

import json
import cgi
import os
import sys
import re
import glob
import shutil
import tarfile

def filterpassword( tarinfo ):
    if os.path.basename(tarinfo.name) == "password.txt":
        return None
    return tarinfo

if __name__ == "__main__":
    form       = cgi.FieldStorage()
    if 'user' in form:
        user       = form['user'].value
    else:
        print("""Content-type: application/html

No user supplied""")
        sys.exit(1)
    tmpfile=f"/tmp/{user}.tgz"
    pp=f"/data/editorSchemes/{user}"
    tfile = tarfile.open( tmpfile, "w:gz" );
    tfile.add( pp, user, filter=filterpassword )
    tfile.close()
    sys.stdout.buffer.write(f"""Content-type: application/octet-stream
Content-Disposition: attachment; filename=editorSchemes-{user}.tgz

""".encode('utf-8'))

#    print("hello")
    shutil.copyfileobj(open(tmpfile,"rb"), sys.stdout.buffer)
    
