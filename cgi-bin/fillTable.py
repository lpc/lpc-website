#!/usr/bin/python3
import cgi
import os
import sys
import re
import json
import datetime
import csv
import re

schemeDir = "/data/fillingSchemes" 

def insertSchemeTable( fillDataDir,datapath ):
    html=""
    # make a hash with the paths to all filling schemes saved on the server
    schemehash = {}
    for ( root, dirs, files ) in os.walk( fillDataDir ):
        for csvfile in files :
            (filen, ext) = os.path.splitext( csvfile )
            if ext == ".csv":
                mo = re.search( ".+(fillingSchemes.+)", root)
                if mo :
                    schemehash[ filen ] = os.path.join( "..", mo.group(1), csvfile )

    
    if os.access(datapath,os.R_OK):
        fillh = json.load(open(datapath))
        for filln in sorted(fillh.keys()):
            f = fillh[filln]
            if f['scheme'] in schemehash:
                link = '<a href="' + schemehash[f['scheme']] + '">'
                linke = '</a>'
            else:
                link = ''
                linke = ''
            html+="<tr><td>" + str(filln) + "</td><td>" + f['start_sb'] + "</td><td>" + f['length_sb'] + "</td><td>" + link + f['scheme'] + linke + "</td></tr>\n"

    return html



RELPATTERN = re.compile( ".+(fillingSchemes.+)")

def procDir( cdir ):
    html=""
    for dir in sorted(os.listdir( cdir )):
        if os.path.isdir( os.path.join( cdir,dir)) and dir.isdigit():
            mo = RELPATTERN.match( cdir );
            relpattern = "../fillingSchemes"
            if mo:
                relpattern = "../" + mo.group(1)
            html+='<li data-relpath="'+ relpattern +'" data-path="'+ cdir +'">' + dir + '<ul class="noborder">\n'
            html+="</ul></li>\n"
    return html
########################################################################


def makePage(year):
    dataurl=f"/data/lumiData/{year}/FillTable.json"

    fillDataDir = os.path.join( schemeDir, str(year) )


    html='''<!DOCTYPE HTML>
    <html lang="en">
    <head>
      <meta charset="UTF-8">
      <title>Filling Schemes</title>
      <link rel="stylesheet" type="text/css" href="/css/lpc.css">
      <link rel="stylesheet" type="text/css" href="/css/lhc.css">
      <link rel="stylesheet" type="text/css" href="/js/themes/blue/style.css">
      <link rel="stylesheet" type="text/css" href="/css/jquery-ui.css">
      <script src="/js/jquery-2.2.0.min.js"></script>
      <script src="/js/jquery-ui.min.js"></script>
      <script src="/js/jquery.tablesorter.min.js"></script>
      <script src="/js/lhctab.js"></script>
      <script>
      doit = function() {
        $('table#schemeTable').tablesorter( { headers: { 3 : { sorter: false },0 : {sorter: false } } } );
      }
      $.fn.directtext = function() {
        var str = '';

        this.contents().each(function() {
            if (this.nodeType == 3) {
                str += this.textContent || this.innerText || '';
            }
        });
        return str;
      };

      </script>
    </head>
    <body onload="doit()">
        <!-- page header -->


        <table id="table_header" style="width:100%">
          <tr>
            <td style="width:72px">
              <a href="http://user.web.cern.ch/user/Welcome.asp">
                <img src="/images/CERNlogo.gif" alt="CERN" width="72" height="72">
              </a>
            </td>
            <td>
              <p class="header-headline">
                Filling Schemes
              </p>
              <p class="center">
               <a href="filling_schemes.py">Filling Schemes (details)</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href="/">LPC home</a>
              </p>
            </td>
            <td style="width:72px">
              <img alt="LPC: 79977" src="/images/lpcnum.gif" width="72" height="72">
            </td>
          </tr>
        </table>

        <p style="text-align: center">
          To find/download the filling scheme with a script there has been implemented a web-service which is 
          <a href="../doc/schemeServiceDoc.html">documented here</a>.
        </p> 

        <hr>

        <!-- page header end -->

        <div id="error"></div>
        <div id="debug"></div>
        <div id="contents">

        <div id="schemeMenu">
          <ul id="schemeMenu">
    '''  

    html+=procDir( schemeDir )

    html+='''
          </ul>
          <script>
            $('div#schemeMenu li').on( "click", function(event) {
              event.stopPropagation();
              var year = $(event.target).directtext();
              if (year != "") {
                var url = window.location.href.split('?')[0] + '?year=' + $(event.target).directtext();
                window.location.href = url;
              }
              });
            $(function() { $('ul#schemeMenu').menu({position: {my: "left top", at: "right top" } }); } ); 
          </script>
        </div>

        <div id='schemeTable'>
          <table id="schemeTable" class="tablesorter" style="border: 1px solid grey">
            <thead>
               <tr><th colspan="4" style="font-size: 200%; text-align: center">'''
    html+= str(year)
    html+='''</th></tr>
               <tr><th style="min-width: 50px">Fill No</th><th style="min-width: 120px">Date Time</th><th style="min-width: 80px">Duration SB<br>[hh:mm:ss]</th><th style="min-width: 100px">Filling Scheme</th></tr>
            </thead>
            <tbody>
    '''
    html+=insertSchemeTable( fillDataDir, dataurl )
    html+='''
            </tbody>
          </table>
        </div>

      </div>
    </body>
    </html>
    '''
    return html

if __name__ == "__main__":
    form = cgi.FieldStorage()
    if "year" in form:
        year = form["year"].value
    else:
        year = str(datetime.date.today().year)

    html=makePage(year)
    print('''Content-Type: text/html

''')
    print(html)
