#!/usr/bin/python3
import json
import cgi
import os
import re
import datetime

schemeDir = "/data/fillingSchemes"

def loadTable(year):
    error=""
    
    # get the filltable generated from timber            
    
    datapath=f"/data/lumiData/{year}/FillTable.json"
    if os.access(datapath,os.R_OK):
        timbertab=json.load(open(datapath))
    else:
        timbertab={}

    # Now get the annotated fill table which we want to view/edit here
    tablepath = f"/data/FillTables/FillTable_{year}.json"
    if os.access(tablepath,os.R_OK):
        anntab=json.load(open(tablepath))
    else:
        anntab={}
    
    # Now add all non existing entries of the timber table into the Fill Table
    for fill, entry in timbertab.items() :
        if fill not in anntab:
            anntab[ fill ] = entry

    # Now find all filling schemes on disk
    schemes=[]
    for (root,dirs,files) in os.walk( schemeDir ): 
        for filen in files:
            schemes.append(filen)
            
    # mark the ones which have a filling scheme on server
    for fill in anntab:
        anntab[fill]['hasscheme']=anntab[fill]["scheme"]+".csv"  in schemes

    return (error,anntab, year)


if __name__ == "__main__":
    form = cgi.FieldStorage()
    if "year" in form:
        year = form["year"].value
    else:
        year = str(datetime.date.today().year)

    action = form['action'].value
    (error, data, year) = loadTable(year);
    status="loaded"
    if error: status="problem"
    res = { "status" : status, 
            "action" : action,
            "data"  : data,
            "year"  : year,
            "error" : error }


    print("Content-type: application/json")
    print()
    print(json.dumps( res ))

