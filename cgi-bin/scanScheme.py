import os
import re

def findScheme( schemeName, schemeroot ):
    error = "Scheme not found: " + schemeName
    spath = ""
    relpath = ""
    for root,dirs,files in os.walk( schemeroot ):
        for fi in files:
            if fi == schemeName:
                spath = root
                relpath = os.path.join( "..", "fillingSchemes", os.path.relpath( spath, schemeroot ))
                error = ""
                break
    return (spath, relpath, error)


def scanFile( filename ):
    description = ''
    txtfile = os.path.splitext(filename)[0] + ".txt"
    if os.path.isfile( txtfile ):
        description = open( txtfile, 'r' ).read()
    inf = open( filename, 'r')
    state = "init"
    injections = { 'ring_1' : [],
                   'ring_2' : [],
                   'injections_1' : 0,
                   'injections_2' : 0,
                   'description'  : description }
    beam = 0
    for line in inf:
        line = line.strip()
        if state == "init":
            if re.match( 'idx,inj Nbr,	Ring,RF Bucket,Bu Spac \(ns\),bu per PS batch,SPS Batch spac,PSbatch nbr', line) :
#            if line ==  'idx,inj Nbr,	Ring,RF Bucket,Bu Spac (ns),bu per PS batch,SPS Batch spac,PSbatch nbr,bu Int[1e9p]' :
                state = "injections"
                continue
            continue
        if state == "injections":
            if line == "Bu Spac : space in ns between bunches in the same PS train" :
                state = "buckets"
                continue
            tokens = line.split(',')
            beam = tokens[2].strip()
            if beam == 'ring_1':
                injections['injections_1'] += 1
            elif beam == 'ring_2':
                injections['injections_2'] += 1
                

        if state == "buckets":
            if re.match( "(LONG RANGE FOR )?BEAM 1", line ) or re.match( "(LONG RANGE FOR )?BEAM1", line ):
#            if line == "BEAM 1":
                beam = 'ring_1'
                continue
            if re.match( "(LONG RANGE FOR )?BEAM 2", line ) or re.match( "(LONG RANGE FOR )?BEAM2", line ):
#            if line == "BEAM 2":
                beam = 'ring_2'
                continue
            if line == "RFbucket,Slot,Head-On IP1,Head-On IP2,Head-On IP5,Head-On IP8,Head-On Tot,LR IP1,LR IP2,LR IP5,LR IP8,LR Tot":
                buckets = []
                state = "beam"
            continue

        if state == "beam":
            if line == "":
                # here code to insert the buckets
                injections[beam].append( buckets )
                if beam == 2:
                    state == "stop"
                    break
                else:
                    state = "buckets"
                continue

            tokens = line.split(',')
            bucket = int( tokens[0] )
            buckets.append( bucket )
            
    inf.close()
    return injections

def analyseScheme( scheme ):
    collisions = { 'non_colliding_1' : 0,
                   'non_colliding_2' : 0,
                   'ATLAS' : 0,
                   'CMS'   : 0,
                   'LHCb'  : 0,
                   'ALICE' : 0 }

    r1 = scheme['ring_1'][0]
    r2 = scheme['ring_2'][0]
    bunches = { 'bunches_1' : len(r1),
                'bunches_2' : len(r2) }

    IPCollisions = { 'ATLASCMS'   : [ [], [] ],
                     'ALICE'      : [ [], [] ],
                     'LHCb'       : [ [], [] ],
                     'NC'         : [ [], [] ]
    }
    
    for bucket in r1:
        nc = 1
        if bucket in r2:
            collisions[ 'ATLAS' ] += 1
            collisions[ 'CMS'   ] += 1
            IPCollisions[ 'ATLASCMS' ][0].append( bucket )
            IPCollisions[ 'ATLASCMS' ][1].append( bucket )
            nc = 0
        tb = bucket+8910
        if tb > 35640 :
            tb -= 35640
        if tb in r2 :
            collisions['ALICE'] += 1
            IPCollisions[ 'ALICE' ][0].append( bucket )
            IPCollisions[ 'ALICE' ][1].append( tb )
            nc=0
        tb = bucket-8910-30
        if tb < 1:
            tb += 35640
        if tb in r2:
            collisions['LHCb'] += 1
            IPCollisions[ 'LHCb' ][0].append( bucket )
            IPCollisions[ 'LHCb' ][1].append( tb )
            nc=0
        collisions['non_colliding_1'] += nc
        if nc > 0:
            IPCollisions['NC'][0].append( bucket )

    for bucket in r2:
        nc = 1
        if bucket in r1:
            nc = 0
        tb = bucket + 8910 + 30
        if tb > 35640 :
            tb -= 35640
        if tb in r1 :
           nc=0
        tb = bucket-8910
        if tb < 1:
            tb += 35640
        if tb in r1:
            nc=0
        collisions['non_colliding_2'] += nc
        if nc > 0:
            IPCollisions['NC'][1].append( bucket )
    scheme['collisions'] = collisions
    scheme['bunches'] = bunches
    scheme['ipcollisions'] = IPCollisions
