#!/usr/bin/python3
import json
import cgi
import os
import sys
import shutil

datapath="/data/ToyCalendars/calendars.json"

def getCalendars():
    if not os.access(datapath,os.R_OK):
        return {},"No calendar file"
    data= json.load(open(datapath))
    return data,""

def saveCalendars(data):
    shutil.move(datapath,datapath+".backup")
    fh=open(datapath,"w")
    json.dump(data,fh)
    fh.close()

if __name__ == "__main__":
    form       = cgi.FieldStorage()
    dbx = ""
    error = "no calendars"
    calendars = ()
    action = form['action'].value

    calendars,error = getCalendars()

    if action == "saveCalendar":
        key = form['key'].value
        calendar = json.loads(form['calendar'].value)
        calendars[key] = calendar
        saveCalendars(calendars)
    elif action == "deleteCalendar":
        key = form['key'].value
        if not key in calendars:
            error = "The key " + str(key) + " does not exist in the calendar database."
        else:
            del calendars[key]
            saveCalendars(calendars)

    response = { "debug"  : dbx,
                 "error"  : error,
                 "data"   : calendars,
                 "action" : action }

    print("Content-type: application/json")
    print()
    print(json.dumps( response ))
