#!/bin/bash

echo "Content-type: text/html"
echo
export HOME=/opt/app-root/src
export KRB5CCNAME="FILE:/tmp/krb5cc_"`id -u`
PY_ROOT=/data/python
VENV_PATH=$PY_ROOT/venvs/pytimber
mkdir -p $PY_ROOT/venvs
if [ ! -d $VENV_PATH ]; then
  echo "installing software"
  curl -o $PY_ROOT/acc-py-2021.12-installer.sh http://bewww.cern.ch/ap/acc-py/installers/acc-py-2021.12-installer.sh
  bash $PY_ROOT/acc-py-2021.12-installer.sh -p $PY_ROOT/base/2021.12 -b

  $PY_ROOT/base/2021.12/bin/python -m venv $VENV_PATH
  source $VENV_PATH/bin/activate
  python -m pip install -U pytimber --no-cache

  # Resolve the PyTimber Java dependencies
  python -c "import pytimber; print(pytimber.LoggingDB())"
fi
echo "Setting up environment"
source $VENV_PATH/bin/activate
echo "Launching"
exec ./makeFillTable.py 2024
