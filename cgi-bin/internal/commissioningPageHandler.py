#!/usr/bin/python3
import json
import cgi
import os
import glob
import re
import datetime

if __name__ == "__main__":
    form       = cgi.FieldStorage()
    action = form['action'].value
    error = ""

    if action == 'save' or action == 'release':
        year = form['year'].value
        status = "saved"
        commPath=f"/data/commissioning/commissioning_{year}.json" 
        datastr =  form['table'].value
        data = json.loads( datastr )

        fd = open( commPath, "w" )
        json.dump( data, fd )
        fd.close()
        res = { "status" : status,
            "action" : action,
            "error" : error }
    else:
        error =  "action " + action + " not known"
        res = { "status" : "problem" }

    print("Content-type: application/json")
    print()
    print(json.dumps( res ))
