#!/usr/bin/python3
import json
import cgi
import os
import os.path
import glob
import re
import datetime
import shutil
import subprocess

gitDir="/data/injection-schemes"

def runGitCmd(cmd,debug=False):
    git="git"
    if debug: git="echo git"
    rc,output = subprocess.getstatusoutput(f"cd {gitDir};{git} {cmd}")
    return rc,output

def makePage(email,user,password,comment,dataFiles):
    error=""
    html=""
    rc,output=runGitCmd("remote -v")
    if rc:
        return "Failed to find local git repository: "+output,""
    orgrepo=output.split()[1]
    repo=orgrepo.replace("://",f"://{user}:{password}@")
    rc,output=runGitCmd(f"pull {repo}")
    if rc:
        return "Failed to access git repository - check password: "+output.replace(password,"***"),""

    for name in dataFiles:
        if not name.startswith("/data/editorSchemes/lpc"):
            return "Illegal input name: "+name,""
        if not os.access(name,os.R_OK):
            return "Cannot read: "+name,""
        baseName=os.path.basename(name)
        if os.access(f"{gitDir}/{baseName}",os.R_OK):
            return "File already uploaded: "+name,""
    
    for name in dataFiles:
        baseName=os.path.basename(name)
        newName=f"{gitDir}/{baseName}"
        shutil.copyfile(name,newName)
        rc,output=runGitCmd(f"add {newName}")
        if rc:
            return "Failed to add file to local git repository: "+output,""
    comment=comment.replace('"',"'")

    rc,output=runGitCmd(f'config user.name {user}')
    if rc:
        return "Failed to set user name: "+output,""

    rc,output=runGitCmd(f'config user.email {email}')
    if rc:
        return "Failed to set user name: "+output,""


    rc,output=runGitCmd(f'commit -m "{comment}"')
    if rc:
        return "Failed to commit new files: "+output,""

    today=datetime.datetime.today()
    tag=today.strftime("tag-%Y-%m-%d_%H-%M")
    
    rc,output=runGitCmd(f'tag -m "{comment}" {tag}')
    if rc:
        return "Failed to tag commit: "+output,""

    rc,output=runGitCmd(f"push --follow-tags {repo} master")
    if rc:
        return "Failed to push to remote"+output.replace(password,"***"),""

    rc,output=runGitCmd(f"fetch {repo}")
    if rc:
        return "Failed to refetch git repository - check password: "+output.replace(password,"***"),""
    html=f"""Successfully uploaded {len(dataFiles)} schemes<br/>
Check <a href="{orgrepo}">repository</a><br/>"""
    return error,html

if __name__ == "__main__":
    form = cgi.FieldStorage()
    error=""
    user=form['user'].value
    email=os.environ['HTTP_X_FORWARDED_EMAIL']
    if 'comment' in form:
        comment=form['comment'].value
    else:
        error="Please provide comment"
    if 'password' in form:
        password=form['password'].value
    else:
        error="Please provide password"
    dataFiles=[]
    for name in form.keys():
        if name.endswith(".json"):
            dataFiles.append(name)
    if not dataFiles:
        error="Please select at least one file to upload"
    if not error:
        error,html=makePage(email,user,password,comment,dataFiles)
    print("Content-type: text/html")
    print()
    if error:
        print(error)
    else:
        print(html)


