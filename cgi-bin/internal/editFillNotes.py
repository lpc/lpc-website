#!/usr/bin/python3

import cgi
import os
import sys
import re

from datetime import datetime

RELPATTERN = re.compile( ".+(fillingSchemes.+)")
schemeDir = "/data/fillingSchemes"

def procDir( cdir ):
    out = ""
    for dir in sorted(os.listdir( cdir )):
        if os.path.isdir( os.path.join( cdir,dir) ):
            if dir == 'csvlock_taken' or dir == 'csvlock_free':
                continue
            out +=  "<li>" + dir + '<ul class="noborder">'
            out += procDir( os.path.join( cdir, dir ) )
            out +=  "</ul></li>"
    for scheme in sorted(os.listdir( cdir )):
        if scheme.endswith( "csv" ):
            mo = RELPATTERN.match( cdir );
            relpattern = "."
            if mo:
                relpattern = mo.group(1)
            out += '<li class="scheme" data-relpath="' + relpattern + '" data-path="' + cdir + '">' + scheme + "</li>"
    return out


def makePage():
    htmlout = '''<!DOCTYPE html>
<html lang="en">

  <head>
    <meta charset="UTF-8">
    <title>Edit a text annotation for a filling scheme</title>
    <link rel="stylesheet" type="text/css" href="/css/lpc.css">
    <link rel="stylesheet" type="text/css" href="/css/lhc.css">
    <link rel="stylesheet" type="text/css" href="/css/jquery-ui.css">
    <script src="/js/jquery-2.2.0.min.js"></script>
    <script src="/js/jquery-ui.min.js"></script>
    <script src="/js/utils.js"></script>
    <script src="/internal/js/fillNotes.js"></script>
  </head>

  <body>
    <!-- page header -->

    <table id="table_header" style="width:100%">
      <tr>
	<td style="width:72px">
	  <a href="http://user.web.cern.ch/user/Welcome.asp">
	    <img src="/images/CERNlogo.gif" alt="CERN" width="72" height="72">
	  </a>
	</td>
	<td>
	  <p class="header-headline">
	    Edit a Filling Scheme Annotation
	  </p>
	  <p class="center">
	    <a href="/">LPC home</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="/internal">Internal pages</a>
	  </p>
	</td>
	<td style="width:72px">
	  <img alt="LPC: 79977" src="/images/lpcnum.gif" width="72" height="72">
	</td>
      </tr>
    </table>

    <hr>
    
    <!-- page header end -->

    <div id="error"></div>
    <div id="warning"></div>
    <div id="debug"></div>

    <div id="status"></div>
    <div id="contents">

      <div id="schemeMenu">
        <ul id="uschemeMenu">
'''  

    htmlout += procDir( schemeDir )

    htmlout +=  '''
        </ul>
      <script>
        $('div#schemeMenu li').on( "click", function(event) {
          event.stopPropagation();
          postRequest( "load", { schemeName : event.target.textContent, schemePath : event.target.getAttribute( 'data-path' ), relPath : event.target.getAttribute( 'data-relpath' ) });
          });
        $(function() { $('ul#uschemeMenu').menu({position: {my: "left top", at: "right top" } }); } ); 
      </script>
    </div>

    <p id="schemeName"></p>
    <p id="schemeAnnotationText" contenteditable="true">
    <script>
       $('p#schemeAnnotationText').on('keydown', function() {
           $('div#status').html("changed");
           changed = true;
         });
    </script>
   </div>
<!--
<p style="text-align: center">
<button onclick="releaseAnnotation()" title="Copy annotation to production area">release</button>
</p> --!>
<div style="height:50px;"></div>

<hr style="width:80%">
<div style="width: 80%; margin:auto;">
<p>
<h2>Upload new csv file</h2>
<p>
This allows to upload csv files with filling schemes. Files will be copied to the 
development AND productions area. The server checks that the file does not yet exist on the 
server side. It refuses to overwrite an existing file if the overwrite option is not checked.
Files larger than 10MB 
will NOT be uploaded (normal csv files are around 6 MB at the time of writing of this page). 
</p>
<form id="uploadcsv" enctype="multipart/form-data" action="fillNotesHandler.py" method="post">
<table>
<tr><td>
<label>Year</label></td><td> : </td><td><select name="year" id="yearselect">
   <option value="2015">2015</option>
   <option value="2016">2016</option>
   <option value="2017">2017</option>
   <option value="2018">2018</option>
   <option value="2021">2021</option>
   <option value="2022">2022</option>
   <option value="2023">2023</option>
   <option value="2024" selected>2024</option>
</select></td></tr>
<tr>
<td><label>Candidate folder</label></td><td> : </td><td><input name="candidate" type="checkbox" value='1' checked /></tr>
<tr>
<td><label>Force overwrite</label></td><td> : </td><td><input name="overwrite" type="checkbox" value='1'  /></tr>
<tr>
<td><label>File to upload</label></td><td> : </td><td><input type="file" name="csvfile"/></td></tr>
<tr>
<td></td><td></td><td><input type="submit"></td></tr>
</table>
</form>
</div>
<script>
$('form#uploadcsv').submit( uploadCSV );
</script>
</body>
</html>
'''

    return htmlout

if __name__ == "__main__":
    html=makePage()
    print('''Content-Type: text/html

''')
    print(html)
