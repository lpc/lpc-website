#!/usr/bin/python3
import sys
import os
import json
import re
import tarfile
import time
import zipfile
import pickle
from lpcutils import Logger, getsrcdst
from readconfig import readconfig


class FillProcessor:
    
    def __init__( self, config, logger ):
        self.config = config
        self.year = config['year']
        self.workBaseDir = config['workBaseDir']
        self.lumiDir = config['lumiDir']
        self.logger = logger

        self.fillCache = {}
        self.turnAroundCache = {}
        self.cacheDir = os.path.join( self.workBaseDir, self.year, "fillCache" )
        self.turnaroundCacheFile = os.path.join( self.workBaseDir, self.year, "fillCache", "turn_around_cache.json" )
        self.annotDir = os.path.join( self.lumiDir, self.year )
        if not os.path.isdir( self.cacheDir ):
            os.mkdir( self.cacheDir )


    def analyseLumiReg( self, fill, exp ):
        fills = str(fill)
        self.checkLumiregData( fills, [exp] )
        fill = self.fillCache[ fills ][exp]['fill_lumireg']
        pentry = fill['time'][0]
        tsum = 0
        ie = 0
        mini = 9e20
        maxi = 0
        #for entry in fill[1:]:
        for ix in range(1,len(fill['time'])):
            time = fill['time'][ix]
            diff = time-pentry
            pentry = time
            tsum += diff
#            print fills, diff
            if diff < mini:
                mini = diff
            if diff > maxi:
                maxi = diff
            ie += 1
        mean = tsum/ie
        #print(exp + ": mean time difference: {0:.2f} sec     from {1:.2f} to {2:.2f}".format( mean,mini,maxi ))
        return { 'mean': mean, 'min':mini, 'max':maxi }
    


    # Require fill to be in the cache (in memory),
    #         both experiments need to have data in the cache,
    #         both experiments need to have lumireg data in the cache
    #         at least 2 entries must be in both lumireg datasets. 
    def checkLumiregData( self, fillno, exps ):
        fillno = str(fillno)
        if not fillno in self.fillCache :
            self.logger.log("Fill "+fills+" not in cache")
            return False
        for exp in exps:
            if not exp in self.fillCache[fillno]:
                self.logger.log( exp + " data not there for fill " + fillno)
                return False
            if not 'fill_lumireg' in self.fillCache[fillno][exp]:
                self.logger.log( "No lumireg data for fill " + fillno + " and experiment " + exp)
                return False
            if len(self.fillCache[fillno][exp]['fill_lumireg']['time']) < 2:
                self.logger.log(  exp + " data for fill " + fillno + " has not enough entries (2 needed)")
                return False
        return True
        

    
        
    def createRatios( self, fillno, exp1, exp2 ):
        fillno = str(fillno)
        if not self.checkLumiregData( fillno, [exp1, exp2] ):
            return False

        o1 = { 'dat' : self.fillCache[fillno][exp1]['fill_lumireg'],
               'ix' : 0
           }
        o2 = { 'dat' : self.fillCache[fillno][exp2]['fill_lumireg'],
               'ix' : 0
           }

        # find the starting position with possibly small start time difference
        
        timediff_start = o1['dat']['time'][0] - o2['dat']['time'][0]
        stop = False
        
        while not stop:
            if timediff_start > 0:
                #print timediff_start
                if o2['ix']+1 == len(o2['dat']['time']):
                    return False
                tdn = o1['dat']['time'][o1['ix']] - o2['dat']['time'][ o2['ix']+1 ]
                if abs(tdn) < abs(timediff_start):
                    o2['ix'] += 1
                    timediff_start = tdn
                else:
                    stop = True
            else:
                #print timediff_start
                if o1['ix']+1 == len(o1['dat']['time']):
                    return False
                tdn = o1['dat']['time'][o1['ix']+1] -  o2['dat']['time'][ o2['ix'] ]
                if abs(tdn) < abs(timediff_start):
                    o1['ix'] += 1
                    timediff_start = tdn
                else:
                    stop = True
                
        #print "timediff start ", timediff_start

        stop = False;
        ratios = { 'time' : [],
                   'ratio' : [],
                   'dat1' : [],
                   'dat2' : [],
                   'delta' : [],
                   'exp1' : exp1,
                   'exp2' : exp2,
                   'startdiff' : timediff_start}
        while not stop:
            # get the next step: find our who does the bigger step
            o1['ix'] += 1
            o2['ix'] += 1

            if ( o1['ix'] == len(o1['dat']['time']) or o2['ix'] == len(o2['dat']['time']) ):
                stop = True
                continue


            o1['next'] = o1['ix']
            o2['next'] = o2['ix']

            if o1['dat']['time'][o1['next']] > o2['dat']['time'][o2['next']]:
                big = o1
                small = o2
            else:
                big = o2
                small = o1

            deltabig = big['dat']['time'][big['next']] - big['dat']['time'][big['ix'] - 1]

            # now advance the smaller one until it reaches but not surpasses the bigger.
            # during this form the mean
            mean = small['dat']['zsu'][small['next']]
            n = 1
            # !!! need to check i fthe index for small is not too high! (once in the loop it is ok
            # since we check this with the "-1"
            if (small['ix']+1) > (len(small['dat']['time']) - 1):
                stop = True
                continue

            while small['dat']['time'][small['ix']+1] < big['dat']['time'][big['next']]:
                 #advance
                 small['ix'] += 1
                 if small['ix'] == ( len( small['dat']['time'] ) - 1 ):
                     stop = True
                     break
                 small['next'] = small['ix']
                 mean += small['dat']['zsu'][small['next']]
                 n += 1

            mean = mean / n
            ratios['delta'].append( deltabig )
            ratios['time'].append(big['dat']['time'][big['next']])
            if big == o1:
                rat = o1['dat']['zsu'][o1['next']] / mean
                ratios['ratio'].append( rat )
                ratios['dat1'].append( o1['dat']['zsu'][o1['next']] )
                ratios['dat2'].append( mean )
            else:
                rat = mean / o2['dat']['zsu'][o2['next']]
                ratios['ratio'].append( rat )
                ratios['dat1'].append( mean )
                ratios['dat2'].append( o2['dat']['zsu'][o2['next']] )

        # now we have to store the ratios
        self.fillCache[fillno]['lureg_z_analysis'] = ratios

        return ratios

    def extractFile( self, fillno, srcdst, exp ):
        (lufi, lure, luver, luveranno, rever, reveranno ) = (None,None,None,None,None,None)
        versionfile = os.path.join( fillno, "version.txt")
        verannofile = os.path.join( fillno, "version_annotation.txt")
        elfile = os.path.join( fillno, fillno + "_lumi_" + exp + ".txt")
        erfile = os.path.join( fillno, fillno + "_lumireg_" + exp + ".txt")
        if 'lumi' in srcdst:
            lumi_tgz = os.path.join( srcdst['lumi'][1], fillno + ".tgz")
            lumi_zip = os.path.join( srcdst['lumi'][1], fillno + ".zip")
        else:
            lumi_tgz = os.path.join(srcdst['all'][1], fillno + ".tgz")
            lumi_zip = os.path.join(srcdst['all'][1], fillno + ".zip")
            
        if os.path.isfile( lumi_tgz ):
            tf = tarfile.open( lumi_tgz, "r" )
            try:
                lufi = tf.extractfile( elfile )
            except KeyError as ke:
                self.logger.log("Could not find lumi file in fill " + fillno + " for " + exp)
                self.logger.log(str(ke))
            try:
                luver = tf.extractfile( versionfile )
                luveranno = tf.extractfile( verannofile )
            except KeyError as ke:
                self.logger.log("Could not find version files in lumi tar for fill " + fillno + " for " + exp)
                self.logger.log(str(ke))

        elif os.path.isfile( lumi_zip ):
            zf = zipfile.ZipFile( lumi_zip )
            try:
                lufi = zf.open( elfile )
            except KeyError as ke:
                self.logger.log("Could not find lumi file in fill " + fillno + " for " + exp)
                self.logger.log(str(ke))
            try:
                luver = zf.open( versionfile )
                luveranno = zf.open( verannofile )
            except KeyError as ke:
                self.logger.log("Could not find version files in lumi zip for fill " + fillno + " for " + exp)
                self.logger.log(str(ke))
                                


        if 'lumiregion' in srcdst:
            lumir_tgz = os.path.join( srcdst['lumiregion'][1], fillno + ".tgz")
            lumir_zip = os.path.join( srcdst['lumiregion'][1], fillno + ".zip")
        elif exp!='ALICE':
            lumir_tgz = os.path.join(srcdst['all'][1], fillno + ".tgz")
            lumir_zip = os.path.join(srcdst['all'][1], fillno + ".zip")
        else:
            lumir_tgz ='none'
            lumir_zip ='none'

        if os.path.isfile( lumir_tgz ):
            print(lumir_tgz)
            tf = tarfile.open( lumir_tgz, "r" )
            try:
                lure = tf.extractfile( erfile )
            except KeyError as ke:
                self.logger.log("Could not find lumireg file in fill " + fillno + " for " + exp)
                self.logger.log(str(ke))
            try:
                rever = tf.extractfile( versionfile )
                reveranno = tf.extractfile( verannofile )
            except KeyError as ke:
                self.logger.log("Could not find version files in lumiregion tar for fill  " + fillno + " for " + exp)
                self.logger.log(str(ke))


        elif os.path.isfile( lumir_zip ):
            zf = zipfile.ZipFile( lumir_zip )
            try:
                lure = zf.open( erfile )
            except KeyError as ke:
                self.logger.log("Could not find lumireg file in fill " + fillno + " for " + exp + ": " + erfile)
            try:
                rever = zf.open( versionfile )
                reveranno = zf.open( verannofile )
            except KeyError as ke:
                self.logger.log("Could not find version files in lumiregion zip for fill " + fillno + " for " + exp)
                self.logger.log(str(ke))


        return (lufi, lure, luver, luveranno, rever, reveranno)

        
    def cacheFill( self, fillno, expList=None ):
        '''If experiment is given only files from that experiments 
        are processed'''

        fillno = str(fillno)

        if not expList:
            expList = ['ATLAS','CMS','LHCb','ALICE']

        for exp in expList:

            srcdst = getsrcdst( self.config, exp )
            (lufi,lure,luver,luveranno,rever,reveranno) = self.extractFile( fillno, srcdst, exp )

            if lufi == None:
                self.logger.log( "No Massi files in " + exp + " for fill " + fillno )                 
                continue
                    
                   
            # cache the file contents
            ludata = { 'time' : [],
                       'stab' : [],
                       'l'    : [],
                       'dl'   : []
            }
            luregdata = { 'time' : [],
                          'stab' : [],
                          'x'    : [],
                          'y'    : [],
                          'z'    : [],
                          'xsu'  : [],
                          'ysu'  : [],
                          'zsu'  : [] 
                      }
    
            if lufi:
                ilumi = 0
                tc = 1.0
                tarr = []
                ilucurve = []
                while tc <= 10:
                    tarr.append( {'turn around' : tc,
                                  'lumi per time' : 0.0,
                                  'tlpt' : 0,          # temporarily needed: saves the time of 'lumi per time'
                                  'opt length' : 0.0 } )
                    tc += 0.5
                    
                timarr = []
                ix = 0
                for line in lufi:
                    #ludata.append( map( float, line.split() ) )
                    try:
                        fline = list(map( float, line.split() ))
                    except ValueError:
                        print(f"ERROR in following line (#{ix}) for fill {fillno} in {exp}")
                        print(line)
                        continue
                    if len(fline)<4:
                        print(f"ERROR in following line (#{ix}) for fill {fillno} in {exp}")
                        print(line)
                        continue
                    ludata['time'].append( fline[0] )
                    ludata['stab'].append( fline[1] )
                    ludata['l'].append( fline[2] )
                    if ix > 0:
                        ilumi += fline[2] * (fline[0] - ludata['time'][ix-1])
                    else:
                        ilumi = 0
                    ilucurve.append( ilumi )
                    ludata['dl'].append( fline[3] )
                    timarr.append( fline[0] - ludata['time'][0])
                    ix+=1
                            

                # now we try to smoothen the curve of integrate lumi.
                #npilucurve = np.array( ilucurve )
                #ilfcurve = savgol_filter( npilucurve, 5, 3 ) 
                ix = 0
                for tim in timarr:
                    for ta in tarr:
                        tc = ta['turn around']
                        lpt = ilucurve[ix] / (tim + tc * 3600)
                        if lpt > ta['lumi per time']:
                            ta['lumi per time'] = lpt
                            ta['tlpt'] = tim
                        else:
                            ta['opt length'] = ta['tlpt']

                    ix += 1

                # if tlpt is not opt length in the final array, this means that there was a local maximum but then at the end the lumi
                # per time went higher than the local maximum but did not cause a second maximum (e.g. due to beta star leveling 
                # at the end of the fill)
                        
                if lure:
                    for line in lure:
                        fline = list(map( float,line.split() ))
                        luregdata['time'].append( fline[0] )
                        luregdata['stab'].append( fline[1] )
                        luregdata['x'].append( fline[2] )
                        luregdata['y'].append( fline[4] )
                        luregdata['z'].append( fline[6] )
                        luregdata['xsu'].append( fline[8] )
                        luregdata['ysu'].append( fline[10] )
                        luregdata['zsu'].append( fline[12] )
    
            sf = self.fillCache
            tac = self.turnAroundCache
            if not (fillno in sf):
                sf[fillno] = {}
            if not ( fillno in tac ):
                tac[fillno] = {}
            if not exp in sf[fillno]:
                sf[fillno][exp] = {}
            if not exp in tac[fillno] :
                tac[fillno][exp] = {}
            if lufi:
                sf[fillno][exp]['fill_lumi'] = ludata
                tac[fillno][exp]['fldat'] = tarr
#                tac[fillno][exp]['ilumiraw'] = ilucurve
#                tac[fillno][exp]['ilumismooth'] = ilfcurve.tolist()
            if lure:
                sf[fillno][exp]['fill_lumireg'] = luregdata

            self.createRatios( fillno, 'CMS', 'ATLAS')

            self.extractMetaData( 'lumi', exp, fillno, luver, luveranno )
            self.extractMetaData( 'lumireg', exp, fillno, rever, reveranno )


    def extractMetaData( self, tag, exp, fillno, luver, luveranno):
        vc = self.versionCache
        if luver and luveranno:
            version = luver.read().decode('utf-8').strip()
            #print "tag " + tag + " + exp : ", exp , "  version : ", version
            vc[exp][tag][fillno] = version
            if fillno not in vc['fills'][tag]:
                vc['fills'][tag][fillno] = { 'ALICE' : '', 'ATLAS' : '', 'CMS' : '', 'LHCb' : ''}
            vc['fills'][tag][fillno][exp] = version

            for line in luveranno:
                line = line.decode('utf-8').strip()

                # ignore empty lines
                mo = re.match( r"\s*$", line )
                if mo:
                    continue

                mo = re.match( r"(\d+)\s*:\s*(.+)$",line )
                if mo:
                    (digit,desc) = mo.groups()
                    desc = mo.group(2)
                    vc[exp][tag]['digit_annotation'][digit] = desc
                    continue
                mo = re.match( r"(\d+)\s*(\d+)\s+:\s*(.+)$", line)
                if mo:
                    (digit,value,desc) = mo.groups()
                    if not digit in vc[exp][tag]['value_annotation']:
                        vc[exp][tag]['value_annotation'][digit] = {}
                    vc[exp][tag]['value_annotation'][digit][value] = desc
                    continue
                mo = re.match( r"(_\d+)\s*:\s*(.+)$", line )
                if mo:
                    (value,desc) = mo.groups()
                    if not value in vc[exp][tag]['exception_annotation']:
                        vc[exp][tag]['exception_annotation'][value] = {'description' : desc,
                                                                              'fills' : [ fillno ] }
                    else:
                        vc[exp][tag]['exception_annotation'][value]['description'] = desc
                        vc[exp][tag]['exception_annotation'][value]['fills'].append( fillno )
                    continue

                self.logger.log("Illegal syntax in annotation file for " + exp + " in fill " + fillno + ' : ' + line)
                continue


    def loadCache( self, fillarr = [] ):
        for fill in fillarr:
            fill = str(fill)
            filename = os.path.join(self.cacheDir, fill + ".json") 
            if os.path.isfile(filename):
                #print fill
                cfd = open( filename, "r" )
                fillcache = json.load( cfd )
                cfd.close()
                self.fillCache[ fill ] = fillcache
        filename = os.path.join( self.annotDir, "massi_annotations.json")
        if not os.path.isfile( filename ):
            self.versionCache = { 'ALICE' : {'lumi'    : { 'digit_annotation' : {},
                                                           'value_annotation' : {},
                                                           'exception_annotation' : {} },
                                             'lumireg' : { 'digit_annotation' : {},
                                                           'value_annotation' : {},
                                                           'exception_annotation' : {} }
                                         },
                                  'ATLAS' : {'lumi'    : { 'digit_annotation' : {},
                                                           'value_annotation' : {},
                                                           'exception_annotation' : {} },
                                             'lumireg' : { 'digit_annotation' : {},
                                                           'value_annotation' : {},
                                                           'exception_annotation' : {} }
                                         },
                                  'CMS'   : {'lumi'    : { 'digit_annotation' : {},
                                                           'value_annotation' : {},
                                                           'exception_annotation' : {} },
                                             'lumireg' : { 'digit_annotation' : {},
                                                           'value_annotation' : {},
                                                           'exception_annotation' : {} }
                                         },
                                  'LHCb'  : {'lumi'    : { 'digit_annotation' : {},
                                                           'value_annotation' : {},
                                                           'exception_annotation' : {} },
                                             'lumireg' : { 'digit_annotation' : {},
                                                           'value_annotation' : {},
                                                           'exception_annotation' : {} }
                                         },
                                  'fills' : { 'lumi'    : {},
                                              'lumireg' : {} }
                              }
        else:
            fd = open( filename, 'r' )
            self.versionCache = json.load(fd)

    def clearCache( self ):
        self.fillCache = {}
        self.versionCache = {}

    def getFill( self, fillno ):
        fillno = str(fillno)
        if not fillno in self.fillCache:
            self.loadCache( [fillno] )
            return self.fillCache[fillno]

    def dumpCache( self, fillarr = None ):
        if not fillarr:
            fillarr = sorted(self.fillCache.keys())

        for key in fillarr:
            key = str(key)
            if key not in self.fillCache:
                continue
            ndir = os.path.join( self.cacheDir, key + ".json" )
            #print "fill " + key + " to " + ndir
            cfd = open( ndir, "w" )
            json.dump( self.fillCache[key], cfd )
            cfd.close()        
            
        filename = os.path.join( self.annotDir, "massi_annotations.json")
        fd = open( filename, 'w' )
        json.dump( self.versionCache, fd )
        fd.close
        

    def addLumiregionInfoToCache( self, fillno ):
        fillno = str(fillno)
        self.loadCache( [fillno] )
        self.createRatios( fillno, 'CMS', 'ATLAS' )
        self.dumpCache( [fillno] )

    def updateCache( self, fillno, expList = None ):
        self.loadTurnaroundCache()
        self.loadCache( [fillno] )
        self.cacheFill( fillno )
        self.dumpCache( [fillno] )
        self.dumpTurnaroundCache() 

    def dumpTurnaroundCache( self ):
        tmpName="%s_%d" % (self.turnaroundCacheFile,os.getpid())
        fd = open( tmpName, 'w' )
        json.dump( self.turnAroundCache, fd )
        fd.close()        
        os.system("cp %s %s" % (tmpName,self.turnaroundCacheFile)) #Brian: should be move not copy, but for now leave files to see if they are corrupted sometimes

    def loadTurnaroundCache( self ):
        if os.path.isfile( self.turnaroundCacheFile ):
            try:
                fd = open( self.turnaroundCacheFile, 'r' )
                self.turnAroundCache = json.load( fd )
                fd.close()
            except ValueError:
                self.turnAroundCache = {}
        else:
            self.turnAroundCache = {}

################### test ################
if __name__ == '__main__':
    config = readconfig( "../../config/lumi_config_2023.json" )
    logger = Logger( config["logfile"] )

    fp = FillProcessor( config, logger )
    
    updates = { 'ATLAS' : { 'fills' : [7652,7662] } }
    for (exp,upd) in updates.items():
        for fillno in updates[exp]['fills']:
            logger.log( "updating " + exp + " fill " + str(fillno))
            fp.updateCache( fillno, [exp] )

