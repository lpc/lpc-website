#!/usr/bin/python3
import json
import cgi
import os
import glob
import re
import datetime
import shutil

schemeDir = "/data/fillingSchemes"

if __name__ == "__main__":
    form = cgi.FieldStorage()

    action = form['action'].value
    error = ""

    if action == 'load' :
        data = json.loads(form['data'].value)
        relPath = data['relPath']
        schemePath = os.path.join( data['schemePath'], data['schemeName'])
        schemeName = os.path.basename(os.path.splitext( schemePath )[0]);
        txtfile = os.path.splitext( schemePath )[0] + ".txt"
        text = "empty"
        if os.path.isfile( txtfile ):
            fd = open(txtfile, 'r')
            text = fd.read()
            fd.close()
        data = text
        res = { "status" : "loaded", 
                "action" : action,
                "data"  : { 'text' : text,
                            'schemeName' : schemeName,
                            'relPath' : relPath,
                            'txtfile' : txtfile,
                            },
                "error" : error }

    elif action == 'save' :
        datastr =  form['data'].value
        datastr = re.sub( r'<div([^>]*)>', '<div>', datastr )
        datastr = re.sub( r'<\/?span[^>]*>', '', datastr )
        data = json.loads( datastr )
        txtfile = data['txtfile']
        text = data['text']

        fd = open( txtfile, "w" )
        fd.write( text )
        fd.close()

        res = { "status" : "saved",
                "action" : action,
                "error"  : error }

#not used anymore as we update directly
##    elif action == 'release' :
##        datastr =  form['data'].value
##        data = json.loads( datastr )
##        txtfile = data['txtfile']
##        relPath = data['relPath']
##        if not os.path.isfile( txtfile ) :
##            error = txtfile + " does not exist! Cannot release!"
##            res = { "status" : "problem",
##                    "error"  : error,
##                    "action" : action };
##        else:
##            dest = os.path.abspath(os.path.join( relPath, os.path.basename(txtfile) ));
##            src = txtfile;
##            shutil.copy( src, dest)

##            res = { 'action' : action,
##                    'status' : 'released',
##    #                'debug'  : txtfile + ">>>" + relPath + " -- " + src + " -- " + dest
##                    };
    elif action == 'upload':
        year = form['year'].value
        candidate = ""
        if 'candidate' in form:
            candidate = "candidates"
        fileitem = form['csvfile']
        status = ""
        if fileitem.filename:
            name = os.path.basename( fileitem.filename )
            schemepath = os.path.join( schemeDir, year, candidate )
            os.system(f"mkdir -p {schemepath}")
            filepath = os.path.join( schemeDir, year, candidate, name )
            # we read the file in chunks to avoid DOS attacks
            icount = 0
            filecontents = b''
            while 1:
                chunk = fileitem.file.read( 1000000 );
                if not chunk: break
                filecontents += chunk
                icount += 1
                if icount == 10:
                    error = "File too large!"
                    break
            filesize = len( filecontents )

            if os.path.isfile( filepath ) and not 'overwrite' in form:
                error = "File " + filepath  + " exists already. Refusing to overwrite file."
            else:
                fd = open( filepath, "wb" )
                fd.write( filecontents )
                fd.close()
                status = "uploaded"
        else:
            error = "You have to select a csv file first"
            status = "nothing done"

        res = { 'status' : status,
                'error'  : error,
                'debug'  : "" }
    else:
        error = "action " + action + " not known"
        res = { "status" : "problem",
                "error"  : error }


    print("Content-type: application/json")
    print()
    print(json.dumps( res ))

