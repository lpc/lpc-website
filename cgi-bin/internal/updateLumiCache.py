from FillProcessor import FillProcessor

def updateFillCache(config,logger,updates):
    logger.log( "updateLumiCache" )

    fp = FillProcessor( config, logger )

    logger.log( "cache to be updated for \n" + repr(updates) )
    logger.logtime()

    for exp in updates:
        for fillno in updates[exp]:
            logger.log( "updating " + exp + " fill " + str(fillno))
            fp.updateCache( fillno, [exp] )
    logger.logtime()
