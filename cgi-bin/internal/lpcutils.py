import re
import os
import sys
import smtplib
from datetime import datetime
from email.mime.text import MIMEText

class Logger:

    def __init__( self, logfile ):
        self.logfile = logfile
        self.lfd = open( logfile, "a+" )        

    def logToStd( self ):
        self.lfd = sys.stdout

    def log( self, message ):
        timestr = datetime.today().strftime( "%Y/%m/%d %H:%M:%S")
        logstr = "\n" + timestr + "\n"
        mindent = logstr + "    " + re.sub( r"\n", "\n    ", message )
        if mindent[:-1] != "\n" :
            mindent += "\n"        
        self.lfd.write( mindent )
        return mindent

    def logtime( self ):
        timestr = datetime.today().strftime( "%Y/%m/%d %H:%M:%S")
        logstr = "\n" + timestr + "\n"
        self.lfd.write(logstr )

    def logindented( self, message ):        
        mindent = "  " + re.sub( r"\n", message )
        if not mindent.endswith( "\n" ):
            mindent += "\n"
        self.lfd.write( mindent )


class Mailer:
    def __init__( self, logger, credentialFile ):
        self.address = "lpc@cern.ch"
        if not os.path.isfile( credentialFile ):
            logger.log( "    No credentials for sending emails (" + credentialFile + ")... bailing out.\n")
            sys.exit(-1)
        pwdfile = open( credentialFile, 'r' )
        password = pwdfile.read()
        self.password = password.strip()
        pwdfile.close()

    def sendMail( self, subject, message ):
        
        msg = MIMEText( message )
        msg['Subject'] = subject
        msg['From'] = self.address
        msg['To'] = self.address

        s = smtplib.SMTP( 'smtp.cern.ch' )
        s.starttls()
        s.login( 'lpc', self.password )
        s.sendmail( msg['From'], msg['To'], msg.as_string() )
        s.quit()

def getsrcdst( config, exp ):
    srcdst = {}
    luminew = os.path.join( config['srcDir'][exp], config['year'], 'lumi' )
    lumiregnew = os.path.join( config['srcDir'][exp], config['year'], 'lumiregion' )
    beamgasnew = os.path.join( config['srcDir'][exp], config['year'], 'beamgas' )

    #print "new lumi ", luminew

    if os.path.isdir( luminew ):
        dst = os.path.join( config['workBaseDir'], config['year'], 'measurements', exp, 'lumi' )
        if not os.path.isdir( dst ):
            os.makedirs( dst )
        srcdst['lumi'] = [luminew, dst]
        
        # if lumireg data exists
        if os.path.isdir( lumiregnew ):
            dst = os.path.join( config['workBaseDir'], config['year'], 'measurements', exp, 'lumiregion' )
            if not os.path.isdir( dst ):
                os.makedirs( dst )
            srcdst['lumiregion'] = [lumiregnew, dst]

        # if lumireg data exists
        if os.path.isdir( beamgasnew ):
            dst = os.path.join( config['workBaseDir'], config['year'], 'measurements', exp, 'beamgas' )
            if not os.path.isdir( dst ):
                os.makedirs( dst )
            srcdst['beamgas'] = [beamgasnew, dst]

        return srcdst


    # now the oid case for 2016 and before: all in one tgz file:
    src = config['srcDir'][exp] 
    dst = os.path.join( config['workBaseDir'], config['year'], 'measurements', exp )

    return { 'all' : [src,dst] }

