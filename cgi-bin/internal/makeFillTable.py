#!/usr/bin/env python3

import json
import os
import pprint
import sys
from datetime import datetime, timedelta
import time

import pytimber

def getTimberFillTable(startTime,stopTime):
    ldb = pytimber.LoggingDB()
    print("Retrieve data")
    sys.stdout.flush()
    data=ldb.get(["HX:FILLN","HX:BMODE","LHC:INJECTION_SCHEME"],
                 startTime,stopTime)
    print("Got data")
    sys.stdout.flush()
    return data

def mkFillTable(data):
    
    fills=zip(*data["HX:FILLN"])
    bmodes=zip(*data["HX:BMODE"])
    schemes=zip(*data["LHC:INJECTION_SCHEME"])
    
    fillno="none"
    lastMode="none"
    scheme="none"
    fillCache={}
    stime=0
    nextscheme="none"
    for stoptime,nextfill in list(fills)+[(time.time(),0)]:
        stablebeamStart=0
        stablestopTime=0
        lastStablebeamStart=0
        stablebeamLength=0
        for modetime,mode in bmodes:
            if modetime>stoptime: break
            if mode=="STABLE":
                if stablebeamStart==0: stablebeamStart=modetime
                lastStablebeamStart=modetime
            elif lastMode=="STABLE":
                stablebeamLength+=modetime-lastStablebeamStart
                stablestopTime=modetime
            lastMode=mode
        if lastMode=="STABLE":
            print("In stable beam during fill change? Fill",fillno)
            stablebeamLength+=stoptime-lastStablebeamStart
            stablestopTime=stoptime
            lastMode="none"
        if stablebeamLength and fillno != "none":
            dtStart = datetime.fromtimestamp(stablebeamStart)
            dtStop= datetime.fromtimestamp( stablestopTime )
            delta = timedelta (seconds=int(stablebeamLength))
            if stime<stablebeamStart:
                scheme=nextscheme
                for stime,nextscheme in schemes:
                    if stime>stablebeamStart:
                        break
                    scheme=nextscheme
            fill={ 'fillno' : fillno,
                   'starttime' : int(stablebeamStart*1000),
                   'start_sb'  : dtStart.strftime( '%Y-%m-%d %H:%M:%S' ),
                   'stoptime'  : int(stablestopTime*1000),
                   'stop_sb'   : dtStop.strftime( '%Y-%m-%d %H:%M:%S' ),
                   'scheme' : scheme,
                   'sblength' : int(stablebeamLength*1000),
                   'length_sb' : str(delta)
                 }
            fillCache[fillno]=fill
        fillno=str(int(nextfill))
    return fillCache

if __name__ == "__main__":
    year=int(sys.argv[1])
    print("Working on year",year)
    sys.stdout.flush()
    data=getTimberFillTable(datetime(year,1,1),datetime(year+1,1,1))
    cache=mkFillTable(data)
    print(cache)
    fh=open(f"/data/lumiData/{year}/FillTable.json","w")
    json.dump(cache,fh)
    fh.close()
