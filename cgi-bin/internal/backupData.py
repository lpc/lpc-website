#!/usr/bin/python3
import json
import cgi
import os
import glob
import sys
import datetime
import shutil
import subprocess

destination="/eos/project/l/lpc/backup-data"

if __name__ == "__main__":
    today=datetime.date.today()
    error=""
    rc,out=subprocess.getstatusoutput("rm -f /tmp/backup.tgz")
    if rc:
        error=out
    else:
        rc,out=subprocess.getstatusoutput("tar cfz /tmp/backup.tgz /data")
        if rc:
            error=out
        else:
            shutil.copyfile("/tmp/backup.tgz",f"{destination}/backup-day-{today.day}.tgz")
            if today.day==1:
                shutil.copyfile("/tmp/backup.tgz",f"{destination}/backup-month-{today.month}.tgz")
            if today.day==1 and today.month==1:
                shutil.copyfile("/tmp/backup.tgz",f"{destination}/backup-year-{today.year}.tgz")
    obj = { "error": error }
    print("Content-type: application/json")
    print()
    print(json.dumps( obj ))

