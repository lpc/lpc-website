#!/usr/bin/env python3

import cgi
import datetime
import sys

import readconfig
from lpcutils import Logger

import copySummaryFiles
import getLumiJson
import updateLumiCache


if __name__ == "__main__":
    print("Content-type: text/html")
    print()
    form = cgi.FieldStorage()
    error=""
    if "year" in form:
        year = form["year"].value
    else:
        year = str(datetime.date.today().year)

    configFile=f"/opt/app-root/src/config/lumi_config_{year}.json"

    config = readconfig.readconfig( configFile )
    logger = Logger( config["logfile"] )

    newFills,errors=copySummaryFiles.copyFiles(config,logger)

    integLumi=getLumiJson.summarizeMassiData(config,logger)
    logger.log("Integrated luminosity: "+str(integLumi))
    updateLumiCache.updateFillCache(config,logger,newFills)
    
               
