#!/usr/bin/python3
import glob
import hashlib
import os
import os.path
import readconfig
import subprocess
import shutil
import re
import requests

from lpcutils import Logger, getsrcdst

###########################################################


###########################################################

def copyFiles(config,logger):
    dstbase = os.path.join( config['workBaseDir'], str(config["year"]), 'measurements' )

    logger.log(str(config))
    logger.log(str(os.listdir("/eos/project/c/cms-runcoordination/LHCFILES/2022/lumi/")))
    logger.log( "Copying massi files to " + dstbase )

    EXPERIMENTS = config['srcDir'].keys()

    errors=[]
    fillsFound={}
    
    for exp in EXPERIMENTS:

        logger.log("Looking for new MASSI files from "+exp)
        fillsFound[exp]=[]
        extraoptions = " "

        selection = config["dataFormat"][exp]

        f1 = int(config["firstFill"])
        f2 = int(config["firstFillIon"])

        firstFill = min( f1, f2 )
        if f1 == 0:
            firstFill = f2
        elif f2 == 0:
            firstFill = f1


        l1 = int(config["lastFill"])
        l2 = int(config["lastFillIon"])
        lastFill = max( l1,l2 )

        srcdst = getsrcdst( config, exp )

        for k,val in srcdst.items():
            src = val[0]
            dst = val[1]

            dstFiles={}
            if os.access(dst,os.R_OK):
                with os.scandir(dst) as it:
                    for entry in it:
                        stat=entry.stat()
                        dstFiles[entry.name]=(stat.st_size,stat.st_mtime)
            else:
                os.mkdir(dst)

            srcFiles={}
            if os.access(src,os.R_OK):
                with os.scandir(src) as it:
                    for entry in it:
                        if not entry.name.endswith(selection): continue
                        try:
                            fillno=int(entry.name.split(".")[0])
                        except ValueError:
                            logger.log("Misformed input name: "+entry.name)
                            continue
                        if fillno<firstFill or fillno>lastFill: continue
                        stat=entry.stat()
                        srcFiles[entry.name]=(stat.st_size,stat.st_mtime)
                    
            toArchive=[]
            for entry in srcFiles:
                if not entry in dstFiles or srcFiles[entry]!=dstFiles[entry]:
                    if entry in dstFiles:
                        srcMD5=hashlib.md5(open(os.path.join(src,entry),'rb').read()).hexdigest()
                        dstMD5=hashlib.md5(open(os.path.join(dst,entry),'rb').read()).hexdigest()
                        if srcMD5==dstMD5:
                            logger.log(f"Not copying {entry} as hash has not changed")
                            continue
                        else:
                            logger.log(f"Hash changed for {entry}: {srcMD5} {dstMD5}")
                    toArchive.append(entry)
            logger.log("To be copied from "+src+": "+str(toArchive))

            fileType=dst.split("/")[-1]  # a bit hackish

            for name in toArchive:
                out=shutil.copy2(os.path.join(src,name),dst)
                logger.log("Copied to: "+out)
                fillno=int(name.split(".")[0])
                fillsFound[exp].append(fillno)
                if "lumi" in fileType:
                    logger.log("Will upload to NXCALS:"+name)
                    # archive to nxcals through interface from michi
                    filedata = {'file': open(os.path.join(src,name), 'rb')}
                    result=requests.post("http://cs-ccr-lhcswgpn:4242/update/massi"+fileType,
                                         files=filedata)
                    if result.status_code!=200:
                        logger.log("ERROR uploading code: "+str(result.status_code))
                        logger.log(str(result.text))


    return fillsFound,errors

if __name__ == "__main__":
    config = readconfig.readconfig( "lumi_config.json" )
    logger = Logger( config["logfile"] )
    result=copyFiles(config,logger)
    print(result)

