#!/usr/bin/python3
import json
import cgi
import datetime
import os

schemeDir = "/data/fillingSchemes"

def loadTable(year):
    
    # get the filltable generated from timber            
    
    datapath=f"/data/lumiData/{year}/FillTable.json"
    if os.access(datapath,os.R_OK):
        timbertab=json.load(open(datapath))
    else:
        timbertab={}

    # Now get the annotated fill table which we want to view/edit here
    tablepath = f"/data/FillTables/draft/FillTable_{year}.json"
    if os.access(tablepath,os.R_OK):
        anntab=json.load(open(tablepath))
    else:
        anntab={}
    
    # Now add all non existing entries of the timber table into the Fill Table
    for fill, entry in timbertab.items() :
        if fill not in anntab:
            anntab[ fill ] = entry
        else:
            # for already existing fills correct some data which might have changed
            anntab[ fill ]['length_sb'] = entry['length_sb'] # this could change if the editor is called
                           # in the middle of a fill which toggles from STABLE BEAMS to ADJUST and back
            anntab[ fill ]['start_sb'] = entry['start_sb']  # in principle this should never change
            
    # Now make all fills completely editable, which are not in the Timber
    for fill, entry in anntab.items():
        if fill not in timbertab:
            entry['fulledit'] = True

    # Now find all filling schemes on disk
    schemes=[]
    for (root,dirs,files) in os.walk( schemeDir ): 
        for filen in files:
            schemes.append(filen)
            
    # mark the ones which have a filling scheme on server
    for fill in anntab:
        anntab[fill]['hasscheme']=anntab[fill]["scheme"]+".csv"  in schemes

            
    return anntab


if __name__ == "__main__":
    form = cgi.FieldStorage()
    error=""
    if "year" in form:
        year = form["year"].value
    else:
        year = str(datetime.date.today().year)

    action = form['action'].value

    if action == 'load' or action == 'loadro' :
        data = loadTable(year);
        res = { "status" : "loaded", 
                "action" : action,
                "data"  : data,
                "year"  : year,
                "error" : error }

    elif action == 'save' :
        year = form['year'].value
        datastr =  form['table'].value
        data = json.loads( datastr )
        tablepath = f"/data/FillTables/draft/FillTable_{year}.json"

        fd = open( tablepath, "w" )
        json.dump( data, fd )
        fd.close()

        res = { "status" : "saved",
                "action" : action,
                "error" : error }

    elif action == 'release' :
        year = form['year'].value
        datastr =  form['table'].value
        data = json.loads( datastr )
        tablepath = f"/data/FillTables/FillTable_{year}.json"

        fd = open( tablepath, "w" )
        json.dump( data, fd )
        fd.close()

        res = { "status" : "released",
                "action" : action,
                "error" : error }
    else:
        error =  "action " + action + " not known"
        res = { "status" : "problem" }


    print("Content-type: application/json")
    print()
    print(json.dumps( res ))

