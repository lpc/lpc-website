#!/usr/bin/env python3

import cgi
import glob
import os
import os.path
import sys
import re
import json
import datetime
import csv
import re

#be able to print utf-8
sys.stdout = open(sys.stdout.fileno(), mode='w', encoding='utf8', buffering=1)

def makePage(year,schemes,uploaded):
    user=os.environ['HTTP_X_FORWARDED_USER']
    html='''<html lang="en">

  <head>
    <title>LPC Schemes Uploader</title>
    <link rel="stylesheet" type="text/css" href="/css/lpc.css">
  </head>

  <body>

    <!-- page header -->

    <table border="0" id="table1" width="100%">
      <tr>
        <td width="90">
          <a href="http://user.web.cern.ch/user/Welcome.asp">
            <img border="0" src="/images/CERNlogo.gif" alt="CERN" align="texttop" width="72" height="72">
          </a>
        </td>
        <td>
          <p class="header-headline">Upload filling schemes from editor</p>
          <p align="center">
            <a href="/">LPC home</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="/internal">Internal pages</a>
          </p>
        </td>
        <td width="90">
          <img border="0" src="/images/lpcnum.gif" width="72" height="72" align="right">
        </td>
      </tr>
    </table>

    <hr>
    
    <!-- page header end -->
    <div id="debug">&nbsp;</div>

    <div id="error">&nbsp;</div>
    
    <div id="search" style="margin:auto; width:60%;">
    <form id="upload" name="upload" method="post" action="schemesHandler.py">
    <label>User: </label> 
    <input id="user" name="user" title="Password" type="text" value="'''+user+'''" readonly/>&nbsp;
    <br />
    <label>Password: </label> 
    <input id="password" name="password" title="Password" type="password" />&nbsp;
    <br />
    <label>Comment to use: </label> 
    <input id="comment" size=100 name="comment" title="Commit comment" type="text" /><br />
    <input type="submit" value="Upload"/>
    </form>
    </div>
<div>
'''

    html+='''<table align="center" class="clearAndSimple">'''
    
    prevArea="none"

    for scheme in sorted(schemes):
        area=scheme.split('/')[5]
        fullname='/'.join(scheme.split('/')[5:-1])
        name=scheme.split('/')[-1]
        if name==area: area="/"
        if area!=prevArea:
            if prevArea!="none":
                html+="      </tbody>"
            html+=f"""<thead>
		<tr>
			<th colspan="3" style="text-align:center;border-top: 1px solid black; border-bottom: none">{area}</th>
		</tr>
		<tr>
			<th style="width: 150px">Area</th>
			<th>Scheme</th>
			<th width="150px">Status</th>
		</tr>
	</thead>
	<tbody id="listbody">
"""
            prevArea=area


        if name in uploaded:
            status="Uploaded"
        else:
            status=f'<input form="upload" type="checkbox" name="{scheme}"/>'
        html+=f'''<tr>
			<td>{fullname}</td>
			<td><a href="/schemeEditor.html?user=lpc&scheme={year}/{area}/{name}" target="_blank">{name}</a></td>
			<td>{status}</td>
		</tr>
'''

    html+='''      </tbody>
    </table>
</div>
<hr/>
<p>&nbsp;</p>
  </body>
</html>
'''

    return html

if __name__ == "__main__":

    form = cgi.FieldStorage()
    if 'year' in form:
        year=form['year'].value
    else:
        year=str(datetime.date.today().year)

    schemes=glob.glob(f"/data/editorSchemes/lpc/{year}/**/*.json",recursive=True)
    uploaded=os.listdir("/data/injection-schemes")
    html=makePage(year,schemes,uploaded)
    print('''Content-Type: text/html

''')
    print(html)
