#!/usr/bin/python3

import cgi
import os
import re
import json
import glob


if __name__ == "__main__":
    srcPath="/opt/app-root/src/website"
    confName="/etc/httpd/conf/httpd.conf"
    #FIXME: should find more robust way of finding the calendar.html page
    if os.access(confName,os.R_OK):
        conf=open(confName).read()
        idx=conf.find("DocumentRoot ")
        eidx=conf.find('\n',idx)
        srcPath=conf[idx:eidx].split('"')[1]

    htmlout = '''Content-Type: text/html

    '''

    fd = open( os.path.join( srcPath, "calendar.html" ),'r')
    calhtml = fd.read()
    fd.close()

    #calhtml = calhtml.replace( '</title>\n','</title>\n<base href="https://lpc.web.cern.ch/git-source/">\n')
    calhtml = calhtml.replace( 'LPC home</a>', 'LPC home</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="/internal">Internal pages</a>' )

    admintools = '''
    <table>
    <tr><td><label>Calendar name</label></td><td><input id="calname" type="text"/></td></tr>
    <tr><td><label>Year</label></td><td><input id="calyear" type="number"/></td></tr>
    <tr><td><label>Label</label></td><td><input id="callabel" type="text"> (for key in json file; format: yyyy-v1-v2) </td></tr>

    <tr><td></td><td> <button onclick="saveCalendar()">Save/update</button> <button onclick="deleteCalendar()">Delete</button> </td></tr>
    </table>
    <hr>
    '''

    calhtml = calhtml.replace( "<!-- page header end -->", "<!-- page header end -->\n" + admintools )

    htmlout += calhtml

    print(htmlout)

