#!/usr/bin/python3

import tarfile
import zipfile
import csv
import glob
import sys
import datetime
import json
import os

import readconfig
from lpcutils import Logger
import re

# create a quick and dirty period lookup table
# get the smallest and the largest fillnumber
periodlookup = {}
fillperiods = {}

def process( exp, fill, integlumi, DIRBASE, logger ):
    filln = str(fill['fill'])

    # check if we have the new directory structure or not
    if os.path.isdir( os.path.join( DIRBASE, exp, 'lumi' ) ):
        tgz = os.path.join( DIRBASE, exp, 'lumi', filln +".tgz")
        zipf = os.path.join( DIRBASE, exp, 'lumi', filln +".zip")
    else:
        tgz = os.path.join( DIRBASE, exp, filln +".tgz")
        zipf = os.path.join( DIRBASE, exp, filln +".zip")

    member = filln + "/" + filln + "_summary_" + exp + ".txt"
    if os.path.isfile( tgz ):
        try:  # missing summary file encountered in lhcb
            tf = tarfile.open( tgz, "r" )
            sf = tf.extractfile( member )
            sfl = sf.readline().decode('utf-8')
            sfl = re.sub( '\s+',' ',sfl).strip() # multiple ' ' encountered in LHCb
            summarr = sfl.split(' ')
        except:
            logger.log("Problem extracting summary file for " + exp + " in fill " + filln)
            summarr = [0,0,0,0]
    elif os.path.isfile( zipf ):
        zf = zipfile.ZipFile(zipf)
        ozf = zf.open(member)
        summarr = ozf.readline().split('\t')
    else:
        logger.log( "No Massi files in " + exp + " for fill " + filln )
        summarr = [0,0,0,0]
    if summarr == ['']:
        logger.log( "summary empty  for fill " + filln + " in " + exp) 
        summarr = [0,0,0,0]
    fill[exp + '_integ_fillLumi'] = float(summarr[3])
    fill[exp + '_peak_fillLumi'] = summarr[2]
    fill[exp + '_timeSB_massifile'] = float(summarr[1]) - float(summarr[0])
    integlumi = integlumi + float(fill[exp + '_integ_fillLumi'])/1000000000.0
    fill[exp + '_integ_yearLumi'] = integlumi

    fillnum = int(filln)
    
    if fillnum in periodlookup:
        period = periodlookup[fillnum]
        if period not in fillperiods:
            fillperiods[period] = { }
        if fillnum not in fillperiods[period] :
            fillperiods[period][fillnum] = {}

        fillperiods[period][fillnum][exp] = float(summarr[3])
    else:
        logger.log("There is a bug since " + str(fillnum) + " is not in the periodlookup")

    return integlumi

def summarizeMassiData(config,logger):
    logger.log( "getLumiJson" )

    DIRBASE = os.path.join( config['workBaseDir'], config['year'], "measurements" )
    LUMIDIR = os.path.join( config['lumiDir'], config['year'])


    for period,rlist in config['periods'].items():
        for duo in rlist:
            for fillno in range(duo[0],duo[1]+1):
                periodlookup[fillno] = period


    sbfills = []
    state = 'no'
    fill = 0
    entry = {}

    ftd = open( os.path.join( LUMIDIR, "FillTable.json" ), 'r' )
    fillTable = json.load( ftd )
    ftd.close()

    keys = []
    for key in fillTable:
        keys.append( key )
    keys.sort( key=int)

    sbfills = []
    for fill in keys:
        fill = fillTable[fill]
        entry = { 'startUnix' : fill['starttime'],
                  'stopUnix' : fill['stoptime'],
                  'startDateTime' : fill['start_sb'],
                  'fill' : fill['fillno'],
                  'timeSB' : fill['length_sb'] }
        sbfills.append( entry )

    cmsinteg = 0.0
    atlasinteg = 0.0
    lhcbinteg = 0.0
    aliceinteg = 0.0

    for fill in sbfills:
        cmsinteg = process( 'CMS', fill, cmsinteg, DIRBASE, logger )
        atlasinteg = process( 'ATLAS', fill, atlasinteg, DIRBASE, logger )
        lhcbinteg = process( 'LHCb', fill, lhcbinteg, DIRBASE, logger )
        aliceinteg = process( 'ALICE', fill, aliceinteg, DIRBASE, logger )

    outfile = os.path.join( LUMIDIR, "lumidata.json")

    of = open( outfile, 'w')

    infodata = { 'firstFill_pp' : config['firstFill'],
                 'lastFill_pp' : config['lastFill'],
                 'firstFill_ions' : config['firstFillIon'],
                 'lastFill_ions' : config['lastFillIon'],
                 'year' : config['year']
                 }

    resdata = { 'infodata' : infodata,
                'filldata' : sbfills }
    of.write( json.dumps(resdata, indent=4) )
    of.close()

    # The runperioddata.json file is used by the Accelerator statistics page and 
    # therefore should be maintained. It makes sure that the massi file information
    # is used for the lumi data presented by this page and so that page is consistent
    # with the LPC pages. Please try to keep this file up to date...
    #outfile = os.path.join( config['webBaseDir'], config['year'], "runperioddata.json")
    #of = open(outfile, 'w')
    #of.write( json.dumps( fillperiods, indent=4 ) )
    #of.close()

    logger.log( "Re-generated the lumidata.json file" )
    return(cmsinteg,atlasinteg,lhcbinteg,aliceinteg)

if __name__ == "__main__":
    config = readconfig.readconfig( "lumi_config.json" )
    logger = Logger( config["logfile"] )
    result=summarizeMassiData(config,logger)
    print(result)

