#!/usr/bin/python3

import cgi
import os
import re
import json
import glob
import sys

#be able to print utf-8
sys.stdout = open(sys.stdout.fileno(), mode='w', encoding='utf8', buffering=1)

def makePage(data,orgname):
    nfiles = []
    htmlout = '''<!DOCTYPE HTML>
    <html>
    <head>
      <title>Edit LPC Meeting Summaries</title>
      <meta charset="UTF-8">
      <link rel="stylesheet" type="text/css" href="/css/lpc.css">
      <link rel="stylesheet" type="text/css" href="/css/jquery-ui.css">
      <link rel="stylesheet" type="text/css" href="/css/htmledit.css">
      <script src="/js/jquery-2.2.0.min.js"></script>
      <script src="/js/jquery-ui.min.js"></script>
      <script src="/internal/js/ckeditor/ckeditor.js"></script>
      <script src="/internal/js/editMinutes.js"></script>
    </head>
    <body>
        <!-- page header -->

        <table id="table_header" style="width:100%">
          <tr>
            <td style="width:72px">
              <a href="http://user.web.cern.ch/user/Welcome.asp">
                <img src="/images/CERNlogo.gif" alt="CERN" width="72" height="72">
              </a>
            </td>
            <td>
              <p class="header-headline">
                Edit LPC Minutes
              </p>
              <p class="center">
                <a href="/">LPC home</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="/cgi-bin/listMinutes.py">LPC minutes</a>
              </p>
            </td>
            <td style="width:72px">
              <img alt="LPC: 79977" src="/images/lpcnum.gif" width="72" height="72">
            </td>
          </tr>
        </table>

        <hr>

        <!-- page header end -->

<!--        <div id="error"></div> -->
        <div id="warning"></div>
        <div id="debug"></div>
        <div id="contents">
    '''

    formhtml = '''
    <div id="error"></div>
    <form name="editMeeting" id="editMeeting">
    <div style="display: inline-block">
    <h2>Meeting info</h2>
    <table>
    <tr><td><label>Date of the meeting: </label></td><td><input name="date" id="date" type="date" value="'''+data['date']+'''" /></td></tr>
    <tr><td><label>Filename suffix: </label></td><td><input name="suffix" id="suffix" type="text" value="'''+data['suffix']+'''" /></td></tr>
    <tr><td><label>URL of the indico agenda: </label></td><td><input name="indicourl" id="indicourl" type="url" value="'''+data['indicourl']+'''" /></td></tr>
    <tr><td><label>Short description of the meeting: </label> </td><td><input name="shortdesc" id="shortdesc" type="text" value="'''+data['shortdesc']+'''" /></td></tr>
    <tr><td><label>Purpose of the meeting:</label></td><td><textarea name="purpose" id="purpose">'''+data['purpose']+'''</textarea></td></tr>
    </table>
    </div>
    <div style="display:inline-block">
    <h2 style="margin-left: 20px;">Editor actions</h2>
    <table style="margin-left:20px">
    <tr><td><label>Save internal summary: </label></td><td><input type="button" value="Save" onclick="saveSummary( 'edit' )"></td></tr>
    <tr><td><label>Publish summary as draft: </label></td><td><input type="button" value="Save as draft" onclick="saveSummary( 'draft' )"></td></tr>
    <tr><td><label>Publish summary as final: </label></td><td><input type="button" value="Save as final" onclick="saveSummary( 'final' )"></td></tr>
    </table>
    </div>
    </form>
    '''


    htmlout += formhtml
    htmlout += '''
    <div id="pagetext" contenteditable="true" class="minutes-text">
'''+data['text']+'''    </div>
    </div>
    </body>
    </html>
    '''
    return htmlout

def loadMinutes(name):
    name=os.path.basename(name)
    for area in ['edit','draft','final']:
        path=f"/data/lpc-minutes/{area}/{name}.json"
        if os.access(path,os.R_OK):
            data=json.load(open(path))
            return data
    return { 'text': "",
             'date': "",
             'suffix': "",
             'indicourl': "https://indico.cern.ch/event/",
             'shortdesc': "",
             'purpose': ""
         }


if __name__ == "__main__":
    form = cgi.FieldStorage()
    if 'minutes' in form:
        minName=form['minutes'].value
        data=loadMinutes(minName)
        html=makePage(data,minName)
    else:
        html="No minutes specified"

    print('''Content-Type: text/html

''')
    print(html)
