#!/usr/bin/python3
import json
import cgi
import os
import os.path
import glob
import re
import datetime
import shutil

def formatDate( date, reverse=False ):
    dat = datetime.datetime.strptime( date, "%Y-%m-%d" )
    if ( reverse ):
        return datetime.datetime.strftime( dat, "%Y-%m-%d" )
    return datetime.datetime.strftime( dat, "%d-%m-%Y" )

def findMinutes(name):
    found=[]
    name=os.path.basename(name)
    for area in ['final','draft','edit']:
        path=f"/data/lpc-minutes/{area}/{name}.json"
        if os.access(path,os.R_OK):
            found.append(path)
    return found

def saveSummary(form) :
    orgName = form['orgName'].value
    destination = form['destination'].value
    text = form['text'].value
    date = form['date'].value
    if "indicourl" in form:
        indicourl = form['indicourl'].value
    else:
        indicourl = ""
    shortdesc = form['shortdesc'].value
    purpose = form['purpose'].value
    if 'suffix' in form:
        suffix = os.path.basename(form['suffix'].value)
    else:
        suffix = ""

    newName=formatDate( date, True )
    if suffix:
        newName+="_"+ suffix
    retName=""
    if newName!=orgName:
        existingNames=findMinutes(newName)
        if existingNames:
            return "Cannot overwrite existing minutes with same name - use a different suffix",""
        retName=newName
        orgFiles=findMinutes(orgName)
        for fname in orgFiles:
            os.remove(fname)

    datarecord = { 'text' : text,
                   'date' : date,
                   'suffix' : suffix,
                   'indicourl' : indicourl,
                   'shortdesc' : shortdesc,
                   'purpose' : purpose }
    editName=f"/data/lpc-minutes/edit/{newName}.json"
    draftName=f"/data/lpc-minutes/draft/{newName}.json"
    finalName=f"/data/lpc-minutes/final/{newName}.json"

    fd = open( editName, "w" )
    json.dump( datarecord, fd )
    fd.close()

    if destination!="edit":

        saveName=draftName
        delName=finalName
        if destination=='final':
            saveName=finalName
            delName=draftName 

        fd = open( saveName, "w" )
        json.dump( datarecord, fd )
        fd.close()

        if os.access(delName,os.R_OK):
            os.remove(delName)

    return "",retName

if __name__ == "__main__":
    form = cgi.FieldStorage()
    res = { "status" : "" }
    error,newName=saveSummary(form);
    res['error'] = error
    res['newName'] = newName

    print("Content-type: application/json")
    print()
    print(json.dumps( res ))

