#!/usr/bin/python3
import json
import cgi
import os
import glob
import sys

if __name__ == "__main__":
    debug = ""
    error = ""
    status = ""

    form       = cgi.FieldStorage()
    user       = form['user'].value
    action     = form['action'].value
    etype      = form['type'].value


    pp=f"/data/editorSchemes/{user}"

    if not os.path.isdir( pp ):
        error = f'User "{user}" does not exist.'
        status="ERROR"
        retdat={}

    elif action == "loadBatches":
        bfile = os.path.join( pp, etype + ".dat" );
        if os.path.isfile( bfile ):
            fd = open( bfile, 'r' )
            retdat = json.load( fd )
            fd.close()
            status = "loaded"
        else:
            retdat = {}

    elif action == "removeBatch" :
        pname   = form['pname'].value
        bfile   = os.path.join( pp, etype + ".dat" )
        batches = {}
        if os.path.isfile( bfile ) :
            fd      = open( bfile, 'r' )
            batches = json.load( fd )
            fd.close()

        if pname in batches:
            del batches[pname]
            fd = open( bfile, 'w' )
            json.dump( batches, fd )
            fd.close()
            status = "deleted"
        else:
            status = 'unknown pattern "' + pname + '"'

        retdat = batches

    elif action == "saveBatch" :
        pname   = form['pname'].value
        pattern = json.loads(form['pattern'].value)
        bfile   = os.path.join( pp, etype + ".dat" )
        batches = {}
        if os.path.isfile( bfile ) :
            fd      = open( bfile, 'r' )
            batches = json.load( fd )
            fd.close()

        batches[pname] = pattern
        fd = open( bfile, 'w' )
        json.dump( batches, fd )
        fd.close()

        status = "saved"
        retdat = batches




    obj = { "debug"  : debug,
            "error"  : error,
            "status" : status,
            "data"   : retdat,
            "action" : action }

    print("Content-type: application/json")
    print()
    print(json.dumps( obj ))

