#!/usr/bin/python3
import cgi
import os
import sys
import re
import json
import datetime
import csv
import re

FILL_H = {}
schemeDir = "/data/fillingSchemes"


def makeFillHash():
    for year in [2015,2016,2017,2018,2022,2023,2024,2025]:
        datapath=f"/data/lumiData/{year}/FillTable.json"
        if os.access(datapath,os.R_OK):
            filltable = json.load( open(datapath) )
            for fill,data in filltable.items():
                FILL_H[ str(fill) ] = data['scheme']
                
def getFillData( filldata ):
    filename = filldata['name'] + ".csv"
    for (root,dirs,files) in os.walk( schemeDir ): 
        for filen in files:
            if filen == filename:
                fd = open( os.path.join( root, filen ) )
                fileContent = fd.read()
                fd.close()
                filldata['csv'] = fileContent
                
def returnError( text ):
    result = { 'error' : text }
    resultstr = json.dumps( result )
    print("Content-Type: application/json")
    print()
    print(resultstr)
    sys.exit()




if __name__ == "__main__":
    form = cgi.FieldStorage()
    fmt = "json"
    if "fill" in form:
        filllist = form.getlist("fill")
    else:
        returnError( 'No \'fill\' query parameter in request.' )
        sys.exit(-1)
    if "fmt" in form:
        fmt = form.getfirst("fmt")
        if not fmt in ["json", "download"]:
            returnError( "Unknown format: '" + fmt + "'." )

    makeFillHash()

    error = ""
    result = { 'fills' : {} }
    for fill in filllist:
        if fill in FILL_H:
            filldata = { "name" : FILL_H[ fill ] }
            getFillData( filldata )
            result['fills'][fill] = filldata
        else:
            error += ("No scheme found for fill " + fill + ". " )


    if error != "":
        result['error'] = error

    if fmt == "download" :
        if len( result['fills'] ) > 1:
            returnError( "You can only download one fill at a time (only one fill parameter allowed)" )
        if 'error' in result:
            returnError( result['error'] )

        filldat = list(result['fills'].values())[0]
        filename = filldat['name'] + ".csv"

        print("Content-type: application/octet-stream")
        print("Content-Disposition: attachment; filename=\"" + filename + "\"")
        print()
        print(filldat['csv'])


    if fmt == "json" :
        resultstr = json.dumps( result )

        print("Content-Type: application/json")
        print()
        print(resultstr)
    

