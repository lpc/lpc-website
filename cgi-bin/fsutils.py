import os
import re
import shutil
import json

def checkpassword( userdir, form ):
    pwdfile = os.path.join( userdir, "password.txt")
    if not os.path.isfile( pwdfile ):
        return (True, "")

    if not "password" in form:
        error = "You have to enter a password."
        return (False, error)

    sentpassword = form['password'].value
    fd = open( pwdfile, 'r' )
    password = fd.read().strip()
    fd.close()

    if  sentpassword != password :
        return (False, "Password does not match. ")

    return( True, "" )


def getJsonFile( path ):
    if not os.path.isfile( path ):
        return( None, "No such file: " + path )

    fd = open( path )
    data = json.load( fd )
    fd.close()
    return ( data, "" )

def saveJsonFile( data, path ):
    if not os.path.isfile( path ):
        return( None, "No such file: " + path )

    fd = open( path, 'w' )
    data = json.dump( data, fd )
    fd.close()
    return

# Assumes that pp has been checked to be valid
def handlefsops( pp, form, action ):

    error = ""
    # A flag to indicate if we handled the request
    handled = False
    withFiles = True
    # remove directory
    # 'dir' parameter, directory must be empty
    if action == "removeDirectory" :
        withFiles = False;
        handled = True
        dirname = form['dir'].value;
        dpath = os.path.join(pp, dirname)
        # some minimal security
        mo1 = re.search( r"\.\.", dpath )
        if mo1:
            error = "Illegal operation"
        elif not os.listdir(dpath):
            (pwdok, error) = checkpassword(pp, form)
            if pwdok:
                os.rmdir(dpath)            
        else:
            error = "Directory \"" + dirname + "\" is not empty!";

        
    # create directory
    # 'parent' (optional), 'newdir'
    elif action == "createDirectory" :
        handled = True
        withFiles = False;
        if 'parent' in form:
            parent = form['parent'].value
        else:
            parent = ""
        newDirName = form['newdir'].value.strip()
        newpath = os.path.join( pp, parent, newDirName )
        # some minimal security
        mo1 = re.search( r"\.\.", newpath )
        if mo1:
            error = "Illegal operation"
        else:
            (pwdok, error) = checkpassword(pp, form)
            if pwdok:
                if os.path.exists( newpath ):
                    error = "Already exists. Choose another name."
                else:
                    try:
                        os.mkdir( newpath )
                    except :
                        # catches when parent dir does not exist e.g. have a
                        # non-existing user in user entry and try to create a dir
                        error = "Impossible operation"

            
    # delete a file
    # 'filename' is the file and MUST be a file. 
    elif action == "delete":
        handled = True
        # check if directory exists
        if not os.path.isdir( pp ):
            error = "There is no user called: \"" + user + "\""
            retdat = ""
        else:
            dfile = form['filename'].value
            dfile = os.path.join( pp, dfile )    
            # some minimal security
            mo1 = re.search( r"\.\.", dfile )
            if mo1:
                error = "Illegal operation"
            else:
                (pwdok, error) = checkpassword(pp, form)
                if pwdok :
                    if os.path.isfile( dfile ):
                        os.remove( dfile )
                    else:
                        error = "There is no scheme \"" + dfile + "\" to delete."
    
    elif action == "move":
        handled = True
        src = os.path.join( pp, form['src'].value )
        dst = os.path.join( pp, form['dst'].value )
        # minimal security: do not allow ".." in the paths
        mo1 = re.search( r"\.\.", src )
        mo2 = re.search( r"\.\.", dst )
        if mo1 or mo2:
            error = "illegal operation"
        else:
            (pwdok, error) = checkpassword(pp, form)
            if pwdok:
                shutil.move( src, dst )
    
    return (handled, error, withFiles)
