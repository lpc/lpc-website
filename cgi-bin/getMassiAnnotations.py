#!/usr/bin/python3

import cgi
import os
import json
import datetime

startyear = '2017'

def addTable(tag, exp, annot ):
    html=""
    if annot == {}:
        return ""
    html+='<span  class="bold">'
    html+='</span><br><span class="italic">'
    dann = annot[exp][tag]['digit_annotation']
    vann = annot[exp][tag]['value_annotation']

    dstr = repr(vann)
    #print vann
    skeys = sorted(dann.keys(), key=int)

    point = ''
    digit = 'k'
    vstr = ""
    for key in skeys:
        vstr += point + digit
        digit = chr(ord(digit)+1)
        point = '.'
    html+= vstr + " [ _number(s) ... ]</span>"

    html+='''<table class="massidigits" style="border: 1px solid black;">
<tr><th class="borderbottom">digit</th><th class="borderbottom"></th><th class="borderbottom">meaning and possible values</th></tr>'''
    digit = 'k'
    for key in skeys :
        html+= '<tr><td class="digit">' + digit + "</td><td>:</td><td>" + dann[str(key)] + "<br>"
        digit = chr(ord(digit)+1)
        try:
            pvals = vann[str(key)]
            pvk = sorted( pvals.keys(), key=int)

            html+='<table class="massivalues"><tr><th class="digit">value</th><th></th><th>meaning</th></tr>'

            for pk in pvk:
                html+='<tr><td class="digit">' + pk + '</td><td>:</td><td>' + pvals[str(pk)] + '</td></tr>'

                
            html+='</table>'
        except :
            html+="<em>inconsistent annotation file</em> " 
        html+="</td></tr>"
    html+='</table>'
    return html
    
def addVersionTable( tag, annot ):
    if annot == {}:
        return ""
    html='''
        <div>
          <table id="annotTable" class="tablesorter" style="border: 1px solid grey; width:auto; margin:auto;">
            <thead>
               <tr><th style="min-width:50px">Fill No</th><th style="min-width:50px">ALICE</th><th style="min-width:50px">ATLAS</th><th style="min-width:50px">CMS</th><th style="min-width:50px">LHCb</th></tr>
            </thead>
            <tbody>
    '''
    
    vh = annot['fills'][tag]
    for fillno in sorted( vh.keys(), key=int ):
        versions = vh[str(fillno)]
        html+='<tr><td>'+str(fillno)+'</td><td>'+versions['ALICE']+"</td><td>"+versions['ATLAS']+'</td><td>'+versions['CMS']+'</td><td>'+versions['LHCb']+'</td></tr>'
    
    html+='''
            </tbody>
          </table>
        </div>'''
    return html
    
def makeTable(year):
    annot = {}
    error = ""
    datapath=f"/data/lumiData/{year}/massi_annotations.json"
    if os.access(datapath,os.R_OK):
        annot = json.load(open(datapath))
    else:
        error = f"No data found for {year}"

    currentyear = str(datetime.date.today().year)

    htmlPage='''<!DOCTYPE HTML>
<html lang="en">
<head> 
  <meta charset="UTF-8">
  <title>Massi File Annotations (versioning)</title>
  <link rel="stylesheet" type="text/css" href="../css/lhc.css">
  <link rel="stylesheet" type="text/css" href="../css/lpc.css">
  <link rel="stylesheet" type="text/css" href="../js/themes/blue/style.css">
  <link rel="stylesheet" type="text/css" href="../css/jquery-ui.css">
  <script src="../js/jquery-2.2.0.min.js"></script>
  <script src="../js/jquery-ui.min.js"></script>
  <script src="../js/jquery.tablesorter.min.js"></script>
  <script src="../js/lhctab.js"></script>
  <script>
  doit = function() {
    $('table#annotTable').tablesorter( );
  }
  $.fn.directtext = function() {
    var str = '';

    this.contents().each(function() {
        if (this.nodeType == 3) {
            str += this.textContent || this.innerText || '';
        }
    });
    return str;
  };

  </script>
</head>


<body onload="doit()">
    <!-- page header -->


    <table id="table_header" style="width:100%">
      <tr>
	<td style="width:72px">
	  <a href="http://user.web.cern.ch/user/Welcome.asp">
	    <img src="/images/CERNlogo.gif" alt="CERN" width="72" height="72">
	  </a>
	</td>
	<td>
	  <p class="header-headline">
	    Massi File Annotations (versioning)
	  </p>
	  <p class="center">
	   <a href="/">LPC home</a>
	  </p>
	</td>
	<td style="width:72px">
	  <img alt="LPC: 79977" src="/images/lpcnum.gif" width="72" height="72">
	</td>
      </tr>
    </table>

    <hr>
    
    <!-- page header end -->

    <div id="error">'''
    htmlPage+=error
    htmlPage+='''</div>
    <div id="debug"></div>
    <div id="contents" class="minutes-text">
<h2>Massi File versioning in the experiments</h2>
<p>
Each experiments develops its appropriate versioning system according to the guidelines
in the <a href="../doc/MassiFileDefinition_v2.html" target="_blank">Massi File Specifications</a>.
Below, the versioning systems of the various experiments are explained. This information
is extracted automatically from the Massi files. On the bottom of the page a table 
lists the versions of the current Massi files for each fill number in each experiment.
</p>
<p>
Massi file versioning is available as of 2017.
</p>
<script>
var baseu = location.protocol + '//' + location.host + location.pathname
</script>
'''
    htmlPage+='<p><select class="topselector" id="yearselect" onChange="window.location.href=baseu+\'?year=\'+$(\'#yearselect\').val()">'
    for oy in reversed(range(int(startyear),int(currentyear)+1)):
        if oy in [2019,2020,2021,2022]: continue # skip LS2
        sel = ""
        if oy == int(year):
            sel = "selected"
        htmlPage+='<option value="'+str(oy)+'" '+sel+'>'+str(oy)+'</option>'
    htmlPage+= '''</select></p>
<table id="massiannot"><colgroup>
   <col style="background-color: #ffffb0;">
   <col style="background-color: #e0ffd0;">
</colgroup>
''' 
    htmlPage+='<tr style="border-bottom: 1px solid black;"><th style="width:50%">Luminosity version annotation</th><th>Lumi-region version annotation</th></tr>'


    for exp in [ 'ALICE','ATLAS','CMS','LHCb' ]:
        htmlPage+='<tr><td>'
        htmlPage+='<p><b>'
        htmlPage+=exp
        htmlPage+='</b></p>'
        htmlPage+=addTable('lumi', exp, annot )
        htmlPage+='</td><td>'
        htmlPage+='<p><b>'
        htmlPage+=exp
        htmlPage+='</b></p>'
        htmlPage+=addTable( 'lumireg', exp, annot )
        htmlPage+='</td>'
        htmlPage+='</tr>'        

    htmlPage+='</table>'
    
    htmlPage+="""<p>&nbsp;</p>
<hr style='width:80%;'>
<p>&nbsp;</p>"""
    htmlPage+='''<table style="width:100%;">
<colgroup>
   <col style="background-color: #ffffb0;">
   <col style="background-color: #e0ffd0;">
</colgroup>
''' 
    htmlPage+="<tr>"
    htmlPage+=f'<th>{year} Massi-Luminosity-File Versions</th><th>{year} Massi-Luminosity-Region-File Versions</th></tr>'
    htmlPage+='<tr><td>'
    htmlPage+=addVersionTable('lumi', annot)
    htmlPage+="</td><td>"
    htmlPage+=addVersionTable('lumireg', annot)
    htmlPage+='''
    </td></tr></table>
  </div>
</body>
</html>
'''
    return htmlPage

if __name__ == "__main__":
    form = cgi.FieldStorage()

    currentyear = str(datetime.date.today().year)

    # get the annotation data
    if "year" in form:
        year = form["year"].value
    else:
        year = currentyear
    
    htmlPage=makeTable(year)
    print("""Content-Type: text/html

    """+htmlPage)
