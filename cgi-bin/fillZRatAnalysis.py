#!/usr/bin/python3

import cgi
import os
import sys
import json
import tarfile
import zipfile

#FIXME: consider moving cache files to shared /data area

DIRBASE = "/eos/project/l/lpc/public/MassiFiles"

def error(errstr):
    if test :
        print(f"""Content-Type: text/html
<!DOCTYPE HTML>
<html>
<body>
{errstr}
</body>
</html>
""")
        sys.exit()


def getFillCache(fillnr,year):
    data={}

    datapath = f"{DIRBASE}/{year}/fillCache/{fillnr}.json"

    if os.access(datapath,os.R_OK):
        data=json.load(open(datapath))

    return data


if __name__ == "__main__":
    error = ""
    debug = "no debug"
    data  = {}

    form = cgi.FieldStorage()
    fillnr=int(form['fillnr'].value)
    year=int(form['year'].value)

    fillcache=getFillCache(fillnr,year)
    
    data = { 'action' : form['action'].value,
             'fillnr' : fillnr,
             'year' : year,
             'cmd' : form['cmd'].value,
             'data' : fillcache };

    reply = { 'data'  : data,
              'debug' : debug, 
              'error' : error }

    print("Content-type: application/json")
    print()
    print(json.dumps( reply ))

