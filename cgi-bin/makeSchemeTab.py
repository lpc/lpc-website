#!/usr/bin/python3
import json
import os
import urllib.parse
from scanScheme import scanFile, analyseScheme

def scanArea(query):
    querydict = urllib.parse.parse_qs( query )
    schemeName = querydict["schemeName"][0].strip()
    schemePath = querydict["schemePath"][0]
    relPath    = querydict["relPath"][0]

    schemeDir = os.path.join( schemePath, schemeName )
    cachefile = os.path.join( schemeDir, "schemeCache.json" )

    cachetime = 0
    if os.path.isfile( cachefile ):
        cachetime = os.path.getmtime( cachefile )
        fd = open(cachefile, 'r')
        result = json.load( fd )
        fd.close()
    else:
        result = { 'schemes' : {},
                   'relpath' : relPath + "/" + schemeName }

    cacheUpdate = False
    for scheme in sorted(os.listdir( schemeDir )):
        scheme = scheme.strip()
        if scheme.endswith( "csv" ):
            schemepath = os.path.join( schemeDir, scheme )
            txtpath = os.path.splitext(schemepath)[0] + ".txt"            
            if (os.path.getmtime( schemepath ) > cachetime) or (os.path.isfile( txtpath ) and os.path.getmtime( txtpath ) > cachetime) :
                injections = scanFile( schemepath )
                analyseScheme( injections )

                result['schemes'][scheme] = { 'description'  : injections['description'],
                                              'injections_1' : injections['injections_1'],
                                              'injections_2' : injections['injections_2'],
                                              'collisions'   : injections['collisions'],
                                              'bunches_1'    : injections['bunches']['bunches_1'],
                                              'bunches_2'    : injections['bunches']['bunches_2'],
                }
                cacheUpdate = True

    jresult = json.dumps( result )

    if cacheUpdate:
        cachefile = os.path.join( schemeDir, "schemeCache.json" )
        fd = open( cachefile, "w" )
        fd.write( jresult )
        fd.close()
    return jresult

if __name__ == "__main__":
    jresult=scanArea(str(os.environ["QUERY_STRING"]))
    print("Content-type: application/json")
    print()
    print('{ "debug" : "", "data" : ' + jresult + ' }')
    
