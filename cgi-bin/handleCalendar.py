#!/usr/bin/python3
import json
import cgi
import os
import sys

def getCalendars():
    datapath="/data/ToyCalendars/calendars.json"
    if not os.access(datapath,os.R_OK):
        return "No calendar file",{}
    data= json.load(open(datapath))
    return "",data

if __name__ == "__main__":
    form       = cgi.FieldStorage()
    dbx = ""
    error = "no calendars"
    calendars = ()
    action = form['action'].value
    error,calendars = getCalendars()

    response = { "debug"  : dbx,
                 "error"  : error,
                 "data"   : calendars,
                 "action" : action }

    print("Content-type: application/json")
    print()
    print(json.dumps( response ))

