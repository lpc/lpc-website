#!/usr/bin/python3

import cgi
import datetime
import json
import os
import sys


def getData(year):
    datapath=f"/data/lumiData/{year}/lumidata.json"
    zratepath=f"/data/lumiData/{year}/zCountingData.json"
    turnaroundpath=f"/data/lumiData/{year}/turn_around_cache.json"

    jsondoc = {}
    zratedoc  = {}
    tadoc = {}

    if os.access(datapath,os.R_OK):
        jsondoc=json.load(open(datapath))

    if os.access(zratepath,os.R_OK):
        zratedoc=json.load(open(zratepath))

    if os.access(turnaroundpath,os.R_OK):
        tadoc=json.load(open(turnaroundpath))

    error = ""

    # now get the marker data
    tablepath = f"/data/EventAnnotations/EventAnnotations_{year}.json"
    markers = []

    if os.path.isfile( tablepath ):
        fd = open( tablepath, 'r' )
        markers = json.load( fd )
        fd.close()

    data = jsondoc
    data['markers'] = markers
    data['zratio'] = zratedoc
    data['turnaround'] = tadoc
    data['error'] = error
    return data


if __name__ == "__main__":
    form = cgi.FieldStorage()
    if "year" in form:
        year = form["year"].value
    else:
        year = str(datetime.date.today().year)

    year=int(year)
    if year<2015 or year>2050: 
        print("""Content-type: application/json

Bad year given""")
        sys.exit(1)

    data=getData(year)
    jsondoc = json.dumps( data )

    print("Content-type: application/json")
    print()
    print(jsondoc)
