#!/usr/bin/python3

import cgi
import os
import sys
import json
import tarfile
import zipfile

DIRBASE = "/eos/project/l/lpc/public/MassiFiles"

def error(errstr):
    if test :
        print(f"""Content-Type: text/html
<!DOCTYPE HTML>
<html>
<body>
{errstr}
</body>
</html>
""")
        sys.exit()


def getFillData(fillnr,year,exp):
    datadir = f"{DIRBASE}/{year}/measurements/{exp}"
    if os.path.isdir( os.path.join( datadir, 'lumi' ) ):
        tgz = os.path.join( datadir, 'lumi' ,f"{fillnr}.tgz")
        zipf = os.path.join( datadir, 'lumi', f"{fillnr}.zip")        
    else:
        tgz = os.path.join( datadir, f"{fillnr}.tgz")
        zipf = os.path.join( datadir, f"{fillnr}.zip")

    member = f"{fillnr}/{fillnr}_lumi_{exp}.txt"

    data = []
    sf = ""
    if os.path.isfile( tgz ):
        try:  
            tf = tarfile.open( tgz, "r" )
            sf = tf.extractfile( member )
        except:
            error( "Problem extracting summary file for " + exp + " in fill " +fillstr 
                   + "  file: " + tgz + "  member file: " + member)
            

    elif os.path.isfile( zipf ):
        zf = zipfile.ZipFile(zipf)
        sf = zf.open(member)

    else:
        error("No Massi files in " + exp + " for fill " + fillstr )

    for line in sf :
        data.append( list(map( float, line.split() ) ))

    return data


if __name__ == "__main__":
    error = ""
    debug = "no debug"
    data  = {}

    form = cgi.FieldStorage()
    fillnr=int(form['fillnr'].value)
    year=int(form['year'].value)
    exp=form['exp'].value

    if not exp in ["ATLAS","CMS","LHCb","ALICE"]:
        print("Content-type: application/json")
        print()
        print('{"error": "unknown experiment"}')
        sys.exit(1)

    filldata=getFillData(fillnr,year,exp)
    
    data['fillData'] = { 'action' : form['action'].value,
                         'fillnr' : form[ 'fillnr' ].value,
                         'year' : form['year'].value,
                         'exp' : form['exp'].value,
                         'data' : filldata };

    reply = { 'data'  : data,
              'debug' : debug, 
              'error' : error }

    print("Content-type: application/json")
    print()
    print(json.dumps( reply ))

