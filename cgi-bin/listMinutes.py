#!/usr/bin/env python3

import cgi
import os
import os.path
import sys
import re
import json
import datetime
import csv
import re

#be able to print utf-8
sys.stdout = open(sys.stdout.fileno(), mode='w', encoding='utf8', buffering=1)

def formatDate( date, reverse=False ):
    dat = datetime.datetime.strptime( date, "%Y-%m-%d" )
    if ( reverse ):
        return datetime.datetime.strftime( dat, "%Y-%m-%d" )
    return datetime.datetime.strftime( dat, "%d-%m-%Y" )

def makePage(minutes,search,internal):

    html='''<html lang="en">

  <head>
    <title>LPC meetings</title>
    <link rel="stylesheet" type="text/css" href="/css/lpc.css">
  </head>

  <body>

    <!-- page header -->

    <table border="0" id="table1" width="100%">
      <tr>
        <td width="90">
          <a href="http://user.web.cern.ch/user/Welcome.asp">
            <img border="0" src="/images/CERNlogo.gif" alt="CERN" align="texttop" width="72" height="72">
          </a>
        </td>
        <td>
          <p class="header-headline">LPC meetings</p>
          <p align="center">
            <a href="/">LPC home</a>
          </p>
        </td>
        <td width="90">
          <img border="0" src="/images/lpcnum.gif" width="72" height="72" align="right">
        </td>
      </tr>
    </table>

    <hr>
    
    <!-- page header end -->
    <div id="debug">&nbsp;</div>

    <div id="error">&nbsp;</div>
    
    <div id="search" style="margin:auto; width:450px;">
    <form id="search" name="search">
      <label>Search in minutes: </label> 
      <input id="search" name="search" title="Text string to search for" type="text" value="'''+search+'''" />&nbsp;
      <input type="submit" hidden />
    </form>
    </div>
<div>
'''
    if internal:
        html+='''<div align="center"><button onclick="window.location.href='internal/editMinutes.py?minutes=new';">Add new entry</button></div>'''
    html+='''<table align="center" class="clearAndSimple">'''
    prevYear="none"
    columns=4
    editButton=""

    for data in minutes:
        date=data['date']
        year=date.split('-')[0]
        if year!=prevYear:
            if internal: 
                columns=5
                editButton="""
<th>Edit</tf>"""
            if year!="none":
                html+="      </tbody>"
            html+=f"""<thead>
		<tr>
			<th colspan="{columns}" style="text-align:center;border-top: 1px solid black; border-bottom: none">{year}</th>
		</tr>
		<tr>
			<th style="width: 100px">Date</th>
			<th>Topics</th>
			<th width="150px">Minutes</th>{editButton}
			<th>Indico</th>
		</tr>
	</thead>
	<tbody id="listbody">
"""
            prevYear=year
        summaryType="Summary"
        if data['area']=='draft':
            summaryType="Draft summary"
        if data['area']=='edit':
            summaryType="Initial notes"

        if internal:
            editButton=f'''
<td><a href="internal/editMinutes.py?minutes={data["name"]}">Edit</a></td>'''

        html+=f'''<tr>
			<td>{date[:10]}</td>
			<td>{data['shortdesc']}</td>
			<td><a href="showMinutes.py?minutes={data['name']}">{summaryType}</a></td>{editButton}
			<td><a href="{data['indicourl']}">Indico</a></td>
		</tr>
'''

    html+='''      </tbody>
    </table>
</div>
<hr/>
<p>&nbsp;</p>
  </body>
</html>
'''

    return html

def loadMinutes(name):
    name=os.path.basename(name)
    for area in ['final','draft','edit']:
        path=f"/data/lpc-minutes/{area}/{name}.json"
        if os.access(path,os.R_OK):
            data=json.load(open(path))
            data['area']=area
            data['name']=name
            return data
    return {}

def loadList(internal):
    areas= ['final','draft']
    if internal: areas+=['edit']
    minutes=[]
    for area in areas:
        for name in os.listdir(f"/data/lpc-minutes/{area}"):
            if name.endswith(".json") and name.startswith('20'): 
                name=name[:-5]
                if not name in minutes:
                    minutes.append(name)
    return list(sorted(minutes,reverse=True))

if __name__ == "__main__":
    internal=os.access("/opt/app-root/src/cgi-bin/internal",os.F_OK)

    form = cgi.FieldStorage()
    search=""
    if 'search' in form:
        search=form['search'].value

    minuteNames=loadList(internal)
    minutes=[]
    for name in minuteNames:
        data=loadMinutes(name)
        if search=="" or search in data['purpose'] or search in data['text'] or search in data['shortdesc']:
            minutes.append(data)
    html=makePage(minutes,search,internal)
    print('''Content-Type: text/html

''')
    print(html)
