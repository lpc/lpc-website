#!/usr/bin/python3

import cgi
import os
import sys
import json
from datetime import datetime

def makeCommPage(year):
    commPath=f"/data/commissioning/commissioning_{year}.json" 
    commData=[]

    if os.access( commPath, os.R_OK):
        commData=json.load(open(commPath))

    htmlout = '''<!DOCTYPE html>

      <head>
        <meta charset="UTF-8">
        <title>LPC commissioning page</title>
        <link rel="stylesheet" type="text/css" href="/css/lpc.css">
        <script src="/js/jquery-2.2.0.min.js"></script>
        <script src="/js/jquery-ui.min.js"></script>
        <script src="/js/commissioningPage.js"></script>
      </head>

      <body onload="doScroll()">

        <!-- page header -->
        <table id="table1" style="width:100%">
          <tr>
            <td style="width:72px">
              <a href="http://user.web.cern.ch/user/Welcome.asp">
                <img src="/images/CERNlogo.gif" alt="CERN" width="72" height="72">
              </a>
            </td>
            <td>
              <p class="header-headline">
                LHC Commissioning Plan ''' + year + '''
              </p>
              <p class="center">
                <a href="/">LPC home</a>
              </p>
            </td>
            <td style="width:72px">
              <img alt="LPC: 79977" src="/images/lpcnum.gif" width="72" height="72">
            </td>
          </tr>
        </table>

        <hr>

        <!-- page header end -->
        <p class="centerfixed">
           This table is updated continuously. Dates in the future are likely to change. This table only contains
           the most important LHC commissioning steps which are relevant for the experiments. The main
           commissioning schedule is maintained by LHC experts and the link is available from the LPC
           Home Page. 
        </p>
        <p>&nbsp;
        </p>
        <table class="commissioning">
          <thead>
            <tr><th style="width:155px">Date</th><th style="width:200px">Task</th><th>Details</th></tr>
          </thead>
          <tbody>
    '''

    for item in commData:
        htmlout += '<tr data-date="'+item['date']+'"> <td>'+item['datestr']+'</td><td>'+item['title']+'</td><td>'+item['description']+'</td></tr>'

    htmlout += '''
          </tbody>
        </table>

      </body>
    </html>
    '''
    return htmlout

if __name__ == "__main__":
    form       = cgi.FieldStorage()
    if 'year' in form:
        year       = form['year'].value
    else:
        year = str(datetime.now().year)

    htmlout=makeCommPage(year)

    print('''Content-Type: text/html

    '''+htmlout)

