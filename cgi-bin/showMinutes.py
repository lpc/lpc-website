#!/usr/bin/env python3

import cgi
import os
import os.path
import sys
import re
import json
import datetime
import csv
import re

#be able to print utf-8
sys.stdout = open(sys.stdout.fileno(), mode='w', encoding='utf8', buffering=1)


def formatDate( date, reverse=False ):
    dat = datetime.datetime.strptime( date, "%Y-%m-%d" )
    if ( reverse ):
        return datetime.datetime.strftime( dat, "%Y-%m-%d" )
    return datetime.datetime.strftime( dat, "%d-%m-%Y" )

def makePage(minutes):

    template='''<html lang="en">

  <head>
    <title>LPC meetings</title>
    <link rel="stylesheet" type="text/css" href="/css/lpc.css">
  </head>

  <body>

    <!-- page header -->

    <table border="0" id="table1" width="100%">
      <tr>
        <td width="90">
          <a href="http://user.web.cern.ch/user/Welcome.asp">
            <img border="0" src="/images/CERNlogo.gif" alt="CERN" align="texttop" width="72" height="72">
          </a>
        </td>
        <td>
          <p class="header-headline">LPC meeting summary __date__ - __state__</p>
          <p align="center">
            <a href="listMinutes.py">Minutes overview</a> &nbsp;&nbsp;&nbsp;&nbsp; <a href="/">LPC home</a>
          </p>
        </td>
        <td width="90">
          <img border="0" src="/images/lpcnum.gif" width="72" height="72" align="right">
        </td>
      </tr>
    </table>

    <hr>
    
    <!-- page header end -->

    <div class="indico align-right">
      <iframe class="indico-iframe" src="__indicourl__" style="width: 900px; height: 500px;"></iframe>
    </div>


    <div class="minutes-text" id="minutes-text">
      <h1>Minutes and Summary</h1>

      <p>
        <strong>Main purpose of the meeting: </strong> 
        __purpose__
      </p>
      
      __text__

    </div>

  </body>
</html>
'''
    datstr = formatDate( data['date'] )
    template = re.sub( r'__date__', datstr, template, re.MULTILINE ) 
    template = re.sub( r'__state__', data['area'], template, re.MULTILINE ) 
    # replace indico frame
    template = re.sub( r'__indicourl__', data['indicourl'], template, re.MULTILINE )
    # replace purpose
    template = re.sub( r'__purpose__', data['purpose'], template, re.MULTILINE )
    # insert text
    template = re.sub( r'__text__', data['text'], template, re.MULTILINE )

    return template

def loadMinutes(name):
    name=os.path.basename(name)
    for area in ['final','draft','edit']:
        path=f"/data/lpc-minutes/{area}/{name}.json"
        if os.access(path,os.R_OK):
            data=json.load(open(path))
            data['area']=area
            return data
    return {}

if __name__ == "__main__":
    form = cgi.FieldStorage()
    if 'minutes' in form:
        minName=form['minutes'].value
        data=loadMinutes(minName)
        if data:
            html=makePage(data)
        else:
            html="Requested minutes not found"
    else:
        html="No minutes specified"

    print('''Content-Type: text/html

''')
    print(html)
