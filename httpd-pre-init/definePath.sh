#!/bin/bash

# Note this is not robust against changes in the docker image structure
if [ -z ${HTTPROOT+x} ]; then
    export HTTPROOT=/opt/app-root/src/website
    export CGIROOT=/opt/app-root/src/cgi-bin
fi

if [ -z ${CSSCOLOR+x} ]; then
    export CSSCOLOR=White
fi

sed -i s%/opt/app-root/src%${HTTPROOT}% /etc/httpd/conf/httpd.conf
sed -i s%/opt/rh/httpd24/root/var/www/cgi-bin%${CGIROOT}% /etc/httpd/conf/httpd.conf
sed -i s/AliceBlue/${CSSCOLOR}/ /opt/app-root/src/website/css/lpc.css

if [ -z ${ALLOWINTERNAL+x} ]; then
    rm -rf /opt/app-root/src/website/internal
    rm -rf /opt/app-root/src/cgi-bin/internal
    rm -rf /opt/app-root/src/scripts
fi
